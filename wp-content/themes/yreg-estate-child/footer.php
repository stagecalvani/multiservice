<?php
/**
 * The template for displaying the footer.
 */

				yreg_estate_close_wrapper();	// <!-- </.content> -->

				yreg_estate_profiler_add_point(esc_html__('After Page content', 'yreg-estate'));
	
				// Show main sidebar
				get_sidebar();

				if (yreg_estate_get_custom_option('body_style')!='fullscreen') yreg_estate_close_wrapper();	// <!-- </.content_wrap> -->
				?>
			
			</div>		<!-- </.page_content_wrap> -->
			
			<?php
			
			if (yreg_estate_get_custom_option('show_custom_shortcode_box_in_footer')=='yes') {
				$scbox = yreg_estate_get_custom_option('custom_shortcode_box');
				echo '<div class="custom_shortcode_box"><div class="content_wrap">';
				echo trim(yreg_estate_do_shortcode($scbox));
				echo '</div></div>';
			}
			
			
			
			if (yreg_estate_get_custom_option('show_emailer_in_footer')=='yes') {
				echo '<div class="contacts_emailer_wrap"><div class="content_wrap">';
				echo trim(yreg_estate_do_shortcode('[trx_emailer title="'.yreg_estate_get_custom_option('emailer_in_footer_title').'" top="inherit" bottom="inherit" left="inherit" right="inherit"]'));
				echo '</div></div>';
			}
			
			// Footer sidebar
			$footer_show  = yreg_estate_get_custom_option('show_sidebar_footer');
			$sidebar_name = yreg_estate_get_custom_option('sidebar_footer');
			if (!yreg_estate_param_is_off($footer_show) && is_active_sidebar($sidebar_name)) { 
				yreg_estate_storage_set('current_sidebar', 'footer');
				?>
				<footer class="footer_wrap widget_area scheme_<?php echo esc_attr(yreg_estate_get_custom_option('sidebar_footer_scheme')); ?>">
					<div class="footer_wrap_inner widget_area_inner">
						<div class="content_wrap">
							<div class="columns_wrap"><?php
							ob_start();
							do_action( 'before_sidebar' );
							if ( !dynamic_sidebar($sidebar_name) ) {
								// Put here html if user no set widgets in sidebar
							}
							do_action( 'after_sidebar' );
							$out = ob_get_contents();
							ob_end_clean();
							echo trim(chop(preg_replace("/<\/aside>[\r\n\s]*<aside/", "</aside><aside", $out)));
							?></div>	<!-- /.columns_wrap -->
						</div>	<!-- /.content_wrap -->
					</div>	<!-- /.footer_wrap_inner -->
				</footer>	<!-- /.footer_wrap -->
				<?php
			}
			
			
			// Footer contacts
			if (yreg_estate_get_custom_option('show_contacts_in_footer')=='yes') {
				
				if ( (yreg_estate_get_custom_option('show_logo_footer')=='yes') 
						or (yreg_estate_get_custom_option('show_textbox_footer')=='yes') 
						or (yreg_estate_get_custom_option('show_socbox_footer')=='yes') ) { ?>
					
				<footer class="contacts_wrap scheme_<?php echo esc_attr(yreg_estate_get_custom_option('contacts_scheme')); ?>">
						<div class="contacts_wrap_inner">
							<div class="content_wrap">
								
								<div class="columns_wrap">
								
									<?php
									if (yreg_estate_get_custom_option('show_logo_footer')=='yes') {
										echo '<div class="column-1_4 show_logo_footer">';
										yreg_estate_show_logo(false, false, true, false, false, false);
										echo '</div>';
									}

									if (yreg_estate_get_custom_option('show_textbox_footer')=='yes') {
										echo '<div class="column-2_4">';
										if ( yreg_estate_get_custom_option('title_textbox_footer') != '' ) {
											echo '<h5>' . yreg_estate_get_custom_option('title_textbox_footer') . '</h5>';
										}
										if ( yreg_estate_get_custom_option('content_textbox_footer') != '' ) {
											echo yreg_estate_get_custom_option('content_textbox_footer');
										}
										
										echo '</div>';
									}
                                                                        echo '<div class="column-1_4 show_logo_footer">';
									if (yreg_estate_get_custom_option('show_socbox_footer')=='yes') {
                                                                                if ( yreg_estate_get_custom_option('title_socbox_footer') != '' ) {
                                                                                    echo '<img src="'.get_bloginfo("stylesheet_directory").'/../../uploads/2016/06/logo-multiservice-quindicesimo.JPG" />'; 
                                                                                }
                                                                                    echo trim(yreg_estate_sc_socials(array('size'=>"small")));
                                                                                if ( yreg_estate_get_custom_option('title_socbox_footer') != '' ) {
                                                                                    echo '<img src="'.get_bloginfo("stylesheet_directory").'/../../uploads/2016/06/logo-fiaip.png" />';  
                                                                                }
                                                                                    echo trim(yreg_estate_sc_socials(array('size'=>"small")));
                                                                        }
                                                                        echo '</div>';
									?>
									<div class="cL"></div>
								</div>
								
								
								
								

								
							</div>	<!-- /.content_wrap -->
						</div>	<!-- /.contacts_wrap_inner -->
					</footer>	<!-- /.contacts_wrap -->
				
				<?php
				}
			}

			// Copyright area
			$copyright_style = yreg_estate_get_custom_option('show_copyright_in_footer');
			if (!yreg_estate_param_is_off($copyright_style)) {
				?> 
				<div class="copyright_wrap copyright_style_<?php echo esc_attr($copyright_style); ?>  scheme_<?php echo esc_attr(yreg_estate_get_custom_option('copyright_scheme')); ?>">
					<div class="copyright_wrap_inner">
						<div class="content_wrap">
							<?php							
                                                        echo '<span id="calvani_link"><a href="http://www.calvaniartdesign.it" target="_blank">Artwork: Calvani Art Design</a></span>'
							?>
							<div class="copyright_text"><?php echo force_balance_tags(yreg_estate_get_custom_option('footer_copyright')); ?></div>
						</div>
					</div>
				</div>
				<?php
			}

			yreg_estate_profiler_add_point(esc_html__('After Footer', 'yreg-estate'));
			?>
			
		</div>	<!-- /.page_wrap -->

	</div>		<!-- /.body_wrap -->

<?php
// Post/Page views counter
get_template_part(yreg_estate_get_file_slug('templates/_parts/views-counter.php'));

// Front customizer
if (yreg_estate_get_custom_option('show_theme_customizer')=='yes') {
	get_template_part(yreg_estate_get_file_slug('core/core.customizer/front.customizer.php'));
}
?>

<a href="#" class="scroll_to_top icon-up" title="<?php esc_attr_e('Scroll to top', 'yreg-estate'); ?>"></a>

<div class="custom_html_section">
<?php echo force_balance_tags(yreg_estate_get_custom_option('custom_code')); ?>
</div>

<?php
echo force_balance_tags(yreg_estate_get_custom_option('gtm_code2'));

yreg_estate_profiler_add_point(esc_html__('After Theme HTML output', 'yreg-estate'));

wp_footer(); 
?>

</body>
</html>