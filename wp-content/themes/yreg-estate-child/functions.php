<?php
/**
 * Theme sprecific functions and definitions
 */

/* Theme setup section
------------------------------------------------------------------- */

// Set the content width based on the theme's design and stylesheet.
if ( ! isset( $content_width ) ) $content_width = 1170; /* pixels */

// Add theme specific actions and filters
// Attention! Function were add theme specific actions and filters handlers must have priority 1
if ( !function_exists( 'yreg_estate_theme_setup' ) ) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_theme_setup', 1 );
	function yreg_estate_theme_setup() {

		// Register theme menus
		add_filter( 'yreg_estate_filter_add_theme_menus',		'yreg_estate_add_theme_menus' );

		// Register theme sidebars
		add_filter( 'yreg_estate_filter_add_theme_sidebars',	'yreg_estate_add_theme_sidebars' );

		// Set options for importer
		add_filter( 'yreg_estate_filter_importer_options',		'yreg_estate_set_importer_options' );

		
		// Add theme specified classes into the body
		add_filter( 'body_class', 'yreg_estate_body_classes' );
		
		// Set list of the theme required plugins
		yreg_estate_storage_set('required_plugins', array(
			'booked',
//			'buddypress',		// Attention! This slug used to install both BuddyPress and bbPress
//			'calcfields',
			'essgrids',
//			'instagram_feed',
			'instagram_widget',
//			'learndash',
//			'mailchimp',
//			'mega_main_menu',
//			'responsive_poll',
			'revslider',
//			'tribe_events',
//			'trx_donations',
			'trx_utils',
			'visual_composer',
//			'woocommerce',
//			'html5_jquery_audio_player'
			)
		);
		
	}
}


// Add theme specified classes into the body
if ( !function_exists('yreg_estate_body_classes') ) {
	function yreg_estate_body_classes( $classes ) {

		$classes[] = 'yreg_estate_body';
		$classes[] = 'body_style_' . trim(yreg_estate_get_custom_option('body_style'));
		$classes[] = 'body_' . (yreg_estate_get_custom_option('body_filled')=='yes' ? 'filled' : 'transparent');
		$classes[] = 'theme_skin_' . trim(yreg_estate_get_custom_option('theme_skin'));
		$classes[] = 'article_style_' . trim(yreg_estate_get_custom_option('article_style'));
		
		$blog_style = yreg_estate_get_custom_option(is_singular() && !yreg_estate_storage_get('blog_streampage') ? 'single_style' : 'blog_style');
		$classes[] = 'layout_' . trim($blog_style);
		$classes[] = 'template_' . trim(yreg_estate_get_template_name($blog_style));
		
		$body_scheme = yreg_estate_get_custom_option('body_scheme');
		if (empty($body_scheme)  || yreg_estate_is_inherit_option($body_scheme)) $body_scheme = 'original';
		$classes[] = 'scheme_' . $body_scheme;

		$top_panel_position = yreg_estate_get_custom_option('top_panel_position');
		if (!yreg_estate_param_is_off($top_panel_position)) {
			$classes[] = 'top_panel_show';
			$classes[] = 'top_panel_' . trim($top_panel_position);
		} else 
			$classes[] = 'top_panel_hide';
		$classes[] = yreg_estate_get_sidebar_class();

		if (yreg_estate_get_custom_option('show_video_bg')=='yes' && (yreg_estate_get_custom_option('video_bg_youtube_code')!='' || yreg_estate_get_custom_option('video_bg_url')!=''))
			$classes[] = 'video_bg_show';

		if (yreg_estate_get_theme_option('page_preloader')!='')
			$classes[] = 'preloader';

		return $classes;
	}
}


// Add/Remove theme nav menus
if ( !function_exists( 'yreg_estate_add_theme_menus' ) ) {
	//add_filter( 'yreg_estate_filter_add_theme_menus', 'yreg_estate_add_theme_menus' );
	function yreg_estate_add_theme_menus($menus) {
		//For example:
		//$menus['menu_footer'] = esc_html__('Footer Menu', 'yreg-estate');
		//if (isset($menus['menu_panel'])) unset($menus['menu_panel']);
		return $menus;
	}
}


// Add theme specific widgetized areas
if ( !function_exists( 'yreg_estate_add_theme_sidebars' ) ) {
	//add_filter( 'yreg_estate_filter_add_theme_sidebars',	'yreg_estate_add_theme_sidebars' );
	function yreg_estate_add_theme_sidebars($sidebars=array()) {
		if (is_array($sidebars)) {
			$theme_sidebars = array(
				'sidebar_main'		=> esc_html__( 'Main Sidebar', 'yreg-estate' ),
				'sidebar_footer'	=> esc_html__( 'Footer Sidebar', 'yreg-estate' ),
                                'sidebar_1'	        => esc_html__( 'Sidebar Immobili', 'yreg-estate' )
			);
			if (function_exists('yreg_estate_exists_woocommerce') && yreg_estate_exists_woocommerce()) {
				$theme_sidebars['sidebar_cart']  = esc_html__( 'WooCommerce Cart Sidebar', 'yreg-estate' );
			}
			$sidebars = array_merge($theme_sidebars, $sidebars);
		}
		return $sidebars;
	}
}


// Set theme specific importer options
if ( !function_exists( 'yreg_estate_set_importer_options' ) ) {
	//add_filter( 'yreg_estate_filter_importer_options',	'yreg_estate_set_importer_options' );
	function yreg_estate_set_importer_options($options=array()) {
		if (is_array($options)) {
			$options['debug'] = yreg_estate_get_theme_option('debug_mode')=='yes';
			$options['domain_dev'] = 'estate.dv.axiomthemes.com';
			$options['domain_demo'] = 'estate.axiomthemes.com';
			$options['menus'] = array(
				'menu-main'	  => esc_html__('Main menu', 'yreg-estate'),
				'menu-user'	  => esc_html__('User menu', 'yreg-estate'),
				'menu-footer' => esc_html__('Footer menu', 'yreg-estate')
			);
			$options['file_with_attachments'] = array(				// Array with names of the attachments
//				'http://estate.axiomthemes.com/wp-content/imports/uploads.zip',		// Name of the remote file with attachments
	//			'demo/uploads.zip',									// Name of the local file with attachments
				'http://estate.axiomthemes.com/wp-content/imports/uploads.001',
				'http://estate.axiomthemes.com/wp-content/imports/uploads.002',
				'http://estate.axiomthemes.com/wp-content/imports/uploads.003',
				'http://estate.axiomthemes.com/wp-content/imports/uploads.004',
				'http://estate.axiomthemes.com/wp-content/imports/uploads.005',
				'http://estate.axiomthemes.com/wp-content/imports/uploads.006',
				'http://estate.axiomthemes.com/wp-content/imports/uploads.007',
				'http://estate.axiomthemes.com/wp-content/imports/uploads.008',
				'http://estate.axiomthemes.com/wp-content/imports/uploads.009',
				'http://estate.axiomthemes.com/wp-content/imports/uploads.010',
				'http://estate.axiomthemes.com/wp-content/imports/uploads.011',
				'http://estate.axiomthemes.com/wp-content/imports/uploads.012'
//				'http://estate.axiomthemes.com/wp-content/imports/uploads.013'
			);
			$options['attachments_by_parts'] = true;				// Files above are parts of single file - large media archive. They are must be concatenated in one file before unpacking
		}
		return $options;
	}
}


/* Include framework core files
------------------------------------------------------------------- */
// If now is WP Heartbeat call - skip loading theme core files (to reduce server and DB uploads)
// Remove comments below only if your theme not work with own post types and/or taxonomies
//if (!isset($_POST['action']) || $_POST['action']!="heartbeat") {
	get_template_part('fw/loader');
//}

        
add_filter('yreg_estate_filter_get_blog_title',	'my_yreg_estate_property_get_blog_title', 9, 2);


// Filter to detect current page title
if ( !function_exists( 'my_yreg_estate_property_get_blog_title' ) ) {
	//add_filter('yreg_estate_filter_get_blog_title',	'yreg_estate_property_get_blog_title', 9, 2);
	function my_yreg_estate_property_get_blog_title($title, $page) {
		if (!empty($title)) return $title;
		if ( yreg_estate_strpos($page, 'property')!==false ) {
			if ( $page == 'property_category' ) {
				$term = get_term_by( 'slug', get_query_var( 'property_group' ), 'property_group', OBJECT);
				$title = $term->name;
			} else if ( $page == 'property_item' ) {
				$title = yreg_estate_get_post_title();
			} else {
				$title = esc_html__(get_the_title(), 'yreg-estate');
			}
		}
		return $title;
	}
}

add_action( 'after_setup_theme', 'multiservice_setup', 1 ); // Run as soon as possible

function multiservice_setup() {
    wp_register_style( 'shadowboxcss', get_bloginfo( 'stylesheet_directory' ) . '/js/shadowbox.css', array(), 2.95444, 'all' );
    if( !is_admin() ) {
        wp_enqueue_style( 'shadowboxcss' );
    }
}  

add_action('wp_enqueue_scripts', 'load_scripts');

function load_scripts() {
    if (!is_admin()) {
        wp_register_script('shadowbox', get_bloginfo('stylesheet_directory') . '/js/shadowbox.js', array('jquery'), '2.9995');
        wp_enqueue_script('shadowbox');
    }
}

function add_form_the_content($content){
    if(is_singular('property')){
        $form_html = do_shortcode('[contact-form-7 id="1357" title="Contact form proprieta"]'); 
        $form_html = '<div id="form_richiesta_informazioni">'.$form_html.'</div>';
        $id = get_the_ID();
        $id_immobile = get_post_meta( $id, 'yreg_estate_property_id_web', true ); 
        $id_js_html  .= '<script>
            var id_immobile_js = "'.  $id_immobile .'";
        </script>';
        $content = $content.$id_js_html.$form_html;
        
    }
    return $content;
}

add_filter( 'the_content', 'add_form_the_content' );

function truncate($text, $length = 100, $ending = '...', $exact = false, $considerHtml = true) {
    if ($considerHtml) {
        // if the plain text is shorter than the maximum length, return the whole text
        if (strlen(preg_replace('/<.*?>/', '', $text)) <= $length) {
            return $text;
        }
        // splits all html-tags to scanable lines
        preg_match_all('/(<.+?>)?([^<>]*)/s', $text, $lines, PREG_SET_ORDER);
        $total_length = strlen($ending);
        $open_tags = array();
        $truncate = '';
        foreach ($lines as $line_matchings) {
            // if there is any html-tag in this line, handle it and add it (uncounted) to the output
            if (!empty($line_matchings[1])) {
                // if it's an "empty element" with or without xhtml-conform closing slash
                if (preg_match('/^<(\s*.+?\/\s*|\s*(img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param)(\s.+?)?)>$/is', $line_matchings[1])) {
                    // do nothing
                    // if tag is a closing tag
                } else if (preg_match('/^<\s*\/([^\s]+?)\s*>$/s', $line_matchings[1], $tag_matchings)) {
                    // delete tag from $open_tags list
                    $pos = array_search($tag_matchings[1], $open_tags);
                    if ($pos !== false) {
                        unset($open_tags[$pos]);
                    }
                    // if tag is an opening tag
                } else if (preg_match('/^<\s*([^\s>!]+).*?>$/s', $line_matchings[1], $tag_matchings)) {
                    // add tag to the beginning of $open_tags list
                    array_unshift($open_tags, strtolower($tag_matchings[1]));
                }
                // add html-tag to $truncate'd text
                $truncate .= $line_matchings[1];
            }
            // calculate the length of the plain text part of the line; handle entities as one character
            $content_length = strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', ' ', $line_matchings[2]));
            if ($total_length + $content_length > $length) {
                // the number of characters which are left
                $left = $length - $total_length;
                $entities_length = 0;
                // search for html entities
                if (preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', $line_matchings[2], $entities, PREG_OFFSET_CAPTURE)) {
                    // calculate the real length of all entities in the legal range
                    foreach ($entities[0] as $entity) {
                        if ($entity[1] + 1 - $entities_length <= $left) {
                            $left--;
                            $entities_length += strlen($entity[0]);
                        } else {
                            // no more characters left
                            break;
                        }
                    }
                }
                $truncate .= substr($line_matchings[2], 0, $left + $entities_length);
                // maximum lenght is reached, so get off the loop
                break;
            } else {
                $truncate .= $line_matchings[2];
                $total_length += $content_length;
            }
            // if the maximum length is reached, get off the loop
            if ($total_length >= $length) {
                break;
            }
        }
    } else {
        if (strlen($text) <= $length) {
            return $text;
        } else {
            $truncate = substr($text, 0, $length - strlen($ending));
        }
    }
    // if the words shouldn't be cut in the middle...
    if (!$exact) {
        // ...search the last occurance of a space...
        $spacepos = strrpos($truncate, ' ');
        if (isset($spacepos)) {
            // ...and cut the text in this position
            $truncate = substr($truncate, 0, $spacepos);
        }
    }
    // add the defined ending to the text
    $truncate .= $ending;
    if ($considerHtml) {
        // close all unclosed html-tags
        foreach ($open_tags as $tag) {
            $truncate .= '</' . $tag . '>';
        }
    }
    return $truncate;
}

//nascondo il div in amministrazione wordpress relativo alla versione di aggiornamento

function hidden_div() {
    $current_user = wp_get_current_user();
    $user_ID = $current_user->data->ID;
    $user_name= $current_user->data->user_login;
    $user_role= $current_user->roles[0];
              
    if($user_role == 'multiservice'){  
        echo '<script>
            jQuery(document).ready(function($){
                jQuery(".update-nag").css("display","none");
            });
        </script>';
    }
}

add_action('admin_head', 'hidden_div');