<?php
/**
 * YREG_ESTATE Framework: return lists
 *
 * @package yreg_estate
 * @since yreg_estate 1.0
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }



// Return styles list
if ( !function_exists( 'yreg_estate_get_list_styles' ) ) {
	function yreg_estate_get_list_styles($from=1, $to=2, $prepend_inherit=false) {
		$list = array();
		for ($i=$from; $i<=$to; $i++)
			$list[$i] = sprintf(esc_html__('Style %d', 'yreg-estate'), $i);
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}


// Return list of the shortcodes margins
if ( !function_exists( 'yreg_estate_get_list_margins' ) ) {
	function yreg_estate_get_list_margins($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_margins'))=='') {
			$list = array(
				'null'		=> esc_html__('0 (No margin)',	'yreg-estate'),
				'tiny'		=> esc_html__('Tiny',		'yreg-estate'),
				'small'		=> esc_html__('Small',		'yreg-estate'),
				'medium'	=> esc_html__('Medium',		'yreg-estate'),
				'xmedium'	=> esc_html__('X-Medium',		'yreg-estate'),
				'large'		=> esc_html__('Large',		'yreg-estate'),
				'xlarge'		=> esc_html__('X-Large',		'yreg-estate'),
				'xxlarge'		=> esc_html__('XX-Large',		'yreg-estate'),
				'huge'		=> esc_html__('Huge',		'yreg-estate')
				);
			$list = apply_filters('yreg_estate_filter_list_margins', $list);
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_margins', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}


// Return list of the animations
if ( !function_exists( 'yreg_estate_get_list_animations' ) ) {
	function yreg_estate_get_list_animations($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_animations'))=='') {
			$list = array(
				'none'			=> esc_html__('- None -',	'yreg-estate'),
				'bounced'		=> esc_html__('Bounced',		'yreg-estate'),
				'flash'			=> esc_html__('Flash',		'yreg-estate'),
				'flip'			=> esc_html__('Flip',		'yreg-estate'),
				'pulse'			=> esc_html__('Pulse',		'yreg-estate'),
				'rubberBand'	=> esc_html__('Rubber Band',	'yreg-estate'),
				'shake'			=> esc_html__('Shake',		'yreg-estate'),
				'swing'			=> esc_html__('Swing',		'yreg-estate'),
				'tada'			=> esc_html__('Tada',		'yreg-estate'),
				'wobble'		=> esc_html__('Wobble',		'yreg-estate')
				);
			$list = apply_filters('yreg_estate_filter_list_animations', $list);
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_animations', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}


// Return list of the line styles
if ( !function_exists( 'yreg_estate_get_list_line_styles' ) ) {
	function yreg_estate_get_list_line_styles($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_line_styles'))=='') {
			$list = array(
				'solid'	=> esc_html__('Solid', 'yreg-estate'),
				'dashed'=> esc_html__('Dashed', 'yreg-estate'),
				'dotted'=> esc_html__('Dotted', 'yreg-estate'),
				'double'=> esc_html__('Double', 'yreg-estate'),
				'image'	=> esc_html__('Image', 'yreg-estate')
				);
			$list = apply_filters('yreg_estate_filter_list_line_styles', $list);
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_line_styles', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}


// Return list of the enter animations
if ( !function_exists( 'yreg_estate_get_list_animations_in' ) ) {
	function yreg_estate_get_list_animations_in($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_animations_in'))=='') {
			$list = array(
				'none'				=> esc_html__('- None -',			'yreg-estate'),
				'bounceIn'			=> esc_html__('Bounce In',			'yreg-estate'),
				'bounceInUp'		=> esc_html__('Bounce In Up',		'yreg-estate'),
				'bounceInDown'		=> esc_html__('Bounce In Down',		'yreg-estate'),
				'bounceInLeft'		=> esc_html__('Bounce In Left',		'yreg-estate'),
				'bounceInRight'		=> esc_html__('Bounce In Right',	'yreg-estate'),
				'fadeIn'			=> esc_html__('Fade In',			'yreg-estate'),
				'fadeInUp'			=> esc_html__('Fade In Up',			'yreg-estate'),
				'fadeInDown'		=> esc_html__('Fade In Down',		'yreg-estate'),
				'fadeInLeft'		=> esc_html__('Fade In Left',		'yreg-estate'),
				'fadeInRight'		=> esc_html__('Fade In Right',		'yreg-estate'),
				'fadeInUpBig'		=> esc_html__('Fade In Up Big',		'yreg-estate'),
				'fadeInDownBig'		=> esc_html__('Fade In Down Big',	'yreg-estate'),
				'fadeInLeftBig'		=> esc_html__('Fade In Left Big',	'yreg-estate'),
				'fadeInRightBig'	=> esc_html__('Fade In Right Big',	'yreg-estate'),
				'flipInX'			=> esc_html__('Flip In X',			'yreg-estate'),
				'flipInY'			=> esc_html__('Flip In Y',			'yreg-estate'),
				'lightSpeedIn'		=> esc_html__('Light Speed In',		'yreg-estate'),
				'rotateIn'			=> esc_html__('Rotate In',			'yreg-estate'),
				'rotateInUpLeft'	=> esc_html__('Rotate In Down Left','yreg-estate'),
				'rotateInUpRight'	=> esc_html__('Rotate In Up Right',	'yreg-estate'),
				'rotateInDownLeft'	=> esc_html__('Rotate In Up Left',	'yreg-estate'),
				'rotateInDownRight'	=> esc_html__('Rotate In Down Right','yreg-estate'),
				'rollIn'			=> esc_html__('Roll In',			'yreg-estate'),
				'slideInUp'			=> esc_html__('Slide In Up',		'yreg-estate'),
				'slideInDown'		=> esc_html__('Slide In Down',		'yreg-estate'),
				'slideInLeft'		=> esc_html__('Slide In Left',		'yreg-estate'),
				'slideInRight'		=> esc_html__('Slide In Right',		'yreg-estate'),
				'zoomIn'			=> esc_html__('Zoom In',			'yreg-estate'),
				'zoomInUp'			=> esc_html__('Zoom In Up',			'yreg-estate'),
				'zoomInDown'		=> esc_html__('Zoom In Down',		'yreg-estate'),
				'zoomInLeft'		=> esc_html__('Zoom In Left',		'yreg-estate'),
				'zoomInRight'		=> esc_html__('Zoom In Right',		'yreg-estate')
				);
			$list = apply_filters('yreg_estate_filter_list_animations_in', $list);
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_animations_in', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}


// Return list of the out animations
if ( !function_exists( 'yreg_estate_get_list_animations_out' ) ) {
	function yreg_estate_get_list_animations_out($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_animations_out'))=='') {
			$list = array(
				'none'				=> esc_html__('- None -',	'yreg-estate'),
				'bounceOut'			=> esc_html__('Bounce Out',			'yreg-estate'),
				'bounceOutUp'		=> esc_html__('Bounce Out Up',		'yreg-estate'),
				'bounceOutDown'		=> esc_html__('Bounce Out Down',		'yreg-estate'),
				'bounceOutLeft'		=> esc_html__('Bounce Out Left',		'yreg-estate'),
				'bounceOutRight'	=> esc_html__('Bounce Out Right',	'yreg-estate'),
				'fadeOut'			=> esc_html__('Fade Out',			'yreg-estate'),
				'fadeOutUp'			=> esc_html__('Fade Out Up',			'yreg-estate'),
				'fadeOutDown'		=> esc_html__('Fade Out Down',		'yreg-estate'),
				'fadeOutLeft'		=> esc_html__('Fade Out Left',		'yreg-estate'),
				'fadeOutRight'		=> esc_html__('Fade Out Right',		'yreg-estate'),
				'fadeOutUpBig'		=> esc_html__('Fade Out Up Big',		'yreg-estate'),
				'fadeOutDownBig'	=> esc_html__('Fade Out Down Big',	'yreg-estate'),
				'fadeOutLeftBig'	=> esc_html__('Fade Out Left Big',	'yreg-estate'),
				'fadeOutRightBig'	=> esc_html__('Fade Out Right Big',	'yreg-estate'),
				'flipOutX'			=> esc_html__('Flip Out X',			'yreg-estate'),
				'flipOutY'			=> esc_html__('Flip Out Y',			'yreg-estate'),
				'hinge'				=> esc_html__('Hinge Out',			'yreg-estate'),
				'lightSpeedOut'		=> esc_html__('Light Speed Out',		'yreg-estate'),
				'rotateOut'			=> esc_html__('Rotate Out',			'yreg-estate'),
				'rotateOutUpLeft'	=> esc_html__('Rotate Out Down Left',	'yreg-estate'),
				'rotateOutUpRight'	=> esc_html__('Rotate Out Up Right',		'yreg-estate'),
				'rotateOutDownLeft'	=> esc_html__('Rotate Out Up Left',		'yreg-estate'),
				'rotateOutDownRight'=> esc_html__('Rotate Out Down Right',	'yreg-estate'),
				'rollOut'			=> esc_html__('Roll Out',		'yreg-estate'),
				'slideOutUp'		=> esc_html__('Slide Out Up',		'yreg-estate'),
				'slideOutDown'		=> esc_html__('Slide Out Down',	'yreg-estate'),
				'slideOutLeft'		=> esc_html__('Slide Out Left',	'yreg-estate'),
				'slideOutRight'		=> esc_html__('Slide Out Right',	'yreg-estate'),
				'zoomOut'			=> esc_html__('Zoom Out',			'yreg-estate'),
				'zoomOutUp'			=> esc_html__('Zoom Out Up',		'yreg-estate'),
				'zoomOutDown'		=> esc_html__('Zoom Out Down',	'yreg-estate'),
				'zoomOutLeft'		=> esc_html__('Zoom Out Left',	'yreg-estate'),
				'zoomOutRight'		=> esc_html__('Zoom Out Right',	'yreg-estate')
				);
			$list = apply_filters('yreg_estate_filter_list_animations_out', $list);
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_animations_out', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return classes list for the specified animation
if (!function_exists('yreg_estate_get_animation_classes')) {
	function yreg_estate_get_animation_classes($animation, $speed='normal', $loop='none') {
		// speed:	fast=0.5s | normal=1s | slow=2s
		// loop:	none | infinite
		return yreg_estate_param_is_off($animation) ? '' : 'animated '.esc_attr($animation).' '.esc_attr($speed).(!yreg_estate_param_is_off($loop) ? ' '.esc_attr($loop) : '');
	}
}


// Return list of categories
if ( !function_exists( 'yreg_estate_get_list_categories' ) ) {
	function yreg_estate_get_list_categories($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_categories'))=='') {
			$list = array();
			$args = array(
				'type'                     => 'post',
				'child_of'                 => 0,
				'parent'                   => '',
				'orderby'                  => 'name',
				'order'                    => 'ASC',
				'hide_empty'               => 0,
				'hierarchical'             => 1,
				'exclude'                  => '',
				'include'                  => '',
				'number'                   => '',
				'taxonomy'                 => 'category',
				'pad_counts'               => false );
			$taxonomies = get_categories( $args );
			if (is_array($taxonomies) && count($taxonomies) > 0) {
				foreach ($taxonomies as $cat) {
					$list[$cat->term_id] = $cat->name;
				}
			}
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_categories', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}


// Return list of taxonomies
if ( !function_exists( 'yreg_estate_get_list_terms' ) ) {
	function yreg_estate_get_list_terms($prepend_inherit=false, $taxonomy='category') {
		if (($list = yreg_estate_storage_get('list_taxonomies_'.($taxonomy)))=='') {
			$list = array();
			if ( is_array($taxonomy) || taxonomy_exists($taxonomy) ) {
				$terms = get_terms( $taxonomy, array(
					'child_of'                 => 0,
					'parent'                   => '',
					'orderby'                  => 'name',
					'order'                    => 'ASC',
					'hide_empty'               => 0,
					'hierarchical'             => 1,
					'exclude'                  => '',
					'include'                  => '',
					'number'                   => '',
					'taxonomy'                 => $taxonomy,
					'pad_counts'               => false
					)
				);
			} else {
				$terms = yreg_estate_get_terms_by_taxonomy_from_db($taxonomy);
			}
			if (!is_wp_error( $terms ) && is_array($terms) && count($terms) > 0) {
				foreach ($terms as $cat) {
					$list[$cat->term_id] = $cat->name;	// . ($taxonomy!='category' ? ' /'.($cat->taxonomy).'/' : '');
				}
			}
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_taxonomies_'.($taxonomy), $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return list of post's types
if ( !function_exists( 'yreg_estate_get_list_posts_types' ) ) {
	function yreg_estate_get_list_posts_types($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_posts_types'))=='') {
			/* 
			// This way to return all registered post types
			$types = get_post_types();
			if (in_array('post', $types)) $list['post'] = esc_html__('Post', 'yreg-estate');
			if (is_array($types) && count($types) > 0) {
				foreach ($types as $t) {
					if ($t == 'post') continue;
					$list[$t] = yreg_estate_strtoproper($t);
				}
			}
			*/
			// Return only theme inheritance supported post types
			$list = apply_filters('yreg_estate_filter_list_post_types', $list);
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_posts_types', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}


// Return list post items from any post type and taxonomy
if ( !function_exists( 'yreg_estate_get_list_posts' ) ) {
	function yreg_estate_get_list_posts($prepend_inherit=false, $opt=array()) {
		$opt = array_merge(array(
			'post_type'			=> 'post',
			'post_status'		=> 'publish',
			'taxonomy'			=> 'category',
			'taxonomy_value'	=> '',
			'posts_per_page'	=> -1,
			'orderby'			=> 'post_date',
			'order'				=> 'desc',
			'return'			=> 'id'
			), is_array($opt) ? $opt : array('post_type'=>$opt));

		$hash = 'list_posts_'.($opt['post_type']).'_'.($opt['taxonomy']).'_'.($opt['taxonomy_value']).'_'.($opt['orderby']).'_'.($opt['order']).'_'.($opt['return']).'_'.($opt['posts_per_page']);
		if (($list = yreg_estate_storage_get($hash))=='') {
			$list = array();
			$list['none'] = esc_html__("- Not selected -", 'yreg-estate');
			$args = array(
				'post_type' => $opt['post_type'],
				'post_status' => $opt['post_status'],
				'posts_per_page' => $opt['posts_per_page'],
				'ignore_sticky_posts' => true,
				'orderby'	=> $opt['orderby'],
				'order'		=> $opt['order']
			);
			if (!empty($opt['taxonomy_value'])) {
				$args['tax_query'] = array(
					array(
						'taxonomy' => $opt['taxonomy'],
						'field' => (int) $opt['taxonomy_value'] > 0 ? 'id' : 'slug',
						'terms' => $opt['taxonomy_value']
					)
				);
			}
			$posts = get_posts( $args );
			if (is_array($posts) && count($posts) > 0) {
				foreach ($posts as $post) {
					$list[$opt['return']=='id' ? $post->ID : $post->post_title] = $post->post_title;
				}
			}
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set($hash, $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}


// Return list pages
if ( !function_exists( 'yreg_estate_get_list_pages' ) ) {
	function yreg_estate_get_list_pages($prepend_inherit=false, $opt=array()) {
		$opt = array_merge(array(
			'post_type'			=> 'page',
			'post_status'		=> 'publish',
			'posts_per_page'	=> -1,
			'orderby'			=> 'title',
			'order'				=> 'asc',
			'return'			=> 'id'
			), is_array($opt) ? $opt : array('post_type'=>$opt));
		return yreg_estate_get_list_posts($prepend_inherit, $opt);
	}
}


// Return list of registered users
if ( !function_exists( 'yreg_estate_get_list_users' ) ) {
	function yreg_estate_get_list_users($prepend_inherit=false, $roles=array('administrator', 'editor', 'author', 'contributor', 'shop_manager')) {
		if (($list = yreg_estate_storage_get('list_users'))=='') {
			$list = array();
			$list['none'] = esc_html__("- Not selected -", 'yreg-estate');
			$args = array(
				'orderby'	=> 'display_name',
				'order'		=> 'ASC' );
			$users = get_users( $args );
			if (is_array($users) && count($users) > 0) {
				foreach ($users as $user) {
					$accept = true;
					if (is_array($user->roles)) {
						if (is_array($user->roles) && count($user->roles) > 0) {
							$accept = false;
							foreach ($user->roles as $role) {
								if (in_array($role, $roles)) {
									$accept = true;
									break;
								}
							}
						}
					}
					if ($accept) $list[$user->user_login] = $user->display_name;
				}
			}
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_users', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}


// Return slider engines list, prepended inherit (if need)
if ( !function_exists( 'yreg_estate_get_list_sliders' ) ) {
	function yreg_estate_get_list_sliders($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_sliders'))=='') {
			$list = array(
				'swiper' => esc_html__("Posts slider (Swiper)", 'yreg-estate')
			);
			$list = apply_filters('yreg_estate_filter_list_sliders', $list);
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_sliders', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}


// Return slider controls list, prepended inherit (if need)
if ( !function_exists( 'yreg_estate_get_list_slider_controls' ) ) {
	function yreg_estate_get_list_slider_controls($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_slider_controls'))=='') {
			$list = array(
				'no'		=> esc_html__('None', 'yreg-estate'),
				'side'		=> esc_html__('Side', 'yreg-estate'),
//				'bottom'	=> esc_html__('Bottom', 'yreg-estate'),
				'pagination'=> esc_html__('Pagination', 'yreg-estate')
				);
			$list = apply_filters('yreg_estate_filter_list_slider_controls', $list);
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_slider_controls', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}


// Return slider controls classes
if ( !function_exists( 'yreg_estate_get_slider_controls_classes' ) ) {
	function yreg_estate_get_slider_controls_classes($controls) {
		if (yreg_estate_param_is_off($controls))	$classes = 'sc_slider_nopagination sc_slider_nocontrols';
		else if ($controls=='bottom')			$classes = 'sc_slider_nopagination sc_slider_controls sc_slider_controls_bottom';
		else if ($controls=='pagination')		$classes = 'sc_slider_pagination sc_slider_pagination_bottom sc_slider_nocontrols';
		else									$classes = 'sc_slider_nopagination sc_slider_controls sc_slider_controls_side';
		return $classes;
	}
}

// Return list with popup engines
if ( !function_exists( 'yreg_estate_get_list_popup_engines' ) ) {
	function yreg_estate_get_list_popup_engines($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_popup_engines'))=='') {
			$list = array(
				"pretty"	=> esc_html__("Pretty photo", 'yreg-estate'),
				"magnific"	=> esc_html__("Magnific popup", 'yreg-estate')
				);
			$list = apply_filters('yreg_estate_filter_list_popup_engines', $list);
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_popup_engines', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return menus list, prepended inherit
if ( !function_exists( 'yreg_estate_get_list_menus' ) ) {
	function yreg_estate_get_list_menus($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_menus'))=='') {
			$list = array();
			$list['default'] = esc_html__("Default", 'yreg-estate');
			$menus = wp_get_nav_menus();
			if (is_array($menus) && count($menus) > 0) {
				foreach ($menus as $menu) {
					$list[$menu->slug] = $menu->name;
				}
			}
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_menus', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return custom sidebars list, prepended inherit and main sidebars item (if need)
if ( !function_exists( 'yreg_estate_get_list_sidebars' ) ) {
	function yreg_estate_get_list_sidebars($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_sidebars'))=='') {
			if (($list = yreg_estate_storage_get('registered_sidebars'))=='') $list = array();
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_sidebars', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return sidebars positions
if ( !function_exists( 'yreg_estate_get_list_sidebars_positions' ) ) {
	function yreg_estate_get_list_sidebars_positions($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_sidebars_positions'))=='') {
			$list = array(
				'none'  => esc_html__('Hide',  'yreg-estate'),
				'left'  => esc_html__('Left',  'yreg-estate'),
				'right' => esc_html__('Right', 'yreg-estate')
				);
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_sidebars_positions', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return sidebars class
if ( !function_exists( 'yreg_estate_get_sidebar_class' ) ) {
	function yreg_estate_get_sidebar_class() {
		$sb_main = yreg_estate_get_custom_option('show_sidebar_main');
		$sb_outer = yreg_estate_get_custom_option('show_sidebar_outer');
		return (yreg_estate_param_is_off($sb_main) ? 'sidebar_hide' : 'sidebar_show sidebar_'.($sb_main))
				. ' ' . (yreg_estate_param_is_off($sb_outer) ? 'sidebar_outer_hide' : 'sidebar_outer_show sidebar_outer_'.($sb_outer));
	}
}

// Return body styles list, prepended inherit
if ( !function_exists( 'yreg_estate_get_list_body_styles' ) ) {
	function yreg_estate_get_list_body_styles($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_body_styles'))=='') {
			$list = array(
				'boxed'	=> esc_html__('Boxed',		'yreg-estate'),
				'wide'	=> esc_html__('Wide',		'yreg-estate')
				);
			if (yreg_estate_get_theme_setting('allow_fullscreen')) {
				$list['fullwide']	= esc_html__('Fullwide',	'yreg-estate');
				$list['fullscreen']	= esc_html__('Fullscreen',	'yreg-estate');
			}
			$list = apply_filters('yreg_estate_filter_list_body_styles', $list);
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_body_styles', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return skins list, prepended inherit
if ( !function_exists( 'yreg_estate_get_list_skins' ) ) {
	function yreg_estate_get_list_skins($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_skins'))=='') {
			$list = yreg_estate_get_list_folders("skins");
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_skins', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return css-themes list
if ( !function_exists( 'yreg_estate_get_list_themes' ) ) {
	function yreg_estate_get_list_themes($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_themes'))=='') {
			$list = yreg_estate_get_list_files("css/themes");
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_themes', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return templates list, prepended inherit
if ( !function_exists( 'yreg_estate_get_list_templates' ) ) {
	function yreg_estate_get_list_templates($mode='') {
		if (($list = yreg_estate_storage_get('list_templates_'.($mode)))=='') {
			$list = array();
			$tpl = yreg_estate_storage_get('registered_templates');
			if (is_array($tpl) && count($tpl) > 0) {
				foreach ($tpl as $k=>$v) {
					if ($mode=='' || in_array($mode, explode(',', $v['mode'])))
						$list[$k] = !empty($v['icon']) 
									? $v['icon'] 
									: (!empty($v['title']) 
										? $v['title'] 
										: yreg_estate_strtoproper($v['layout'])
										);
				}
			}
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_templates_'.($mode), $list);
		}
		return $list;
	}
}

// Return blog styles list, prepended inherit
if ( !function_exists( 'yreg_estate_get_list_templates_blog' ) ) {
	function yreg_estate_get_list_templates_blog($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_templates_blog'))=='') {
			$list = yreg_estate_get_list_templates('blog');
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_templates_blog', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return blogger styles list, prepended inherit
if ( !function_exists( 'yreg_estate_get_list_templates_blogger' ) ) {
	function yreg_estate_get_list_templates_blogger($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_templates_blogger'))=='') {
			$list = yreg_estate_array_merge(yreg_estate_get_list_templates('blogger'), yreg_estate_get_list_templates('blog'));
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_templates_blogger', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return single page styles list, prepended inherit
if ( !function_exists( 'yreg_estate_get_list_templates_single' ) ) {
	function yreg_estate_get_list_templates_single($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_templates_single'))=='') {
			$list = yreg_estate_get_list_templates('single');
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_templates_single', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return header styles list, prepended inherit
if ( !function_exists( 'yreg_estate_get_list_templates_header' ) ) {
	function yreg_estate_get_list_templates_header($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_templates_header'))=='') {
			$list = yreg_estate_get_list_templates('header');
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_templates_header', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return form styles list, prepended inherit
if ( !function_exists( 'yreg_estate_get_list_templates_forms' ) ) {
	function yreg_estate_get_list_templates_forms($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_templates_forms'))=='') {
			$list = yreg_estate_get_list_templates('forms');
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_templates_forms', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return article styles list, prepended inherit
if ( !function_exists( 'yreg_estate_get_list_article_styles' ) ) {
	function yreg_estate_get_list_article_styles($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_article_styles'))=='') {
			$list = array(
				"boxed"   => esc_html__('Boxed', 'yreg-estate'),
				"stretch" => esc_html__('Stretch', 'yreg-estate')
				);
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_article_styles', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return post-formats filters list, prepended inherit
if ( !function_exists( 'yreg_estate_get_list_post_formats_filters' ) ) {
	function yreg_estate_get_list_post_formats_filters($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_post_formats_filters'))=='') {
			$list = array(
				"no"      => esc_html__('All posts', 'yreg-estate'),
				"thumbs"  => esc_html__('With thumbs', 'yreg-estate'),
				"reviews" => esc_html__('With reviews', 'yreg-estate'),
				"video"   => esc_html__('With videos', 'yreg-estate'),
				"audio"   => esc_html__('With audios', 'yreg-estate'),
				"gallery" => esc_html__('With galleries', 'yreg-estate')
				);
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_post_formats_filters', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return portfolio filters list, prepended inherit
if ( !function_exists( 'yreg_estate_get_list_portfolio_filters' ) ) {
	function yreg_estate_get_list_portfolio_filters($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_portfolio_filters'))=='') {
			$list = array(
				"hide"		=> esc_html__('Hide', 'yreg-estate'),
				"tags"		=> esc_html__('Tags', 'yreg-estate'),
				"categories"=> esc_html__('Categories', 'yreg-estate')
				);
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_portfolio_filters', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return hover styles list, prepended inherit
if ( !function_exists( 'yreg_estate_get_list_hovers' ) ) {
	function yreg_estate_get_list_hovers($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_hovers'))=='') {
			$list = array();
			$list['circle effect1']  = esc_html__('Circle Effect 1',  'yreg-estate');
			$list['circle effect2']  = esc_html__('Circle Effect 2',  'yreg-estate');
			$list['circle effect3']  = esc_html__('Circle Effect 3',  'yreg-estate');
			$list['circle effect4']  = esc_html__('Circle Effect 4',  'yreg-estate');
			$list['circle effect5']  = esc_html__('Circle Effect 5',  'yreg-estate');
			$list['circle effect6']  = esc_html__('Circle Effect 6',  'yreg-estate');
			$list['circle effect7']  = esc_html__('Circle Effect 7',  'yreg-estate');
			$list['circle effect8']  = esc_html__('Circle Effect 8',  'yreg-estate');
			$list['circle effect9']  = esc_html__('Circle Effect 9',  'yreg-estate');
			$list['circle effect10'] = esc_html__('Circle Effect 10',  'yreg-estate');
			$list['circle effect11'] = esc_html__('Circle Effect 11',  'yreg-estate');
			$list['circle effect12'] = esc_html__('Circle Effect 12',  'yreg-estate');
			$list['circle effect13'] = esc_html__('Circle Effect 13',  'yreg-estate');
			$list['circle effect14'] = esc_html__('Circle Effect 14',  'yreg-estate');
			$list['circle effect15'] = esc_html__('Circle Effect 15',  'yreg-estate');
			$list['circle effect16'] = esc_html__('Circle Effect 16',  'yreg-estate');
			$list['circle effect17'] = esc_html__('Circle Effect 17',  'yreg-estate');
			$list['circle effect18'] = esc_html__('Circle Effect 18',  'yreg-estate');
			$list['circle effect19'] = esc_html__('Circle Effect 19',  'yreg-estate');
			$list['circle effect20'] = esc_html__('Circle Effect 20',  'yreg-estate');
			$list['square effect1']  = esc_html__('Square Effect 1',  'yreg-estate');
			$list['square effect2']  = esc_html__('Square Effect 2',  'yreg-estate');
			$list['square effect3']  = esc_html__('Square Effect 3',  'yreg-estate');
	//		$list['square effect4']  = esc_html__('Square Effect 4',  'yreg-estate');
			$list['square effect5']  = esc_html__('Square Effect 5',  'yreg-estate');
			$list['square effect6']  = esc_html__('Square Effect 6',  'yreg-estate');
			$list['square effect7']  = esc_html__('Square Effect 7',  'yreg-estate');
			$list['square effect8']  = esc_html__('Square Effect 8',  'yreg-estate');
			$list['square effect9']  = esc_html__('Square Effect 9',  'yreg-estate');
			$list['square effect10'] = esc_html__('Square Effect 10',  'yreg-estate');
			$list['square effect11'] = esc_html__('Square Effect 11',  'yreg-estate');
			$list['square effect12'] = esc_html__('Square Effect 12',  'yreg-estate');
			$list['square effect13'] = esc_html__('Square Effect 13',  'yreg-estate');
			$list['square effect14'] = esc_html__('Square Effect 14',  'yreg-estate');
			$list['square effect15'] = esc_html__('Square Effect 15',  'yreg-estate');
			$list['square effect_dir']   = esc_html__('Square Effect Dir',   'yreg-estate');
			$list['square effect_shift'] = esc_html__('Square Effect Shift', 'yreg-estate');
			$list['square effect_book']  = esc_html__('Square Effect Book',  'yreg-estate');
			$list['square effect_more']  = esc_html__('Square Effect More',  'yreg-estate');
			$list['square effect_fade']  = esc_html__('Square Effect Fade',  'yreg-estate');
			$list = apply_filters('yreg_estate_filter_portfolio_hovers', $list);
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_hovers', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}


// Return list of the blog counters
if ( !function_exists( 'yreg_estate_get_list_blog_counters' ) ) {
	function yreg_estate_get_list_blog_counters($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_blog_counters'))=='') {
			$list = array(
				'views'		=> esc_html__('Visualizzazioni', 'yreg-estate'),
				'likes'		=> esc_html__('Likes', 'yreg-estate'),
				/*'rating'	=> esc_html__('Rating', 'yreg-estate'),*/
				'comments'	=> esc_html__('Comments', 'yreg-estate')
				);
			$list = apply_filters('yreg_estate_filter_list_blog_counters', $list);
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_blog_counters', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return list of the item sizes for the portfolio alter style, prepended inherit
if ( !function_exists( 'yreg_estate_get_list_alter_sizes' ) ) {
	function yreg_estate_get_list_alter_sizes($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_alter_sizes'))=='') {
			$list = array(
					'1_1' => esc_html__('1x1', 'yreg-estate'),
					'1_2' => esc_html__('1x2', 'yreg-estate'),
					'2_1' => esc_html__('2x1', 'yreg-estate'),
					'2_2' => esc_html__('2x2', 'yreg-estate'),
					'1_3' => esc_html__('1x3', 'yreg-estate'),
					'2_3' => esc_html__('2x3', 'yreg-estate'),
					'3_1' => esc_html__('3x1', 'yreg-estate'),
					'3_2' => esc_html__('3x2', 'yreg-estate'),
					'3_3' => esc_html__('3x3', 'yreg-estate')
					);
			$list = apply_filters('yreg_estate_filter_portfolio_alter_sizes', $list);
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_alter_sizes', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return extended hover directions list, prepended inherit
if ( !function_exists( 'yreg_estate_get_list_hovers_directions' ) ) {
	function yreg_estate_get_list_hovers_directions($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_hovers_directions'))=='') {
			$list = array(
				'left_to_right' => esc_html__('Left to Right',  'yreg-estate'),
				'right_to_left' => esc_html__('Right to Left',  'yreg-estate'),
				'top_to_bottom' => esc_html__('Top to Bottom',  'yreg-estate'),
				'bottom_to_top' => esc_html__('Bottom to Top',  'yreg-estate'),
				'scale_up'      => esc_html__('Scale Up',  'yreg-estate'),
				'scale_down'    => esc_html__('Scale Down',  'yreg-estate'),
				'scale_down_up' => esc_html__('Scale Down-Up',  'yreg-estate'),
				'from_left_and_right' => esc_html__('From Left and Right',  'yreg-estate'),
				'from_top_and_bottom' => esc_html__('From Top and Bottom',  'yreg-estate')
			);
			$list = apply_filters('yreg_estate_filter_portfolio_hovers_directions', $list);
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_hovers_directions', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}


// Return list of the label positions in the custom forms
if ( !function_exists( 'yreg_estate_get_list_label_positions' ) ) {
	function yreg_estate_get_list_label_positions($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_label_positions'))=='') {
			$list = array(
				'top'		=> esc_html__('Top',		'yreg-estate'),
				'bottom'	=> esc_html__('Bottom',		'yreg-estate'),
				'left'		=> esc_html__('Left',		'yreg-estate'),
				'over'		=> esc_html__('Over',		'yreg-estate')
			);
			$list = apply_filters('yreg_estate_filter_label_positions', $list);
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_label_positions', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}


// Return list of the bg image positions
if ( !function_exists( 'yreg_estate_get_list_bg_image_positions' ) ) {
	function yreg_estate_get_list_bg_image_positions($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_bg_image_positions'))=='') {
			$list = array(
				'left top'	   => esc_html__('Left Top', 'yreg-estate'),
				'center top'   => esc_html__("Center Top", 'yreg-estate'),
				'right top'    => esc_html__("Right Top", 'yreg-estate'),
				'left center'  => esc_html__("Left Center", 'yreg-estate'),
				'center center'=> esc_html__("Center Center", 'yreg-estate'),
				'right center' => esc_html__("Right Center", 'yreg-estate'),
				'left bottom'  => esc_html__("Left Bottom", 'yreg-estate'),
				'center bottom'=> esc_html__("Center Bottom", 'yreg-estate'),
				'right bottom' => esc_html__("Right Bottom", 'yreg-estate')
			);
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_bg_image_positions', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}


// Return list of the bg image repeat
if ( !function_exists( 'yreg_estate_get_list_bg_image_repeats' ) ) {
	function yreg_estate_get_list_bg_image_repeats($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_bg_image_repeats'))=='') {
			$list = array(
				'repeat'	=> esc_html__('Repeat', 'yreg-estate'),
				'repeat-x'	=> esc_html__('Repeat X', 'yreg-estate'),
				'repeat-y'	=> esc_html__('Repeat Y', 'yreg-estate'),
				'no-repeat'	=> esc_html__('No Repeat', 'yreg-estate')
			);
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_bg_image_repeats', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}


// Return list of the bg image attachment
if ( !function_exists( 'yreg_estate_get_list_bg_image_attachments' ) ) {
	function yreg_estate_get_list_bg_image_attachments($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_bg_image_attachments'))=='') {
			$list = array(
				'scroll'	=> esc_html__('Scroll', 'yreg-estate'),
				'fixed'		=> esc_html__('Fixed', 'yreg-estate'),
				'local'		=> esc_html__('Local', 'yreg-estate')
			);
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_bg_image_attachments', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}


// Return list of the bg tints
if ( !function_exists( 'yreg_estate_get_list_bg_tints' ) ) {
	function yreg_estate_get_list_bg_tints($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_bg_tints'))=='') {
			$list = array(
				'white'	=> esc_html__('White', 'yreg-estate'),
				'light'	=> esc_html__('Light', 'yreg-estate'),
				'dark'	=> esc_html__('Dark', 'yreg-estate')
			);
			$list = apply_filters('yreg_estate_filter_bg_tints', $list);
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_bg_tints', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return custom fields types list, prepended inherit
if ( !function_exists( 'yreg_estate_get_list_field_types' ) ) {
	function yreg_estate_get_list_field_types($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_field_types'))=='') {
			$list = array(
				'text'     => esc_html__('Text',  'yreg-estate'),
				'textarea' => esc_html__('Text Area','yreg-estate'),
				'password' => esc_html__('Password',  'yreg-estate'),
				'radio'    => esc_html__('Radio',  'yreg-estate'),
				'checkbox' => esc_html__('Checkbox',  'yreg-estate'),
				'select'   => esc_html__('Select',  'yreg-estate'),
				'date'     => esc_html__('Date','yreg-estate'),
				'time'     => esc_html__('Time','yreg-estate'),
				'button'   => esc_html__('Button','yreg-estate')
			);
			$list = apply_filters('yreg_estate_filter_field_types', $list);
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_field_types', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return Google map styles
if ( !function_exists( 'yreg_estate_get_list_googlemap_styles' ) ) {
	function yreg_estate_get_list_googlemap_styles($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_googlemap_styles'))=='') {
			$list = array(
				'default' => esc_html__('Default', 'yreg-estate'),
				'simple' => esc_html__('Simple', 'yreg-estate'),
				'greyscale' => esc_html__('Greyscale', 'yreg-estate'),
				'greyscale2' => esc_html__('Greyscale 2', 'yreg-estate'),
				'invert' => esc_html__('Invert', 'yreg-estate'),
				'dark' => esc_html__('Dark', 'yreg-estate'),
				'style1' => esc_html__('Custom style 1', 'yreg-estate'),
				'style2' => esc_html__('Custom style 2', 'yreg-estate'),
				'style3' => esc_html__('Custom style 3', 'yreg-estate')
			);
			$list = apply_filters('yreg_estate_filter_googlemap_styles', $list);
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_googlemap_styles', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return iconed classes list
if ( !function_exists( 'yreg_estate_get_list_icons' ) ) {
	function yreg_estate_get_list_icons($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_icons'))=='') {
			$list = yreg_estate_parse_icons_classes(yreg_estate_get_file_dir("css/fontello/css/fontello-codes.css"));
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_icons', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return socials list
if ( !function_exists( 'yreg_estate_get_list_socials' ) ) {
	function yreg_estate_get_list_socials($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_socials'))=='') {
			$list = yreg_estate_get_list_files("images/socials", "png");
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_socials', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return flags list
if ( !function_exists( 'yreg_estate_get_list_flags' ) ) {
	function yreg_estate_get_list_flags($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_flags'))=='') {
			$list = yreg_estate_get_list_files("images/flags", "png");
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_flags', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return list with 'Yes' and 'No' items
if ( !function_exists( 'yreg_estate_get_list_yesno' ) ) {
	function yreg_estate_get_list_yesno($prepend_inherit=false) {
		$list = array(
			'yes' => esc_html__("Yes", 'yreg-estate'),
			'no'  => esc_html__("No", 'yreg-estate')
		);
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return list with 'On' and 'Of' items
if ( !function_exists( 'yreg_estate_get_list_onoff' ) ) {
	function yreg_estate_get_list_onoff($prepend_inherit=false) {
		$list = array(
			"on" => esc_html__("On", 'yreg-estate'),
			"off" => esc_html__("Off", 'yreg-estate')
		);
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return list with 'Show' and 'Hide' items
if ( !function_exists( 'yreg_estate_get_list_showhide' ) ) {
	function yreg_estate_get_list_showhide($prepend_inherit=false) {
		$list = array(
			"show" => esc_html__("Show", 'yreg-estate'),
			"hide" => esc_html__("Hide", 'yreg-estate')
		);
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return list with 'Ascending' and 'Descending' items
if ( !function_exists( 'yreg_estate_get_list_orderings' ) ) {
	function yreg_estate_get_list_orderings($prepend_inherit=false) {
		$list = array(
			"asc" => esc_html__("Ascending", 'yreg-estate'),
			"desc" => esc_html__("Descending", 'yreg-estate')
		);
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return list with 'Horizontal' and 'Vertical' items
if ( !function_exists( 'yreg_estate_get_list_directions' ) ) {
	function yreg_estate_get_list_directions($prepend_inherit=false) {
		$list = array(
			"horizontal" => esc_html__("Horizontal", 'yreg-estate'),
			"vertical" => esc_html__("Vertical", 'yreg-estate')
		);
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return list with item's shapes
if ( !function_exists( 'yreg_estate_get_list_shapes' ) ) {
	function yreg_estate_get_list_shapes($prepend_inherit=false) {
		$list = array(
			"round"  => esc_html__("Round", 'yreg-estate'),
			"square" => esc_html__("Square", 'yreg-estate')
		);
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return list with item's sizes
if ( !function_exists( 'yreg_estate_get_list_sizes' ) ) {
	function yreg_estate_get_list_sizes($prepend_inherit=false) {
		$list = array(
			"tiny"   => esc_html__("Tiny", 'yreg-estate'),
			"small"  => esc_html__("Small", 'yreg-estate'),
			"medium" => esc_html__("Medium", 'yreg-estate'),
			"large"  => esc_html__("Large", 'yreg-estate')
		);
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return list with slider (scroll) controls positions
if ( !function_exists( 'yreg_estate_get_list_controls' ) ) {
	function yreg_estate_get_list_controls($prepend_inherit=false) {
		$list = array(
			"hide" => esc_html__("Hide", 'yreg-estate'),
			"side" => esc_html__("Side", 'yreg-estate'),
			"bottom" => esc_html__("Bottom", 'yreg-estate')
		);
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return list with float items
if ( !function_exists( 'yreg_estate_get_list_floats' ) ) {
	function yreg_estate_get_list_floats($prepend_inherit=false) {
		$list = array(
			"none" => esc_html__("None", 'yreg-estate'),
			"left" => esc_html__("Float Left", 'yreg-estate'),
			"right" => esc_html__("Float Right", 'yreg-estate')
		);
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return list with alignment items
if ( !function_exists( 'yreg_estate_get_list_alignments' ) ) {
	function yreg_estate_get_list_alignments($justify=false, $prepend_inherit=false) {
		$list = array(
			"none" => esc_html__("None", 'yreg-estate'),
			"left" => esc_html__("Left", 'yreg-estate'),
			"center" => esc_html__("Center", 'yreg-estate'),
			"right" => esc_html__("Right", 'yreg-estate')
		);
		if ($justify) $list["justify"] = esc_html__("Justify", 'yreg-estate');
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return list with horizontal positions
if ( !function_exists( 'yreg_estate_get_list_hpos' ) ) {
	function yreg_estate_get_list_hpos($prepend_inherit=false, $center=false) {
		$list = array();
		$list['left'] = esc_html__("Left", 'yreg-estate');
		if ($center) $list['center'] = esc_html__("Center", 'yreg-estate');
		$list['right'] = esc_html__("Right", 'yreg-estate');
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return list with vertical positions
if ( !function_exists( 'yreg_estate_get_list_vpos' ) ) {
	function yreg_estate_get_list_vpos($prepend_inherit=false, $center=false) {
		$list = array();
		$list['top'] = esc_html__("Top", 'yreg-estate');
		if ($center) $list['center'] = esc_html__("Center", 'yreg-estate');
		$list['bottom'] = esc_html__("Bottom", 'yreg-estate');
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return sorting list items
if ( !function_exists( 'yreg_estate_get_list_sortings' ) ) {
	function yreg_estate_get_list_sortings($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_sortings'))=='') {
			$list = array(
				"date" => esc_html__("Date", 'yreg-estate'),
				"title" => esc_html__("Alphabetically", 'yreg-estate'),
				"views" => esc_html__("Popular (views count)", 'yreg-estate'),
				"comments" => esc_html__("Most commented (comments count)", 'yreg-estate'),
				"author_rating" => esc_html__("Author rating", 'yreg-estate'),
				"users_rating" => esc_html__("Visitors (users) rating", 'yreg-estate'),
				"random" => esc_html__("Random", 'yreg-estate')
			);
			$list = apply_filters('yreg_estate_filter_list_sortings', $list);
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_sortings', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return list with columns widths
if ( !function_exists( 'yreg_estate_get_list_columns' ) ) {
	function yreg_estate_get_list_columns($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_columns'))=='') {
			$list = array(
				"none" => esc_html__("None", 'yreg-estate'),
				"1_1" => esc_html__("100%", 'yreg-estate'),
				"1_2" => esc_html__("1/2", 'yreg-estate'),
				"1_3" => esc_html__("1/3", 'yreg-estate'),
				"2_3" => esc_html__("2/3", 'yreg-estate'),
				"1_4" => esc_html__("1/4", 'yreg-estate'),
				"3_4" => esc_html__("3/4", 'yreg-estate'),
				"1_5" => esc_html__("1/5", 'yreg-estate'),
				"2_5" => esc_html__("2/5", 'yreg-estate'),
				"3_5" => esc_html__("3/5", 'yreg-estate'),
				"4_5" => esc_html__("4/5", 'yreg-estate'),
				"1_6" => esc_html__("1/6", 'yreg-estate'),
				"5_6" => esc_html__("5/6", 'yreg-estate'),
				"1_7" => esc_html__("1/7", 'yreg-estate'),
				"2_7" => esc_html__("2/7", 'yreg-estate'),
				"3_7" => esc_html__("3/7", 'yreg-estate'),
				"4_7" => esc_html__("4/7", 'yreg-estate'),
				"5_7" => esc_html__("5/7", 'yreg-estate'),
				"6_7" => esc_html__("6/7", 'yreg-estate'),
				"1_8" => esc_html__("1/8", 'yreg-estate'),
				"3_8" => esc_html__("3/8", 'yreg-estate'),
				"5_8" => esc_html__("5/8", 'yreg-estate'),
				"7_8" => esc_html__("7/8", 'yreg-estate'),
				"1_9" => esc_html__("1/9", 'yreg-estate'),
				"2_9" => esc_html__("2/9", 'yreg-estate'),
				"4_9" => esc_html__("4/9", 'yreg-estate'),
				"5_9" => esc_html__("5/9", 'yreg-estate'),
				"7_9" => esc_html__("7/9", 'yreg-estate'),
				"8_9" => esc_html__("8/9", 'yreg-estate'),
				"1_10"=> esc_html__("1/10", 'yreg-estate'),
				"3_10"=> esc_html__("3/10", 'yreg-estate'),
				"7_10"=> esc_html__("7/10", 'yreg-estate'),
				"9_10"=> esc_html__("9/10", 'yreg-estate'),
				"1_11"=> esc_html__("1/11", 'yreg-estate'),
				"2_11"=> esc_html__("2/11", 'yreg-estate'),
				"3_11"=> esc_html__("3/11", 'yreg-estate'),
				"4_11"=> esc_html__("4/11", 'yreg-estate'),
				"5_11"=> esc_html__("5/11", 'yreg-estate'),
				"6_11"=> esc_html__("6/11", 'yreg-estate'),
				"7_11"=> esc_html__("7/11", 'yreg-estate'),
				"8_11"=> esc_html__("8/11", 'yreg-estate'),
				"9_11"=> esc_html__("9/11", 'yreg-estate'),
				"10_11"=> esc_html__("10/11", 'yreg-estate'),
				"1_12"=> esc_html__("1/12", 'yreg-estate'),
				"5_12"=> esc_html__("5/12", 'yreg-estate'),
				"7_12"=> esc_html__("7/12", 'yreg-estate'),
				"10_12"=> esc_html__("10/12", 'yreg-estate'),
				"11_12"=> esc_html__("11/12", 'yreg-estate')
			);
			$list = apply_filters('yreg_estate_filter_list_columns', $list);
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_columns', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return list of locations for the dedicated content
if ( !function_exists( 'yreg_estate_get_list_dedicated_locations' ) ) {
	function yreg_estate_get_list_dedicated_locations($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_dedicated_locations'))=='') {
			$list = array(
				"default" => esc_html__('As in the post defined', 'yreg-estate'),
				"center"  => esc_html__('Above the text of the post', 'yreg-estate'),
				"left"    => esc_html__('To the left the text of the post', 'yreg-estate'),
				"right"   => esc_html__('To the right the text of the post', 'yreg-estate'),
				"alter"   => esc_html__('Alternates for each post', 'yreg-estate')
			);
			$list = apply_filters('yreg_estate_filter_list_dedicated_locations', $list);
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_dedicated_locations', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return post-format name
if ( !function_exists( 'yreg_estate_get_post_format_name' ) ) {
	function yreg_estate_get_post_format_name($format, $single=true) {
		$name = '';
		if ($format=='gallery')		$name = $single ? esc_html__('gallery', 'yreg-estate') : esc_html__('galleries', 'yreg-estate');
		else if ($format=='video')	$name = $single ? esc_html__('video', 'yreg-estate') : esc_html__('videos', 'yreg-estate');
		else if ($format=='audio')	$name = $single ? esc_html__('audio', 'yreg-estate') : esc_html__('audios', 'yreg-estate');
		else if ($format=='image')	$name = $single ? esc_html__('image', 'yreg-estate') : esc_html__('images', 'yreg-estate');
		else if ($format=='quote')	$name = $single ? esc_html__('quote', 'yreg-estate') : esc_html__('quotes', 'yreg-estate');
		else if ($format=='link')	$name = $single ? esc_html__('link', 'yreg-estate') : esc_html__('links', 'yreg-estate');
		else if ($format=='status')	$name = $single ? esc_html__('status', 'yreg-estate') : esc_html__('statuses', 'yreg-estate');
		else if ($format=='aside')	$name = $single ? esc_html__('aside', 'yreg-estate') : esc_html__('asides', 'yreg-estate');
		else if ($format=='chat')	$name = $single ? esc_html__('chat', 'yreg-estate') : esc_html__('chats', 'yreg-estate');
		else						$name = $single ? esc_html__('standard', 'yreg-estate') : esc_html__('standards', 'yreg-estate');
		return apply_filters('yreg_estate_filter_list_post_format_name', $name, $format);
	}
}

// Return post-format icon name (from Fontello library)
if ( !function_exists( 'yreg_estate_get_post_format_icon' ) ) {
	function yreg_estate_get_post_format_icon($format) {
		$icon = 'icon-';
		if ($format=='gallery')		$icon .= 'pictures';
		else if ($format=='video')	$icon .= 'video';
		else if ($format=='audio')	$icon .= 'note';
		else if ($format=='image')	$icon .= 'picture';
		else if ($format=='quote')	$icon .= 'quote';
		else if ($format=='link')	$icon .= 'link';
		else if ($format=='status')	$icon .= 'comment';
		else if ($format=='aside')	$icon .= 'doc-text';
		else if ($format=='chat')	$icon .= 'chat';
		else						$icon .= 'book-open';
		return apply_filters('yreg_estate_filter_list_post_format_icon', $icon, $format);
	}
}

// Return fonts styles list, prepended inherit
if ( !function_exists( 'yreg_estate_get_list_fonts_styles' ) ) {
	function yreg_estate_get_list_fonts_styles($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_fonts_styles'))=='') {
			$list = array(
				'i' => esc_html__('I','yreg-estate'),
				'u' => esc_html__('U', 'yreg-estate')
			);
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_fonts_styles', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return Google fonts list
if ( !function_exists( 'yreg_estate_get_list_fonts' ) ) {
	function yreg_estate_get_list_fonts($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_fonts'))=='') {
			$list = array();
			$list = yreg_estate_array_merge($list, yreg_estate_get_list_font_faces());
			// Google and custom fonts list:
			//$list['Advent Pro'] = array(
			//		'family'=>'sans-serif',																						// (required) font family
			//		'link'=>'Advent+Pro:100,100italic,300,300italic,400,400italic,500,500italic,700,700italic,900,900italic',	// (optional) if you use Google font repository
			//		'css'=>yreg_estate_get_file_url('/css/font-face/Advent-Pro/stylesheet.css')									// (optional) if you use custom font-face
			//		);
			$list = yreg_estate_array_merge($list, array(
				'Advent Pro' => array('family'=>'sans-serif'),
				'Alegreya Sans' => array('family'=>'sans-serif'),
				'Arimo' => array('family'=>'sans-serif'),
				'Asap' => array('family'=>'sans-serif'),
				'Averia Sans Libre' => array('family'=>'cursive'),
				'Averia Serif Libre' => array('family'=>'cursive'),
				'Bree Serif' => array('family'=>'serif',),
				'Cabin' => array('family'=>'sans-serif'),
				'Cabin Condensed' => array('family'=>'sans-serif'),
				'Caudex' => array('family'=>'serif'),
				'Comfortaa' => array('family'=>'cursive'),
				'Cousine' => array('family'=>'sans-serif'),
				'Crimson Text' => array('family'=>'serif'),
				'Cuprum' => array('family'=>'sans-serif'),
				'Dosis' => array('family'=>'sans-serif'),
				'Economica' => array('family'=>'sans-serif'),
				'Exo' => array('family'=>'sans-serif'),
				'Expletus Sans' => array('family'=>'cursive'),
				'Karla' => array('family'=>'sans-serif'),
				'Lato' => array('family'=>'sans-serif'),
				'Lekton' => array('family'=>'sans-serif'),
				'Lobster Two' => array('family'=>'cursive'),
				'Maven Pro' => array('family'=>'sans-serif'),
				'Merriweather' => array('family'=>'serif'),
				'Montserrat' => array('family'=>'sans-serif'),
				'Neuton' => array('family'=>'serif'),
				'Noticia Text' => array('family'=>'serif'),
				'Old Standard TT' => array('family'=>'serif'),
				'Open Sans' => array('family'=>'sans-serif'),
				'Orbitron' => array('family'=>'sans-serif'),
				'Oswald' => array('family'=>'sans-serif'),
				'Overlock' => array('family'=>'cursive'),
				'Oxygen' => array('family'=>'sans-serif'),
				'Philosopher' => array('family'=>'serif'),
				'PT Serif' => array('family'=>'serif'),
				'Puritan' => array('family'=>'sans-serif'),
				'Raleway' => array('family'=>'sans-serif'),
				'Roboto' => array('family'=>'sans-serif'),
				'Roboto Slab' => array('family'=>'sans-serif'),
				'Roboto Condensed' => array('family'=>'sans-serif'),
				'Rosario' => array('family'=>'sans-serif'),
				'Share' => array('family'=>'cursive'),
				'Signika' => array('family'=>'sans-serif'),
				'Signika Negative' => array('family'=>'sans-serif'),
				'Source Sans Pro' => array('family'=>'sans-serif'),
				'Tinos' => array('family'=>'serif'),
				'Ubuntu' => array('family'=>'sans-serif'),
				'Vollkorn' => array('family'=>'serif')
				)
			);
			$list = apply_filters('yreg_estate_filter_list_fonts', $list);
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_fonts', $list);
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}

// Return Custom font-face list
if ( !function_exists( 'yreg_estate_get_list_font_faces' ) ) {
	function yreg_estate_get_list_font_faces($prepend_inherit=false) {
		static $list = false;
		if (is_array($list)) return $list;
		$list = array();
		$dir = yreg_estate_get_folder_dir("css/font-face");
		if ( is_dir($dir) ) {
			$hdir = @ opendir( $dir );
			if ( $hdir ) {
				while (($file = readdir( $hdir ) ) !== false ) {
					$pi = pathinfo( ($dir) . '/' . ($file) );
					if ( substr($file, 0, 1) == '.' || ! is_dir( ($dir) . '/' . ($file) ) )
						continue;
					$css = file_exists( ($dir) . '/' . ($file) . '/' . ($file) . '.css' ) 
						? yreg_estate_get_folder_url("css/font-face/".($file).'/'.($file).'.css')
						: (file_exists( ($dir) . '/' . ($file) . '/stylesheet.css' ) 
							? yreg_estate_get_folder_url("css/font-face/".($file).'/stylesheet.css')
							: '');
					if ($css != '')
						$list[$file.' ('.esc_html__('uploaded font', 'yreg-estate').')'] = array('css' => $css);
				}
				@closedir( $hdir );
			}
		}
		return $list;
	}
}
?>