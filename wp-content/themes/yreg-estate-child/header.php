<?php
/**
 * The Header for our theme.
 */
// Theme init - don't remove next row! Load custom options
yreg_estate_core_init_theme();

yreg_estate_profiler_add_point(esc_html__('Before Theme HTML output', 'yreg-estate'));

$theme_skin = yreg_estate_esc(yreg_estate_get_custom_option('theme_skin'));
$body_scheme = yreg_estate_get_custom_option('body_scheme');
if (empty($body_scheme) || yreg_estate_is_inherit_option($body_scheme))
	$body_scheme = 'original';
$blog_style = yreg_estate_get_custom_option(is_singular() && !yreg_estate_storage_get('blog_streampage') ? 'single_style' : 'blog_style');
$body_style = yreg_estate_get_custom_option('body_style');
$article_style = yreg_estate_get_custom_option('article_style');
$top_panel_style = yreg_estate_get_custom_option('top_panel_style');
$top_panel_position = yreg_estate_get_custom_option('top_panel_position');
$top_panel_scheme = yreg_estate_get_custom_option('top_panel_scheme');
$video_bg_show = yreg_estate_get_custom_option('show_video_bg') == 'yes' && (yreg_estate_get_custom_option('video_bg_youtube_code') != '' || yreg_estate_get_custom_option('video_bg_url') != '');
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="<?php echo 'scheme_' . esc_attr($body_scheme); ?>">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1<?php if (yreg_estate_get_theme_option('responsive_layouts') == 'yes') echo ', maximum-scale=1'; ?>">
		<meta name="format-detection" content="telephone=no">
                <script src="http://maps.google.com/maps/api/js?key=AIzaSyDkJu5txIIMXSAyMHLnMeWXXHKRv-MsG-A&sensor=false&libraries=places"></script>
		<link rel="profile" href="http://gmpg.org/xfn/11" />
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

		<?php
		if (($preloader = yreg_estate_get_theme_option('page_preloader')) != '') {
			$clr = yreg_estate_get_scheme_color('bg_color');
			?>
			<style type="text/css">
				<!--
				#page_preloader { background-color: <?php echo esc_attr($clr); ?>; background-image:url(<?php echo esc_url($preloader); ?>); background-position:center; background-repeat:no-repeat; position:fixed; z-index:1000000; left:0; top:0; right:0; bottom:0; opacity: 0.8; }
				-->
			</style>
			<?php
		}

		if (!function_exists('has_site_icon') || !has_site_icon()) {
			$favicon = yreg_estate_get_custom_option('favicon');
			if (!$favicon) {
				if (file_exists(yreg_estate_get_file_dir('skins/' . ($theme_skin) . '/images/favicon.ico')))
					$favicon = yreg_estate_get_file_url('skins/' . ($theme_skin) . '/images/favicon.ico');
				if (!$favicon && file_exists(yreg_estate_get_file_dir('favicon.ico')))
					$favicon = yreg_estate_get_file_url('favicon.ico');
			}
			if ($favicon) {
				?><link rel="icon" type="image/x-icon" href="<?php echo esc_url($favicon); ?>" /><?php
			}
		}

		wp_head();
		?>
	</head>
	
	<body <?php body_class();?>>
	<body <?php
	body_class('yreg_estate_body body_style_' . esc_attr($body_style)
			. ' body_' . (yreg_estate_get_custom_option('body_filled') == 'yes' ? 'filled' : 'transparent')
			. ' theme_skin_' . esc_attr($theme_skin)
			. ' article_style_' . esc_attr($article_style)
			. ' layout_' . esc_attr($blog_style)
			. ' template_' . esc_attr(yreg_estate_get_template_name($blog_style))
			. ' scheme_' . $body_scheme
			. (!yreg_estate_param_is_off($top_panel_position) ? ' top_panel_show top_panel_' . esc_attr($top_panel_position) : 'top_panel_hide')
			. ' ' . esc_attr(yreg_estate_get_sidebar_class())
			. ($video_bg_show ? ' video_bg_show' : '')
			. (yreg_estate_get_theme_option('page_preloader') != '' ? ' preloader' : '')
	);
	?>
		>
			<?php
			yreg_estate_profiler_add_point(esc_html__('BODY start', 'yreg-estate'));

			echo force_balance_tags(yreg_estate_get_custom_option('gtm_code'));

			// Page preloader
			if ($preloader != '') {
				?><div id="page_preloader"></div><?php
		}

		do_action('before');
		?>

		<?php
		$class = $style = '';
		if ($body_style == 'boxed' || yreg_estate_get_custom_option('bg_image_load') == 'always') {
			if (($img = (int) yreg_estate_get_custom_option('bg_image', 0)) > 0)
				$class = 'bg_image_' . ($img);
			else if (($img = (int) yreg_estate_get_custom_option('bg_pattern', 0)) > 0)
				$class = 'bg_pattern_' . ($img);
			else if (($img = yreg_estate_get_custom_option('bg_color', '')) != '')
				$style = 'background-color: ' . ($img) . ';';
			else if (yreg_estate_get_custom_option('bg_custom') == 'yes') {
				if (($img = yreg_estate_get_custom_option('bg_image_custom')) != '')
					$style = 'background: url(' . esc_url($img) . ') ' . str_replace('_', ' ', yreg_estate_get_custom_option('bg_image_custom_position')) . ' no-repeat fixed;';
				else if (($img = yreg_estate_get_custom_option('bg_pattern_custom')) != '')
					$style = 'background: url(' . esc_url($img) . ') 0 0 repeat fixed;';
				else if (($img = yreg_estate_get_custom_option('bg_image')) > 0)
					$class = 'bg_image_' . ($img);
				else if (($img = yreg_estate_get_custom_option('bg_pattern')) > 0)
					$class = 'bg_pattern_' . ($img);
				if (($img = yreg_estate_get_custom_option('bg_color')) != '')
					$style .= 'background-color: ' . ($img) . ';';
			}
		}
		?>
                                
		<div class="body_wrap<?php echo!empty($class) ? ' ' . esc_attr($class) : ''; ?>"<?php echo!empty($style) ? ' style="' . esc_attr($style) . '"' : ''; ?>>

			<?php
			if ($video_bg_show) {
				$youtube = yreg_estate_get_custom_option('video_bg_youtube_code');
				$video = yreg_estate_get_custom_option('video_bg_url');
				$overlay = yreg_estate_get_custom_option('video_bg_overlay') == 'yes';
				if (!empty($youtube)) {
					?>
					<div class="video_bg<?php echo!empty($overlay) ? ' video_bg_overlay' : ''; ?>" data-youtube-code="<?php echo esc_attr($youtube); ?>"></div>
					<?php
				} else if (!empty($video)) {
					$info = pathinfo($video);
					$ext = !empty($info['extension']) ? $info['extension'] : 'src';
					?>
					<div class="video_bg<?php echo!empty($overlay) ? ' video_bg_overlay' : ''; ?>"><video class="video_bg_tag" width="1280" height="720" data-width="1280" data-height="720" data-ratio="16:9" preload="metadata" autoplay loop src="<?php echo esc_url($video); ?>"><source src="<?php echo esc_url($video); ?>" type="video/<?php echo esc_attr($ext); ?>"></source></video></div>
					<?php
				}
			}                                

			?>

			<div class="page_wrap">

				<?php
				// Top of page section: page title and breadcrumbs
				$show_title = yreg_estate_get_custom_option('show_page_title') == 'yes';
				$show_navi = $show_title && is_single() && yreg_estate_is_woocommerce_page();
				$show_breadcrumbs = yreg_estate_get_custom_option('show_breadcrumbs') == 'yes';

				yreg_estate_profiler_add_point(esc_html__('Before Page Header', 'yreg-estate'));
				// Top panel 'Above' or 'Over'
				if (in_array($top_panel_position, array('above', 'over'))) {
					yreg_estate_show_post_layout(array(
						'layout' => $top_panel_style,
						'position' => $top_panel_position,
						'scheme' => $top_panel_scheme
							), false);
					yreg_estate_profiler_add_point(esc_html__('After show menu', 'yreg-estate'));
                                        
					if ($show_title || $show_breadcrumbs) {
						?>
						<div class="top_panel_title top_panel_style_<?php echo esc_attr(str_replace('header_', '', $top_panel_style)); ?> <?php echo (!empty($show_title) ? ' title_present' . ($show_navi ? ' navi_present' : '') : '') . (!empty($show_breadcrumbs) ? ' breadcrumbs_present' : ''); ?> scheme_<?php echo esc_attr($top_panel_scheme); ?>">
							<div class="top_panel_title_inner top_panel_inner_style_<?php echo esc_attr(str_replace('header_', '', $top_panel_style)); ?> <?php echo (!empty($show_title) ? ' title_present_inner' : '') . (!empty($show_breadcrumbs) ? ' breadcrumbs_present_inner' : ''); ?>">
								<div class="content_wrap">
									<?php 
									if ($show_title) {
										if ($show_navi) {
											?><div class="post_navi"><?php
											previous_post_link('<span class="post_navi_item post_navi_prev">%link</span>', '%title', true, '', 'product_cat');
											echo '<span class="post_navi_delimiter"></span>';
											next_post_link('<span class="post_navi_item post_navi_next">%link</span>', '%title', true, '', 'product_cat');
											?></div><?php
										} else {
											?><h1 class="page_title"><?php echo strip_tags(yreg_estate_get_blog_title()); ?></h1><?php
											}
										}
										if ($show_breadcrumbs) {
											?><div class="breadcrumbs"><?php if (!is_404()) yreg_estate_show_breadcrumbs(); ?></div><?php
										}
										?>
								</div>
							</div>
						</div>
                            
                                                
						<?php
					}
				}
				
				if (yreg_estate_get_custom_option('top_panel_style') == 'header_1' and ( $top_panel_position != 'hide' )) echo '</div></header>';
				// Slider
				get_template_part(yreg_estate_get_file_slug('templates/headers/_parts/slider.php'));
				
				if (yreg_estate_get_custom_option('show_property_search')=='yes') { ?>
					
				<div class="ps_header">
					<div class="content_wrap">
						<?php
						echo trim(yreg_estate_do_shortcode('[vc_row][vc_column][trx_block dark_bg="yes"][trx_property_search][/trx_block][/vc_column][/vc_row]'));
						?>
					</div>
				</div>
					
				<?php }	?>
				
				
				

				<div class="page_content_wrap page_paddings_<?php echo esc_attr(yreg_estate_get_custom_option('body_paddings')); ?>">

<?php
yreg_estate_profiler_add_point(esc_html__('Before Page content', 'yreg-estate'));
// Content and sidebar wrapper
if ($body_style != 'fullscreen')
	yreg_estate_open_wrapper('<div class="content_wrap">');

// Main content wrapper
yreg_estate_open_wrapper('<div class="content">');

?>