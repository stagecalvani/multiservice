/* global jQuery:false */
/* global YREG_ESTATE_STORAGE:false */

var id_immobile_js;

// Theme-specific first load actions
//==============================================
function yreg_estate_theme_ready_actions() {
	"use strict";
	// Put here your init code for the theme-specific actions
	// It will be called before core actions
	date_time();
}


// Theme-specific scroll actions
//==============================================
function yreg_estate_theme_scroll_actions() {
	"use strict";
	// Put here your theme-specific code for scroll actions
	// It will be called when page is scrolled (before core actions)
}


// Theme-specific resize actions
//==============================================
function yreg_estate_theme_resize_actions() {
	"use strict";
	// Put here your theme-specific code for resize actions
	// It will be called when window is resized (before core actions)
	
	estate_max_height();
	maxh();
}


// Theme-specific shortcodes init
//=====================================================
function yreg_estate_theme_sc_init(cont) {
	"use strict";
	// Put here your theme-specific code for init shortcodes
	// It will be called before core init shortcodes
	// @param cont - jQuery-container with shortcodes (init only inside this container)
}


// Theme-specific post-formats init
//=====================================================
function yreg_estate_theme_init_post_formats() {
	"use strict";
	// Put here your theme-specific code for init post-formats
	// It will be called before core init post_formats when page is loaded or after 'Load more' or 'Infonite scroll' actions
}





function estate_max_height() {
	"use strict";
	jQuery('.eMaxHBox .eMaxH').each(function(){
		jQuery(this).css('height', 'inherit');
	});
	var maxH = 0;
	jQuery('.eMaxHBox .eMaxH').each(function(){
		//console.log(1);
		if ( jQuery(this).height() > maxH ) {
			maxH = jQuery(this).height();
		}
	});
	jQuery('.eMaxHBox .eMaxH').height(maxH);
	
}


jQuery(document).ready(function($){  
    if($('a.prettyphoto').length>0){
        $('a.prettyphoto img').attr('alt','Multiservice Foto');
        $('a.prettyphoto img').attr('title','Multiservice Foto');
    }
    jQuery(".estateCheckBox").change(function(){
		
        if(jQuery(this).is(":checked")){  //если чекбокс отмечен, то
            jQuery(this).parent("label").addClass("estateLabelCheckBoxSelected"); //добавляем класс LabelSelected ярлыку 
        }else{   
            jQuery(this).parent("label").removeClass("estateLabelCheckBoxSelected");  //иначе удаляем его
        }
    });  
    if($('form.wpcf7-form').length>0){
        $('input[name="id_immobile"]').attr('value',id_immobile_js);
    }
    $('aside#social-widget-2').prepend($('aside#nav_menu-3'));
    if($('#form_richiesta_informazioni').length>0){
        $('#stampa_scheda_btn a').bind('click',function(event){
            event.preventDefault();
            window.print();
        });
        $('#stampa_scheda_btn').insertBefore($('#form_richiesta_informazioni'));
    }
    $( 'select[name="ps_status"]' ).change(function() {
        var val_selected = $( this ).val();
        if(val_selected == 'sale' ){
            setRangePriceSelector(2000000,200000);
            setRangeAreaMq(1000,100);
        }else if(val_selected=='rent'){
            setRangePriceSelector(2000,500);
            setRangeAreaMq(500,100);
        }else if(val_selected=='activity'){
            setRangePriceSelector(2000000,200000);
            setRangeAreaMq(10000,10000);
        }else if(val_selected=='commerce'){
            setRangePriceSelector(10000000,10000000);
            setRangeAreaMq(50000000,50000000);
        }
    });
    
});  


jQuery(function() {
    setRangePriceSelector(2000000,200000);
    setRangeAreaMq(1000,100);
});

function setRangeAreaMq(maximum,range){
    "use strict";
    var js_area_min = parseInt(jQuery( ".ps_area_min" ).val());
    var js_area_max = maximum;
    var js_area_big = maximum;
    var area_min = js_area_min.toLocaleString();
    var area_max= js_area_max.toLocaleString();
    var step = range;
 jQuery( "#slider-range-area" ).slider({
    range: true,
    step: step,
    min: 0,
    max: js_area_big,
    values: [ js_area_min, js_area_max ],
    
    slide: function( event, ui ) {
     var ui_values_0 = ui.values[0].toLocaleString();
     var ui_values_1 = ui.values[1].toLocaleString();
     jQuery( ".ps_area .ps_area_min" ).val(ui.values[0]);
     jQuery( ".ps_area .ps_area_max" ).val(ui.values[1]);   
        if ( (ui.values[0]==0) && (ui.values[1]==js_area_big) ) {
            jQuery( ".ps_area_info_value" ).html(area_max + " mq");
        }else {
            jQuery( ".ps_area_info_value" ).html( ui_values_0 + " mq" + " - " + ui_values_1 + " mq" );
        }
       }
      });
      if ( (js_area_min==0) && (js_area_max==js_area_big) ) {
            jQuery( ".ps_area_info_value" ).html(  area_max + " mq"  );
      }else {
          jQuery( ".ps_area_info_value" ).html( area_min + " mq" + " - " + area_max + " mq" );
      }           
}

// setto il range e Max alue per il prezzo delle categorie
function setRangePriceSelector(maximum,range){
    "use strict";
    var js_price_min = parseInt(jQuery( ".ps_price_min" ).val());
    var js_price_max = maximum;
    var js_price_big = maximum;
    var step = range;
   
    var price_min = js_price_min.toLocaleString()+',00';
    var price_max = js_price_max.toLocaleString()+',00';
     jQuery( "#slider-range-price" ).slider({
      range: true,
      step: step,
      min: 0,
      max: js_price_big,
      values: [ js_price_min, js_price_max ],
      slide: function( event, ui ) {
        var ui_value_0 = ui.values[0].toLocaleString()+',00';
        var ui_value_1 = ui.values[1].toLocaleString()+',00';
        jQuery( ".ps_price .ps_price_min" ).val(ui.values[0]);
        jQuery( ".ps_price .ps_price_max" ).val(ui.values[1]);
        
        if ( (ui.values[0]==0) && (ui.values[1]==js_price_big) ) {
            jQuery( ".ps_price_info_value" ).html("€"+ price_max );
        } else {
            jQuery( ".ps_price_info_value" ).html( "€" + ui_value_0 + " - €" + ui_value_1 );
        }
     }
    });
    if((js_price_min==0)&&(js_price_max==js_price_big)){
       jQuery( ".ps_price_info_value" ).html( "€"+ price_max );
    } else {
       jQuery( ".ps_price_info_value" ).html( "€" + price_min + " - €" + price_max );
    }
}



function maxh() {
	"use strict";
	if (jQuery('.maxHBox').length>0) {
		jQuery('.maxHBox').each(function (){
			"use strict";
			var maxH = 0;
			jQuery(this).find('.maxHBoxItem').each(function (){
				"use strict";
				if ( jQuery(this).height() > maxH ) {
					maxH = jQuery(this).height();
				}
				console.log(maxH);
			});
			jQuery(this).find('.maxHBoxItem').height(maxH);
			console.log('xXx');
		});
	}
}


function date_time() {
	"use strict";
	jQuery('.custom_form_slider_item .cfs_date').before('<div class="cfs_date_time"></div>');
	jQuery('.custom_form_slider_item .cfs_date').appendTo('.custom_form_slider_item .cfs_date_time');
	jQuery('.custom_form_slider_item .cfs_time').appendTo('.custom_form_slider_item .cfs_date_time');
	
}

jQuery(window).load(function(){ 
    Shadowbox.init({
            handleOversize: "drag",
            modal: false,
            overlayOpacity: .6,
            overlayColor: '#000000',
            players:  ['img', 'html', 'iframe', 'qt', 'wmp', 'swf', 'flv']
     });
    if(jQuery('aside.af_TellaFriend_widget').length > 0){
            var url = jQuery('aside.af_TellaFriend_widget a').attr('href');
            jQuery('aside.af_TellaFriend_widget a').attr('href','#');
            jQuery('aside.af_TellaFriend_widget a').removeAttr('onclick');
            jQuery('aside.af_TellaFriend_widget a').attr('rel','shadowbox');
            jQuery('aside.af_TellaFriend_widget a').bind('click',function(event){
                event.preventDefault();
                   // $(window).load(function(){
                            Shadowbox.open({
                                content:    url, 
                                player:       "iframe", 
                                height:     420,
                                width:      580
                            });
                     // });
                });
        }
});

