<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('yreg_estate_sc_br_theme_setup')) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_sc_br_theme_setup' );
	function yreg_estate_sc_br_theme_setup() {
		add_action('yreg_estate_action_shortcodes_list', 		'yreg_estate_sc_br_reg_shortcodes');
		if (function_exists('yreg_estate_exists_visual_composer') && yreg_estate_exists_visual_composer())
			add_action('yreg_estate_action_shortcodes_list_vc','yreg_estate_sc_br_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_br clear="left|right|both"]
*/

if (!function_exists('yreg_estate_sc_br')) {	
	function yreg_estate_sc_br($atts, $content = null) {
		if (yreg_estate_in_shortcode_blogger()) return '';
		extract(yreg_estate_html_decode(shortcode_atts(array(
			"clear" => ""
		), $atts)));
		$output = in_array($clear, array('left', 'right', 'both', 'all')) 
			? '<div class="clearfix" style="clear:' . str_replace('all', 'both', $clear) . '"></div>'
			: '<br />';
		return apply_filters('yreg_estate_shortcode_output', $output, 'trx_br', $atts, $content);
	}
	yreg_estate_require_shortcode("trx_br", "yreg_estate_sc_br");
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_br_reg_shortcodes' ) ) {
	//add_action('yreg_estate_action_shortcodes_list', 'yreg_estate_sc_br_reg_shortcodes');
	function yreg_estate_sc_br_reg_shortcodes() {
	
		yreg_estate_sc_map("trx_br", array(
			"title" => esc_html__("Break", 'yreg-estate'),
			"desc" => wp_kses_data( __("Line break with clear floating (if need)", 'yreg-estate') ),
			"decorate" => false,
			"container" => false,
			"params" => array(
				"clear" => 	array(
					"title" => esc_html__("Clear floating", 'yreg-estate'),
					"desc" => wp_kses_data( __("Clear floating (if need)", 'yreg-estate') ),
					"value" => "",
					"type" => "checklist",
					"options" => array(
						'none' => esc_html__('None', 'yreg-estate'),
						'left' => esc_html__('Left', 'yreg-estate'),
						'right' => esc_html__('Right', 'yreg-estate'),
						'both' => esc_html__('Both', 'yreg-estate')
					)
				)
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_br_reg_shortcodes_vc' ) ) {
	//add_action('yreg_estate_action_shortcodes_list_vc', 'yreg_estate_sc_br_reg_shortcodes_vc');
	function yreg_estate_sc_br_reg_shortcodes_vc() {
/*
		vc_map( array(
			"base" => "trx_br",
			"name" => esc_html__("Line break", 'yreg-estate'),
			"description" => wp_kses_data( __("Line break or Clear Floating", 'yreg-estate') ),
			"category" => esc_html__('Content', 'yreg-estate'),
			'icon' => 'icon_trx_br',
			"class" => "trx_sc_single trx_sc_br",
			"content_element" => true,
			"is_container" => false,
			"show_settings_on_create" => true,
			"params" => array(
				array(
					"param_name" => "clear",
					"heading" => esc_html__("Clear floating", 'yreg-estate'),
					"description" => wp_kses_data( __("Select clear side (if need)", 'yreg-estate') ),
					"class" => "",
					"value" => "",
					"value" => array(
						esc_html__('None', 'yreg-estate') => 'none',
						esc_html__('Left', 'yreg-estate') => 'left',
						esc_html__('Right', 'yreg-estate') => 'right',
						esc_html__('Both', 'yreg-estate') => 'both'
					),
					"type" => "dropdown"
				)
			)
		) );
		
		class WPBakeryShortCode_Trx_Br extends YREG_ESTATE_VC_ShortCodeSingle {}
*/
	}
}
?>