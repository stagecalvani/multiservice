<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('yreg_estate_sc_chat_theme_setup')) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_sc_chat_theme_setup' );
	function yreg_estate_sc_chat_theme_setup() {
		add_action('yreg_estate_action_shortcodes_list', 		'yreg_estate_sc_chat_reg_shortcodes');
		if (function_exists('yreg_estate_exists_visual_composer') && yreg_estate_exists_visual_composer())
			add_action('yreg_estate_action_shortcodes_list_vc','yreg_estate_sc_chat_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_chat id="unique_id" link="url" title=""]Et adipiscing integer, scelerisque pid, augue mus vel tincidunt porta[/trx_chat]
[trx_chat id="unique_id" link="url" title=""]Et adipiscing integer, scelerisque pid, augue mus vel tincidunt porta[/trx_chat]
...
*/

if (!function_exists('yreg_estate_sc_chat')) {	
	function yreg_estate_sc_chat($atts, $content=null){	
		if (yreg_estate_in_shortcode_blogger()) return '';
		extract(yreg_estate_html_decode(shortcode_atts(array(
			// Individual params
			"photo" => "",
			"title" => "",
			"link" => "",
			// Common params
			"id" => "",
			"class" => "",
			"css" => "",
			"animation" => "",
			"width" => "",
			"height" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$class .= ($class ? ' ' : '') . yreg_estate_get_css_position_as_classes($top, $right, $bottom, $left);
		$css .= yreg_estate_get_css_dimensions_from_values($width, $height);
		$title = $title=='' ? $link : $title;
		if (!empty($photo)) {
			if ($photo > 0) {
				$attach = wp_get_attachment_image_src( $photo, 'full' );
				if (isset($attach[0]) && $attach[0]!='')
					$photo = $attach[0];
			}
			$photo = yreg_estate_get_resized_image_tag($photo, 75, 75);
		}
		$content = do_shortcode($content);
		if (yreg_estate_substr($content, 0, 2)!='<p') $content = '<p>' . ($content) . '</p>';
		$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
				. ' class="sc_chat' . (!empty($class) ? ' '.esc_attr($class) : '') . '"' 
				. (!yreg_estate_param_is_off($animation) ? ' data-animation="'.esc_attr(yreg_estate_get_animation_classes($animation)).'"' : '')
				. ($css ? ' style="'.esc_attr($css).'"' : '') 
				. '>'
					. '<div class="sc_chat_inner">'
						. ($photo ? '<div class="sc_chat_avatar">'.($photo).'</div>' : '')
						. ($title == '' ? '' : ('<div class="sc_chat_title">' . ($link!='' ? '<a href="'.esc_url($link).'">' : '') . ($title) . ($link!='' ? '</a>' : '') . '</div>'))
						. '<div class="sc_chat_content">'.($content).'</div>'
					. '</div>'
				. '</div>';
		return apply_filters('yreg_estate_shortcode_output', $output, 'trx_chat', $atts, $content);
	}
	yreg_estate_require_shortcode('trx_chat', 'yreg_estate_sc_chat');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_chat_reg_shortcodes' ) ) {
	//add_action('yreg_estate_action_shortcodes_list', 'yreg_estate_sc_chat_reg_shortcodes');
	function yreg_estate_sc_chat_reg_shortcodes() {
	
		yreg_estate_sc_map("trx_chat", array(
			"title" => esc_html__("Chat", 'yreg-estate'),
			"desc" => wp_kses_data( __("Chat message", 'yreg-estate') ),
			"decorate" => true,
			"container" => true,
			"params" => array(
				"title" => array(
					"title" => esc_html__("Item title", 'yreg-estate'),
					"desc" => wp_kses_data( __("Chat item title", 'yreg-estate') ),
					"value" => "",
					"type" => "text"
				),
				"photo" => array(
					"title" => esc_html__("Item photo", 'yreg-estate'),
					"desc" => wp_kses_data( __("Select or upload image or write URL from other site for the item photo (avatar)", 'yreg-estate') ),
					"readonly" => false,
					"value" => "",
					"type" => "media"
				),
				"link" => array(
					"title" => esc_html__("Item link", 'yreg-estate'),
					"desc" => wp_kses_data( __("Chat item link", 'yreg-estate') ),
					"value" => "",
					"type" => "text"
				),
				"_content_" => array(
					"title" => esc_html__("Chat item content", 'yreg-estate'),
					"desc" => wp_kses_data( __("Current chat item content", 'yreg-estate') ),
					"rows" => 4,
					"value" => "",
					"type" => "textarea"
				),
				"width" => yreg_estate_shortcodes_width(),
				"height" => yreg_estate_shortcodes_height(),
				"top" => yreg_estate_get_sc_param('top'),
				"bottom" => yreg_estate_get_sc_param('bottom'),
				"left" => yreg_estate_get_sc_param('left'),
				"right" => yreg_estate_get_sc_param('right'),
				"id" => yreg_estate_get_sc_param('id'),
				"class" => yreg_estate_get_sc_param('class'),
				"animation" => yreg_estate_get_sc_param('animation'),
				"css" => yreg_estate_get_sc_param('css')
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_chat_reg_shortcodes_vc' ) ) {
	//add_action('yreg_estate_action_shortcodes_list_vc', 'yreg_estate_sc_chat_reg_shortcodes_vc');
	function yreg_estate_sc_chat_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_chat",
			"name" => esc_html__("Chat", 'yreg-estate'),
			"description" => wp_kses_data( __("Chat message", 'yreg-estate') ),
			"category" => esc_html__('Content', 'yreg-estate'),
			'icon' => 'icon_trx_chat',
			"class" => "trx_sc_container trx_sc_chat",
			"content_element" => true,
			"is_container" => true,
			"show_settings_on_create" => true,
			"params" => array(
				array(
					"param_name" => "title",
					"heading" => esc_html__("Item title", 'yreg-estate'),
					"description" => wp_kses_data( __("Title for current chat item", 'yreg-estate') ),
					"admin_label" => true,
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "photo",
					"heading" => esc_html__("Item photo", 'yreg-estate'),
					"description" => wp_kses_data( __("Select or upload image or write URL from other site for the item photo (avatar)", 'yreg-estate') ),
					"class" => "",
					"value" => "",
					"type" => "attach_image"
				),
				array(
					"param_name" => "link",
					"heading" => esc_html__("Link URL", 'yreg-estate'),
					"description" => wp_kses_data( __("URL for the link on chat title click", 'yreg-estate') ),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				/*
				array(
					"param_name" => "content",
					"heading" => esc_html__("Chat item content", 'yreg-estate'),
					"description" => wp_kses_data( __("Current chat item content", 'yreg-estate') ),
					"class" => "",
					"value" => "",
					"type" => "textarea_html"
				),
				*/
				yreg_estate_get_vc_param('id'),
				yreg_estate_get_vc_param('class'),
				yreg_estate_get_vc_param('animation'),
				yreg_estate_get_vc_param('css'),
				yreg_estate_vc_width(),
				yreg_estate_vc_height(),
				yreg_estate_get_vc_param('margin_top'),
				yreg_estate_get_vc_param('margin_bottom'),
				yreg_estate_get_vc_param('margin_left'),
				yreg_estate_get_vc_param('margin_right')
			),
			'js_view' => 'VcTrxTextContainerView'
		
		) );
		
		class WPBakeryShortCode_Trx_Chat extends YREG_ESTATE_VC_ShortCodeContainer {}
	}
}
?>