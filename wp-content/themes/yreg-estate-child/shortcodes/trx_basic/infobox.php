<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('yreg_estate_sc_infobox_theme_setup')) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_sc_infobox_theme_setup' );
	function yreg_estate_sc_infobox_theme_setup() {
		add_action('yreg_estate_action_shortcodes_list', 		'yreg_estate_sc_infobox_reg_shortcodes');
		if (function_exists('yreg_estate_exists_visual_composer') && yreg_estate_exists_visual_composer())
			add_action('yreg_estate_action_shortcodes_list_vc','yreg_estate_sc_infobox_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_infobox id="unique_id" style="regular|info|success|error|result" static="0|1"]Et adipiscing integer, scelerisque pid, augue mus vel tincidunt porta[/trx_infobox]
*/

if (!function_exists('yreg_estate_sc_infobox')) {	
	function yreg_estate_sc_infobox($atts, $content=null){	
		if (yreg_estate_in_shortcode_blogger()) return '';
		extract(yreg_estate_html_decode(shortcode_atts(array(
			// Individual params
			"style" => "regular",
			"closeable" => "no",
			"icon" => "",
			"color" => "",
			"bg_color" => "",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$class .= ($class ? ' ' : '') . yreg_estate_get_css_position_as_classes($top, $right, $bottom, $left);
		$css .= ($color !== '' ? 'color:' . esc_attr($color) .';' : '')
			. ($bg_color !== '' ? 'background-color:' . esc_attr($bg_color) .';' : '');
		if (empty($icon)) {
			if ($icon=='none')
				$icon = '';
			else if ($style=='regular')
				$icon = 'icon-cog';
			else if ($style=='success')
				$icon = 'icon-check';
			else if ($style=='error')
				$icon = 'icon-cancel';
			else if ($style=='info')
				$icon = 'icon-info-circled';
			else if ($style=='warning')
				$icon = 'icon-attention';
		}
		$content = do_shortcode($content);
		$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
				. ' class="sc_infobox sc_infobox_style_' . esc_attr($style) 
					. (yreg_estate_param_is_on($closeable) ? ' sc_infobox_closeable' : '') 
					. (!empty($class) ? ' '.esc_attr($class) : '') 
					. ($icon!='' && !yreg_estate_param_is_inherit($icon) ? ' sc_infobox_iconed '. esc_attr($icon) : '') 
					. '"'
				. (!yreg_estate_param_is_off($animation) ? ' data-animation="'.esc_attr(yreg_estate_get_animation_classes($animation)).'"' : '')
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
				. '>'
				. trim($content)
				. '</div>';
		return apply_filters('yreg_estate_shortcode_output', $output, 'trx_infobox', $atts, $content);
	}
	yreg_estate_require_shortcode('trx_infobox', 'yreg_estate_sc_infobox');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_infobox_reg_shortcodes' ) ) {
	//add_action('yreg_estate_action_shortcodes_list', 'yreg_estate_sc_infobox_reg_shortcodes');
	function yreg_estate_sc_infobox_reg_shortcodes() {
	
		yreg_estate_sc_map("trx_infobox", array(
			"title" => esc_html__("Infobox", 'yreg-estate'),
			"desc" => wp_kses_data( __("Insert infobox into your post (page)", 'yreg-estate') ),
			"decorate" => false,
			"container" => true,
			"params" => array(
				"style" => array(
					"title" => esc_html__("Style", 'yreg-estate'),
					"desc" => wp_kses_data( __("Infobox style", 'yreg-estate') ),
					"value" => "regular",
					"type" => "checklist",
					"dir" => "horizontal",
					"options" => array(
						'regular' => esc_html__('Regular', 'yreg-estate'),
						'info' => esc_html__('Info', 'yreg-estate'),
						'warning' => esc_html__('Warning', 'yreg-estate'),
						'success' => esc_html__('Success', 'yreg-estate'),
						'error' => esc_html__('Error', 'yreg-estate')
					)
				),
				"closeable" => array(
					"title" => esc_html__("Closeable box", 'yreg-estate'),
					"desc" => wp_kses_data( __("Create closeable box (with close button)", 'yreg-estate') ),
					"value" => "no",
					"type" => "switch",
					"options" => yreg_estate_get_sc_param('yes_no')
				),
				"icon" => array(
					"title" => esc_html__("Custom icon",  'yreg-estate'),
					"desc" => wp_kses_data( __('Select icon for the infobox from Fontello icons set. If empty - use default icon',  'yreg-estate') ),
					"value" => "",
					"type" => "icons",
					"options" => yreg_estate_get_sc_param('icons')
				),
				"color" => array(
					"title" => esc_html__("Text color", 'yreg-estate'),
					"desc" => wp_kses_data( __("Any color for text and headers", 'yreg-estate') ),
					"value" => "",
					"type" => "color"
				),
				"bg_color" => array(
					"title" => esc_html__("Background color", 'yreg-estate'),
					"desc" => wp_kses_data( __("Any background color for this infobox", 'yreg-estate') ),
					"value" => "",
					"type" => "color"
				),
				"_content_" => array(
					"title" => esc_html__("Infobox content", 'yreg-estate'),
					"desc" => wp_kses_data( __("Content for infobox", 'yreg-estate') ),
					"divider" => true,
					"rows" => 4,
					"value" => "",
					"type" => "textarea"
				),
				"top" => yreg_estate_get_sc_param('top'),
				"bottom" => yreg_estate_get_sc_param('bottom'),
				"left" => yreg_estate_get_sc_param('left'),
				"right" => yreg_estate_get_sc_param('right'),
				"id" => yreg_estate_get_sc_param('id'),
				"class" => yreg_estate_get_sc_param('class'),
				"animation" => yreg_estate_get_sc_param('animation'),
				"css" => yreg_estate_get_sc_param('css')
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_infobox_reg_shortcodes_vc' ) ) {
	//add_action('yreg_estate_action_shortcodes_list_vc', 'yreg_estate_sc_infobox_reg_shortcodes_vc');
	function yreg_estate_sc_infobox_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_infobox",
			"name" => esc_html__("Infobox", 'yreg-estate'),
			"description" => wp_kses_data( __("Box with info or error message", 'yreg-estate') ),
			"category" => esc_html__('Content', 'yreg-estate'),
			'icon' => 'icon_trx_infobox',
			"class" => "trx_sc_container trx_sc_infobox",
			"content_element" => true,
			"is_container" => true,
			"show_settings_on_create" => true,
			"params" => array(
				array(
					"param_name" => "style",
					"heading" => esc_html__("Style", 'yreg-estate'),
					"description" => wp_kses_data( __("Infobox style", 'yreg-estate') ),
					"admin_label" => true,
					"class" => "",
					"value" => array(
							esc_html__('Regular', 'yreg-estate') => 'regular',
							esc_html__('Info', 'yreg-estate') => 'info',
							esc_html__('Warning', 'yreg-estate') => 'warning',
							esc_html__('Success', 'yreg-estate') => 'success',
							esc_html__('Error', 'yreg-estate') => 'error',
							esc_html__('Result', 'yreg-estate') => 'result'
						),
					"type" => "dropdown"
				),
				array(
					"param_name" => "closeable",
					"heading" => esc_html__("Closeable", 'yreg-estate'),
					"description" => wp_kses_data( __("Create closeable box (with close button)", 'yreg-estate') ),
					"class" => "",
					"value" => array(esc_html__('Close button', 'yreg-estate') => 'yes'),
					"type" => "checkbox"
				),
				array(
					"param_name" => "icon",
					"heading" => esc_html__("Custom icon", 'yreg-estate'),
					"description" => wp_kses_data( __("Select icon for the infobox from Fontello icons set. If empty - use default icon", 'yreg-estate') ),
					"class" => "",
					"value" => yreg_estate_get_sc_param('icons'),
					"type" => "dropdown"
				),
				array(
					"param_name" => "color",
					"heading" => esc_html__("Text color", 'yreg-estate'),
					"description" => wp_kses_data( __("Any color for the text and headers", 'yreg-estate') ),
					"class" => "",
					"value" => "",
					"type" => "colorpicker"
				),
				array(
					"param_name" => "bg_color",
					"heading" => esc_html__("Background color", 'yreg-estate'),
					"description" => wp_kses_data( __("Any background color for this infobox", 'yreg-estate') ),
					"class" => "",
					"value" => "",
					"type" => "colorpicker"
				),
				/*
				array(
					"param_name" => "content",
					"heading" => esc_html__("Message text", 'yreg-estate'),
					"description" => wp_kses_data( __("Message for the infobox", 'yreg-estate') ),
					"class" => "",
					"value" => "",
					"type" => "textarea_html"
				),
				*/
				yreg_estate_get_vc_param('id'),
				yreg_estate_get_vc_param('class'),
				yreg_estate_get_vc_param('animation'),
				yreg_estate_get_vc_param('css'),
				yreg_estate_get_vc_param('margin_top'),
				yreg_estate_get_vc_param('margin_bottom'),
				yreg_estate_get_vc_param('margin_left'),
				yreg_estate_get_vc_param('margin_right')
			),
			'js_view' => 'VcTrxTextContainerView'
		) );
		
		class WPBakeryShortCode_Trx_Infobox extends YREG_ESTATE_VC_ShortCodeContainer {}
	}
}
?>