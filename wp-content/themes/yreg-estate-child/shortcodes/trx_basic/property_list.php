<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('yreg_estate_sc_property_list_theme_setup')) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_sc_property_list_theme_setup' );
	function yreg_estate_sc_property_list_theme_setup() {
		add_action('yreg_estate_action_shortcodes_list', 		'yreg_estate_sc_property_list_reg_shortcodes');
		if (function_exists('yreg_estate_exists_visual_composer') && yreg_estate_exists_visual_composer())
			add_action('yreg_estate_action_shortcodes_list_vc','yreg_estate_sc_property_list_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

if (!function_exists('yreg_estate_sc_property_list')) {	
	function yreg_estate_sc_property_list($atts, $content=null){	
		if (yreg_estate_in_shortcode_blogger()) return '';
		extract(yreg_estate_html_decode(shortcode_atts(array(
			// Individual params
			"list" => "prstyle",
			"columns" => "2",
			// Common params
			"id" => "",
			"class" => "",
			"css" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$class .= ($class ? ' ' : '') . yreg_estate_get_css_position_as_classes($top, $right, $bottom, $left);

		$post_id = get_the_ID();
		$post = get_post( $post_id );
		$post_type = $post->post_type;
		$output = '';
		
		if ( $post_type == 'property' ) {
			
			if ( $list == 'prstyle' )
				$pr_list = get_post_meta( $post_id, 'yreg_estate_property_style_list', true );
			
			if ( $list == 'pramenities' )
				$pr_list = get_post_meta( $post_id, 'yreg_estate_property_amenities_list', true );
			
			if ( $list == 'proptions' )
				$pr_list = get_post_meta( $post_id, 'yreg_estate_property_options_list', true );
			
			
			if (!empty($pr_list)) {
				$pr_arr = explode('|', $pr_list);
			}
			$pr_arr =  array_filter($pr_arr);
			
			$col = (int) $columns;
			if ( $col > 3 ) $col = 3;
			
			
			if ( !empty($pr_arr) ) {
				$output .= '<div' . ($id ? ' id="'.esc_attr($id) . '"' : '') 
					. ' class="sc_property_list'. (!empty($class) ? ' '.esc_attr($class) : '') . '"'
					. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
					. '>';
				
				
				if ($col > 1) {
					
					$output .= '<div class="columns_wrap sc_columns">';
					
					for ($i=1; $i<=$col; $i++) {
						$c[$i] = '';
						$c[$i] .= '<div class="column-1_'.$col.' sc_column_item sc_column_item_'.$i.'">';
						$c[$i] .= '<ul class="sc_list sc_list_style_iconed">';
					}
					
					$i = 1;
					foreach ($pr_arr as $value) {
						$c[$i] .= '<li class="sc_list_item">';
						$c[$i] .= '<span class="sc_list_icon icon-stop"></span>';
						$c[$i] .= esc_html($value);
						$c[$i] .= '</li>';
						$i = $i + 1;
						if ( $i > $col ) { $i = 1; }
					}
					
					for ($i=1; $i<=$col; $i++) {
						$c[$i] .= '</ul>';
						$c[$i] .= '</div>';
					}
					
					for ($i=1; $i<=$col; $i++) { $output .= $c[$i]; }
					
				} else {
					$output .= '<ul class="sc_list sc_list_style_iconed">';
					foreach ($pr_arr as $value) {
						$output .= '<li class="sc_list_item">';
						$output .= '<span class="sc_list_icon icon-stop"></span>';
						$output .= esc_html($value);
						$output .= '</li>';
					}
					$output .= '</ul>';
				}
				if ($col > 1) {$output .= '</div>';}
				
				$output .= '</div>';
			}

		}
		
		return apply_filters('yreg_estate_shortcode_output', $output, 'trx_property_list', $atts, $content);
	}
	yreg_estate_require_shortcode('trx_property_list', 'yreg_estate_sc_property_list');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_property_list_reg_shortcodes' ) ) {
	//add_action('yreg_estate_action_shortcodes_list', 'yreg_estate_sc_property_list_reg_shortcodes');
	function yreg_estate_sc_property_list_reg_shortcodes() {
	
		yreg_estate_sc_map("trx_property_list", array(
			"title" => esc_html__("Property list", 'yreg-estate'),
			"desc" => wp_kses_data( __("Insert property list into your post (page)", 'yreg-estate') ),
			"decorate" => false,
			"container" => false,
			"params" => array(
				"list" => array(
					"title" => esc_html__("Property list", 'yreg-estate'),
					"desc" => wp_kses_data( __("Select property list", 'yreg-estate') ),
					"value" => "prstyle",
					"type" => "select",
					"options" => array(
						'prstyle' => esc_html__('Property style', 'yreg-estate'),
						'pramenities' => esc_html__('Property amenities', 'yreg-estate'),
						'proptions' => esc_html__('Property options', 'yreg-estate')
					),
				),
				"columns" => array(
					"title" => esc_html__("Columns", 'yreg-estate'),
					"desc" => wp_kses_data( __("Select columns property list", 'yreg-estate') ),
					"value" => "2",
					"type" => "select",
					"options" => array(
						'1' => esc_html__('1 Columns', 'yreg-estate'),
						'2' => esc_html__('2 Columns', 'yreg-estate'),
						'3' => esc_html__('3 Columns', 'yreg-estate')
					),
				),
				


				"top" => yreg_estate_get_sc_param('top'),
				"bottom" => yreg_estate_get_sc_param('bottom'),
				"left" => yreg_estate_get_sc_param('left'),
				"right" => yreg_estate_get_sc_param('right'),
				"id" => yreg_estate_get_sc_param('id'),
				"class" => yreg_estate_get_sc_param('class'),
				"css" => yreg_estate_get_sc_param('css')
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_property_list_reg_shortcodes_vc' ) ) {
	//add_action('yreg_estate_action_shortcodes_list_vc', 'yreg_estate_sc_property_list_reg_shortcodes_vc');
	function yreg_estate_sc_property_list_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_property_list",
			"name" => esc_html__("Property list", 'yreg-estate'),
			"description" => wp_kses_data( __("Insert property_list (delimiter)", 'yreg-estate') ),
			"category" => esc_html__('Content', 'yreg-estate'),
			"class" => "trx_sc_single trx_sc_property_list",
			'icon' => 'icon_trx_property_list',
			"content_element" => true,
			"is_container" => false,
			"show_settings_on_create" => true,
			"params" => array(
				array(
					"param_name" => "list",
					"heading" => esc_html__("Property list", 'yreg-estate'),
					"description" => wp_kses_data( __("Select property list", 'yreg-estate') ),
					"admin_label" => true,
					"class" => "",
//					"group" => esc_html__('Query', 'yreg-estate'),
					"std" => 'prstyle',
					"value" => array(
							esc_html__('Property style', 'yreg-estate') => 'prstyle',
							esc_html__('Property amenities', 'yreg-estate') => 'pramenities',
							esc_html__('Property options', 'yreg-estate') => 'proptions'
						),
					"type" => "dropdown"
				),
				array(
					"param_name" => "columns",
					"heading" => esc_html__("Columns", 'yreg-estate'),
					"description" => wp_kses_data( __("Select property list", 'yreg-estate') ),
					"admin_label" => true,
					"class" => "",
//					"group" => esc_html__('Query', 'yreg-estate'),
					"std" => '2',
					"value" => array(
							esc_html__('1 Columns', 'yreg-estate') => '1',
							esc_html__('2 Columns', 'yreg-estate') => '2',
							esc_html__('3 Columns', 'yreg-estate') => '3'
						),
					"type" => "dropdown"
				),
				


				yreg_estate_get_vc_param('id'),
				yreg_estate_get_vc_param('class'),
				yreg_estate_get_vc_param('css'),
				yreg_estate_get_vc_param('margin_top'),
				yreg_estate_get_vc_param('margin_bottom'),
				yreg_estate_get_vc_param('margin_left'),
				yreg_estate_get_vc_param('margin_right')
			)
		) );
		
		class WPBakeryShortCode_Trx_Property_List extends YREG_ESTATE_VC_ShortCodeSingle {}
	}
}
?>