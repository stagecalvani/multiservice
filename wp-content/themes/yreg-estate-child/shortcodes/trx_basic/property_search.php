<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('yreg_estate_sc_property_search_theme_setup')) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_sc_property_search_theme_setup' );
	function yreg_estate_sc_property_search_theme_setup() {
		add_action('yreg_estate_action_shortcodes_list', 		'yreg_estate_sc_property_search_reg_shortcodes');
		if (function_exists('yreg_estate_exists_visual_composer') && yreg_estate_exists_visual_composer())
			add_action('yreg_estate_action_shortcodes_list_vc','yreg_estate_sc_property_search_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_property_search id="unique_id" top="margin_in_pixels" bottom="margin_in_pixels"]
*/

if (!function_exists('yreg_estate_sc_property_search')) {	
	function yreg_estate_sc_property_search($atts, $content=null){	
		if (yreg_estate_in_shortcode_blogger()) return '';
		extract(yreg_estate_html_decode(shortcode_atts(array(
			// Individual params
//			"style" => "",
//			"color" => "",
//			"title" => "",
//			"position" => "",
//			"image" => "",
//			"repeat" => "no",
			// Common params
			"id" => "",
			"class" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		if (empty($position)) $position = 'center center';
		$class .= ($class ? ' ' : '') . yreg_estate_get_css_position_as_classes($top, $right, $bottom, $left);
		
		$url = '';
		$page = 'property';
		$url = yreg_estate_property_get_stream_page_link($url, $page);
		
		$output = '<div' . ($id ? ' id="'.esc_attr($id) . '"' : '') 
				. ' class="sc_property_search scps ' . (!empty($class) ? ' '.esc_attr($class) : '') . '"'
				. '>';
		
		$output .= '<form method="get" action="' . esc_url($url) . '">';
		
		/* ***** show_status ***** */
		$ps_status = '';
		if ( isset($_GET['ps_status']) ) {
			$ps_status = htmlspecialchars(trim($_GET['ps_status']));
		}
		$output .= '<div class="sc_ps_status"><select class="mb" name="ps_status">';
		if ( $ps_status=='-1' ) { $selected="selected"; } else { $selected=""; }
		$output .= '<option '. esc_html($selected).' value="-1">'.esc_html__('Tutti i tipi di contratto', 'yreg-estate').'</option>';
		if ( $ps_status=='sale' ) { $selected="selected"; } else { $selected=""; }
		$output .= '<option '. esc_html($selected).' value="sale">'.esc_html__('Vendita residenziale', 'yreg-estate').'</option>';
		if ( $ps_status=='rent' ) { $selected="selected"; } else { $selected=""; }
		$output .= '<option '. esc_html($selected).' value="rent">'.esc_html__('Affitto residenziale', 'yreg-estate').'</option>';
		if ( $ps_status=='activity' ) { $selected="selected"; } else { $selected=""; }
		$output .= '<option '. esc_html($selected).' value="activity">'.esc_html__('Attività / licenza', 'yreg-estate').'</option>';
                if ( $ps_status=='commerce' ) { $selected="selected"; } else { $selected=""; }
		$output .= '<option '. esc_html($selected).' value="commerce">'.esc_html__('Commerciale', 'yreg-estate').'</option>';
                $output .= '</select></div>';
		
		
		
		/* ***** show_location ***** */
		$ps_location = '';
		if ( isset($_GET['ps_location']) ) {
			$ps_location = htmlspecialchars(trim($_GET['ps_location']));
		}
		$list_location = yreg_estate_get_property_list('property_location_list');
		$output .= '<div class="sc_ps_location"><select class="mb" name="ps_location">';
		if ( $ps_location=='-1' ) { $selected="selected"; } else { $selected=""; }
		$output .= '<option '. esc_html($selected).' value="-1">'.esc_html__('Tutte le località', 'yreg-estate').'</option>';
		foreach ($list_location as $key => $value) {
			if ( $ps_location == $value ) { $selected="selected"; } else { $selected=""; }
			$output .= '<option '. esc_html($selected).' value="' . esc_html($key) . '">' . esc_html($value) . '</option>';
		}
		$output .= '</select></div>';
		
		
		
		/* ***** show_type ***** */
		$ps_type = '';
		if ( isset($_GET['ps_type']) ) {
			$ps_type = htmlspecialchars(trim($_GET['ps_type']));
		}
		$list_type = yreg_estate_get_property_list('property_type_list');
		$output .= '<div class="sc_ps_type"><select class="mb" name="ps_type">';
		if ( $ps_type=='-1' ) { $selected="selected"; } else { $selected=""; }
		$output .= '<option '. esc_html($selected).' value="-1">'.esc_html__('Qualisasi tipologia', 'yreg-estate').'</option>';
		foreach ($list_type as $key => $value) {
			if ( $ps_type == $value ) { $selected="selected"; } else { $selected=""; }
			$output .= '<option '. esc_html($selected).' value="' . esc_html($key) . '">' . esc_html($value) . '</option>';
		}
		$output .= '</select></div>';
		
		
		
		/* ***** show_style ***** */
                /*$ps_style = '';
		if ( isset($_GET['ps_style']) ) {
			$ps_style = htmlspecialchars(trim($_GET['ps_style']));
		}
		$list_style = yreg_estate_get_property_list('property_style_list');
		$output .= '<div class="sc_ps_style"><select class="mb" name="ps_style">';
		if ( $ps_style=='-1' ) { $selected="selected"; } else { $selected=""; }
		$output .= '<option '. esc_html($selected).' value="-1">'.esc_html__('Tutti gli stili', 'yreg-estate').'</option>';
		foreach ($list_style as $key => $value) {
			if ( $ps_style == $value ) { $selected="selected"; } else { $selected=""; }
			$output .= '<option '. esc_html($selected).' value="' . esc_html($value) . '">' . esc_html($value) . '</option>';
		}
		$output .= '</select></div>';		*/
		
		
		
		/* ***** show_bedrooms ***** */
		$ps_bedrooms = '';
		if ( isset($_GET['ps_bedrooms']) ) {
			$ps_bedrooms = htmlspecialchars(trim($_GET['ps_bedrooms']));
		}
		$output .= '<div class="sc_ps_bedrooms"><select class="mb" name="ps_bedrooms">';
		if ( $ps_bedrooms=='-1' ) { $selected="selected"; } else { $selected=""; }
		$output .= '<option '. esc_html($selected).' value="-1">'.esc_html__('Numero Camere da letto', 'yreg-estate').'</option>';
		if ( $ps_bedrooms=='1' ) { $selected="selected"; } else { $selected=""; }
		$output .= '<option '. esc_html($selected).' value="1">'.esc_html__('Almeno una camera da letto', 'yreg-estate').'</option>';
		if ( $ps_bedrooms=='2' ) { $selected="selected"; } else { $selected=""; }
		$output .= '<option '. esc_html($selected).' value="2">'.esc_html__('Almeno due camere da letto', 'yreg-estate').'</option>';
		if ( $ps_bedrooms=='3' ) { $selected="selected"; } else { $selected=""; }
		$output .= '<option '. esc_html($selected).' value="3">'.esc_html__('Almeno tre camere da letto', 'yreg-estate').'</option>';
		if ( $ps_bedrooms=='4' ) { $selected="selected"; } else { $selected=""; }
		$output .= '<option '. esc_html($selected).' value="4">'.esc_html__('Almeno quattro camere da letto', 'yreg-estate').'</option>';
		if ( $ps_bedrooms=='5' ) { $selected="selected"; } else { $selected=""; }
		$output .= '<option '. esc_html($selected).' value="5">'.esc_html__('Almeno cinque camere da letto', 'yreg-estate').'</option>';
		$output .= '</select></div>';
		
		
		
		/* ***** show_bathrooms ***** */
		$ps_bathrooms = '';
		if ( isset($_GET['ps_bathrooms']) ) {
			$ps_bathrooms = htmlspecialchars(trim($_GET['ps_bathrooms']));
		}
		$output .= '<div class="sc_ps_bathrooms"><select class="mb" name="ps_bathrooms">';
		if ( $ps_bathrooms=='-1' ) { $selected="selected"; } else { $selected=""; }
		$output .= '<option '. esc_html($selected).' value="-1">'.esc_html__('Numero Bagni', 'yreg-estate').'</option>';
		if ( $ps_bathrooms=='1' ) { $selected="selected"; } else { $selected=""; }
		$output .= '<option '. esc_html($selected).' value="1">'.esc_html__('Almeno un bagno', 'yreg-estate').'</option>';
		if ( $ps_bathrooms=='2' ) { $selected="selected"; } else { $selected=""; }
		$output .= '<option '. esc_html($selected).' value="2">'.esc_html__('Almeno due bagni', 'yreg-estate').'</option>';
		if ( $ps_bathrooms=='3' ) { $selected="selected"; } else { $selected=""; }
		$output .= '<option '. esc_html($selected).' value="3">'.esc_html__('Almeno tre bagni', 'yreg-estate').'</option>';
		if ( $ps_bathrooms=='4' ) { $selected="selected"; } else { $selected=""; }
		$output .= '<option '. esc_html($selected).' value="4">'.esc_html__('Almeno quattro bagni', 'yreg-estate').'</option>';
		if ( $ps_bathrooms=='5' ) { $selected="selected"; } else { $selected=""; }
		$output .= '<option '. esc_html($selected).' value="5">'.esc_html__('Almeno cinque bagni', 'yreg-estate').'</option>';
		$output .= '</select></div>';
		
		
		
		/* ***** show_area ***** */
		$ps_area_big = (int) yreg_estate_get_custom_option('property_search_area_max');
		$ps_area_min = 0; $ps_area_max = $ps_area_big;
		if ( isset($_GET['ps_area_min']) ) {
			$ps_area_min = (int) htmlspecialchars(trim($_GET['ps_area_min']));
		}
		if ( isset($_GET['ps_area_max']) ) {
			$ps_area_max = (int) htmlspecialchars(trim($_GET['ps_area_max']));
		}
		$output .= '<div class="sc_ps_area"><div class="ps_area ps_range_slider">';
		$output .= '<div class="ps_area_info">';
		$output .= '<div class="ps_area_info_title">Superficie</div>';
		$output .= '<div class="ps_area_info_value"></div>';
		$output .= '<div class="cL"></div>';
		$output .= '</div>';
		$output .= '<div id="slider-range-area"></div>';
		$output .= '<input type="hidden" class="mb ps_area_min" name="ps_area_min" value="' . esc_html($ps_area_min) . '" >';
		$output .= '<input type="hidden" class="mb ps_area_max" name="ps_area_max" value="' . esc_html($ps_area_max) . '" >';
		$output .= '<input type="hidden" class="mb ps_area_big" name="ps_area_big" value="' . esc_html($ps_area_big) . '" >';
		$output .= '</div></div>';
		
		
		
		/* ***** show_price ***** */
		$ps_price_big = (int) yreg_estate_get_custom_option('property_search_price_max');
		$ps_price_min = 0; $ps_price_max = $ps_price_big;
		if ( isset($_GET['ps_price_min']) ) {
			$ps_price_min = (int) htmlspecialchars(trim($_GET['ps_price_min']));
		}
		if ( isset($_GET['ps_price_max']) ) {
			$ps_price_max = (int) htmlspecialchars(trim($_GET['ps_price_max']));
		}

		$output .= '<div class="sc_ps_price"><div class="ps_price ps_range_slider">';
		$output .= '<div class="ps_price_info">';
		$output .= '<div class="ps_price_info_title">Prezzo</div>';
		$output .= '<div class="ps_price_info_value"></div>';
		$output .= '<div class="cL"></div>';
		$output .= '</div>';
		$output .= '<div id="slider-range-price"></div>';
		$output .= '<input type="hidden" class="mb ps_price_min" name="ps_price_min" value="' . esc_html($ps_price_min) . '" >';
		$output .= '<input type="hidden" class="mb ps_price_max" name="ps_price_max" value="' . esc_html($ps_price_max) . '" >';
		$output .= '<input type="hidden" class="mb ps_price_big" name="ps_price_big" value="' . esc_html($ps_price_big) . '" >';
		$output .= '</div></div>';
		$output .= '<div class="sc_ps_submit">';
		$output .= '<input type="submit" class="sc_button sc_button_box sc_button_style_style2 sc_button_size_small ps" value="Cerca">';
		$output .= '</div>';	
		$output .= '</form>';
		$output .= '</div>';
		return apply_filters('yreg_estate_shortcode_output', $output, 'trx_property_search', $atts, $content);
	}
	yreg_estate_require_shortcode('trx_property_search', 'yreg_estate_sc_property_search');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_property_search_reg_shortcodes' ) ) {
	//add_action('yreg_estate_action_shortcodes_list', 'yreg_estate_sc_property_search_reg_shortcodes');
	function yreg_estate_sc_property_search_reg_shortcodes() {
	
		yreg_estate_sc_map("trx_property_search", array(
			"title" => esc_html__("Property search", 'yreg-estate'),
			"desc" => wp_kses_data( __("", 'yreg-estate') ),
			"decorate" => false,
			"container" => false,
			"params" => array(
//				"style" => array(
//					"title" => esc_html__("Style", 'yreg-estate'),
//					"desc" => wp_kses_data( __("Line style", 'yreg-estate') ),
//					"value" => "solid",
//					"dir" => "horizontal",
//					"options" => yreg_estate_get_list_line_styles(),
//					"type" => "checklist"
//				),
//				"image" => array(
//					"title" => esc_html__("Image as separator", 'yreg-estate'),
//					"desc" => wp_kses_data( __("Select or upload image or write URL from other site to use it as separator", 'yreg-estate') ),
//					"readonly" => false,
//					"dependency" => array(
//						'style' => array('image')
//					),
//					"value" => "",
//					"type" => "media"
//				),
//				"repeat" => array(
//					"title" => esc_html__("Repeat image", 'yreg-estate'),
//					"desc" => wp_kses_data( __("To repeat an image or to show single picture", 'yreg-estate') ),
//					"dependency" => array(
//						'style' => array('image')
//					),
//					"value" => "no",
//					"type" => "switch",
//					"options" => yreg_estate_get_sc_param('yes_no')
//				),
//				"color" => array(
//					"title" => esc_html__("Color", 'yreg-estate'),
//					"desc" => wp_kses_data( __("Line color", 'yreg-estate') ),
//					"dependency" => array(
//						'style' => array('solid', 'dashed', 'dotted', 'double')
//					),
//					"value" => "",
//					"type" => "color"
//				),
//				"title" => array(
//					"title" => esc_html__("Title", 'yreg-estate'),
//					"desc" => wp_kses_data( __("Title that is going to be placed in the center of the line (if not empty)", 'yreg-estate') ),
//					"value" => "",
//					"type" => "text"
//				),
//				"position" => array(
//					"title" => esc_html__("Title position", 'yreg-estate'),
//					"desc" => wp_kses_data( __("Title position", 'yreg-estate') ),
//					"dependency" => array(
//						'title' => array('not_empty')
//					),
//					"value" => "center center",
//					"options" => yreg_estate_get_list_bg_image_positions(),
//					"type" => "select"
//				),
//				"width" => yreg_estate_shortcodes_width(),
//				"height" => yreg_estate_shortcodes_height(),
				"top" => yreg_estate_get_sc_param('top'),
				"bottom" => yreg_estate_get_sc_param('bottom'),
				"left" => yreg_estate_get_sc_param('left'),
				"right" => yreg_estate_get_sc_param('right'),
				"id" => yreg_estate_get_sc_param('id'),
				"class" => yreg_estate_get_sc_param('class'),
//				"animation" => yreg_estate_get_sc_param('animation'),
//				"css" => yreg_estate_get_sc_param('css')
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_property_search_reg_shortcodes_vc' ) ) {
	//add_action('yreg_estate_action_shortcodes_list_vc', 'yreg_estate_sc_property_search_reg_shortcodes_vc');
	function yreg_estate_sc_property_search_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_property_search",
			"name" => esc_html__("Property search", 'yreg-estate'),
			"description" => wp_kses_data( __("", 'yreg-estate') ),
			"category" => esc_html__('Content', 'yreg-estate'),
			"class" => "trx_sc_single trx_sc_property_search",
			'icon' => 'icon_trx_property_search',
			"content_element" => true,
			"is_container" => false,
			"show_settings_on_create" => true,
			"params" => array(
//				array(
//					"param_name" => "style",
//					"heading" => esc_html__("Style", 'yreg-estate'),
//					"description" => wp_kses_data( __("Line style", 'yreg-estate') ),
//					"admin_label" => true,
//					"class" => "",
//					"std" => "solid",
//					"value" => array_flip(yreg_estate_get_list_line_styles()),
//					"type" => "dropdown"
//				),
//				array(
//					"param_name" => "image",
//					"heading" => esc_html__("Image as separator", 'yreg-estate'),
//					"description" => wp_kses_data( __("Select or upload image or write URL from other site to use it as separator", 'yreg-estate') ),
//					'dependency' => array(
//						'element' => 'style',
//						'value' => array('image')
//					),
//					"class" => "",
//					"value" => "",
//					"type" => "attach_image"
//				),
//				array(
//					"param_name" => "repeat",
//					"heading" => esc_html__("Repeat image", 'yreg-estate'),
//					"description" => wp_kses_data( __("To repeat an image or to show single picture", 'yreg-estate') ),
//					'dependency' => array(
//						'element' => 'style',
//						'value' => array('image')
//					),
//					"class" => "",
//					"value" => array("Repeat image" => "yes" ),
//					"type" => "checkbox"
//				),
//				array(
//					"param_name" => "color",
//					"heading" => esc_html__("Line color", 'yreg-estate'),
//					"description" => wp_kses_data( __("Line color", 'yreg-estate') ),
//					'dependency' => array(
//						'element' => 'style',
//						'value' => array('solid','dotted','dashed','double')
//					),
//					"class" => "",
//					"value" => "",
//					"type" => "colorpicker"
//				),
//				array(
//					"param_name" => "title",
//					"heading" => esc_html__("Title", 'yreg-estate'),
//					"description" => wp_kses_data( __("Title that is going to be placed in the center of the line (if not empty)", 'yreg-estate') ),
//					"admin_label" => true,
//					"class" => "",
//					"value" => "",
//					"type" => "textfield"
//				),
//				array(
//					"param_name" => "position",
//					"heading" => esc_html__("Title position", 'yreg-estate'),
//					"description" => wp_kses_data( __("Title position", 'yreg-estate') ),
//					"admin_label" => true,
//					"class" => "",
//					"std" => "center center",
//					"value" => array_flip(yreg_estate_get_list_bg_image_positions()),
//					"type" => "dropdown"
//				),
				yreg_estate_get_vc_param('id'),
				yreg_estate_get_vc_param('class'),
//				yreg_estate_get_vc_param('animation'),
//				yreg_estate_get_vc_param('css'),
//				yreg_estate_vc_width(),
//				yreg_estate_vc_height(),
				yreg_estate_get_vc_param('margin_top'),
				yreg_estate_get_vc_param('margin_bottom'),
				yreg_estate_get_vc_param('margin_left'),
				yreg_estate_get_vc_param('margin_right')
			)
		) );
		
		class WPBakeryShortCode_Trx_Property_Search extends YREG_ESTATE_VC_ShortCodeSingle {}
	}
}
?>