<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('yreg_estate_sc_section_theme_setup')) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_sc_section_theme_setup' );
	function yreg_estate_sc_section_theme_setup() {
		add_action('yreg_estate_action_shortcodes_list', 		'yreg_estate_sc_section_reg_shortcodes');
		if (function_exists('yreg_estate_exists_visual_composer') && yreg_estate_exists_visual_composer())
			add_action('yreg_estate_action_shortcodes_list_vc','yreg_estate_sc_section_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_section id="unique_id" class="class_name" style="css-styles" dedicated="yes|no"]Et adipiscing integer, scelerisque pid, augue mus vel tincidunt porta[/trx_section]
*/

yreg_estate_storage_set('sc_section_dedicated', '');

if (!function_exists('yreg_estate_sc_section')) {	
	function yreg_estate_sc_section($atts, $content=null){	
		if (yreg_estate_in_shortcode_blogger()) return '';
		extract(yreg_estate_html_decode(shortcode_atts(array(
			// Individual params
			"dedicated" => "no",
			"align" => "none",
			"columns" => "none",
			"pan" => "no",
			"scroll" => "no",
			"scroll_dir" => "horizontal",
			"scroll_controls" => "hide",
			"color" => "",
			"scheme" => "",
			"dark_bg" => "no",
			"bg_color" => "",
			"bg_image" => "",
			"bg_overlay" => "",
			"bg_texture" => "",
			"bg_tile" => "no",
			"bg_padding" => "yes",
			"font_size" => "",
			"font_line_height" => "",
			"font_weight" => "",
			"title" => "",
			"subtitle" => "",
			"description" => "",
			"link_caption" => esc_html__('Learn more', 'yreg-estate'),
			"link" => '',
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"width" => "",
			"height" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
	
		if ($bg_image > 0) {
			$attach = wp_get_attachment_image_src( $bg_image, 'full' );
			if (isset($attach[0]) && $attach[0]!='')
				$bg_image = $attach[0];
		}
	
		if ($bg_overlay > 0) {
			if ($bg_color=='') $bg_color = yreg_estate_get_scheme_color('bg');
			$rgb = yreg_estate_hex2rgb($bg_color);
		}
		if ( $dark_bg == 'yes' ) {	$class .= ' scheme_dark '; }
		$class .= ($class ? ' ' : '') . yreg_estate_get_css_position_as_classes($top, $right, $bottom, $left);
		$css .= ($color !== '' ? 'color:' . esc_attr($color) . ';' : '')
			.($bg_color !== '' && $bg_overlay==0 ? 'background-color:' . esc_attr($bg_color) . ';' : '')
			.($bg_image !== '' ? 'background-image:url(' . esc_url($bg_image) . ');'.(yreg_estate_param_is_on($bg_tile) ? 'background-repeat:repeat;' : 'background-repeat:no-repeat;background-size:cover;') : '')
			.(!yreg_estate_param_is_off($pan) ? 'position:relative;' : '')
			.($font_size != '' ? 'font-size:' . esc_attr(yreg_estate_prepare_css_value($font_size)) . '; ' : '')
			.($font_line_height != '' ? 'line-height:' . esc_attr(yreg_estate_prepare_css_value($font_line_height)) . '; ' : '')
			.($font_weight != '' && !yreg_estate_param_is_inherit($font_weight) ? 'font-weight:' . esc_attr($font_weight) . '; ' : '');
		$css_dim = yreg_estate_get_css_dimensions_from_values($width, $height);
		if ($bg_image == '' && $bg_color == '' && $bg_overlay==0 && $bg_texture==0 && yreg_estate_strlen($bg_texture)<2) $css .= $css_dim;
		
		$width  = yreg_estate_prepare_css_value($width);
		$height = yreg_estate_prepare_css_value($height);
	
		if ((!yreg_estate_param_is_off($scroll) || !yreg_estate_param_is_off($pan)) && empty($id)) $id = 'sc_section_'.str_replace('.', '', mt_rand());
	
		if (!yreg_estate_param_is_off($scroll)) yreg_estate_enqueue_slider();
	
		$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
				. ' class="sc_section' 
					. ($class ? ' ' . esc_attr($class) : '') 
					. ($scheme && !yreg_estate_param_is_off($scheme) && !yreg_estate_param_is_inherit($scheme) ? ' scheme_'.esc_attr($scheme) : '') 
					. ($align && $align!='none' ? ' align'.esc_attr($align) : '') 
					. (!empty($columns) && $columns!='none' ? ' column-'.esc_attr($columns) : '') 
					. (yreg_estate_param_is_on($scroll) && !yreg_estate_param_is_off($scroll_controls) ? ' sc_scroll_controls sc_scroll_controls_'.esc_attr($scroll_dir).' sc_scroll_controls_type_'.esc_attr($scroll_controls) : '')
					. '"'
				. (!yreg_estate_param_is_off($animation) ? ' data-animation="'.esc_attr(yreg_estate_get_animation_classes($animation)).'"' : '')
				. ($css!='' || $css_dim!='' ? ' style="'.esc_attr($css.$css_dim).'"' : '')
				.'>' 
				. '<div class="sc_section_inner">'
					. ($bg_image !== '' || $bg_color !== '' || $bg_overlay>0 || $bg_texture>0 || yreg_estate_strlen($bg_texture)>2
						? '<div class="sc_section_overlay'.($bg_texture>0 ? ' texture_bg_'.esc_attr($bg_texture) : '') . '"'
							. ' style="' . ($bg_overlay>0 ? 'background-color:rgba('.(int)$rgb['r'].','.(int)$rgb['g'].','.(int)$rgb['b'].','.min(1, max(0, $bg_overlay)).');' : '')
								. (yreg_estate_strlen($bg_texture)>2 ? 'background-image:url('.esc_url($bg_texture).');' : '')
								. '"'
								. ($bg_overlay > 0 ? ' data-overlay="'.esc_attr($bg_overlay).'" data-bg_color="'.esc_attr($bg_color).'"' : '')
								. '>'
								. '<div class="sc_section_content' . (yreg_estate_param_is_on($bg_padding) ? ' padding_on' : ' padding_off') . '"'
									. ' style="'.esc_attr($css_dim).'"'
									. '>'
						: '')
					. (yreg_estate_param_is_on($scroll) 
						? '<div id="'.esc_attr($id).'_scroll" class="sc_scroll sc_scroll_'.esc_attr($scroll_dir).' swiper-slider-container scroll-container"'
							. ' style="'.($height != '' ? 'height:'.esc_attr($height).';' : '') . ($width != '' ? 'width:'.esc_attr($width).';' : '').'"'
							. '>'
							. '<div class="sc_scroll_wrapper swiper-wrapper">' 
							. '<div class="sc_scroll_slide swiper-slide">' 
						: '')
					. (yreg_estate_param_is_on($pan) 
						? '<div id="'.esc_attr($id).'_pan" class="sc_pan sc_pan_'.esc_attr($scroll_dir).'">' 
						: '')
							. (!empty($subtitle) ? '<h6 class="sc_section_subtitle sc_item_subtitle">' . trim(yreg_estate_strmacros($subtitle)) . '</h6>' : '')
							. (!empty($title) ? '<h2 class="sc_section_title sc_item_title">' . trim(yreg_estate_strmacros($title)) . '</h2>' : '')
							. (!empty($description) ? '<div class="sc_section_descr sc_item_descr">' . trim(yreg_estate_strmacros($description)) . '</div>' : '')
							. do_shortcode($content)
							. (!empty($link) ? '<div class="sc_section_button sc_item_button">'.yreg_estate_do_shortcode('[trx_button link="'.esc_url($link).'" icon="icon-right"]'.esc_html($link_caption).'[/trx_button]').'</div>' : '')
					. (yreg_estate_param_is_on($pan) ? '</div>' : '')
					. (yreg_estate_param_is_on($scroll) 
						? '</div></div><div id="'.esc_attr($id).'_scroll_bar" class="sc_scroll_bar sc_scroll_bar_'.esc_attr($scroll_dir).' '.esc_attr($id).'_scroll_bar"></div></div>'
							. (!yreg_estate_param_is_off($scroll_controls) ? '<div class="sc_scroll_controls_wrap"><a class="sc_scroll_prev" href="#"></a><a class="sc_scroll_next" href="#"></a></div>' : '')
						: '')
					. ($bg_image !== '' || $bg_color !== '' || $bg_overlay > 0 || $bg_texture>0 || yreg_estate_strlen($bg_texture)>2 ? '</div></div>' : '')
					. '</div>'
				. '</div>';
		if (yreg_estate_param_is_on($dedicated)) {
			if (yreg_estate_storage_get('sc_section_dedicated')=='') {
				yreg_estate_storage_set('sc_section_dedicated', $output);
			}
			$output = '';
		}
		return apply_filters('yreg_estate_shortcode_output', $output, 'trx_section', $atts, $content);
	}
	yreg_estate_require_shortcode('trx_section', 'yreg_estate_sc_section');
	yreg_estate_require_shortcode('trx_block', 'yreg_estate_sc_section');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_section_reg_shortcodes' ) ) {
	//add_action('yreg_estate_action_shortcodes_list', 'yreg_estate_sc_section_reg_shortcodes');
	function yreg_estate_sc_section_reg_shortcodes() {
	
		$sc = array(
			"title" => esc_html__("Block container", 'yreg-estate'),
			"desc" => wp_kses_data( __("Container for any block ([section] analog - to enable nesting)", 'yreg-estate') ),
			"decorate" => true,
			"container" => true,
			"params" => array(
				"title" => array(
					"title" => esc_html__("Title", 'yreg-estate'),
					"desc" => wp_kses_data( __("Title for the block", 'yreg-estate') ),
					"value" => "",
					"type" => "text"
				),
				"subtitle" => array(
					"title" => esc_html__("Subtitle", 'yreg-estate'),
					"desc" => wp_kses_data( __("Subtitle for the block", 'yreg-estate') ),
					"value" => "",
					"type" => "text"
				),
				"description" => array(
					"title" => esc_html__("Description", 'yreg-estate'),
					"desc" => wp_kses_data( __("Short description for the block", 'yreg-estate') ),
					"value" => "",
					"type" => "textarea"
				),
				"link" => array(
					"title" => esc_html__("Button URL", 'yreg-estate'),
					"desc" => wp_kses_data( __("Link URL for the button at the bottom of the block", 'yreg-estate') ),
					"value" => "",
					"type" => "text"
				),
				"link_caption" => array(
					"title" => esc_html__("Button caption", 'yreg-estate'),
					"desc" => wp_kses_data( __("Caption for the button at the bottom of the block", 'yreg-estate') ),
					"value" => "",
					"type" => "text"
				),
				"dedicated" => array(
					"title" => esc_html__("Dedicated", 'yreg-estate'),
					"desc" => wp_kses_data( __("Use this block as dedicated content - show it before post title on single page", 'yreg-estate') ),
					"value" => "no",
					"type" => "switch",
					"options" => yreg_estate_get_sc_param('yes_no')
				),
				"align" => array(
					"title" => esc_html__("Align", 'yreg-estate'),
					"desc" => wp_kses_data( __("Select block alignment", 'yreg-estate') ),
					"value" => "none",
					"type" => "checklist",
					"dir" => "horizontal",
					"options" => yreg_estate_get_sc_param('align')
				),
				"columns" => array(
					"title" => esc_html__("Columns emulation", 'yreg-estate'),
					"desc" => wp_kses_data( __("Select width for columns emulation", 'yreg-estate') ),
					"value" => "none",
					"type" => "checklist",
					"options" => yreg_estate_get_sc_param('columns')
				), 
				"pan" => array(
					"title" => esc_html__("Use pan effect", 'yreg-estate'),
					"desc" => wp_kses_data( __("Use pan effect to show section content", 'yreg-estate') ),
					"divider" => true,
					"value" => "no",
					"type" => "switch",
					"options" => yreg_estate_get_sc_param('yes_no')
				),
				"scroll" => array(
					"title" => esc_html__("Use scroller", 'yreg-estate'),
					"desc" => wp_kses_data( __("Use scroller to show section content", 'yreg-estate') ),
					"divider" => true,
					"value" => "no",
					"type" => "switch",
					"options" => yreg_estate_get_sc_param('yes_no')
				),
				"scroll_dir" => array(
					"title" => esc_html__("Scroll and Pan direction", 'yreg-estate'),
					"desc" => wp_kses_data( __("Scroll and Pan direction (if Use scroller = yes or Pan = yes)", 'yreg-estate') ),
					"dependency" => array(
						'pan' => array('yes'),
						'scroll' => array('yes')
					),
					"value" => "horizontal",
					"type" => "switch",
					"size" => "big",
					"options" => yreg_estate_get_sc_param('dir')
				),
				"scroll_controls" => array(
					"title" => esc_html__("Scroll controls", 'yreg-estate'),
					"desc" => wp_kses_data( __("Show scroll controls (if Use scroller = yes)", 'yreg-estate') ),
					"dependency" => array(
						'scroll' => array('yes')
					),
					"value" => "hide",
					"type" => "checklist",
					"options" => yreg_estate_get_sc_param('controls')
				),
				"scheme" => array(
					"title" => esc_html__("Color scheme", 'yreg-estate'),
					"desc" => wp_kses_data( __("Select color scheme for this block", 'yreg-estate') ),
					"value" => "",
					"type" => "checklist",
					"options" => yreg_estate_get_sc_param('schemes')
				),
				"dark_bg" => array(
					"title" => esc_html__("Dark background?", 'yreg-estate'),
					"desc" => wp_kses_data( __("Inverts some colors", 'yreg-estate') ),
					"divider" => true,
					"value" => "no",
					"type" => "switch",
					"options" => yreg_estate_get_sc_param('yes_no')
				),
				"color" => array(
					"title" => esc_html__("Fore color", 'yreg-estate'),
					"desc" => wp_kses_data( __("Any color for objects in this section", 'yreg-estate') ),
					"divider" => true,
					"value" => "",
					"type" => "color"
				),
				"bg_color" => array(
					"title" => esc_html__("Background color", 'yreg-estate'),
					"desc" => wp_kses_data( __("Any background color for this section", 'yreg-estate') ),
					"value" => "",
					"type" => "color"
				),
				"bg_image" => array(
					"title" => esc_html__("Background image URL", 'yreg-estate'),
					"desc" => wp_kses_data( __("Select or upload image or write URL from other site for the background", 'yreg-estate') ),
					"readonly" => false,
					"value" => "",
					"type" => "media"
				),
				"bg_tile" => array(
					"title" => esc_html__("Tile background image", 'yreg-estate'),
					"desc" => wp_kses_data( __("Do you want tile background image or image cover whole block?", 'yreg-estate') ),
					"value" => "no",
					"dependency" => array(
						'bg_image' => array('not_empty')
					),
					"type" => "switch",
					"options" => yreg_estate_get_sc_param('yes_no')
				),
				"bg_overlay" => array(
					"title" => esc_html__("Overlay", 'yreg-estate'),
					"desc" => wp_kses_data( __("Overlay color opacity (from 0.0 to 1.0)", 'yreg-estate') ),
					"min" => "0",
					"max" => "1",
					"step" => "0.1",
					"value" => "0",
					"type" => "spinner"
				),
				"bg_texture" => array(
					"title" => esc_html__("Texture", 'yreg-estate'),
					"desc" => wp_kses_data( __("Predefined texture style from 1 to 11. 0 - without texture.", 'yreg-estate') ),
					"min" => "0",
					"max" => "11",
					"step" => "1",
					"value" => "0",
					"type" => "spinner"
				),
				"bg_padding" => array(
					"title" => esc_html__("Paddings around content", 'yreg-estate'),
					"desc" => wp_kses_data( __("Add paddings around content in this section (only if bg_color or bg_image enabled).", 'yreg-estate') ),
					"value" => "yes",
					"dependency" => array(
						'compare' => 'or',
						'bg_color' => array('not_empty'),
						'bg_texture' => array('not_empty'),
						'bg_image' => array('not_empty')
					),
					"type" => "switch",
					"options" => yreg_estate_get_sc_param('yes_no')
				),
				"font_size" => array(
					"title" => esc_html__("Font size", 'yreg-estate'),
					"desc" => wp_kses_data( __("Font size of the text (default - in pixels, allows any CSS units of measure)", 'yreg-estate') ),
					"value" => "",
					"type" => "text"
				),
				"font_line_height" => array(
					"title" => esc_html__("Font line height", 'yreg-estate'),
					"desc" => wp_kses_data( __("Font line height of the text (default - in pixels, allows any CSS units of measure)", 'yreg-estate') ),
					"value" => "",
					"type" => "text"
				),
				"font_weight" => array(
					"title" => esc_html__("Font weight", 'yreg-estate'),
					"desc" => wp_kses_data( __("Font weight of the text", 'yreg-estate') ),
					"value" => "",
					"type" => "select",
					"size" => "medium",
					"options" => array(
						'100' => esc_html__('Thin (100)', 'yreg-estate'),
						'300' => esc_html__('Light (300)', 'yreg-estate'),
						'400' => esc_html__('Normal (400)', 'yreg-estate'),
						'700' => esc_html__('Bold (700)', 'yreg-estate')
					)
				),
				"_content_" => array(
					"title" => esc_html__("Container content", 'yreg-estate'),
					"desc" => wp_kses_data( __("Content for section container", 'yreg-estate') ),
					"divider" => true,
					"rows" => 4,
					"value" => "",
					"type" => "textarea"
				),
				"width" => yreg_estate_shortcodes_width(),
				"height" => yreg_estate_shortcodes_height(),
				"top" => yreg_estate_get_sc_param('top'),
				"bottom" => yreg_estate_get_sc_param('bottom'),
				"left" => yreg_estate_get_sc_param('left'),
				"right" => yreg_estate_get_sc_param('right'),
				"id" => yreg_estate_get_sc_param('id'),
				"class" => yreg_estate_get_sc_param('class'),
				"animation" => yreg_estate_get_sc_param('animation'),
				"css" => yreg_estate_get_sc_param('css')
			)
		);
		yreg_estate_sc_map("trx_block", $sc);
		$sc["title"] = esc_html__("Section container", 'yreg-estate');
		$sc["desc"] = esc_html__("Container for any section ([block] analog - to enable nesting)", 'yreg-estate');
		yreg_estate_sc_map("trx_section", $sc);
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_section_reg_shortcodes_vc' ) ) {
	//add_action('yreg_estate_action_shortcodes_list_vc', 'yreg_estate_sc_section_reg_shortcodes_vc');
	function yreg_estate_sc_section_reg_shortcodes_vc() {
	
		$sc = array(
			"base" => "trx_block",
			"name" => esc_html__("Block container", 'yreg-estate'),
			"description" => wp_kses_data( __("Container for any block ([section] analog - to enable nesting)", 'yreg-estate') ),
			"category" => esc_html__('Content', 'yreg-estate'),
			'icon' => 'icon_trx_block',
			"class" => "trx_sc_collection trx_sc_block",
			"content_element" => true,
			"is_container" => true,
			"show_settings_on_create" => true,
			"params" => array(
				array(
					"param_name" => "dedicated",
					"heading" => esc_html__("Dedicated", 'yreg-estate'),
					"description" => wp_kses_data( __("Use this block as dedicated content - show it before post title on single page", 'yreg-estate') ),
					"admin_label" => true,
					"class" => "",
					"value" => array(esc_html__('Use as dedicated content', 'yreg-estate') => 'yes'),
					"type" => "checkbox"
				),
				array(
					"param_name" => "align",
					"heading" => esc_html__("Alignment", 'yreg-estate'),
					"description" => wp_kses_data( __("Select block alignment", 'yreg-estate') ),
					"class" => "",
					"value" => array_flip(yreg_estate_get_sc_param('align')),
					"type" => "dropdown"
				),
				array(
					"param_name" => "columns",
					"heading" => esc_html__("Columns emulation", 'yreg-estate'),
					"description" => wp_kses_data( __("Select width for columns emulation", 'yreg-estate') ),
					"admin_label" => true,
					"class" => "",
					"value" => array_flip(yreg_estate_get_sc_param('columns')),
					"type" => "dropdown"
				),
				array(
					"param_name" => "title",
					"heading" => esc_html__("Title", 'yreg-estate'),
					"description" => wp_kses_data( __("Title for the block", 'yreg-estate') ),
					"admin_label" => true,
					"group" => esc_html__('Captions', 'yreg-estate'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "subtitle",
					"heading" => esc_html__("Subtitle", 'yreg-estate'),
					"description" => wp_kses_data( __("Subtitle for the block", 'yreg-estate') ),
					"group" => esc_html__('Captions', 'yreg-estate'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "description",
					"heading" => esc_html__("Description", 'yreg-estate'),
					"description" => wp_kses_data( __("Description for the block", 'yreg-estate') ),
					"group" => esc_html__('Captions', 'yreg-estate'),
					"class" => "",
					"value" => "",
					"type" => "textarea"
				),
				array(
					"param_name" => "link",
					"heading" => esc_html__("Button URL", 'yreg-estate'),
					"description" => wp_kses_data( __("Link URL for the button at the bottom of the block", 'yreg-estate') ),
					"group" => esc_html__('Captions', 'yreg-estate'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "link_caption",
					"heading" => esc_html__("Button caption", 'yreg-estate'),
					"description" => wp_kses_data( __("Caption for the button at the bottom of the block", 'yreg-estate') ),
					"group" => esc_html__('Captions', 'yreg-estate'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "pan",
					"heading" => esc_html__("Use pan effect", 'yreg-estate'),
					"description" => wp_kses_data( __("Use pan effect to show section content", 'yreg-estate') ),
					"group" => esc_html__('Scroll', 'yreg-estate'),
					"class" => "",
					"value" => array(esc_html__('Content scroller', 'yreg-estate') => 'yes'),
					"type" => "checkbox"
				),
				array(
					"param_name" => "scroll",
					"heading" => esc_html__("Use scroller", 'yreg-estate'),
					"description" => wp_kses_data( __("Use scroller to show section content", 'yreg-estate') ),
					"group" => esc_html__('Scroll', 'yreg-estate'),
					"admin_label" => true,
					"class" => "",
					"value" => array(esc_html__('Content scroller', 'yreg-estate') => 'yes'),
					"type" => "checkbox"
				),
				array(
					"param_name" => "scroll_dir",
					"heading" => esc_html__("Scroll direction", 'yreg-estate'),
					"description" => wp_kses_data( __("Scroll direction (if Use scroller = yes)", 'yreg-estate') ),
					"admin_label" => true,
					"class" => "",
					"group" => esc_html__('Scroll', 'yreg-estate'),
					"value" => array_flip(yreg_estate_get_sc_param('dir')),
					'dependency' => array(
						'element' => 'scroll',
						'not_empty' => true
					),
					"type" => "dropdown"
				),
				array(
					"param_name" => "scroll_controls",
					"heading" => esc_html__("Scroll controls", 'yreg-estate'),
					"description" => wp_kses_data( __("Show scroll controls (if Use scroller = yes)", 'yreg-estate') ),
					"class" => "",
					"group" => esc_html__('Scroll', 'yreg-estate'),
					'dependency' => array(
						'element' => 'scroll',
						'not_empty' => true
					),
					"value" => array_flip(yreg_estate_get_sc_param('controls')),
					"type" => "dropdown"
				),
				array(
					"param_name" => "scheme",
					"heading" => esc_html__("Color scheme", 'yreg-estate'),
					"description" => wp_kses_data( __("Select color scheme for this block", 'yreg-estate') ),
					"group" => esc_html__('Colors and Images', 'yreg-estate'),
					"class" => "",
					"value" => array_flip(yreg_estate_get_sc_param('schemes')),
					"type" => "dropdown"
				),
				array(
					"param_name" => "dark_bg",
					"heading" => esc_html__("Dark background?", 'yreg-estate'),
					"description" => wp_kses_data( __("Inverts some colors", 'yreg-estate') ),
					"group" => esc_html__('Colors and Images', 'yreg-estate'),
					"class" => "",
					"value" => array(esc_html__('Show inverts some colors', 'yreg-estate') => 'yes'),
					"type" => "checkbox"
				),
				array(
					"param_name" => "color",
					"heading" => esc_html__("Fore color", 'yreg-estate'),
					"description" => wp_kses_data( __("Any color for objects in this section", 'yreg-estate') ),
					"group" => esc_html__('Colors and Images', 'yreg-estate'),
					"class" => "",
					"value" => "",
					"type" => "colorpicker"
				),
				array(
					"param_name" => "bg_color",
					"heading" => esc_html__("Background color", 'yreg-estate'),
					"description" => wp_kses_data( __("Any background color for this section", 'yreg-estate') ),
					"group" => esc_html__('Colors and Images', 'yreg-estate'),
					"class" => "",
					"value" => "",
					"type" => "colorpicker"
				),
				array(
					"param_name" => "bg_image",
					"heading" => esc_html__("Background image URL", 'yreg-estate'),
					"description" => wp_kses_data( __("Select background image from library for this section", 'yreg-estate') ),
					"group" => esc_html__('Colors and Images', 'yreg-estate'),
					"class" => "",
					"value" => "",
					"type" => "attach_image"
				),
				array(
					"param_name" => "bg_tile",
					"heading" => esc_html__("Tile background image", 'yreg-estate'),
					"description" => wp_kses_data( __("Do you want tile background image or image cover whole block?", 'yreg-estate') ),
					"group" => esc_html__('Colors and Images', 'yreg-estate'),
					"class" => "",
					'dependency' => array(
						'element' => 'bg_image',
						'not_empty' => true
					),
					"std" => "no",
					"value" => array(esc_html__('Tile background image', 'yreg-estate') => 'yes'),
					"type" => "checkbox"
				),
				array(
					"param_name" => "bg_overlay",
					"heading" => esc_html__("Overlay", 'yreg-estate'),
					"description" => wp_kses_data( __("Overlay color opacity (from 0.0 to 1.0)", 'yreg-estate') ),
					"group" => esc_html__('Colors and Images', 'yreg-estate'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "bg_texture",
					"heading" => esc_html__("Texture", 'yreg-estate'),
					"description" => wp_kses_data( __("Texture style from 1 to 11. Empty or 0 - without texture.", 'yreg-estate') ),
					"group" => esc_html__('Colors and Images', 'yreg-estate'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "bg_padding",
					"heading" => esc_html__("Paddings around content", 'yreg-estate'),
					"description" => wp_kses_data( __("Add paddings around content in this section (only if bg_color or bg_image enabled).", 'yreg-estate') ),
					"group" => esc_html__('Colors and Images', 'yreg-estate'),
					"class" => "",
					'dependency' => array(
						'element' => array('bg_color','bg_texture','bg_image'),
						'not_empty' => true
					),
					"std" => "yes",
					"value" => array(esc_html__('Disable padding around content in this block', 'yreg-estate') => 'no'),
					"type" => "checkbox"
				),
				array(
					"param_name" => "font_size",
					"heading" => esc_html__("Font size", 'yreg-estate'),
					"description" => wp_kses_data( __("Font size of the text (default - in pixels, allows any CSS units of measure)", 'yreg-estate') ),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "font_line_height",
					"heading" => esc_html__("Font line height", 'yreg-estate'),
					"description" => wp_kses_data( __("Font line height of the text (default - in pixels, allows any CSS units of measure)", 'yreg-estate') ),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "font_weight",
					"heading" => esc_html__("Font weight", 'yreg-estate'),
					"description" => wp_kses_data( __("Font weight of the text", 'yreg-estate') ),
					"class" => "",
					"value" => array(
						esc_html__('Default', 'yreg-estate') => 'inherit',
						esc_html__('Thin (100)', 'yreg-estate') => '100',
						esc_html__('Light (300)', 'yreg-estate') => '300',
						esc_html__('Normal (400)', 'yreg-estate') => '400',
						esc_html__('Bold (700)', 'yreg-estate') => '700'
					),
					"type" => "dropdown"
				),
				/*
				array(
					"param_name" => "content",
					"heading" => esc_html__("Container content", 'yreg-estate'),
					"description" => wp_kses_data( __("Content for section container", 'yreg-estate') ),
					"class" => "",
					"value" => "",
					"type" => "textarea_html"
				),
				*/
				yreg_estate_get_vc_param('id'),
				yreg_estate_get_vc_param('class'),
				yreg_estate_get_vc_param('animation'),
				yreg_estate_get_vc_param('css'),
				yreg_estate_vc_width(),
				yreg_estate_vc_height(),
				yreg_estate_get_vc_param('margin_top'),
				yreg_estate_get_vc_param('margin_bottom'),
				yreg_estate_get_vc_param('margin_left'),
				yreg_estate_get_vc_param('margin_right')
			)
		);
		
		// Block
		vc_map($sc);
		
		// Section
		$sc["base"] = 'trx_section';
		$sc["name"] = esc_html__("Section container", 'yreg-estate');
		$sc["description"] = wp_kses_data( __("Container for any section ([block] analog - to enable nesting)", 'yreg-estate') );
		$sc["class"] = "trx_sc_collection trx_sc_section";
		$sc["icon"] = 'icon_trx_section';
		vc_map($sc);
		
		class WPBakeryShortCode_Trx_Block extends YREG_ESTATE_VC_ShortCodeCollection {}
		class WPBakeryShortCode_Trx_Section extends YREG_ESTATE_VC_ShortCodeCollection {}
	}
}
?>