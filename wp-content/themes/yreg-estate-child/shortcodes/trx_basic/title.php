<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('yreg_estate_sc_title_theme_setup')) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_sc_title_theme_setup' );
	function yreg_estate_sc_title_theme_setup() {
		add_action('yreg_estate_action_shortcodes_list', 		'yreg_estate_sc_title_reg_shortcodes');
		if (function_exists('yreg_estate_exists_visual_composer') && yreg_estate_exists_visual_composer())
			add_action('yreg_estate_action_shortcodes_list_vc','yreg_estate_sc_title_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_title id="unique_id" style='regular|iconed' icon='' image='' background="on|off" type="1-6"]Et adipiscing integer, scelerisque pid, augue mus vel tincidunt porta[/trx_title]
*/

if (!function_exists('yreg_estate_sc_title')) {	
	function yreg_estate_sc_title($atts, $content=null){	
		if (yreg_estate_in_shortcode_blogger()) return '';
		extract(yreg_estate_html_decode(shortcode_atts(array(
			// Individual params
			"type" => "1",
			"style" => "regular",
			"subtitle" => "",
			"align" => "",
			"font_weight" => "",
			"font_size" => "",
			"color" => "",
			"icon" => "",
			"image" => "",
			"picture" => "",
			"image_size" => "small",
			"position" => "left",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"width" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$class .= ($class ? ' ' : '') . yreg_estate_get_css_position_as_classes($top, $right, $bottom, $left);
		$css .= yreg_estate_get_css_dimensions_from_values($width)
			.($align && $align!='none' && !yreg_estate_param_is_inherit($align) ? 'text-align:' . esc_attr($align) .';' : '')
			.($color ? 'color:' . esc_attr($color) .';' : '')
			.($font_weight && !yreg_estate_param_is_inherit($font_weight) ? 'font-weight:' . esc_attr($font_weight) .';' : '')
			.($font_size   ? 'font-size:' . esc_attr($font_size) .';' : '')
			;
		$type = min(6, max(1, $type));
		if ($picture > 0) {
			$attach = wp_get_attachment_image_src( $picture, 'full' );
			if (isset($attach[0]) && $attach[0]!='')
				$picture = $attach[0];
		}
		$pic = $style!='iconed' 
			? '' 
			: '<span class="sc_title_icon sc_title_icon_'.esc_attr($position).'  sc_title_icon_'.esc_attr($image_size).($icon!='' && $icon!='none' ? ' '.esc_attr($icon) : '')
				.(!empty($subtitle) ? ' sc_left' : '')
				.'"'.'>'
				.($picture ? '<img src="'.esc_url($picture).'" alt="" />' : '')
				.(empty($picture) && $image && $image!='none' ? '<img src="'.esc_url(yreg_estate_strpos($image, 'http:')!==false ? $image : yreg_estate_get_file_url('images/icons/'.($image).'.png')).'" alt="" />' : '')
				.'</span>';
		$output = '<h' . esc_attr($type) . ($id ? ' id="'.esc_attr($id).'"' : '')
				. ' class="sc_title sc_title_'.esc_attr($style)
					.($align && $align!='none' && !yreg_estate_param_is_inherit($align) ? ' sc_align_' . esc_attr($align) : '')
					.(!empty($class) ? ' '.esc_attr($class) : '')
					.'"'
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
				. (!yreg_estate_param_is_off($animation) ? ' data-animation="'.esc_attr(yreg_estate_get_animation_classes($animation)).'"' : '')
				. '>'
					. ($pic)
					.(!empty($subtitle) ? '<span class="sc_title_box">' : '')
					. do_shortcode($content)
					.(!empty($subtitle) ? '<span class="sc_title_subtitle">'.$subtitle.'</span></span>' : '')
				. '</h' . esc_attr($type) . '>';
		return apply_filters('yreg_estate_shortcode_output', $output, 'trx_title', $atts, $content);
	}
	yreg_estate_require_shortcode('trx_title', 'yreg_estate_sc_title');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_title_reg_shortcodes' ) ) {
	//add_action('yreg_estate_action_shortcodes_list', 'yreg_estate_sc_title_reg_shortcodes');
	function yreg_estate_sc_title_reg_shortcodes() {
	
		yreg_estate_sc_map("trx_title", array(
			"title" => esc_html__("Title", 'yreg-estate'),
			"desc" => wp_kses_data( __("Create header tag (1-6 level) with many styles", 'yreg-estate') ),
			"decorate" => false,
			"container" => true,
			"params" => array(
				"_content_" => array(
					"title" => esc_html__("Title content", 'yreg-estate'),
					"desc" => wp_kses_data( __("Title content", 'yreg-estate') ),
					"rows" => 4,
					"value" => "",
					"type" => "textarea"
				),
				"subtitle" => array(
					"title" => esc_html__("Subtitle content", 'yreg-estate'),
					"desc" => wp_kses_data( __("Subtitle content", 'yreg-estate') ),
					"value" => "",
					"type" => "text"
				),
				"type" => array(
					"title" => esc_html__("Title type", 'yreg-estate'),
					"desc" => wp_kses_data( __("Title type (header level)", 'yreg-estate') ),
					"divider" => true,
					"value" => "1",
					"type" => "select",
					"options" => array(
						'1' => esc_html__('Header 1', 'yreg-estate'),
						'2' => esc_html__('Header 2', 'yreg-estate'),
						'3' => esc_html__('Header 3', 'yreg-estate'),
						'4' => esc_html__('Header 4', 'yreg-estate'),
						'5' => esc_html__('Header 5', 'yreg-estate'),
						'6' => esc_html__('Header 6', 'yreg-estate'),
					)
				),
				"style" => array(
					"title" => esc_html__("Title style", 'yreg-estate'),
					"desc" => wp_kses_data( __("Title style", 'yreg-estate') ),
					"value" => "regular",
					"type" => "select",
					"options" => array(
						'regular' => esc_html__('Regular', 'yreg-estate'),
						'iconed' => esc_html__('With icon (image)', 'yreg-estate')
					)
				),
				"align" => array(
					"title" => esc_html__("Alignment", 'yreg-estate'),
					"desc" => wp_kses_data( __("Title text alignment", 'yreg-estate') ),
					"value" => "",
					"type" => "checklist",
					"dir" => "horizontal",
					"options" => yreg_estate_get_sc_param('align')
				), 
				"font_size" => array(
					"title" => esc_html__("Font_size", 'yreg-estate'),
					"desc" => wp_kses_data( __("Custom font size. If empty - use theme default", 'yreg-estate') ),
					"value" => "",
					"type" => "text"
				),
				"font_weight" => array(
					"title" => esc_html__("Font weight", 'yreg-estate'),
					"desc" => wp_kses_data( __("Custom font weight. If empty or inherit - use theme default", 'yreg-estate') ),
					"value" => "",
					"type" => "select",
					"size" => "medium",
					"options" => array(
						'inherit' => esc_html__('Default', 'yreg-estate'),
						'100' => esc_html__('Thin (100)', 'yreg-estate'),
						'300' => esc_html__('Light (300)', 'yreg-estate'),
						'400' => esc_html__('Normal (400)', 'yreg-estate'),
						'600' => esc_html__('Semibold (600)', 'yreg-estate'),
						'700' => esc_html__('Bold (700)', 'yreg-estate'),
						'900' => esc_html__('Black (900)', 'yreg-estate')
					)
				),
				"color" => array(
					"title" => esc_html__("Title color", 'yreg-estate'),
					"desc" => wp_kses_data( __("Select color for the title", 'yreg-estate') ),
					"value" => "",
					"type" => "color"
				),
				"icon" => array(
					"title" => esc_html__('Title font icon',  'yreg-estate'),
					"desc" => wp_kses_data( __("Select font icon for the title from Fontello icons set (if style=iconed)",  'yreg-estate') ),
					"dependency" => array(
						'style' => array('iconed')
					),
					"value" => "",
					"type" => "icons",
					"options" => yreg_estate_get_sc_param('icons')
				),
				"image" => array(
					"title" => esc_html__('or image icon',  'yreg-estate'),
					"desc" => wp_kses_data( __("Select image icon for the title instead icon above (if style=iconed)",  'yreg-estate') ),
					"dependency" => array(
						'style' => array('iconed')
					),
					"value" => "",
					"type" => "images",
					"size" => "small",
					"options" => yreg_estate_get_sc_param('images')
				),
				"picture" => array(
					"title" => esc_html__('or URL for image file', 'yreg-estate'),
					"desc" => wp_kses_data( __("Select or upload image or write URL from other site (if style=iconed)", 'yreg-estate') ),
					"dependency" => array(
						'style' => array('iconed')
					),
					"readonly" => false,
					"value" => "",
					"type" => "media"
				),
				"image_size" => array(
					"title" => esc_html__('Image (picture) size', 'yreg-estate'),
					"desc" => wp_kses_data( __("Select image (picture) size (if style='iconed')", 'yreg-estate') ),
					"dependency" => array(
						'style' => array('iconed')
					),
					"value" => "small",
					"type" => "checklist",
					"options" => array(
						'small' => esc_html__('Small', 'yreg-estate'),
						'medium' => esc_html__('Medium', 'yreg-estate'),
						'large' => esc_html__('Large', 'yreg-estate')
					)
				),
				"position" => array(
					"title" => esc_html__('Icon (image) position', 'yreg-estate'),
					"desc" => wp_kses_data( __("Select icon (image) position (if style=iconed)", 'yreg-estate') ),
					"dependency" => array(
						'style' => array('iconed')
					),
					"value" => "left",
					"type" => "checklist",
					"options" => array(
						'top' => esc_html__('Top', 'yreg-estate'),
						'left' => esc_html__('Left', 'yreg-estate')
					)
				),
				"top" => yreg_estate_get_sc_param('top'),
				"bottom" => yreg_estate_get_sc_param('bottom'),
				"left" => yreg_estate_get_sc_param('left'),
				"right" => yreg_estate_get_sc_param('right'),
				"id" => yreg_estate_get_sc_param('id'),
				"class" => yreg_estate_get_sc_param('class'),
				"animation" => yreg_estate_get_sc_param('animation'),
				"css" => yreg_estate_get_sc_param('css')
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_title_reg_shortcodes_vc' ) ) {
	//add_action('yreg_estate_action_shortcodes_list_vc', 'yreg_estate_sc_title_reg_shortcodes_vc');
	function yreg_estate_sc_title_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_title",
			"name" => esc_html__("Title", 'yreg-estate'),
			"description" => wp_kses_data( __("Create header tag (1-6 level) with many styles", 'yreg-estate') ),
			"category" => esc_html__('Content', 'yreg-estate'),
			'icon' => 'icon_trx_title',
			"class" => "trx_sc_single trx_sc_title",
			"content_element" => true,
			"is_container" => false,
			"show_settings_on_create" => true,
			"params" => array(
				array(
					"param_name" => "content",
					"heading" => esc_html__("Title content", 'yreg-estate'),
					"description" => wp_kses_data( __("Title content", 'yreg-estate') ),
					"class" => "",
					"value" => "",
					"type" => "textarea_html"
				),
				array(
					"param_name" => "subtitle",
					"heading" => esc_html__("Subtitle content", 'yreg-estate'),
					"description" => wp_kses_data( __("Subtitle content", 'yreg-estate') ),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "type",
					"heading" => esc_html__("Title type", 'yreg-estate'),
					"description" => wp_kses_data( __("Title type (header level)", 'yreg-estate') ),
					"admin_label" => true,
					"class" => "",
					"value" => array(
						esc_html__('Header 1', 'yreg-estate') => '1',
						esc_html__('Header 2', 'yreg-estate') => '2',
						esc_html__('Header 3', 'yreg-estate') => '3',
						esc_html__('Header 4', 'yreg-estate') => '4',
						esc_html__('Header 5', 'yreg-estate') => '5',
						esc_html__('Header 6', 'yreg-estate') => '6'
					),
					"type" => "dropdown"
				),
				array(
					"param_name" => "style",
					"heading" => esc_html__("Title style", 'yreg-estate'),
					"description" => wp_kses_data( __("Title style: only text (regular) or with icon/image (iconed)", 'yreg-estate') ),
					"admin_label" => true,
					"class" => "",
					"value" => array(
						esc_html__('Regular', 'yreg-estate') => 'regular',
						esc_html__('With icon (image)', 'yreg-estate') => 'iconed'
					),
					"type" => "dropdown"
				),
				array(
					"param_name" => "align",
					"heading" => esc_html__("Alignment", 'yreg-estate'),
					"description" => wp_kses_data( __("Title text alignment", 'yreg-estate') ),
					"admin_label" => true,
					"class" => "",
					"value" => array_flip(yreg_estate_get_sc_param('align')),
					"type" => "dropdown"
				),
				array(
					"param_name" => "font_size",
					"heading" => esc_html__("Font size", 'yreg-estate'),
					"description" => wp_kses_data( __("Custom font size. If empty - use theme default", 'yreg-estate') ),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "font_weight",
					"heading" => esc_html__("Font weight", 'yreg-estate'),
					"description" => wp_kses_data( __("Custom font weight. If empty or inherit - use theme default", 'yreg-estate') ),
					"class" => "",
					"value" => array(
						esc_html__('Default', 'yreg-estate') => 'inherit',
						esc_html__('Thin (100)', 'yreg-estate') => '100',
						esc_html__('Light (300)', 'yreg-estate') => '300',
						esc_html__('Normal (400)', 'yreg-estate') => '400',
						esc_html__('Semibold (600)', 'yreg-estate') => '600',
						esc_html__('Bold (700)', 'yreg-estate') => '700',
						esc_html__('Black (900)', 'yreg-estate') => '900'
					),
					"type" => "dropdown"
				),
				array(
					"param_name" => "color",
					"heading" => esc_html__("Title color", 'yreg-estate'),
					"description" => wp_kses_data( __("Select color for the title", 'yreg-estate') ),
					"class" => "",
					"value" => "",
					"type" => "colorpicker"
				),
				array(
					"param_name" => "icon",
					"heading" => esc_html__("Title font icon", 'yreg-estate'),
					"description" => wp_kses_data( __("Select font icon for the title from Fontello icons set (if style=iconed)", 'yreg-estate') ),
					"class" => "",
					"group" => esc_html__('Icon &amp; Image', 'yreg-estate'),
					'dependency' => array(
						'element' => 'style',
						'value' => array('iconed')
					),
					"value" => yreg_estate_get_sc_param('icons'),
					"type" => "dropdown"
				),
				array(
					"param_name" => "image",
					"heading" => esc_html__("or image icon", 'yreg-estate'),
					"description" => wp_kses_data( __("Select image icon for the title instead icon above (if style=iconed)", 'yreg-estate') ),
					"class" => "",
					"group" => esc_html__('Icon &amp; Image', 'yreg-estate'),
					'dependency' => array(
						'element' => 'style',
						'value' => array('iconed')
					),
					"value" => yreg_estate_get_sc_param('images'),
					"type" => "dropdown"
				),
				array(
					"param_name" => "picture",
					"heading" => esc_html__("or select uploaded image", 'yreg-estate'),
					"description" => wp_kses_data( __("Select or upload image or write URL from other site (if style=iconed)", 'yreg-estate') ),
					"group" => esc_html__('Icon &amp; Image', 'yreg-estate'),
					"class" => "",
					"value" => "",
					"type" => "attach_image"
				),
				array(
					"param_name" => "image_size",
					"heading" => esc_html__("Image (picture) size", 'yreg-estate'),
					"description" => wp_kses_data( __("Select image (picture) size (if style=iconed)", 'yreg-estate') ),
					"group" => esc_html__('Icon &amp; Image', 'yreg-estate'),
					"class" => "",
					"value" => array(
						esc_html__('Small', 'yreg-estate') => 'small',
						esc_html__('Medium', 'yreg-estate') => 'medium',
						esc_html__('Large', 'yreg-estate') => 'large'
					),
					"type" => "dropdown"
				),
				array(
					"param_name" => "position",
					"heading" => esc_html__("Icon (image) position", 'yreg-estate'),
					"description" => wp_kses_data( __("Select icon (image) position (if style=iconed)", 'yreg-estate') ),
					"group" => esc_html__('Icon &amp; Image', 'yreg-estate'),
					"class" => "",
					"std" => "left",
					"value" => array(
						esc_html__('Top', 'yreg-estate') => 'top',
						esc_html__('Left', 'yreg-estate') => 'left'
					),
					"type" => "dropdown"
				),
				yreg_estate_get_vc_param('id'),
				yreg_estate_get_vc_param('class'),
				yreg_estate_get_vc_param('animation'),
				yreg_estate_get_vc_param('css'),
				yreg_estate_get_vc_param('margin_top'),
				yreg_estate_get_vc_param('margin_bottom'),
				yreg_estate_get_vc_param('margin_left'),
				yreg_estate_get_vc_param('margin_right')
			),
			'js_view' => 'VcTrxTextView'
		) );
		
		class WPBakeryShortCode_Trx_Title extends YREG_ESTATE_VC_ShortCodeSingle {}
	}
}
?>