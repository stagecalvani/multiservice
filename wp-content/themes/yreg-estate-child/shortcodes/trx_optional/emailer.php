<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('yreg_estate_sc_emailer_theme_setup')) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_sc_emailer_theme_setup' );
	function yreg_estate_sc_emailer_theme_setup() {
		add_action('yreg_estate_action_shortcodes_list', 		'yreg_estate_sc_emailer_reg_shortcodes');
		if (function_exists('yreg_estate_exists_visual_composer') && yreg_estate_exists_visual_composer())
			add_action('yreg_estate_action_shortcodes_list_vc','yreg_estate_sc_emailer_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

//[trx_emailer group=""]

if (!function_exists('yreg_estate_sc_emailer')) {	
	function yreg_estate_sc_emailer($atts, $content = null) {
		if (yreg_estate_in_shortcode_blogger()) return '';
		extract(yreg_estate_html_decode(shortcode_atts(array(
			// Individual params
			"title" => "",
			"group" => "",
			"open" => "yes",
			"align" => "",
			// Common params
			"id" => "",
			"class" => "",
			"css" => "",
			"animation" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => "",
			"width" => "",
			"height" => ""
		), $atts)));
		$class .= ($class ? ' ' : '') . yreg_estate_get_css_position_as_classes($top, $right, $bottom, $left);
		$css .= yreg_estate_get_css_dimensions_from_values($width, $height);
		// Load core messages
		yreg_estate_enqueue_messages();
		$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '')
					. ' class="sc_emailer' . ($align && $align!='none' ? ' align' . esc_attr($align) : '') . (yreg_estate_param_is_on($open) ? ' sc_emailer_opened' : '') . (!empty($class) ? ' '.esc_attr($class) : '') . '"' 
					. ($css ? ' style="'.esc_attr($css).'"' : '') 
					. (!yreg_estate_param_is_off($animation) ? ' data-animation="'.esc_attr(yreg_estate_get_animation_classes($animation)).'"' : '')
					. '>'
				. (!empty($title) ? '<div class="sc_emailer_title">'.esc_attr($title).'</div><div class="sc_emailer_content">' : '')
				. '<form class="sc_emailer_form">'
				. '<input type="text" class="sc_emailer_input" name="email" value="" placeholder="'.esc_attr__('Please, enter you email address.', 'yreg-estate').'">'
				. '<a href="#" class="sc_emailer_button sc_button sc_button_box sc_button_style_style3 sc_button_size_small" title="'.esc_attr__('Submit', 'yreg-estate').'" data-group="'.esc_attr($group ? $group : esc_html__('E-mailer subscription', 'yreg-estate')).'">'.esc_html__('subscribe', 'yreg-estate').'</a>'
				. '</form>'
				. (!empty($title) ? '</div><div class="cL"></div>' : '')
			. '</div>';
		return apply_filters('yreg_estate_shortcode_output', $output, 'trx_emailer', $atts, $content);
	}
	yreg_estate_require_shortcode("trx_emailer", "yreg_estate_sc_emailer");
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_emailer_reg_shortcodes' ) ) {
	//add_action('yreg_estate_action_shortcodes_list', 'yreg_estate_sc_emailer_reg_shortcodes');
	function yreg_estate_sc_emailer_reg_shortcodes() {
	
		yreg_estate_sc_map("trx_emailer", array(
			"title" => esc_html__("E-mail collector", 'yreg-estate'),
			"desc" => wp_kses_data( __("Collect the e-mail address into specified group", 'yreg-estate') ),
			"decorate" => false,
			"container" => false,
			"params" => array(
				"title" => array(
					"title" => esc_html__("Title", 'yreg-estate'),
					"desc" => wp_kses_data( __("", 'yreg-estate') ),
					"value" => "",
					"type" => "text"
				),
				"group" => array(
					"title" => esc_html__("Group", 'yreg-estate'),
					"desc" => wp_kses_data( __("The name of group to collect e-mail address", 'yreg-estate') ),
					"value" => "",
					"type" => "text"
				),
//				"open" => array(
//					"title" => esc_html__("Open", 'yreg-estate'),
//					"desc" => wp_kses_data( __("Initially open the input field on show object", 'yreg-estate') ),
//					"divider" => true,
//					"value" => "yes",
//					"type" => "switch",
//					"options" => yreg_estate_get_sc_param('yes_no')
//				),
				"align" => array(
					"title" => esc_html__("Alignment", 'yreg-estate'),
					"desc" => wp_kses_data( __("Align object to left, center or right", 'yreg-estate') ),
					"divider" => true,
					"value" => "none",
					"type" => "checklist",
					"dir" => "horizontal",
					"options" => yreg_estate_get_sc_param('align')
				), 
				"width" => yreg_estate_shortcodes_width(),
				"height" => yreg_estate_shortcodes_height(),
				"top" => yreg_estate_get_sc_param('top'),
				"bottom" => yreg_estate_get_sc_param('bottom'),
				"left" => yreg_estate_get_sc_param('left'),
				"right" => yreg_estate_get_sc_param('right'),
				"id" => yreg_estate_get_sc_param('id'),
				"class" => yreg_estate_get_sc_param('class'),
				"animation" => yreg_estate_get_sc_param('animation'),
				"css" => yreg_estate_get_sc_param('css')
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_emailer_reg_shortcodes_vc' ) ) {
	//add_action('yreg_estate_action_shortcodes_list_vc', 'yreg_estate_sc_emailer_reg_shortcodes_vc');
	function yreg_estate_sc_emailer_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_emailer",
			"name" => esc_html__("E-mail collector", 'yreg-estate'),
			"description" => wp_kses_data( __("Collect e-mails into specified group", 'yreg-estate') ),
			"category" => esc_html__('Content', 'yreg-estate'),
			'icon' => 'icon_trx_emailer',
			"class" => "trx_sc_single trx_sc_emailer",
			"content_element" => true,
			"is_container" => false,
			"show_settings_on_create" => true,
			"params" => array(
				array(
					"param_name" => "group",
					"heading" => esc_html__("Group", 'yreg-estate'),
					"description" => wp_kses_data( __("The name of group to collect e-mail address", 'yreg-estate') ),
					"admin_label" => true,
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "title",
					"heading" => esc_html__("Title", 'yreg-estate'),
					"description" => wp_kses_data( __("", 'yreg-estate') ),
					"admin_label" => true,
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
//				array(
//					"param_name" => "open",
//					"heading" => esc_html__("Opened", 'yreg-estate'),
//					"description" => wp_kses_data( __("Initially open the input field on show object", 'yreg-estate') ),
//					"class" => "",
//					"value" => array(esc_html__('Initially opened', 'yreg-estate') => 'yes'),
//					"type" => "checkbox"
//				),
				array(
					"param_name" => "align",
					"heading" => esc_html__("Alignment", 'yreg-estate'),
					"description" => wp_kses_data( __("Align field to left, center or right", 'yreg-estate') ),
					"admin_label" => true,
					"class" => "",
					"value" => array_flip(yreg_estate_get_sc_param('align')),
					"type" => "dropdown"
				),
				yreg_estate_get_vc_param('id'),
				yreg_estate_get_vc_param('class'),
				yreg_estate_get_vc_param('animation'),
				yreg_estate_get_vc_param('css'),
				yreg_estate_vc_width(),
				yreg_estate_vc_height(),
				yreg_estate_get_vc_param('margin_top'),
				yreg_estate_get_vc_param('margin_bottom'),
				yreg_estate_get_vc_param('margin_left'),
				yreg_estate_get_vc_param('margin_right')
			)
		) );
		
		class WPBakeryShortCode_Trx_Emailer extends YREG_ESTATE_VC_ShortCodeSingle {}
	}
}
?>