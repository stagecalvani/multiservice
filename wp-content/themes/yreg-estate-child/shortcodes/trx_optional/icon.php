<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('yreg_estate_sc_icon_theme_setup')) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_sc_icon_theme_setup' );
	function yreg_estate_sc_icon_theme_setup() {
		add_action('yreg_estate_action_shortcodes_list', 		'yreg_estate_sc_icon_reg_shortcodes');
		if (function_exists('yreg_estate_exists_visual_composer') && yreg_estate_exists_visual_composer())
			add_action('yreg_estate_action_shortcodes_list_vc','yreg_estate_sc_icon_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_icon id="unique_id" style='round|square' icon='' color="" bg_color="" size="" weight=""]
*/

if (!function_exists('yreg_estate_sc_icon')) {	
	function yreg_estate_sc_icon($atts, $content=null){	
		if (yreg_estate_in_shortcode_blogger()) return '';
		extract(yreg_estate_html_decode(shortcode_atts(array(
			// Individual params
			"icon" => "",
			"color" => "",
			"bg_color" => "",
			"bg_shape" => "",
			"font_size" => "",
			"font_weight" => "",
			"align" => "",
			"link" => "",
			// Common params
			"id" => "",
			"class" => "",
			"css" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$class .= ($class ? ' ' : '') . yreg_estate_get_css_position_as_classes($top, $right, $bottom, $left);
		$css2 = ($font_weight != '' && !yreg_estate_is_inherit_option($font_weight) ? 'font-weight:'. esc_attr($font_weight).';' : '')
			. ($font_size != '' ? 'font-size:' . esc_attr(yreg_estate_prepare_css_value($font_size)) . '; line-height: ' . (!$bg_shape || yreg_estate_param_is_inherit($bg_shape) ? '1' : '1.2') . 'em;' : '')
			. ($color != '' ? 'color:'.esc_attr($color).';' : '')
			. ($bg_color != '' ? 'background-color:'.esc_attr($bg_color).';border-color:'.esc_attr($bg_color).';' : '')
		;
		$output = $icon!='' 
			? ($link ? '<a href="'.esc_url($link).'"' : '<span') . ($id ? ' id="'.esc_attr($id).'"' : '')
				. ' class="sc_icon '.esc_attr($icon)
					. ($bg_shape && !yreg_estate_param_is_inherit($bg_shape) ? ' sc_icon_shape_'.esc_attr($bg_shape) : '')
					. ($align && $align!='none' ? ' align'.esc_attr($align) : '') 
					. (!empty($class) ? ' '.esc_attr($class) : '')
				.'"'
				.($css || $css2 ? ' style="'.($class ? 'display:block;' : '') . ($css) . ($css2) . '"' : '')
				.'>'
				.($link ? '</a>' : '</span>')
			: '';
		return apply_filters('yreg_estate_shortcode_output', $output, 'trx_icon', $atts, $content);
	}
	yreg_estate_require_shortcode('trx_icon', 'yreg_estate_sc_icon');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_icon_reg_shortcodes' ) ) {
	//add_action('yreg_estate_action_shortcodes_list', 'yreg_estate_sc_icon_reg_shortcodes');
	function yreg_estate_sc_icon_reg_shortcodes() {
	
		yreg_estate_sc_map("trx_icon", array(
			"title" => esc_html__("Icon", 'yreg-estate'),
			"desc" => wp_kses_data( __("Insert icon", 'yreg-estate') ),
			"decorate" => false,
			"container" => false,
			"params" => array(
				"icon" => array(
					"title" => esc_html__('Icon',  'yreg-estate'),
					"desc" => wp_kses_data( __('Select font icon from the Fontello icons set',  'yreg-estate') ),
					"value" => "",
					"type" => "icons",
					"options" => yreg_estate_get_sc_param('icons')
				),
				"color" => array(
					"title" => esc_html__("Icon's color", 'yreg-estate'),
					"desc" => wp_kses_data( __("Icon's color", 'yreg-estate') ),
					"dependency" => array(
						'icon' => array('not_empty')
					),
					"value" => "",
					"type" => "color"
				),
				"bg_shape" => array(
					"title" => esc_html__("Background shape", 'yreg-estate'),
					"desc" => wp_kses_data( __("Shape of the icon background", 'yreg-estate') ),
					"dependency" => array(
						'icon' => array('not_empty')
					),
					"value" => "none",
					"type" => "select",
					"options" => array(
						'none' => esc_html__('None', 'yreg-estate'),
						'round' => esc_html__('Round', 'yreg-estate'),
						'square' => esc_html__('Square', 'yreg-estate')
					)
				),
				"bg_color" => array(
					"title" => esc_html__("Icon's background color", 'yreg-estate'),
					"desc" => wp_kses_data( __("Icon's background color", 'yreg-estate') ),
					"dependency" => array(
						'icon' => array('not_empty'),
						'background' => array('round','square')
					),
					"value" => "",
					"type" => "color"
				),
				"font_size" => array(
					"title" => esc_html__("Font size", 'yreg-estate'),
					"desc" => wp_kses_data( __("Icon's font size", 'yreg-estate') ),
					"dependency" => array(
						'icon' => array('not_empty')
					),
					"value" => "",
					"type" => "spinner",
					"min" => 8,
					"max" => 240
				),
				"font_weight" => array(
					"title" => esc_html__("Font weight", 'yreg-estate'),
					"desc" => wp_kses_data( __("Icon font weight", 'yreg-estate') ),
					"dependency" => array(
						'icon' => array('not_empty')
					),
					"value" => "",
					"type" => "select",
					"size" => "medium",
					"options" => array(
						'100' => esc_html__('Thin (100)', 'yreg-estate'),
						'300' => esc_html__('Light (300)', 'yreg-estate'),
						'400' => esc_html__('Normal (400)', 'yreg-estate'),
						'700' => esc_html__('Bold (700)', 'yreg-estate')
					)
				),
				"align" => array(
					"title" => esc_html__("Alignment", 'yreg-estate'),
					"desc" => wp_kses_data( __("Icon text alignment", 'yreg-estate') ),
					"dependency" => array(
						'icon' => array('not_empty')
					),
					"value" => "",
					"type" => "checklist",
					"dir" => "horizontal",
					"options" => yreg_estate_get_sc_param('align')
				), 
				"link" => array(
					"title" => esc_html__("Link URL", 'yreg-estate'),
					"desc" => wp_kses_data( __("Link URL from this icon (if not empty)", 'yreg-estate') ),
					"value" => "",
					"type" => "text"
				),
				"top" => yreg_estate_get_sc_param('top'),
				"bottom" => yreg_estate_get_sc_param('bottom'),
				"left" => yreg_estate_get_sc_param('left'),
				"right" => yreg_estate_get_sc_param('right'),
				"id" => yreg_estate_get_sc_param('id'),
				"class" => yreg_estate_get_sc_param('class'),
				"css" => yreg_estate_get_sc_param('css')
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_icon_reg_shortcodes_vc' ) ) {
	//add_action('yreg_estate_action_shortcodes_list_vc', 'yreg_estate_sc_icon_reg_shortcodes_vc');
	function yreg_estate_sc_icon_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_icon",
			"name" => esc_html__("Icon", 'yreg-estate'),
			"description" => wp_kses_data( __("Insert the icon", 'yreg-estate') ),
			"category" => esc_html__('Content', 'yreg-estate'),
			'icon' => 'icon_trx_icon',
			"class" => "trx_sc_single trx_sc_icon",
			"content_element" => true,
			"is_container" => false,
			"show_settings_on_create" => true,
			"params" => array(
				array(
					"param_name" => "icon",
					"heading" => esc_html__("Icon", 'yreg-estate'),
					"description" => wp_kses_data( __("Select icon class from Fontello icons set", 'yreg-estate') ),
					"admin_label" => true,
					"class" => "",
					"value" => yreg_estate_get_sc_param('icons'),
					"type" => "dropdown"
				),
				array(
					"param_name" => "color",
					"heading" => esc_html__("Text color", 'yreg-estate'),
					"description" => wp_kses_data( __("Icon's color", 'yreg-estate') ),
					"class" => "",
					"value" => "",
					"type" => "colorpicker"
				),
				array(
					"param_name" => "bg_color",
					"heading" => esc_html__("Background color", 'yreg-estate'),
					"description" => wp_kses_data( __("Background color for the icon", 'yreg-estate') ),
					"class" => "",
					"value" => "",
					"type" => "colorpicker"
				),
				array(
					"param_name" => "bg_shape",
					"heading" => esc_html__("Background shape", 'yreg-estate'),
					"description" => wp_kses_data( __("Shape of the icon background", 'yreg-estate') ),
					"admin_label" => true,
					"class" => "",
					"value" => array(
						esc_html__('None', 'yreg-estate') => 'none',
						esc_html__('Round', 'yreg-estate') => 'round',
						esc_html__('Square', 'yreg-estate') => 'square'
					),
					"type" => "dropdown"
				),
				array(
					"param_name" => "font_size",
					"heading" => esc_html__("Font size", 'yreg-estate'),
					"description" => wp_kses_data( __("Icon's font size", 'yreg-estate') ),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "font_weight",
					"heading" => esc_html__("Font weight", 'yreg-estate'),
					"description" => wp_kses_data( __("Icon's font weight", 'yreg-estate') ),
					"class" => "",
					"value" => array(
						esc_html__('Default', 'yreg-estate') => 'inherit',
						esc_html__('Thin (100)', 'yreg-estate') => '100',
						esc_html__('Light (300)', 'yreg-estate') => '300',
						esc_html__('Normal (400)', 'yreg-estate') => '400',
						esc_html__('Bold (700)', 'yreg-estate') => '700'
					),
					"type" => "dropdown"
				),
				array(
					"param_name" => "align",
					"heading" => esc_html__("Icon's alignment", 'yreg-estate'),
					"description" => wp_kses_data( __("Align icon to left, center or right", 'yreg-estate') ),
					"admin_label" => true,
					"class" => "",
					"value" => array_flip(yreg_estate_get_sc_param('align')),
					"type" => "dropdown"
				),
				array(
					"param_name" => "link",
					"heading" => esc_html__("Link URL", 'yreg-estate'),
					"description" => wp_kses_data( __("Link URL from this icon (if not empty)", 'yreg-estate') ),
					"admin_label" => true,
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				yreg_estate_get_vc_param('id'),
				yreg_estate_get_vc_param('class'),
				yreg_estate_get_vc_param('css'),
				yreg_estate_get_vc_param('margin_top'),
				yreg_estate_get_vc_param('margin_bottom'),
				yreg_estate_get_vc_param('margin_left'),
				yreg_estate_get_vc_param('margin_right')
			),
		) );
		
		class WPBakeryShortCode_Trx_Icon extends YREG_ESTATE_VC_ShortCodeSingle {}
	}
}
?>