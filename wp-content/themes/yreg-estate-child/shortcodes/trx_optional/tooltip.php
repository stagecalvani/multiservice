<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('yreg_estate_sc_tooltip_theme_setup')) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_sc_tooltip_theme_setup' );
	function yreg_estate_sc_tooltip_theme_setup() {
		add_action('yreg_estate_action_shortcodes_list', 		'yreg_estate_sc_tooltip_reg_shortcodes');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_tooltip id="unique_id" title="Tooltip text here"]Et adipiscing integer, scelerisque pid, augue mus vel tincidunt porta[/tooltip]
*/

if (!function_exists('yreg_estate_sc_tooltip')) {	
	function yreg_estate_sc_tooltip($atts, $content=null){	
		if (yreg_estate_in_shortcode_blogger()) return '';
		extract(yreg_estate_html_decode(shortcode_atts(array(
			// Individual params
			"title" => "",
			// Common params
			"id" => "",
			"class" => "",
			"css" => ""
		), $atts)));
		$output = '<span' . ($id ? ' id="'.esc_attr($id).'"' : '') 
					. ' class="sc_tooltip_parent'. (!empty($class) ? ' '.esc_attr($class) : '').'"'
					. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
					. '>'
						. do_shortcode($content)
						. '<span class="sc_tooltip">' . ($title) . '</span>'
					. '</span>';
		return apply_filters('yreg_estate_shortcode_output', $output, 'trx_tooltip', $atts, $content);
	}
	yreg_estate_require_shortcode('trx_tooltip', 'yreg_estate_sc_tooltip');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_tooltip_reg_shortcodes' ) ) {
	//add_action('yreg_estate_action_shortcodes_list', 'yreg_estate_sc_tooltip_reg_shortcodes');
	function yreg_estate_sc_tooltip_reg_shortcodes() {
	
		yreg_estate_sc_map("trx_tooltip", array(
			"title" => esc_html__("Tooltip", 'yreg-estate'),
			"desc" => wp_kses_data( __("Create tooltip for selected text", 'yreg-estate') ),
			"decorate" => false,
			"container" => true,
			"params" => array(
				"title" => array(
					"title" => esc_html__("Title", 'yreg-estate'),
					"desc" => wp_kses_data( __("Tooltip title (required)", 'yreg-estate') ),
					"value" => "",
					"type" => "text"
				),
				"_content_" => array(
					"title" => esc_html__("Tipped content", 'yreg-estate'),
					"desc" => wp_kses_data( __("Highlighted content with tooltip", 'yreg-estate') ),
					"divider" => true,
					"rows" => 4,
					"value" => "",
					"type" => "textarea"
				),
				"id" => yreg_estate_get_sc_param('id'),
				"class" => yreg_estate_get_sc_param('class'),
				"css" => yreg_estate_get_sc_param('css')
			)
		));
	}
}
?>