<?php
/**
Template Name: Single post
 */
get_header(); 

$single_style = yreg_estate_storage_get('single_style');
if (empty($single_style)) $single_style = yreg_estate_get_custom_option('single_style');

while ( have_posts() ) { the_post();
	yreg_estate_show_post_layout(
		array(
			'layout' => $single_style,
			'sidebar' => !yreg_estate_param_is_off(yreg_estate_get_custom_option('show_sidebar_main')),
			'content' => yreg_estate_get_template_property($single_style, 'need_content'),
			'terms_list' => yreg_estate_get_template_property($single_style, 'need_terms')
		)
	);
}
if(is_singular('property')):
    $text= get_the_content();
    $text = do_shortcode($text);
    $text = strip_tags($text);
    $truncate_text = truncate($text, 300, '...'); 
    ?><div id="testo_immobili_stampa">
        <?php echo $truncate_text; ?><br /><br />
        <span id="firma">
        Per maggiori informazioni: <br /> <a class="link_stampa" target="_blank" href="<?php the_permalink(); ?>"><?php the_permalink(); ?></a> - 
        <br />Sito web: www.multiserviceimmobiliare.it - E-mail: info@multiserviceimmobiliare.it</span>
    </div>
    <div id="stampa_scheda_btn">
        <a href="#"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/print_image.jpg"/> Stampa Scheda</a>
    </div>
<?php endif;
get_footer();
?>