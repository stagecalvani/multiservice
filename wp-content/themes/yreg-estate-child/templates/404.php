<?php
/*
 * The template for displaying "Page 404"
*/

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }


/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'yreg_estate_template_404_theme_setup' ) ) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_template_404_theme_setup', 1 );
	function yreg_estate_template_404_theme_setup() {
		yreg_estate_add_template(array(
			'layout' => '404',
			'mode'   => 'internal',
			'title'  => 'Pagina 404',
			'theme_options' => array(
				'article_style' => 'stretch'
			)
		));
	}
}

// Template output
if ( !function_exists( 'yreg_estate_template_404_output' ) ) {
	function yreg_estate_template_404_output() {
		?>
		<article class="post_item post_item_404">
			<div class="post_content">
				<h1 class="page_title"><?php esc_html_e( '404', 'yreg-estate' ); ?></h1>
				<h2 class="page_subtitle"><?php esc_html_e('La pagina cercata non è stata trovata', 'yreg-estate'); ?></h2>
				<p class="page_description">
					<?php esc_html_e('Non hai trovato ciò che desideravi?', 'yreg-estate'); ?><br>
					<?php echo wp_kses_data( sprintf( __('Torna al menu <a href="%s">%s</a>.', 'yreg-estate'), esc_url(home_url('/')),'clicca qui') ); ?>
				</p>
				<!--<div class="page_search"><?php echo trim(yreg_estate_sc_search(array('state'=>'fixed', 'title'=>__('To search type and hit enter', 'yreg-estate')))); ?></div>-->
			</div>
		</article>
		<?php
	}
}
?>