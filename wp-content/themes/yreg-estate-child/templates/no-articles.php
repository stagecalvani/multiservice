<?php

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }


/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'yreg_estate_template_no_articles_theme_setup' ) ) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_template_no_articles_theme_setup', 1 );
	function yreg_estate_template_no_articles_theme_setup() {
		yreg_estate_add_template(array(
			'layout' => 'no-articles',
			'mode'   => 'internal',
			'title'  => esc_html__('Nessun articolo trovato', 'yreg-estate')
		));
	}
}

// Template output
if ( !function_exists( 'yreg_estate_template_no_articles_output' ) ) {
	function yreg_estate_template_no_articles_output($post_options, $post_data) {
		?>
		<article class="post_item">
			<div class="post_content">
				<?php if(isset($_GET['ps_keyword'])): ?>
                                        <h2 class="post_title"><?php echo sprintf(esc_html__('Hai cercato: %s', 'yreg-estate'), get_search_query()); ?></h2>
				<?php else: ?>
                                        <h2 class="post_title">Nessun immobile trovato</h2>
				<?php endif; ?>
                                <p><?php esc_html_e('Siamo spiacenti, nessun articolo soddisfa i criteri di ricerca.', 'yreg-estate' ); ?>
				<p><?php echo wp_kses_data( sprintf(__('Per contattarci <a href="%s">%s</a>, altrimenti per tornare alla home <a href="%s">%s</a>.', 'yreg-estate'), esc_url(home_url('/contatti')),'clicca qui' , esc_url(home_url('/')),'clicca qui')); ?>
                                <?php echo trim(yreg_estate_sc_search(array('state'=>"fixed"))); ?>
			</div>	<!-- /.post_content -->
		</article>	<!-- /.post_item -->
		<?php
	}
}
?>