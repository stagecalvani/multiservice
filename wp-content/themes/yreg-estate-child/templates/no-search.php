<?php

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }


/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'yreg_estate_template_no_search_theme_setup' ) ) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_template_no_search_theme_setup', 1 );
	function yreg_estate_template_no_search_theme_setup() {
		yreg_estate_add_template(array(
			'layout' => 'no-search',
			'mode'   => 'internal',
			'title'  => esc_html__('Nessun immobile trovato', 'yreg-estate')
		));
	}
}

// Template output
if ( !function_exists( 'yreg_estate_template_no_search_output' ) ) {
	function yreg_estate_template_no_search_output($post_options, $post_data) {
		?>
		<article class="post_item">
			<div class="post_content">
                                <?php $search = $_GET['ps_keyword'];;?>
                                <?php if( isset($search) && $search!=''){ ?>
                                    <h2 class="post_title"><?php echo ("Hai cercato:");?></h2><h2 class="post_title_red"><?php echo(sprintf(esc_html__('%s', 'yreg-estate'),get_search_query()));?></h2>
                                    <p><?php esc_html_e('Siamo spiacenti, nessun immobile soddisfa i criteri di ricerca.', 'yreg-estate' ); ?>
                                    <br><?php esc_html_e('Inviaci un messaggio con le caratteritiche dell immobile che cerchi ti contatteremo appena disponibile.', 'yreg-estate'); ?></p>
                                    <p><?php echo (wp_kses_data( sprintf(__('Per contattarci <a href="%s">%s</a>, altrimenti per tornare al menu <a href="%s">%s</a>.', 'yreg-estate'), esc_url(home_url('/contatti')),'clicca qui' , esc_url(home_url('/')),'clicca qui'))); ?>
                                <?php }else{ ?>
                                    <p><?php esc_html_e('Siamo spiacenti, nessun immobile soddisfa i criteri di ricerca.', 'yreg-estate' ); ?>
                                    <br><?php esc_html_e('Inviaci un messaggio con le caratteritiche dell immobile che cerchi ti contatteremo appena disponibile.', 'yreg-estate'); ?></p>
                                    <p><?php echo (wp_kses_data( sprintf(__('Per contattarci <a href="%s">%s</a>, altrimenti per tornare al menu <a href="%s">%s</a>.', 'yreg-estate'), esc_url(home_url('/contatti')),'clicca qui' , esc_url(home_url('/')),'clicca qui'))); ?>
                                <?php }?>
                                <?php echo trim(yreg_estate_sc_search(array('state'=>"fixed"))); ?>
			</div>	<!-- /.post_content -->
		</article>	<!-- /.post_item -->
		<?php
	}
}
?>