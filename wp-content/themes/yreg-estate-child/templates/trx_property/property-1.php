<?php

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }


/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'yreg_estate_template_property_1_theme_setup' ) ) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_template_property_1_theme_setup', 1 );
	function yreg_estate_template_property_1_theme_setup() {
		yreg_estate_add_template(array(
			'layout' => 'property-1',
			'template' => 'property-1',
			'mode'   => 'property,blog',
			'need_terms' => 'true',
			'need_columns' => 'true',
			/*'container_classes' => 'sc_slider_nopagination sc_slider_controls sc_slider_controls_bottom',*/
			'title'  => esc_html__('Property / Blogger', 'yreg-estate'),
			'thumb_title'  => esc_html__('Large property image (crop)', 'yreg-estate'),
			'w'		 => 770,
			'h'      => 460
		));
	}
}

// Template output
if ( !function_exists( 'yreg_estate_template_property_1_output' ) ) {
	function yreg_estate_template_property_1_output($post_options, $post_data) {
		$show_title = !empty($post_data['post_title']);
		$parts = explode('_', $post_options['layout']);
		$style = $parts[0];
		$columns = max(1, min(12, empty($parts[1]) ? (!empty($post_options['columns_count']) ? $post_options['columns_count'] : 1) : (int) $parts[1]));
		
//		echo '<hr><pre>';
//		var_dump($post_data);
//		echo '</pre><hr>';

		if ( !isset($post_options['slider']) ) {
			$post_options['slider'] = 'no';
		}
		
		if ( !isset($post_options['tag_animation']) ) {
			$post_options['tag_animation'] = '';
		}
		
		if ( !isset($post_options['property_image']) ) {
			$post_options['property_image'] = $post_data['post_thumb'];
		}
		
		
		$property_status = get_post_meta( $post_data['post_id'], 'yreg_estate_property_status', true );
			$property_status_text = yreg_estate_property_status_text($property_status);
		
		if ( $property_status == 'rent' ) {
			$property_price_per = get_post_meta( $post_data['post_id'], 'yreg_estate_property_price_per', true );
		}
		if ( $property_status == 'negoziable' ) {
			$property_price_per = get_post_meta( $post_data['post_id'], 'yreg_estate_property_price_per2', true );
		}
		$property_price = get_post_meta( $post_data['post_id'], 'yreg_estate_property_price', true );
		$property_price_text = yreg_estate_property_delimiter_text($property_price, '');
		$property_price_sign = get_post_meta( $post_data['post_id'], 'yreg_estate_property_price_sign', true );
		$property_location = get_post_meta( $post_data['post_id'], 'yreg_estate_property_location', true );
		$property_address_1 = get_post_meta( $post_data['post_id'], 'yreg_estate_property_address_1', true );
		$property_address_2 = get_post_meta( $post_data['post_id'], 'yreg_estate_property_address_2', true );
		$property_type = get_post_meta( $post_data['post_id'], 'yreg_estate_property_type', true );
		$property_area = get_post_meta( $post_data['post_id'], 'yreg_estate_property_area', true );
		$property_area_text = yreg_estate_property_delimiter_text($property_area, '') . esc_html__(' mq', 'yreg-estate');
		$property_rooms = (int) get_post_meta( $post_data['post_id'], 'yreg_estate_property_rooms', true );
		$property_bedrooms = (int) get_post_meta( $post_data['post_id'], 'yreg_estate_property_bedrooms', true );
		$property_bathrooms = (int) get_post_meta( $post_data['post_id'], 'yreg_estate_property_bathrooms', true );
		$property_garages = (int) get_post_meta( $post_data['post_id'], 'yreg_estate_property_garages', true );
		$property_build = get_post_meta( $post_data['post_id'], 'yreg_estate_property_build', true );
//		$property_gallery = get_post_meta( $post_data['post_id'], 'yreg_estate_property_gallery', true );
		
		
		$property_price_box = '';
		if ( (int) $property_price != 0 ) {
			$property_price_box .= '<div class="property_price_box">';
			if ( !empty($property_price_sign) ) {
				$property_price_box .= '<span class="property_price_box_sign">'. esc_html($property_price_sign) .'</span>';
			}
			if ( !empty($property_price_text) ) {
				$property_price_box .= '<span class="property_price_box_price">'. esc_html($property_price_text) .'</span>';
			}
			if ( !empty($property_price_per) and ( $property_status == 'rent' ) ) {
				$property_price_box .= '<span class="property_price_box_per">' . esc_html__('/', 'yreg-estate') . esc_html($property_price_per) . '</span>';
			}
                        if ( !empty($property_price_per) and ( $property_status == 'negoziable' ) ) {
				$property_price_box .= '<span class="property_price_box_per">' . esc_html__('/', 'yreg-estate') . esc_html($property_price_per) . '</span>';
			}
			$property_price_box .= '</div>';
		}else{
                    $property_price_box .= '<div class="property_price_box">';
                    $property_price_box .= '<span class="property_price_box_per">'.array_keys(yreg_estate_get_property_list('property_price_per_list3'))[0].'</span>';
		    $property_price_box .= '</div>';
                }
		

		if (yreg_estate_param_is_on($post_options['slider'])) {
			?><div class="swiper-slide" data-style="<?php echo esc_attr($post_options['tag_css_wh']); ?>" style="<?php echo esc_attr($post_options['tag_css_wh']); ?>"><?php
		} else if ($columns > 1) {
			?><div class="column-1_<?php echo esc_attr($columns); ?> column_padding_bottom"><?php
		}
		?>
			<div<?php echo !empty($post_options['tag_id']) ? ' id="'.esc_attr($post_options['tag_id']).'"' : ''; ?> class="sc_property_item sc_property_item_<?php echo esc_attr($post_options['number']) . ($post_options['number'] % 2 == 1 ? ' odd' : ' even') . ($post_options['number'] == 1 ? ' first' : '').(!empty($post_options['tag_class']) ? ' '.esc_attr($post_options['tag_class']) : ''); ?>"<?php echo (!empty($post_options['tag_css']) ? ' style="'.esc_attr($post_options['tag_css']).'"' : '') . (!yreg_estate_param_is_off($post_options['tag_animation']) ? ' data-animation="'.esc_attr(yreg_estate_get_animation_classes($post_options['tag_animation'])).'"' : '');?>>
				<?php if ($post_options['property_image']) { ?>
					<div class="sc_property_image"><?php
						echo (!empty($post_data['post_link']) ? '<a href="'.esc_url($post_data['post_link']).'">'	: '')
							. $property_price_box
							. trim($post_options['property_image'])
							. (!empty($post_data['post_link']) ? '</a>' : ''); 
					?></div>
				<?php } 
                                if(($property_status_text)=='sale'){
                                    $property_status_text2='vendesi';
                                }
                                else if(($property_status_text)=='rent'){
                                    $property_status_text2='affittasi';
                                }
                                else if(($property_status_text)=='negoziable'){
                                    $property_status_text2='vendesi';
                                }
                ?>
				
				<div class="sc_property_info">
					<div class="sc_property_description"><?php echo  esc_html($property_status_text2).' '.esc_html($property_type); ?></div>
					
					<?php
					if ( (strlen($property_address_1)>0) or (strlen($property_address_2)>0)  ) {
					?>
					<div>
						<div class="sc_property_icon">
							<span class="icon-location"></span>
						</div>
						<div class="sc_property_title">
							<div class="sc_property_title_address_1">
								<?php 
								if ( !empty($post_data['post_link']) ) {
									echo '<a href="' . esc_url($post_data['post_link']) . '">' . esc_html($property_address_1) . '</a>';
								} else {
									echo esc_html($property_address_1);
								}
								?>
							</div>
							<div class="sc_property_title_address_2"><?php echo esc_html($property_address_2); ?></div>
						</div>
						<div class="cL"></div>
					</div>
					<?php } ?>
				</div>
				
				<div class="sc_property_info_list">
					
					<?php
						if ( $property_area_text != 0 ) echo '<span class="icon-building113">' . esc_html($property_area_text) . '</span>';
						if ( $property_bedrooms != 0 ) echo '<span class="icon-bed">' . esc_html($property_bedrooms) . '</span>';
						if ( $property_bathrooms != 0 ) echo '<span class="icon-bath">' . esc_html($property_bathrooms) . '</span>';
						if ( $property_garages != 0 ) echo '<span class="icon-warehouse">' . esc_html($property_garages) . '</span>';
					?>
					
				</div>
				
			</div>
		<?php
		if (yreg_estate_param_is_on($post_options['slider']) || $columns > 1) {
			?></div><?php
		}
	}
}
?>