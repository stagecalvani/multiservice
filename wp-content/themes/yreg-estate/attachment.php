<?php
/**
Template Name: Attachment page
 */
get_header(); 

while ( have_posts() ) { the_post();

	// Move yreg_estate_set_post_views to the javascript - counter will work under cache system
	if (yreg_estate_get_custom_option('use_ajax_views_counter')=='no') {
		yreg_estate_set_post_views(get_the_ID());
	}

	yreg_estate_show_post_layout(
		array(
			'layout' => 'attachment',
			'sidebar' => !yreg_estate_param_is_off(yreg_estate_get_custom_option('show_sidebar_main'))
		)
	);

}

get_footer();
?>