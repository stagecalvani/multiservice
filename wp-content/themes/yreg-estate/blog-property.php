<?php
/**
Template Name: Property list
 */
yreg_estate_storage_set('blog_filters', 'property');


if (!function_exists('yreg_estate_property_where')) {
	add_filter('posts_where', 'yreg_estate_property_where', 11, 2);
	function yreg_estate_property_where($where, $query) {
		global $wpdb; 
		if (is_admin() || $query->is_attachment || yreg_estate_strpos($where, "post_type = 'property'")===false) return $where;
		$where = str_replace('\\\\%', '%', $where);
		return $where;
	}
}

$bfp = array();

if ( isset($_GET['ps_keyword']) ) {
	$ps_keyword = htmlspecialchars(trim($_GET['ps_keyword']));
	if ( strlen($ps_keyword) >= 2 ) {
		$bfp['keyword'] = $ps_keyword;
	}
}
if ( isset($_GET['ps_status']) ) {
	$ps_status = htmlspecialchars(trim($_GET['ps_status']));
	if ( $ps_status != '-1' ) {
		$bfp['status'] = $ps_status;
	}
}
if ( isset($_GET['ps_location']) ) {
	$ps_location = htmlspecialchars(trim($_GET['ps_location']));
	if ( $ps_location != '-1' ) {
		$bfp['location'] = $ps_location;
	}
}
if ( isset($_GET['ps_type']) ) {
	$ps_type = htmlspecialchars(trim($_GET['ps_type']));
	if ( $ps_type != '-1' ) {
		$bfp['type'] = $ps_type;
	}
}
if ( isset($_GET['ps_style']) ) {
	$ps_style = htmlspecialchars(trim($_GET['ps_style']));
	if ( $ps_style != '-1' ) {
		$bfp['style'] = $ps_style;
	}
}
if ( isset($_GET['ps_rooms']) ) {
	$ps_rooms = (int) htmlspecialchars(trim($_GET['ps_rooms']));
	if ( $ps_rooms != -1 ) {
		$bfp['rooms'] = $ps_rooms;
	}
}
if ( isset($_GET['ps_bedrooms']) ) {
	$ps_bedrooms = (int) htmlspecialchars(trim($_GET['ps_bedrooms']));
	if ( $ps_bedrooms != -1 ) {
		$bfp['bedrooms'] = $ps_bedrooms;
	}
}
if ( isset($_GET['ps_bathrooms']) ) {
	$ps_bathrooms = (int) htmlspecialchars(trim($_GET['ps_bathrooms']));
	if ( $ps_bathrooms != -1 ) {
		$bfp['bathrooms'] = $ps_bathrooms;
	}
}
if ( isset($_GET['ps_garages']) ) {
	$ps_garages = (int) htmlspecialchars(trim($_GET['ps_garages']));
	if ( $ps_garages != -1 ) {
		$bfp['garages'] = $ps_garages;
	}
}
if ( isset($_GET['ps_area_min']) ) {
	$ps_area_min = (int) htmlspecialchars(trim($_GET['ps_area_min']));
	if ( $ps_area_min != 0 ) {
		$bfp['area_min'] = $ps_area_min;
	}
}
if ( isset($_GET['ps_area_max']) ) {
	$ps_area_max = (int) htmlspecialchars(trim($_GET['ps_area_max']));
	if ( $ps_area_max != ((int) yreg_estate_get_custom_option('property_search_area_max')) ) {
		$bfp['area_max'] = $ps_area_max;
	}
	
}
if ( isset($_GET['ps_price_min']) ) {
	$ps_price_min = (int) htmlspecialchars(trim($_GET['ps_price_min']));
	if ( $ps_price_min != 0 ) {
		$bfp['price_min'] = $ps_price_min;
	}
}
if ( isset($_GET['ps_price_max']) ) {
	$ps_price_max = (int) htmlspecialchars(trim($_GET['ps_price_max']));
	if ( $ps_price_max != ((int) yreg_estate_get_custom_option('property_search_price_max')) ) {
		$bfp['price_max'] = $ps_price_max;
	}
	
}
if ( isset($_GET['ps_amenities']) ) {
	$bfp['amenities'] = $_GET['ps_amenities'];
}
if ( isset($_GET['ps_options']) ) {
	$bfp['options'] = $_GET['ps_options'];
}
yreg_estate_storage_set('blog_filters_property', $bfp);
get_template_part('blog');
?>