<?php
/**
 * Theme sprecific functions and definitions
 */

/* Theme setup section
------------------------------------------------------------------- */

// Set the content width based on the theme's design and stylesheet.
if ( ! isset( $content_width ) ) $content_width = 1170; /* pixels */

// Add theme specific actions and filters
// Attention! Function were add theme specific actions and filters handlers must have priority 1
if ( !function_exists( 'yreg_estate_theme_setup' ) ) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_theme_setup', 1 );
	function yreg_estate_theme_setup() {

		// Register theme menus
		add_filter( 'yreg_estate_filter_add_theme_menus',		'yreg_estate_add_theme_menus' );

		// Register theme sidebars
		add_filter( 'yreg_estate_filter_add_theme_sidebars',	'yreg_estate_add_theme_sidebars' );

		// Set options for importer
		add_filter( 'yreg_estate_filter_importer_options',		'yreg_estate_set_importer_options' );

		
		// Add theme specified classes into the body
		add_filter( 'body_class', 'yreg_estate_body_classes' );
		
		// Set list of the theme required plugins
		yreg_estate_storage_set('required_plugins', array(
			'booked',
//			'buddypress',		// Attention! This slug used to install both BuddyPress and bbPress
//			'calcfields',
			'essgrids',
//			'instagram_feed',
			'instagram_widget',
//			'learndash',
//			'mailchimp',
//			'mega_main_menu',
//			'responsive_poll',
			'revslider',
//			'tribe_events',
//			'trx_donations',
			'trx_utils',
			'visual_composer',
//			'woocommerce',
//			'html5_jquery_audio_player'
			)
		);
		
	}
}


// Add theme specified classes into the body
if ( !function_exists('yreg_estate_body_classes') ) {
	function yreg_estate_body_classes( $classes ) {

		$classes[] = 'yreg_estate_body';
		$classes[] = 'body_style_' . trim(yreg_estate_get_custom_option('body_style'));
		$classes[] = 'body_' . (yreg_estate_get_custom_option('body_filled')=='yes' ? 'filled' : 'transparent');
		$classes[] = 'theme_skin_' . trim(yreg_estate_get_custom_option('theme_skin'));
		$classes[] = 'article_style_' . trim(yreg_estate_get_custom_option('article_style'));
		
		$blog_style = yreg_estate_get_custom_option(is_singular() && !yreg_estate_storage_get('blog_streampage') ? 'single_style' : 'blog_style');
		$classes[] = 'layout_' . trim($blog_style);
		$classes[] = 'template_' . trim(yreg_estate_get_template_name($blog_style));
		
		$body_scheme = yreg_estate_get_custom_option('body_scheme');
		if (empty($body_scheme)  || yreg_estate_is_inherit_option($body_scheme)) $body_scheme = 'original';
		$classes[] = 'scheme_' . $body_scheme;

		$top_panel_position = yreg_estate_get_custom_option('top_panel_position');
		if (!yreg_estate_param_is_off($top_panel_position)) {
			$classes[] = 'top_panel_show';
			$classes[] = 'top_panel_' . trim($top_panel_position);
		} else 
			$classes[] = 'top_panel_hide';
		$classes[] = yreg_estate_get_sidebar_class();

		if (yreg_estate_get_custom_option('show_video_bg')=='yes' && (yreg_estate_get_custom_option('video_bg_youtube_code')!='' || yreg_estate_get_custom_option('video_bg_url')!=''))
			$classes[] = 'video_bg_show';

		if (yreg_estate_get_theme_option('page_preloader')!='')
			$classes[] = 'preloader';

		return $classes;
	}
}


// Add/Remove theme nav menus
if ( !function_exists( 'yreg_estate_add_theme_menus' ) ) {
	//add_filter( 'yreg_estate_filter_add_theme_menus', 'yreg_estate_add_theme_menus' );
	function yreg_estate_add_theme_menus($menus) {
		//For example:
		//$menus['menu_footer'] = esc_html__('Footer Menu', 'yreg-estate');
		//if (isset($menus['menu_panel'])) unset($menus['menu_panel']);
		return $menus;
	}
}


// Add theme specific widgetized areas
if ( !function_exists( 'yreg_estate_add_theme_sidebars' ) ) {
	//add_filter( 'yreg_estate_filter_add_theme_sidebars',	'yreg_estate_add_theme_sidebars' );
	function yreg_estate_add_theme_sidebars($sidebars=array()) {
		if (is_array($sidebars)) {
			$theme_sidebars = array(
				'sidebar_main'		=> esc_html__( 'Main Sidebar', 'yreg-estate' ),
				'sidebar_footer'	=> esc_html__( 'Footer Sidebar', 'yreg-estate' )
			);
			if (function_exists('yreg_estate_exists_woocommerce') && yreg_estate_exists_woocommerce()) {
				$theme_sidebars['sidebar_cart']  = esc_html__( 'WooCommerce Cart Sidebar', 'yreg-estate' );
			}
			$sidebars = array_merge($theme_sidebars, $sidebars);
		}
		return $sidebars;
	}
}


// Set theme specific importer options
if ( !function_exists( 'yreg_estate_set_importer_options' ) ) {
	//add_filter( 'yreg_estate_filter_importer_options',	'yreg_estate_set_importer_options' );
	function yreg_estate_set_importer_options($options=array()) {
		if (is_array($options)) {
			$options['debug'] = yreg_estate_get_theme_option('debug_mode')=='yes';
			$options['domain_dev'] = 'estate.dv.axiomthemes.com';
			$options['domain_demo'] = 'estate.axiomthemes.com';
			$options['menus'] = array(
				'menu-main'	  => esc_html__('Main menu', 'yreg-estate'),
				'menu-user'	  => esc_html__('User menu', 'yreg-estate'),
				'menu-footer' => esc_html__('Footer menu', 'yreg-estate')
			);
			$options['file_with_attachments'] = array(				// Array with names of the attachments
//				'http://estate.axiomthemes.com/wp-content/imports/uploads.zip',		// Name of the remote file with attachments
	//			'demo/uploads.zip',									// Name of the local file with attachments
				'http://estate.axiomthemes.com/wp-content/imports/uploads.001',
				'http://estate.axiomthemes.com/wp-content/imports/uploads.002',
				'http://estate.axiomthemes.com/wp-content/imports/uploads.003',
				'http://estate.axiomthemes.com/wp-content/imports/uploads.004',
				'http://estate.axiomthemes.com/wp-content/imports/uploads.005',
				'http://estate.axiomthemes.com/wp-content/imports/uploads.006',
				'http://estate.axiomthemes.com/wp-content/imports/uploads.007',
				'http://estate.axiomthemes.com/wp-content/imports/uploads.008',
				'http://estate.axiomthemes.com/wp-content/imports/uploads.009',
				'http://estate.axiomthemes.com/wp-content/imports/uploads.010',
				'http://estate.axiomthemes.com/wp-content/imports/uploads.011',
				'http://estate.axiomthemes.com/wp-content/imports/uploads.012'
//				'http://estate.axiomthemes.com/wp-content/imports/uploads.013'
			);
			$options['attachments_by_parts'] = true;				// Files above are parts of single file - large media archive. They are must be concatenated in one file before unpacking
		}
		return $options;
	}
}


/* Include framework core files
------------------------------------------------------------------- */
// If now is WP Heartbeat call - skip loading theme core files (to reduce server and DB uploads)
// Remove comments below only if your theme not work with own post types and/or taxonomies
//if (!isset($_POST['action']) || $_POST['action']!="heartbeat") {
	get_template_part('fw/loader');
//}
?>