<?php
/**
 * YREG_ESTATE Framework: date and time manipulations
 *
 * @package	yreg_estate
 * @since	yreg_estate 1.0
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }

// Convert date from MySQL format (YYYY-mm-dd) to Date (dd.mm.YYYY)
if (!function_exists('yreg_estate_sql_to_date')) {
	function yreg_estate_sql_to_date($str) {
		return (trim($str)=='' || trim($str)=='0000-00-00' ? '' : trim(yreg_estate_substr($str,8,2).'.'.yreg_estate_substr($str,5,2).'.'.yreg_estate_substr($str,0,4).' '.yreg_estate_substr($str,11)));
	}
}

// Convert date from Date format (dd.mm.YYYY) to MySQL format (YYYY-mm-dd)
if (!function_exists('yreg_estate_date_to_sql')) {
	function yreg_estate_date_to_sql($str) {
		if (trim($str)=='') return '';
		$str = strtr(trim($str),'/\-,','....');
		if (trim($str)=='00.00.0000' || trim($str)=='00.00.00') return '';
		$pos = yreg_estate_strpos($str,'.');
		$d=trim(yreg_estate_substr($str,0,$pos));
		$str=yreg_estate_substr($str,$pos+1);
		$pos = yreg_estate_strpos($str,'.');
		$m=trim(yreg_estate_substr($str,0,$pos));
		$y=trim(yreg_estate_substr($str,$pos+1));
		$y=($y<50?$y+2000:($y<1900?$y+1900:$y));
		return ''.($y).'-'.(yreg_estate_strlen($m)<2?'0':'').($m).'-'.(yreg_estate_strlen($d)<2?'0':'').($d);
	}
}

// Return difference or date
if (!function_exists('yreg_estate_get_date_or_difference')) {
	function yreg_estate_get_date_or_difference($dt1, $dt2=null, $max_days=-1) {
		static $gmt_offset = 999;
		if ($gmt_offset==999) $gmt_offset = (int) get_option('gmt_offset');
		if ($max_days < 0) $max_days = yreg_estate_get_theme_option('show_date_after', 30);
		if ($dt2 == null) $dt2 = date('Y-m-d H:i:s');
		$dt2n = strtotime($dt2)+$gmt_offset*3600;
		$dt1n = strtotime($dt1);
		$diff = $dt2n - $dt1n;
		$days = floor($diff / (24*3600));
		if (abs($days) < $max_days)
			return sprintf($days >= 0 ? esc_html__('%s ago', 'yreg-estate') : esc_html__('in %s', 'yreg-estate'), yreg_estate_get_date_difference($days >= 0 ? $dt1 : $dt2, $days >= 0 ? $dt2 : $dt1));
		else
			return yreg_estate_get_date_translations(date(get_option('date_format'), $dt1n));
	}
}

// Difference between two dates
if (!function_exists('yreg_estate_get_date_difference')) {
	function yreg_estate_get_date_difference($dt1, $dt2=null, $short=1, $sec = false) {
		static $gmt_offset = 999;
		if ($gmt_offset==999) $gmt_offset = (int) get_option('gmt_offset');
		if ($dt2 == null) $dt2 = time()+$gmt_offset*3600;
		else $dt2 = strtotime($dt2)+$gmt_offset*3600;
		$dt1 = strtotime($dt1);
		$diff = $dt2 - $dt1;
		$days = floor($diff / (24*3600));
		$months = floor($days / 30);
		$diff -= $days * 24 * 3600;
		$hours = floor($diff / 3600);
		$diff -= $hours * 3600;
		$min = floor($diff / 60);
		$diff -= $min * 60;
		$rez = '';
		if ($months > 0 && $short == 2)
			$rez .= ($rez!='' ? ' ' : '') . sprintf($months > 1 ? esc_html__('%s months', 'yreg-estate') : esc_html__('%s month', 'yreg-estate'), $months);
		if ($days > 0 && ($short < 2 || $rez==''))
			$rez .= ($rez!='' ? ' ' : '') . sprintf($days > 1 ? esc_html__('%s days', 'yreg-estate') : esc_html__('%s day', 'yreg-estate'), $days);
		if ((!$short || $rez=='') && $hours > 0)
			$rez .= ($rez!='' ? ' ' : '') . sprintf($hours > 1 ? esc_html__('%s hours', 'yreg-estate') : esc_html__('%s hour', 'yreg-estate'), $hours);
		if ((!$short || $rez=='') && $min > 0)
			$rez .= ($rez!='' ? ' ' : '') . sprintf($min > 1 ? esc_html__('%s minutes', 'yreg-estate') : esc_html__('%s minute', 'yreg-estate'), $min);
		if ($sec || $rez=='')
			$rez .=  $rez!='' || $sec ? (' ' . sprintf($diff > 1 ? esc_html__('%s seconds', 'yreg-estate') : esc_html__('%s second', 'yreg-estate'), $diff)) : esc_html__('less then minute', 'yreg-estate');
		return $rez;
	}
}

// Prepare month names in date for translation
if (!function_exists('yreg_estate_get_date_translations')) {
	function yreg_estate_get_date_translations($dt) {
		return str_replace(
			array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December',
				  'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'),
			array(
				esc_html__('January', 'yreg-estate'),
				esc_html__('February', 'yreg-estate'),
				esc_html__('March', 'yreg-estate'),
				esc_html__('April', 'yreg-estate'),
				esc_html__('May', 'yreg-estate'),
				esc_html__('June', 'yreg-estate'),
				esc_html__('July', 'yreg-estate'),
				esc_html__('August', 'yreg-estate'),
				esc_html__('September', 'yreg-estate'),
				esc_html__('October', 'yreg-estate'),
				esc_html__('November', 'yreg-estate'),
				esc_html__('December', 'yreg-estate'),
				esc_html__('Jan', 'yreg-estate'),
				esc_html__('Feb', 'yreg-estate'),
				esc_html__('Mar', 'yreg-estate'),
				esc_html__('Apr', 'yreg-estate'),
				esc_html__('May', 'yreg-estate'),
				esc_html__('Jun', 'yreg-estate'),
				esc_html__('Jul', 'yreg-estate'),
				esc_html__('Aug', 'yreg-estate'),
				esc_html__('Sep', 'yreg-estate'),
				esc_html__('Oct', 'yreg-estate'),
				esc_html__('Nov', 'yreg-estate'),
				esc_html__('Dec', 'yreg-estate'),
			),
			$dt);
	}
}
?>