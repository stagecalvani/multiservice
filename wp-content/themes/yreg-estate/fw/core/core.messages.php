<?php
/**
 * YREG_ESTATE Framework: messages subsystem
 *
 * @package	yreg_estate
 * @since	yreg_estate 1.0
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }

// Theme init
if (!function_exists('yreg_estate_messages_theme_setup')) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_messages_theme_setup' );
	function yreg_estate_messages_theme_setup() {
		// Core messages strings
		add_action('yreg_estate_action_add_scripts_inline', 'yreg_estate_messages_add_scripts_inline');
	}
}


/* Session messages
------------------------------------------------------------------------------------- */

if (!function_exists('yreg_estate_get_error_msg')) {
	function yreg_estate_get_error_msg() {
		return yreg_estate_storage_get('error_msg');
	}
}

if (!function_exists('yreg_estate_set_error_msg')) {
	function yreg_estate_set_error_msg($msg) {
		$msg2 = yreg_estate_get_error_msg();
		yreg_estate_storage_set('error_msg', trim($msg2) . ($msg2=='' ? '' : '<br />') . trim($msg));
	}
}

if (!function_exists('yreg_estate_get_success_msg')) {
	function yreg_estate_get_success_msg() {
		return yreg_estate_storage_get('success_msg');
	}
}

if (!function_exists('yreg_estate_set_success_msg')) {
	function yreg_estate_set_success_msg($msg) {
		$msg2 = yreg_estate_get_success_msg();
		yreg_estate_storage_set('success_msg', trim($msg2) . ($msg2=='' ? '' : '<br />') . trim($msg));
	}
}

if (!function_exists('yreg_estate_get_notice_msg')) {
	function yreg_estate_get_notice_msg() {
		return yreg_estate_storage_get('notice_msg');
	}
}

if (!function_exists('yreg_estate_set_notice_msg')) {
	function yreg_estate_set_notice_msg($msg) {
		$msg2 = yreg_estate_get_notice_msg();
		yreg_estate_storage_set('notice_msg', trim($msg2) . ($msg2=='' ? '' : '<br />') . trim($msg));
	}
}


/* System messages (save when page reload)
------------------------------------------------------------------------------------- */
if (!function_exists('yreg_estate_set_system_message')) {
	function yreg_estate_set_system_message($msg, $status='info', $hdr='') {
		update_option('yreg_estate_message', array('message' => $msg, 'status' => $status, 'header' => $hdr));
	}
}

if (!function_exists('yreg_estate_get_system_message')) {
	function yreg_estate_get_system_message($del=false) {
		$msg = get_option('yreg_estate_message', false);
		if (!$msg)
			$msg = array('message' => '', 'status' => '', 'header' => '');
		else if ($del)
			yreg_estate_del_system_message();
		return $msg;
	}
}

if (!function_exists('yreg_estate_del_system_message')) {
	function yreg_estate_del_system_message() {
		delete_option('yreg_estate_message');
	}
}


/* Messages strings
------------------------------------------------------------------------------------- */

if (!function_exists('yreg_estate_messages_add_scripts_inline')) {
	function yreg_estate_messages_add_scripts_inline() {
		echo '<script type="text/javascript">'
			
			. "if (typeof YREG_ESTATE_STORAGE == 'undefined') var YREG_ESTATE_STORAGE = {};"
			
			// Strings for translation
			. 'YREG_ESTATE_STORAGE["strings"] = {'
				. 'ajax_error: 			"' . addslashes(esc_html__('Invalid server answer', 'yreg-estate')) . '",'
				. 'bookmark_add: 		"' . addslashes(esc_html__('Add the bookmark', 'yreg-estate')) . '",'
				. 'bookmark_added:		"' . addslashes(esc_html__('Current page has been successfully added to the bookmarks. You can see it in the right panel on the tab \'Bookmarks\'', 'yreg-estate')) . '",'
				. 'bookmark_del: 		"' . addslashes(esc_html__('Delete this bookmark', 'yreg-estate')) . '",'
				. 'bookmark_title:		"' . addslashes(esc_html__('Enter bookmark title', 'yreg-estate')) . '",'
				. 'bookmark_exists:		"' . addslashes(esc_html__('Current page already exists in the bookmarks list', 'yreg-estate')) . '",'
				. 'search_error:		"' . addslashes(esc_html__('Error occurs in AJAX search! Please, type your query and press search icon for the traditional search way.', 'yreg-estate')) . '",'
				. 'email_confirm:		"' . addslashes(esc_html__('On the e-mail address "%s" we sent a confirmation email. Please, open it and click on the link.', 'yreg-estate')) . '",'
				. 'reviews_vote:		"' . addslashes(esc_html__('Thanks for your vote! New average rating is:', 'yreg-estate')) . '",'
				. 'reviews_error:		"' . addslashes(esc_html__('Error saving your vote! Please, try again later.', 'yreg-estate')) . '",'
				. 'error_like:			"' . addslashes(esc_html__('Error saving your like! Please, try again later.', 'yreg-estate')) . '",'
				. 'error_global:		"' . addslashes(esc_html__('Global error text', 'yreg-estate')) . '",'
				. 'name_empty:			"' . addslashes(esc_html__('The name can\'t be empty', 'yreg-estate')) . '",'
				. 'name_long:			"' . addslashes(esc_html__('Too long name', 'yreg-estate')) . '",'
				. 'email_empty:			"' . addslashes(esc_html__('Too short (or empty) email address', 'yreg-estate')) . '",'
				. 'email_long:			"' . addslashes(esc_html__('Too long email address', 'yreg-estate')) . '",'
				. 'email_not_valid:		"' . addslashes(esc_html__('Invalid email address', 'yreg-estate')) . '",'
				. 'subject_empty:		"' . addslashes(esc_html__('The subject can\'t be empty', 'yreg-estate')) . '",'
				. 'subject_long:		"' . addslashes(esc_html__('Too long subject', 'yreg-estate')) . '",'
				. 'text_empty:			"' . addslashes(esc_html__('The message text can\'t be empty', 'yreg-estate')) . '",'
				. 'text_long:			"' . addslashes(esc_html__('Too long message text', 'yreg-estate')) . '",'
				. 'send_complete:		"' . addslashes(esc_html__("Send message complete!", 'yreg-estate')) . '",'
				. 'send_error:			"' . addslashes(esc_html__('Transmit failed!', 'yreg-estate')) . '",'
				. 'login_empty:			"' . addslashes(esc_html__('The Login field can\'t be empty', 'yreg-estate')) . '",'
				. 'login_long:			"' . addslashes(esc_html__('Too long login field', 'yreg-estate')) . '",'
				. 'login_success:		"' . addslashes(esc_html__('Login success! The page will be reloaded in 3 sec.', 'yreg-estate')) . '",'
				. 'login_failed:		"' . addslashes(esc_html__('Login failed!', 'yreg-estate')) . '",'
				. 'password_empty:		"' . addslashes(esc_html__('The password can\'t be empty and shorter then 4 characters', 'yreg-estate')) . '",'
				. 'password_long:		"' . addslashes(esc_html__('Too long password', 'yreg-estate')) . '",'
				. 'password_not_equal:	"' . addslashes(esc_html__('The passwords in both fields are not equal', 'yreg-estate')) . '",'
				. 'registration_success:"' . addslashes(esc_html__('Registration success! Please log in!', 'yreg-estate')) . '",'
				. 'registration_failed:	"' . addslashes(esc_html__('Registration failed!', 'yreg-estate')) . '",'
				. 'geocode_error:		"' . addslashes(esc_html__('Geocode was not successful for the following reason:', 'yreg-estate')) . '",'
				. 'googlemap_not_avail:	"' . addslashes(esc_html__('Google map API not available!', 'yreg-estate')) . '",'
				. 'editor_save_success:	"' . addslashes(esc_html__("Post content saved!", 'yreg-estate')) . '",'
				. 'editor_save_error:	"' . addslashes(esc_html__("Error saving post data!", 'yreg-estate')) . '",'
				. 'editor_delete_post:	"' . addslashes(esc_html__("You really want to delete the current post?", 'yreg-estate')) . '",'
				. 'editor_delete_post_header:"' . addslashes(esc_html__("Delete post", 'yreg-estate')) . '",'
				. 'editor_delete_success:	"' . addslashes(esc_html__("Post deleted!", 'yreg-estate')) . '",'
				. 'editor_delete_error:		"' . addslashes(esc_html__("Error deleting post!", 'yreg-estate')) . '",'
				. 'editor_caption_cancel:	"' . addslashes(esc_html__('Cancel', 'yreg-estate')) . '",'
				. 'editor_caption_close:	"' . addslashes(esc_html__('Close', 'yreg-estate')) . '"'
				. '};'
			
			. '</script>';
	}
}
?>