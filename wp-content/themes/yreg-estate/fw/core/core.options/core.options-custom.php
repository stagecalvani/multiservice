<?php
/**
 * YREG_ESTATE Framework: Theme options custom fields
 *
 * @package	yreg_estate
 * @since	yreg_estate 1.0
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }


/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'yreg_estate_options_custom_theme_setup' ) ) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_options_custom_theme_setup' );
	function yreg_estate_options_custom_theme_setup() {

		if ( is_admin() ) {
			add_action("admin_enqueue_scripts",	'yreg_estate_options_custom_load_scripts');
		}
		
	}
}

// Load required styles and scripts for custom options fields
if ( !function_exists( 'yreg_estate_options_custom_load_scripts' ) ) {
	//add_action("admin_enqueue_scripts", 'yreg_estate_options_custom_load_scripts');
	function yreg_estate_options_custom_load_scripts() {
		yreg_estate_enqueue_script( 'yreg_estate-options-custom-script',	yreg_estate_get_file_url('core/core.options/js/core.options-custom.js'), array(), null, true );	
	}
}


// Show theme specific fields in Post (and Page) options
if ( !function_exists( 'yreg_estate_show_custom_field' ) ) {
	function yreg_estate_show_custom_field($id, $field, $value) {
		$output = '';
		switch ($field['type']) {
			case 'reviews':
				$output .= '<div class="reviews_block">' . trim(yreg_estate_reviews_get_markup($field, $value, true)) . '</div>';
				break;
	
			case 'mediamanager':
				wp_enqueue_media( );
				$output .= '<a id="'.esc_attr($id).'" class="button mediamanager yreg_estate_media_selector"
					data-param="' . esc_attr($id) . '"
					data-choose="'.esc_attr(isset($field['multiple']) && $field['multiple'] ? esc_html__( 'Choose Images', 'yreg-estate') : esc_html__( 'Choose Image', 'yreg-estate')).'"
					data-update="'.esc_attr(isset($field['multiple']) && $field['multiple'] ? esc_html__( 'Add to Gallery', 'yreg-estate') : esc_html__( 'Choose Image', 'yreg-estate')).'"
					data-multiple="'.esc_attr(isset($field['multiple']) && $field['multiple'] ? 'true' : 'false').'"
					data-linked-field="'.esc_attr($field['media_field_id']).'"
					>' . (isset($field['multiple']) && $field['multiple'] ? esc_html__( 'Choose Images', 'yreg-estate') : esc_html__( 'Choose Image', 'yreg-estate')) . '</a>';
				break;
		}
		return apply_filters('yreg_estate_filter_show_custom_field', $output, $id, $field, $value);
	}
}
?>