<?php
/**
 * YREG_ESTATE Framework: shortcodes manipulations
 *
 * @package	yreg_estate
 * @since	yreg_estate 1.0
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }

// Theme init
if (!function_exists('yreg_estate_sc_theme_setup')) {
	add_action( 'yreg_estate_action_init_theme', 'yreg_estate_sc_theme_setup', 1 );
	function yreg_estate_sc_theme_setup() {
		// Add sc stylesheets
		add_action('yreg_estate_action_add_styles', 'yreg_estate_sc_add_styles', 1);
	}
}

if (!function_exists('yreg_estate_sc_theme_setup2')) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_sc_theme_setup2' );
	function yreg_estate_sc_theme_setup2() {

		if ( !is_admin() || isset($_POST['action']) ) {
			// Enable/disable shortcodes in excerpt
			add_filter('the_excerpt', 					'yreg_estate_sc_excerpt_shortcodes');
	
			// Prepare shortcodes in the content
			if (function_exists('yreg_estate_sc_prepare_content')) yreg_estate_sc_prepare_content();
		}

		// Add init script into shortcodes output in VC frontend editor
		add_filter('yreg_estate_shortcode_output', 'yreg_estate_sc_add_scripts', 10, 4);

		// AJAX: Send contact form data
		add_action('wp_ajax_send_form',			'yreg_estate_sc_form_send');
		add_action('wp_ajax_nopriv_send_form',	'yreg_estate_sc_form_send');

		// Show shortcodes list in admin editor
		add_action('media_buttons',				'yreg_estate_sc_selector_add_in_toolbar', 11);

	}
}


// Register shortcodes styles
if ( !function_exists( 'yreg_estate_sc_add_styles' ) ) {
	//add_action('yreg_estate_action_add_styles', 'yreg_estate_sc_add_styles', 1);
	function yreg_estate_sc_add_styles() {
		// Shortcodes
		yreg_estate_enqueue_style( 'yreg_estate-shortcodes-style',	yreg_estate_get_file_url('shortcodes/theme.shortcodes.css'), array(), null );
	}
}


// Register shortcodes init scripts
if ( !function_exists( 'yreg_estate_sc_add_scripts' ) ) {
	//add_filter('yreg_estate_shortcode_output', 'yreg_estate_sc_add_scripts', 10, 4);
	function yreg_estate_sc_add_scripts($output, $tag='', $atts=array(), $content='') {

		if (yreg_estate_storage_empty('shortcodes_scripts_added')) {
			yreg_estate_storage_set('shortcodes_scripts_added', true);
			//yreg_estate_enqueue_style( 'yreg_estate-shortcodes-style', yreg_estate_get_file_url('shortcodes/theme.shortcodes.css'), array(), null );
			yreg_estate_enqueue_script( 'yreg_estate-shortcodes-script', yreg_estate_get_file_url('shortcodes/theme.shortcodes.js'), array('jquery'), null, true );	
		}
		
		return $output;
	}
}


/* Prepare text for shortcodes
-------------------------------------------------------------------------------- */

// Prepare shortcodes in content
if (!function_exists('yreg_estate_sc_prepare_content')) {
	function yreg_estate_sc_prepare_content() {
		if (function_exists('yreg_estate_sc_clear_around')) {
			$filters = array(
				array('yreg_estate', 'sc', 'clear', 'around'),
				array('widget', 'text'),
				array('the', 'excerpt'),
				array('the', 'content')
			);
			if (function_exists('yreg_estate_exists_woocommerce') && yreg_estate_exists_woocommerce()) {
				$filters[] = array('woocommerce', 'template', 'single', 'excerpt');
				$filters[] = array('woocommerce', 'short', 'description');
			}
			if (is_array($filters) && count($filters) > 0) {
				foreach ($filters as $flt)
					add_filter(join('_', $flt), 'yreg_estate_sc_clear_around', 1);	// Priority 1 to clear spaces before do_shortcodes()
			}
		}
	}
}

// Enable/Disable shortcodes in the excerpt
if (!function_exists('yreg_estate_sc_excerpt_shortcodes')) {
	function yreg_estate_sc_excerpt_shortcodes($content) {
		if (!empty($content)) {
			$content = do_shortcode($content);
			//$content = strip_shortcodes($content);
		}
		return $content;
	}
}



/*
// Remove spaces and line breaks between close and open shortcode brackets ][:
[trx_columns]
	[trx_column_item]Column text ...[/trx_column_item]
	[trx_column_item]Column text ...[/trx_column_item]
	[trx_column_item]Column text ...[/trx_column_item]
[/trx_columns]

convert to

[trx_columns][trx_column_item]Column text ...[/trx_column_item][trx_column_item]Column text ...[/trx_column_item][trx_column_item]Column text ...[/trx_column_item][/trx_columns]
*/
if (!function_exists('yreg_estate_sc_clear_around')) {
	function yreg_estate_sc_clear_around($content) {
		if (!empty($content)) $content = preg_replace("/\](\s|\n|\r)*\[/", "][", $content);
		return $content;
	}
}






/* Shortcodes support utils
---------------------------------------------------------------------- */

// YREG_ESTATE shortcodes load scripts
if (!function_exists('yreg_estate_sc_load_scripts')) {
	function yreg_estate_sc_load_scripts() {
		yreg_estate_enqueue_script( 'yreg_estate-shortcodes-script', yreg_estate_get_file_url('core/core.shortcodes/shortcodes_admin.js'), array('jquery'), null, true );
		yreg_estate_enqueue_script( 'yreg_estate-selection-script',  yreg_estate_get_file_url('js/jquery.selection.js'), array('jquery'), null, true );
	}
}

// YREG_ESTATE shortcodes prepare scripts
if (!function_exists('yreg_estate_sc_prepare_scripts')) {
	function yreg_estate_sc_prepare_scripts() {
		if (!yreg_estate_storage_isset('shortcodes_prepared')) {
			yreg_estate_storage_set('shortcodes_prepared', true);
			$json_parse_func = 'eval';	// 'JSON.parse'
			?>
			<script type="text/javascript">
				jQuery(document).ready(function(){
					try {
						YREG_ESTATE_STORAGE['shortcodes'] = <?php echo trim($json_parse_func); ?>(<?php echo json_encode( yreg_estate_array_prepare_to_json(yreg_estate_storage_get('shortcodes')) ); ?>);
					} catch (e) {}
					YREG_ESTATE_STORAGE['shortcodes_cp'] = '<?php echo is_admin() ? (!yreg_estate_storage_empty('to_colorpicker') ? yreg_estate_storage_get('to_colorpicker') : 'wp') : 'custom'; ?>';	// wp | tiny | custom
				});
			</script>
			<?php
		}
	}
}

// Show shortcodes list in admin editor
if (!function_exists('yreg_estate_sc_selector_add_in_toolbar')) {
	//add_action('media_buttons','yreg_estate_sc_selector_add_in_toolbar', 11);
	function yreg_estate_sc_selector_add_in_toolbar(){

		if ( !yreg_estate_options_is_used() ) return;

		yreg_estate_sc_load_scripts();
		yreg_estate_sc_prepare_scripts();

		$shortcodes = yreg_estate_storage_get('shortcodes');
		$shortcodes_list = '<select class="sc_selector"><option value="">&nbsp;'.esc_html__('- Select Shortcode -', 'yreg-estate').'&nbsp;</option>';

		if (is_array($shortcodes) && count($shortcodes) > 0) {
			foreach ($shortcodes as $idx => $sc) {
				$shortcodes_list .= '<option value="'.esc_attr($idx).'" title="'.esc_attr($sc['desc']).'">'.esc_html($sc['title']).'</option>';
			}
		}

		$shortcodes_list .= '</select>';

		echo trim($shortcodes_list);
	}
}

// YREG_ESTATE shortcodes builder settings
get_template_part(yreg_estate_get_file_slug('core/core.shortcodes/shortcodes_settings.php'));

// VC shortcodes settings
if ( class_exists('WPBakeryShortCode') ) {
	get_template_part(yreg_estate_get_file_slug('core/core.shortcodes/shortcodes_vc.php'));
}

// YREG_ESTATE shortcodes implementation
yreg_estate_autoload_folder( 'shortcodes/trx_basic' );
yreg_estate_autoload_folder( 'shortcodes/trx_optional' );
?>