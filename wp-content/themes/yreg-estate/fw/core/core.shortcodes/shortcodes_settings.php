<?php

// Check if shortcodes settings are now used
if ( !function_exists( 'yreg_estate_shortcodes_is_used' ) ) {
	function yreg_estate_shortcodes_is_used() {
		return yreg_estate_options_is_used() 															// All modes when Theme Options are used
			|| (is_admin() && isset($_POST['action']) 
					&& in_array($_POST['action'], array('vc_edit_form', 'wpb_show_edit_form')))		// AJAX query when save post/page
			|| (is_admin() && yreg_estate_strpos($_SERVER['REQUEST_URI'], 'vc-roles')!==false)			// VC Role Manager
			|| (function_exists('yreg_estate_vc_is_frontend') && yreg_estate_vc_is_frontend());			// VC Frontend editor mode
	}
}

// Width and height params
if ( !function_exists( 'yreg_estate_shortcodes_width' ) ) {
	function yreg_estate_shortcodes_width($w="") {
		return array(
			"title" => esc_html__("Width", 'yreg-estate'),
			"divider" => true,
			"value" => $w,
			"type" => "text"
		);
	}
}
if ( !function_exists( 'yreg_estate_shortcodes_height' ) ) {
	function yreg_estate_shortcodes_height($h='') {
		return array(
			"title" => esc_html__("Height", 'yreg-estate'),
			"desc" => wp_kses_data( __("Width and height of the element", 'yreg-estate') ),
			"value" => $h,
			"type" => "text"
		);
	}
}

// Return sc_param value
if ( !function_exists( 'yreg_estate_get_sc_param' ) ) {
	function yreg_estate_get_sc_param($prm) {
		return yreg_estate_storage_get_array('sc_params', $prm);
	}
}

// Set sc_param value
if ( !function_exists( 'yreg_estate_set_sc_param' ) ) {
	function yreg_estate_set_sc_param($prm, $val) {
		yreg_estate_storage_set_array('sc_params', $prm, $val);
	}
}

// Add sc settings in the sc list
if ( !function_exists( 'yreg_estate_sc_map' ) ) {
	function yreg_estate_sc_map($sc_name, $sc_settings) {
		yreg_estate_storage_set_array('shortcodes', $sc_name, $sc_settings);
	}
}

// Add sc settings in the sc list after the key
if ( !function_exists( 'yreg_estate_sc_map_after' ) ) {
	function yreg_estate_sc_map_after($after, $sc_name, $sc_settings='') {
		yreg_estate_storage_set_array_after('shortcodes', $after, $sc_name, $sc_settings);
	}
}

// Add sc settings in the sc list before the key
if ( !function_exists( 'yreg_estate_sc_map_before' ) ) {
	function yreg_estate_sc_map_before($before, $sc_name, $sc_settings='') {
		yreg_estate_storage_set_array_before('shortcodes', $before, $sc_name, $sc_settings);
	}
}

// Compare two shortcodes by title
if ( !function_exists( 'yreg_estate_compare_sc_title' ) ) {
	function yreg_estate_compare_sc_title($a, $b) {
		return strcmp($a['title'], $b['title']);
	}
}



/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'yreg_estate_shortcodes_settings_theme_setup' ) ) {
//	if ( yreg_estate_vc_is_frontend() )
	if ( (isset($_GET['vc_editable']) && $_GET['vc_editable']=='true') || (isset($_GET['vc_action']) && $_GET['vc_action']=='vc_inline') )
		add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_shortcodes_settings_theme_setup', 20 );
	else
		add_action( 'yreg_estate_action_after_init_theme', 'yreg_estate_shortcodes_settings_theme_setup' );
	function yreg_estate_shortcodes_settings_theme_setup() {
		if (yreg_estate_shortcodes_is_used()) {

			// Sort templates alphabetically
			$tmp = yreg_estate_storage_get('registered_templates');
			ksort($tmp);
			yreg_estate_storage_set('registered_templates', $tmp);

			// Prepare arrays 
			yreg_estate_storage_set('sc_params', array(
			
				// Current element id
				'id' => array(
					"title" => esc_html__("Element ID", 'yreg-estate'),
					"desc" => wp_kses_data( __("ID for current element", 'yreg-estate') ),
					"divider" => true,
					"value" => "",
					"type" => "text"
				),
			
				// Current element class
				'class' => array(
					"title" => esc_html__("Element CSS class", 'yreg-estate'),
					"desc" => wp_kses_data( __("CSS class for current element (optional)", 'yreg-estate') ),
					"value" => "",
					"type" => "text"
				),
			
				// Current element style
				'css' => array(
					"title" => esc_html__("CSS styles", 'yreg-estate'),
					"desc" => wp_kses_data( __("Any additional CSS rules (if need)", 'yreg-estate') ),
					"value" => "",
					"type" => "text"
				),
			
			
				// Switcher choises
				'list_styles' => array(
					'ul'	=> esc_html__('Unordered', 'yreg-estate'),
					'ol'	=> esc_html__('Ordered', 'yreg-estate'),
					'iconed'=> esc_html__('Iconed', 'yreg-estate')
				),

				'yes_no'	=> yreg_estate_get_list_yesno(),
				'on_off'	=> yreg_estate_get_list_onoff(),
				'dir' 		=> yreg_estate_get_list_directions(),
				'align'		=> yreg_estate_get_list_alignments(),
				'float'		=> yreg_estate_get_list_floats(),
				'hpos'		=> yreg_estate_get_list_hpos(),
				'show_hide'	=> yreg_estate_get_list_showhide(),
				'sorting' 	=> yreg_estate_get_list_sortings(),
				'ordering' 	=> yreg_estate_get_list_orderings(),
				'shapes'	=> yreg_estate_get_list_shapes(),
				'sizes'		=> yreg_estate_get_list_sizes(),
				'sliders'	=> yreg_estate_get_list_sliders(),
				'controls'	=> yreg_estate_get_list_controls(),
				'categories'=> yreg_estate_get_list_categories(),
				'columns'	=> yreg_estate_get_list_columns(),
				'images'	=> array_merge(array('none'=>"none"), yreg_estate_get_list_files("images/icons", "png")),
				'icons'		=> array_merge(array("inherit", "none"), yreg_estate_get_list_icons()),
				'locations'	=> yreg_estate_get_list_dedicated_locations(),
				'filters'	=> yreg_estate_get_list_portfolio_filters(),
				'formats'	=> yreg_estate_get_list_post_formats_filters(),
				'hovers'	=> yreg_estate_get_list_hovers(true),
				'hovers_dir'=> yreg_estate_get_list_hovers_directions(true),
				'schemes'	=> yreg_estate_get_list_color_schemes(true),
				'animations'		=> yreg_estate_get_list_animations_in(),
				'margins' 			=> yreg_estate_get_list_margins(true),
				'blogger_styles'	=> yreg_estate_get_list_templates_blogger(),
				'forms'				=> yreg_estate_get_list_templates_forms(),
				'posts_types'		=> yreg_estate_get_list_posts_types(),
				'googlemap_styles'	=> yreg_estate_get_list_googlemap_styles(),
				'field_types'		=> yreg_estate_get_list_field_types(),
				'label_positions'	=> yreg_estate_get_list_label_positions()
				)
			);

			// Common params
			yreg_estate_set_sc_param('animation', array(
				"title" => esc_html__("Animation",  'yreg-estate'),
				"desc" => wp_kses_data( __('Select animation while object enter in the visible area of page',  'yreg-estate') ),
				"value" => "none",
				"type" => "select",
				"options" => yreg_estate_get_sc_param('animations')
				)
			);
			yreg_estate_set_sc_param('top', array(
				"title" => esc_html__("Top margin",  'yreg-estate'),
				"divider" => true,
				"value" => "inherit",
				"type" => "select",
				"options" => yreg_estate_get_sc_param('margins')
				)
			);
			yreg_estate_set_sc_param('bottom', array(
				"title" => esc_html__("Bottom margin",  'yreg-estate'),
				"value" => "inherit",
				"type" => "select",
				"options" => yreg_estate_get_sc_param('margins')
				)
			);
			yreg_estate_set_sc_param('left', array(
				"title" => esc_html__("Left margin",  'yreg-estate'),
				"value" => "inherit",
				"type" => "select",
				"options" => yreg_estate_get_sc_param('margins')
				)
			);
			yreg_estate_set_sc_param('right', array(
				"title" => esc_html__("Right margin",  'yreg-estate'),
				"desc" => wp_kses_data( __("Margins around this shortcode", 'yreg-estate') ),
				"value" => "inherit",
				"type" => "select",
				"options" => yreg_estate_get_sc_param('margins')
				)
			);

			yreg_estate_storage_set('sc_params', apply_filters('yreg_estate_filter_shortcodes_params', yreg_estate_storage_get('sc_params')));

			// Shortcodes list
			//------------------------------------------------------------------
			yreg_estate_storage_set('shortcodes', array());
			
			// Register shortcodes
			do_action('yreg_estate_action_shortcodes_list');

			// Sort shortcodes list
			$tmp = yreg_estate_storage_get('shortcodes');
			uasort($tmp, 'yreg_estate_compare_sc_title');
			yreg_estate_storage_set('shortcodes', $tmp);
		}
	}
}
?>