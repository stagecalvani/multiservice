<?php
/**
 * YREG_ESTATE Framework: theme variables storage
 *
 * @package	yreg_estate
 * @since	yreg_estate 1.0
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }

// Get theme variable
if (!function_exists('yreg_estate_storage_get')) {
	function yreg_estate_storage_get($var_name, $default='') {
		global $YREG_ESTATE_STORAGE;
		return isset($YREG_ESTATE_STORAGE[$var_name]) ? $YREG_ESTATE_STORAGE[$var_name] : $default;
	}
}

// Set theme variable
if (!function_exists('yreg_estate_storage_set')) {
	function yreg_estate_storage_set($var_name, $value) {
		global $YREG_ESTATE_STORAGE;
		$YREG_ESTATE_STORAGE[$var_name] = $value;
	}
}

// Check if theme variable is empty
if (!function_exists('yreg_estate_storage_empty')) {
	function yreg_estate_storage_empty($var_name, $key='', $key2='') {
		global $YREG_ESTATE_STORAGE;
		if (!empty($key) && !empty($key2))
			return empty($YREG_ESTATE_STORAGE[$var_name][$key][$key2]);
		else if (!empty($key))
			return empty($YREG_ESTATE_STORAGE[$var_name][$key]);
		else
			return empty($YREG_ESTATE_STORAGE[$var_name]);
	}
}

// Check if theme variable is set
if (!function_exists('yreg_estate_storage_isset')) {
	function yreg_estate_storage_isset($var_name, $key='', $key2='') {
		global $YREG_ESTATE_STORAGE;
		if (!empty($key) && !empty($key2))
			return isset($YREG_ESTATE_STORAGE[$var_name][$key][$key2]);
		else if (!empty($key))
			return isset($YREG_ESTATE_STORAGE[$var_name][$key]);
		else
			return isset($YREG_ESTATE_STORAGE[$var_name]);
	}
}

// Inc/Dec theme variable with specified value
if (!function_exists('yreg_estate_storage_inc')) {
	function yreg_estate_storage_inc($var_name, $value=1) {
		global $YREG_ESTATE_STORAGE;
		if (empty($YREG_ESTATE_STORAGE[$var_name])) $YREG_ESTATE_STORAGE[$var_name] = 0;
		$YREG_ESTATE_STORAGE[$var_name] += $value;
	}
}

// Concatenate theme variable with specified value
if (!function_exists('yreg_estate_storage_concat')) {
	function yreg_estate_storage_concat($var_name, $value) {
		global $YREG_ESTATE_STORAGE;
		if (empty($YREG_ESTATE_STORAGE[$var_name])) $YREG_ESTATE_STORAGE[$var_name] = '';
		$YREG_ESTATE_STORAGE[$var_name] .= $value;
	}
}

// Get array (one or two dim) element
if (!function_exists('yreg_estate_storage_get_array')) {
	function yreg_estate_storage_get_array($var_name, $key, $key2='', $default='') {
		global $YREG_ESTATE_STORAGE;
		if (empty($key2))
			return !empty($var_name) && !empty($key) && isset($YREG_ESTATE_STORAGE[$var_name][$key]) ? $YREG_ESTATE_STORAGE[$var_name][$key] : $default;
		else
			return !empty($var_name) && !empty($key) && isset($YREG_ESTATE_STORAGE[$var_name][$key][$key2]) ? $YREG_ESTATE_STORAGE[$var_name][$key][$key2] : $default;
	}
}

// Set array element
if (!function_exists('yreg_estate_storage_set_array')) {
	function yreg_estate_storage_set_array($var_name, $key, $value) {
		global $YREG_ESTATE_STORAGE;
		if (!isset($YREG_ESTATE_STORAGE[$var_name])) $YREG_ESTATE_STORAGE[$var_name] = array();
		if ($key==='')
			$YREG_ESTATE_STORAGE[$var_name][] = $value;
		else
			$YREG_ESTATE_STORAGE[$var_name][$key] = $value;
	}
}

// Set two-dim array element
if (!function_exists('yreg_estate_storage_set_array2')) {
	function yreg_estate_storage_set_array2($var_name, $key, $key2, $value) {
		global $YREG_ESTATE_STORAGE;
		if (!isset($YREG_ESTATE_STORAGE[$var_name])) $YREG_ESTATE_STORAGE[$var_name] = array();
		if (!isset($YREG_ESTATE_STORAGE[$var_name][$key])) $YREG_ESTATE_STORAGE[$var_name][$key] = array();
		if ($key2==='')
			$YREG_ESTATE_STORAGE[$var_name][$key][] = $value;
		else
			$YREG_ESTATE_STORAGE[$var_name][$key][$key2] = $value;
	}
}

// Add array element after the key
if (!function_exists('yreg_estate_storage_set_array_after')) {
	function yreg_estate_storage_set_array_after($var_name, $after, $key, $value='') {
		global $YREG_ESTATE_STORAGE;
		if (!isset($YREG_ESTATE_STORAGE[$var_name])) $YREG_ESTATE_STORAGE[$var_name] = array();
		if (is_array($key))
			yreg_estate_array_insert_after($YREG_ESTATE_STORAGE[$var_name], $after, $key);
		else
			yreg_estate_array_insert_after($YREG_ESTATE_STORAGE[$var_name], $after, array($key=>$value));
	}
}

// Add array element before the key
if (!function_exists('yreg_estate_storage_set_array_before')) {
	function yreg_estate_storage_set_array_before($var_name, $before, $key, $value='') {
		global $YREG_ESTATE_STORAGE;
		if (!isset($YREG_ESTATE_STORAGE[$var_name])) $YREG_ESTATE_STORAGE[$var_name] = array();
		if (is_array($key))
			yreg_estate_array_insert_before($YREG_ESTATE_STORAGE[$var_name], $before, $key);
		else
			yreg_estate_array_insert_before($YREG_ESTATE_STORAGE[$var_name], $before, array($key=>$value));
	}
}

// Push element into array
if (!function_exists('yreg_estate_storage_push_array')) {
	function yreg_estate_storage_push_array($var_name, $key, $value) {
		global $YREG_ESTATE_STORAGE;
		if (!isset($YREG_ESTATE_STORAGE[$var_name])) $YREG_ESTATE_STORAGE[$var_name] = array();
		if ($key==='')
			array_push($YREG_ESTATE_STORAGE[$var_name], $value);
		else {
			if (!isset($YREG_ESTATE_STORAGE[$var_name][$key])) $YREG_ESTATE_STORAGE[$var_name][$key] = array();
			array_push($YREG_ESTATE_STORAGE[$var_name][$key], $value);
		}
	}
}

// Pop element from array
if (!function_exists('yreg_estate_storage_pop_array')) {
	function yreg_estate_storage_pop_array($var_name, $key='', $defa='') {
		global $YREG_ESTATE_STORAGE;
		$rez = $defa;
		if ($key==='') {
			if (isset($YREG_ESTATE_STORAGE[$var_name]) && is_array($YREG_ESTATE_STORAGE[$var_name]) && count($YREG_ESTATE_STORAGE[$var_name]) > 0) 
				$rez = array_pop($YREG_ESTATE_STORAGE[$var_name]);
		} else {
			if (isset($YREG_ESTATE_STORAGE[$var_name][$key]) && is_array($YREG_ESTATE_STORAGE[$var_name][$key]) && count($YREG_ESTATE_STORAGE[$var_name][$key]) > 0) 
				$rez = array_pop($YREG_ESTATE_STORAGE[$var_name][$key]);
		}
		return $rez;
	}
}

// Inc/Dec array element with specified value
if (!function_exists('yreg_estate_storage_inc_array')) {
	function yreg_estate_storage_inc_array($var_name, $key, $value=1) {
		global $YREG_ESTATE_STORAGE;
		if (!isset($YREG_ESTATE_STORAGE[$var_name])) $YREG_ESTATE_STORAGE[$var_name] = array();
		if (empty($YREG_ESTATE_STORAGE[$var_name][$key])) $YREG_ESTATE_STORAGE[$var_name][$key] = 0;
		$YREG_ESTATE_STORAGE[$var_name][$key] += $value;
	}
}

// Concatenate array element with specified value
if (!function_exists('yreg_estate_storage_concat_array')) {
	function yreg_estate_storage_concat_array($var_name, $key, $value) {
		global $YREG_ESTATE_STORAGE;
		if (!isset($YREG_ESTATE_STORAGE[$var_name])) $YREG_ESTATE_STORAGE[$var_name] = array();
		if (empty($YREG_ESTATE_STORAGE[$var_name][$key])) $YREG_ESTATE_STORAGE[$var_name][$key] = '';
		$YREG_ESTATE_STORAGE[$var_name][$key] .= $value;
	}
}

// Call object's method
if (!function_exists('yreg_estate_storage_call_obj_method')) {
	function yreg_estate_storage_call_obj_method($var_name, $method, $param=null) {
		global $YREG_ESTATE_STORAGE;
		if ($param===null)
			return !empty($var_name) && !empty($method) && isset($YREG_ESTATE_STORAGE[$var_name]) ? $YREG_ESTATE_STORAGE[$var_name]->$method(): '';
		else
			return !empty($var_name) && !empty($method) && isset($YREG_ESTATE_STORAGE[$var_name]) ? $YREG_ESTATE_STORAGE[$var_name]->$method($param): '';
	}
}

// Get object's property
if (!function_exists('yreg_estate_storage_get_obj_property')) {
	function yreg_estate_storage_get_obj_property($var_name, $prop, $default='') {
		global $YREG_ESTATE_STORAGE;
		return !empty($var_name) && !empty($prop) && isset($YREG_ESTATE_STORAGE[$var_name]->$prop) ? $YREG_ESTATE_STORAGE[$var_name]->$prop : $default;
	}
}
?>