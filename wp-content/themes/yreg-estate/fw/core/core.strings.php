<?php
/**
 * YREG_ESTATE Framework: strings manipulations
 *
 * @package	yreg_estate
 * @since	yreg_estate 1.0
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }

// Check multibyte functions
if ( ! defined( 'YREG_ESTATE_MULTIBYTE' ) ) define( 'YREG_ESTATE_MULTIBYTE', function_exists('mb_strpos') ? 'UTF-8' : false );

if (!function_exists('yreg_estate_strlen')) {
	function yreg_estate_strlen($text) {
		return YREG_ESTATE_MULTIBYTE ? mb_strlen($text) : strlen($text);
	}
}

if (!function_exists('yreg_estate_strpos')) {
	function yreg_estate_strpos($text, $char, $from=0) {
		return YREG_ESTATE_MULTIBYTE ? mb_strpos($text, $char, $from) : strpos($text, $char, $from);
	}
}

if (!function_exists('yreg_estate_strrpos')) {
	function yreg_estate_strrpos($text, $char, $from=0) {
		return YREG_ESTATE_MULTIBYTE ? mb_strrpos($text, $char, $from) : strrpos($text, $char, $from);
	}
}

if (!function_exists('yreg_estate_substr')) {
	function yreg_estate_substr($text, $from, $len=-999999) {
		if ($len==-999999) { 
			if ($from < 0)
				$len = -$from; 
			else
				$len = yreg_estate_strlen($text)-$from;
		}
		return YREG_ESTATE_MULTIBYTE ? mb_substr($text, $from, $len) : substr($text, $from, $len);
	}
}

if (!function_exists('yreg_estate_strtolower')) {
	function yreg_estate_strtolower($text) {
		return YREG_ESTATE_MULTIBYTE ? mb_strtolower($text) : strtolower($text);
	}
}

if (!function_exists('yreg_estate_strtoupper')) {
	function yreg_estate_strtoupper($text) {
		return YREG_ESTATE_MULTIBYTE ? mb_strtoupper($text) : strtoupper($text);
	}
}

if (!function_exists('yreg_estate_strtoproper')) {
	function yreg_estate_strtoproper($text) { 
		$rez = ''; $last = ' ';
		for ($i=0; $i<yreg_estate_strlen($text); $i++) {
			$ch = yreg_estate_substr($text, $i, 1);
			$rez .= yreg_estate_strpos(' .,:;?!()[]{}+=', $last)!==false ? yreg_estate_strtoupper($ch) : yreg_estate_strtolower($ch);
			$last = $ch;
		}
		return $rez;
	}
}

if (!function_exists('yreg_estate_strrepeat')) {
	function yreg_estate_strrepeat($str, $n) {
		$rez = '';
		for ($i=0; $i<$n; $i++)
			$rez .= $str;
		return $rez;
	}
}

if (!function_exists('yreg_estate_strshort')) {
	function yreg_estate_strshort($str, $maxlength, $add='...') {
	//	if ($add && yreg_estate_substr($add, 0, 1) != ' ')
	//		$add .= ' ';
		if ($maxlength < 0) 
			return $str;
		if ($maxlength < 1 || $maxlength >= yreg_estate_strlen($str)) 
			return strip_tags($str);
		$str = yreg_estate_substr(strip_tags($str), 0, $maxlength - yreg_estate_strlen($add));
		$ch = yreg_estate_substr($str, $maxlength - yreg_estate_strlen($add), 1);
		if ($ch != ' ') {
			for ($i = yreg_estate_strlen($str) - 1; $i > 0; $i--)
				if (yreg_estate_substr($str, $i, 1) == ' ') break;
			$str = trim(yreg_estate_substr($str, 0, $i));
		}
		if (!empty($str) && yreg_estate_strpos(',.:;-', yreg_estate_substr($str, -1))!==false) $str = yreg_estate_substr($str, 0, -1);
		return ($str) . ($add);
	}
}

// Clear string from spaces, line breaks and tags (only around text)
if (!function_exists('yreg_estate_strclear')) {
	function yreg_estate_strclear($text, $tags=array()) {
		if (empty($text)) return $text;
		if (!is_array($tags)) {
			if ($tags != '')
				$tags = explode($tags, ',');
			else
				$tags = array();
		}
		$text = trim(chop($text));
		if (is_array($tags) && count($tags) > 0) {
			foreach ($tags as $tag) {
				$open  = '<'.esc_attr($tag);
				$close = '</'.esc_attr($tag).'>';
				if (yreg_estate_substr($text, 0, yreg_estate_strlen($open))==$open) {
					$pos = yreg_estate_strpos($text, '>');
					if ($pos!==false) $text = yreg_estate_substr($text, $pos+1);
				}
				if (yreg_estate_substr($text, -yreg_estate_strlen($close))==$close) $text = yreg_estate_substr($text, 0, yreg_estate_strlen($text) - yreg_estate_strlen($close));
				$text = trim(chop($text));
			}
		}
		return $text;
	}
}

// Return slug for the any title string
if (!function_exists('yreg_estate_get_slug')) {
	function yreg_estate_get_slug($title) {
		return yreg_estate_strtolower(str_replace(array('\\','/','-',' ','.'), '_', $title));
	}
}

// Replace macros in the string
if (!function_exists('yreg_estate_strmacros')) {
	function yreg_estate_strmacros($str) {
		return str_replace(array("{{", "}}", "((", "))", "||"), array("<i>", "</i>", "<b>", "</b>", "<br>"), $str);
	}
}

// Unserialize string (try replace \n with \r\n)
if (!function_exists('yreg_estate_unserialize')) {
	function yreg_estate_unserialize($str) {
		if ( is_serialized($str) ) {
			try {
				$data = unserialize($str);
			} catch (Exception $e) {
				dcl($e->getMessage());
				$data = false;
			}
			if ($data===false) {
				try {
					$data = @unserialize(str_replace("\n", "\r\n", $str));
				} catch (Exception $e) {
					dcl($e->getMessage());
					$data = false;
				}
			}
			//if ($data===false) $data = @unserialize(str_replace(array("\n", "\r"), array('\\n','\\r'), $str));
			return $data;
		} else
			return $str;
	}
}
?>