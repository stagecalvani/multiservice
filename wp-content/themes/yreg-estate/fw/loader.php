<?php
/**
 * YREG_ESTATE Framework
 *
 * @package yreg_estate
 * @since yreg_estate 1.0
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }

// Framework directory path from theme root
if ( ! defined( 'YREG_ESTATE_FW_DIR' ) )			define( 'YREG_ESTATE_FW_DIR', 'fw' );

// Theme timing
if ( ! defined( 'YREG_ESTATE_START_TIME' ) )		define( 'YREG_ESTATE_START_TIME', microtime(true));		// Framework start time
if ( ! defined( 'YREG_ESTATE_START_MEMORY' ) )		define( 'YREG_ESTATE_START_MEMORY', memory_get_usage());	// Memory usage before core loading
if ( ! defined( 'YREG_ESTATE_START_QUERIES' ) )	define( 'YREG_ESTATE_START_QUERIES', get_num_queries());	// DB queries used

// Include theme variables storage
get_template_part(YREG_ESTATE_FW_DIR.'/core/core.storage');

// Theme variables storage
yreg_estate_storage_set('options_prefix', 'yreg_estate');	// Used as prefix for store theme's options in the post meta and wp options
yreg_estate_storage_set('page_template', '');			// Storage for current page template name (used in the inheritance system)
yreg_estate_storage_set('widgets_args', array(			// Arguments to register widgets
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h5 class="widget_title">',
		'after_title'   => '</h5>',
	)
);

/* Theme setup section
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_loader_theme_setup' ) ) {
	add_action( 'after_setup_theme', 'yreg_estate_loader_theme_setup', 20 );
	function yreg_estate_loader_theme_setup() {

		yreg_estate_profiler_add_point(esc_html__('After load theme required files', 'yreg-estate'));

		// Before init theme
		do_action('yreg_estate_action_before_init_theme');

		// Load current values for main theme options
		yreg_estate_load_main_options();

		// Theme core init - only for admin side. In frontend it called from header.php
		if ( is_admin() ) {
			yreg_estate_core_init_theme();
		}
	}
}


/* Include core parts
------------------------------------------------------------------------ */
// Manual load important libraries before load all rest files
// core.strings must be first - we use yreg_estate_str...() in the yreg_estate_get_file_dir()
get_template_part(YREG_ESTATE_FW_DIR.'/core/core.strings');
// core.files must be first - we use yreg_estate_get_file_dir() to include all rest parts
get_template_part(YREG_ESTATE_FW_DIR.'/core/core.files');

// Include debug and profiler
get_template_part(yreg_estate_get_file_slug('core/core.debug.php'));

// Include custom theme files
yreg_estate_autoload_folder( 'includes' );

// Include core files
yreg_estate_autoload_folder( 'core' );

// Include theme-specific plugins and post types
yreg_estate_autoload_folder( 'plugins' );

// Include theme templates
yreg_estate_autoload_folder( 'templates' );

// Include theme widgets
yreg_estate_autoload_folder( 'widgets' );
?>