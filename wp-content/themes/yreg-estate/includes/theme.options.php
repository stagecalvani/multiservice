<?php

/* Theme setup section
-------------------------------------------------------------------- */

// ONLY FOR PROGRAMMERS, NOT FOR CUSTOMER
// Framework settings

yreg_estate_storage_set('settings', array(
	
	'less_compiler'		=> 'lessc',								// no|lessc|less - Compiler for the .less
																// lessc - fast & low memory required, but .less-map, shadows & gradients not supprted
																// less  - slow, but support all features
	'less_nested'		=> false,								// Use nested selectors when compiling less - increase .css size, but allow using nested color schemes
	'less_prefix'		=> '',									// any string - Use prefix before each selector when compile less. For example: 'html '
	'less_separator'	=> '/*---LESS_SEPARATOR---*/',			// string - separator inside .less file to split it when compiling to reduce memory usage
																// (compilation speed gets a bit slow)
	'less_map'			=> 'no',								// no|internal|external - Generate map for .less files. 
																// Warning! You need more then 128Mb for PHP scripts on your server! Supported only if less_compiler=less (see above)
	
	'customizer_demo'	=> true,								// Show color customizer demo (if many color settings) or not (if only accent colors used)

	'allow_fullscreen'	=> false,								// Allow fullscreen and fullwide body styles

	'socials_type'		=> 'icons',								// images|icons - Use this kind of pictograms for all socials: share, social profiles, team members socials, etc.
	'slides_type'		=> 'bg',								// images|bg - Use image as slide's content or as slide's background

	'add_image_size'	=> false,								// Add theme's thumb sizes into WP list sizes. 
																// If false - new image thumb will be generated on demand,
																// otherwise - all thumb sizes will be generated when image is loaded

	'use_list_cache'	=> true,								// Use cache for any lists (increase theme speed, but get 15-20K memory)
	'use_post_cache'	=> true,								// Use cache for post_data (increase theme speed, decrease queries number, but get more memory - up to 300K)

	'allow_profiler'	=> true,								// Allow to show theme profiler when 'debug mode' is on

	'admin_dummy_style' => 2									// 1 | 2 - Progress bar style when import dummy data
	)
);



// Default Theme Options
if ( !function_exists( 'yreg_estate_options_settings_theme_setup' ) ) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_options_settings_theme_setup', 2 );	// Priority 1 for add yreg_estate_filter handlers
	function yreg_estate_options_settings_theme_setup() {
		
		// Clear all saved Theme Options on first theme run
		add_action('after_switch_theme', 'yreg_estate_options_reset');

		// Settings 
		$socials_type = yreg_estate_get_theme_setting('socials_type');
				
		// Prepare arrays 
		yreg_estate_storage_set('options_params', apply_filters('yreg_estate_filter_theme_options_params', array(
			'list_fonts'				=> array('$yreg_estate_get_list_fonts' => ''),
			'list_fonts_styles'			=> array('$yreg_estate_get_list_fonts_styles' => ''),
			'list_socials' 				=> array('$yreg_estate_get_list_socials' => ''),
			'list_icons' 				=> array('$yreg_estate_get_list_icons' => ''),
			'list_posts_types' 			=> array('$yreg_estate_get_list_posts_types' => ''),
			'list_categories' 			=> array('$yreg_estate_get_list_categories' => ''),
			'list_menus'				=> array('$yreg_estate_get_list_menus' => ''),
			'list_sidebars'				=> array('$yreg_estate_get_list_sidebars' => ''),
			'list_positions' 			=> array('$yreg_estate_get_list_sidebars_positions' => ''),
			'list_skins'				=> array('$yreg_estate_get_list_skins' => ''),
			'list_color_schemes'		=> array('$yreg_estate_get_list_color_schemes' => ''),
			'list_bg_tints'				=> array('$yreg_estate_get_list_bg_tints' => ''),
			'list_body_styles'			=> array('$yreg_estate_get_list_body_styles' => ''),
			'list_header_styles'		=> array('$yreg_estate_get_list_templates_header' => ''),
			'list_blog_styles'			=> array('$yreg_estate_get_list_templates_blog' => ''),
			'list_single_styles'		=> array('$yreg_estate_get_list_templates_single' => ''),
			'list_article_styles'		=> array('$yreg_estate_get_list_article_styles' => ''),
			'list_blog_counters' 		=> array('$yreg_estate_get_list_blog_counters' => ''),
			'list_animations_in' 		=> array('$yreg_estate_get_list_animations_in' => ''),
			'list_animations_out'		=> array('$yreg_estate_get_list_animations_out' => ''),
			'list_filters'				=> array('$yreg_estate_get_list_portfolio_filters' => ''),
			'list_hovers'				=> array('$yreg_estate_get_list_hovers' => ''),
			'list_hovers_dir'			=> array('$yreg_estate_get_list_hovers_directions' => ''),
			'list_alter_sizes'			=> array('$yreg_estate_get_list_alter_sizes' => ''),
			'list_sliders' 				=> array('$yreg_estate_get_list_sliders' => ''),
			'list_bg_image_positions'	=> array('$yreg_estate_get_list_bg_image_positions' => ''),
			'list_popups' 				=> array('$yreg_estate_get_list_popup_engines' => ''),
			'list_gmap_styles'		 	=> array('$yreg_estate_get_list_googlemap_styles' => ''),
			'list_yes_no' 				=> array('$yreg_estate_get_list_yesno' => ''),
			'list_on_off' 				=> array('$yreg_estate_get_list_onoff' => ''),
			'list_show_hide' 			=> array('$yreg_estate_get_list_showhide' => ''),
			'list_sorting' 				=> array('$yreg_estate_get_list_sortings' => ''),
			'list_ordering' 			=> array('$yreg_estate_get_list_orderings' => ''),
			'list_locations' 			=> array('$yreg_estate_get_list_dedicated_locations' => '')
			)
		));


		// Theme options array
		yreg_estate_storage_set('options', array(

		
		//###############################
		//#### Customization         #### 
		//###############################
		'partition_customization' => array(
					"title" => esc_html__('Customization', 'yreg-estate'),
					"start" => "partitions",
					"override" => "category,services_group,page,post",
					"icon" => "iconadmin-cog-alt",
					"type" => "partition"
					),
		
		
		// Customization -> Body Style
		//-------------------------------------------------
		
		'customization_body' => array(
					"title" => esc_html__('Body style', 'yreg-estate'),
					"override" => "category,services_group,post,page",
					"icon" => 'iconadmin-picture',
					"start" => "customization_tabs",
					"type" => "tab"
					),
		
		'info_body_1' => array(
					"title" => esc_html__('Body parameters', 'yreg-estate'),
					"desc" => wp_kses_data( __('Select body style, skin and color scheme for entire site. You can override this parameters on any page, post or category', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"type" => "info"
					),

		'body_style' => array(
					"title" => esc_html__('Body style', 'yreg-estate'),
					"desc" => wp_kses_data( __('Select body style:', 'yreg-estate') )
								. ' <br>' 
								. wp_kses_data( __('<b>boxed</b> - if you want use background color and/or image', 'yreg-estate') )
								. ',<br>'
								. wp_kses_data( __('<b>wide</b> - page fill whole window with centered content', 'yreg-estate') )
								. (yreg_estate_get_theme_setting('allow_fullscreen') 
									? ',<br>' . wp_kses_data( __('<b>fullwide</b> - page content stretched on the full width of the window (with few left and right paddings)', 'yreg-estate') )
									: '')
								. (yreg_estate_get_theme_setting('allow_fullscreen') 
									? ',<br>' . wp_kses_data( __('<b>fullscreen</b> - page content fill whole window without any paddings', 'yreg-estate') )
									: ''),
					"override" => "category,services_group,post,page",
					"std" => "wide",
					"options" => yreg_estate_get_options_param('list_body_styles'),
					"dir" => "horizontal",
					"type" => "radio"
					),
		
		'body_paddings' => array(
					"title" => esc_html__('Page paddings', 'yreg-estate'),
					"desc" => wp_kses_data( __('Add paddings above and below the page content', 'yreg-estate') ),
					"override" => "post,page",
					"std" => "yes",
					"options" => array(
						"no"=>esc_html__("No", 'yreg-estate'),
						"yes"=>esc_html__("Yes", 'yreg-estate'),
						"top"=>esc_html__("Top", 'yreg-estate'),
						"bottom"=>esc_html__("Bottom", 'yreg-estate'),
					),
					"type" => "radio",
					),

		'theme_skin' => array(
					"title" => esc_html__('Select theme skin', 'yreg-estate'),
					"desc" => wp_kses_data( __('Select skin for the theme decoration', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "no_less",
					"options" => yreg_estate_get_options_param('list_skins'),
					"type" => "select"
					),

		"body_scheme" => array(
					"title" => esc_html__('Color scheme', 'yreg-estate'),
					"desc" => wp_kses_data( __('Select predefined color scheme for the entire page', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "original",
					"dir" => "horizontal",
					"options" => yreg_estate_get_options_param('list_color_schemes'),
					"type" => "checklist"),
		
		'body_filled' => array(
					"title" => esc_html__('Fill body', 'yreg-estate'),
					"desc" => wp_kses_data( __('Fill the page background with the solid color or leave it transparend to show background image (or video background)', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"
					),

		'info_body_2' => array(
					"title" => esc_html__('Background color and image', 'yreg-estate'),
					"desc" => wp_kses_data( __('Color and image for the site background', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"type" => "info"
					),

		'bg_custom' => array(
					"title" => esc_html__('Use custom background',  'yreg-estate'),
					"desc" => wp_kses_data( __("Use custom color and/or image as the site background", 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "no",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"
					),
		
		'bg_color' => array(
					"title" => esc_html__('Background color',  'yreg-estate'),
					"desc" => wp_kses_data( __('Body background color',  'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"dependency" => array(
						'bg_custom' => array('yes')
					),
					"std" => "#ffffff",
					"type" => "color"
					),

		'bg_pattern' => array(
					"title" => esc_html__('Background predefined pattern',  'yreg-estate'),
					"desc" => wp_kses_data( __('Select theme background pattern (first case - without pattern)',  'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"dependency" => array(
						'bg_custom' => array('yes')
					),
					"std" => "",
					"options" => array(
						0 => yreg_estate_get_file_url('images/spacer.png'),
						1 => yreg_estate_get_file_url('images/bg/pattern_1.jpg'),
						2 => yreg_estate_get_file_url('images/bg/pattern_2.jpg'),
						3 => yreg_estate_get_file_url('images/bg/pattern_3.jpg'),
						4 => yreg_estate_get_file_url('images/bg/pattern_4.jpg'),
						5 => yreg_estate_get_file_url('images/bg/pattern_5.jpg')
					),
					"style" => "list",
					"type" => "images"
					),
		
		'bg_pattern_custom' => array(
					"title" => esc_html__('Background custom pattern',  'yreg-estate'),
					"desc" => wp_kses_data( __('Select or upload background custom pattern. If selected - use it instead the theme predefined pattern (selected in the field above)',  'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"dependency" => array(
						'bg_custom' => array('yes')
					),
					"std" => "",
					"type" => "media"
					),
		
		'bg_image' => array(
					"title" => esc_html__('Background predefined image',  'yreg-estate'),
					"desc" => wp_kses_data( __('Select theme background image (first case - without image)',  'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "",
					"dependency" => array(
						'bg_custom' => array('yes')
					),
					"options" => array(
						0 => yreg_estate_get_file_url('images/spacer.png'),
						1 => yreg_estate_get_file_url('images/bg/image_1_thumb.jpg'),
						2 => yreg_estate_get_file_url('images/bg/image_2_thumb.jpg'),
						3 => yreg_estate_get_file_url('images/bg/image_3_thumb.jpg')
					),
					"style" => "list",
					"type" => "images"
					),
		
		'bg_image_custom' => array(
					"title" => esc_html__('Background custom image',  'yreg-estate'),
					"desc" => wp_kses_data( __('Select or upload background custom image. If selected - use it instead the theme predefined image (selected in the field above)',  'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"dependency" => array(
						'bg_custom' => array('yes')
					),
					"std" => "",
					"type" => "media"
					),
		
		'bg_image_custom_position' => array( 
					"title" => esc_html__('Background custom image position',  'yreg-estate'),
					"desc" => wp_kses_data( __('Select custom image position',  'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "left_top",
					"dependency" => array(
						'bg_custom' => array('yes')
					),
					"options" => array(
						'left_top' => "Left Top",
						'center_top' => "Center Top",
						'right_top' => "Right Top",
						'left_center' => "Left Center",
						'center_center' => "Center Center",
						'right_center' => "Right Center",
						'left_bottom' => "Left Bottom",
						'center_bottom' => "Center Bottom",
						'right_bottom' => "Right Bottom",
					),
					"type" => "select"
					),
		
		'bg_image_load' => array(
					"title" => esc_html__('Load background image', 'yreg-estate'),
					"desc" => wp_kses_data( __('Always load background images or only for boxed body style', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "boxed",
					"size" => "medium",
					"dependency" => array(
						'bg_custom' => array('yes')
					),
					"options" => array(
						'boxed' => esc_html__('Boxed', 'yreg-estate'),
						'always' => esc_html__('Always', 'yreg-estate')
					),
					"type" => "switch"
					),

		
		'info_body_3' => array(
					"title" => esc_html__('Video background', 'yreg-estate'),
					"desc" => wp_kses_data( __('Parameters of the video, used as site background', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"type" => "info"
					),

		'show_video_bg' => array(
					"title" => esc_html__('Show video background',  'yreg-estate'),
					"desc" => wp_kses_data( __("Show video as the site background", 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "no",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"
					),

		'video_bg_youtube_code' => array(
					"title" => esc_html__('Youtube code for video bg',  'yreg-estate'),
					"desc" => wp_kses_data( __("Youtube code of video", 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"dependency" => array(
						'show_video_bg' => array('yes')
					),
					"std" => "",
					"type" => "text"
					),

		'video_bg_url' => array(
					"title" => esc_html__('Local video for video bg',  'yreg-estate'),
					"desc" => wp_kses_data( __("URL to video-file (uploaded on your site)", 'yreg-estate') ),
					"readonly" =>false,
					"override" => "category,services_group,post,page",
					"dependency" => array(
						'show_video_bg' => array('yes')
					),
					"before" => array(	'title' => esc_html__('Choose video', 'yreg-estate'),
										'action' => 'media_upload',
										'multiple' => false,
										'linked_field' => '',
										'type' => 'video',
										'captions' => array('choose' => esc_html__( 'Choose Video', 'yreg-estate'),
															'update' => esc_html__( 'Select Video', 'yreg-estate')
														)
								),
					"std" => "",
					"type" => "media"
					),

		'video_bg_overlay' => array(
					"title" => esc_html__('Use overlay for video bg', 'yreg-estate'),
					"desc" => wp_kses_data( __('Use overlay texture for the video background', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"dependency" => array(
						'show_video_bg' => array('yes')
					),
					"std" => "no",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"
					),
		
		
		
		
		
		// Customization -> Header
		//-------------------------------------------------
		
		'customization_header' => array(
					"title" => esc_html__("Header", 'yreg-estate'),
					"override" => "category,services_group,post,page",
					"icon" => 'iconadmin-window',
					"type" => "tab"),
		
		"info_header_1" => array(
					"title" => esc_html__('Top panel', 'yreg-estate'),
					"desc" => wp_kses_data( __('Top panel settings. It include user menu area (with contact info, cart button, language selector, login/logout menu and user menu) and main menu area (with logo and main menu).', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"type" => "info"),
		
		"top_panel_style" => array(
					"title" => esc_html__('Top panel style', 'yreg-estate'),
					"desc" => wp_kses_data( __('Select desired style of the page header', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "header_1",
					"options" => yreg_estate_get_options_param('list_header_styles'),
					"style" => "list",
					"type" => "images"),

		"top_panel_image" => array(
					"title" => esc_html__('Top panel image', 'yreg-estate'),
					"desc" => wp_kses_data( __('Select default background image of the page header (if not single post or featured image for current post is not specified)', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
//					"dependency" => array(
//						'top_panel_style' => array('header_1'),
//						'top_panel_style' => array('header_7')
//					),
					"std" => "",
					"type" => "media"),
		
		"top_panel_position" => array( 
					"title" => esc_html__('Top panel position', 'yreg-estate'),
					"desc" => wp_kses_data( __('Select position for the top panel with logo and main menu', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "above",
					"options" => array(
						'hide'  => esc_html__('Hide', 'yreg-estate'),
						'above' => esc_html__('Above slider', 'yreg-estate'),
						'over'  => esc_html__('Over slider', 'yreg-estate')
					),
					"type" => "checklist"),

		"top_panel_scheme" => array(
					"title" => esc_html__('Top panel color scheme', 'yreg-estate'),
					"desc" => wp_kses_data( __('Select predefined color scheme for the top panel', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "original",
					"dir" => "horizontal",
					"options" => array(
						'original'  => esc_html__('Original', 'yreg-estate'),
						'white' => esc_html__('White', 'yreg-estate'),
						'red' => esc_html__('Red', 'yreg-estate')
					),
					"type" => "checklist"),
			
		"show_company_address_header" => array(
					"title" => esc_html__('Show company address in header', 'yreg-estate'),
					"desc" => wp_kses_data( __('Show post/page/category company address in header', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
			
		"show_email_header" => array(
					"title" => esc_html__('Show email in header', 'yreg-estate'),
					"desc" => wp_kses_data( __('Show post/page/category email in header', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
			
		"show_phone_header" => array(
					"title" => esc_html__('Show contact phone in header', 'yreg-estate'),
					"desc" => wp_kses_data( __('Show post/page/category contact phone in header', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
			
			

		
		"show_page_title" => array(
					"title" => esc_html__('Show Page title', 'yreg-estate'),
					"desc" => wp_kses_data( __('Show post/page/category title', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
		
		"show_breadcrumbs" => array(
					"title" => esc_html__('Show Breadcrumbs', 'yreg-estate'),
					"desc" => wp_kses_data( __('Show path to current category (post, page)', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
			
		"show_property_search" => array(
					"title" => esc_html__('Show property search box in header', 'yreg-estate'),
					"desc" => wp_kses_data( __('', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "no",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
		
		"breadcrumbs_max_level" => array(
					"title" => esc_html__('Breadcrumbs max nesting', 'yreg-estate'),
					"desc" => wp_kses_data( __("Max number of the nested categories in the breadcrumbs (0 - unlimited)", 'yreg-estate') ),
					"dependency" => array(
						'show_breadcrumbs' => array('yes')
					),
					"std" => "0",
					"min" => 0,
					"max" => 100,
					"step" => 1,
					"type" => "spinner"),

		
		
		
		"info_header_2" => array( 
					"title" => esc_html__('Main menu style and position', 'yreg-estate'),
					"desc" => wp_kses_data( __('Select the Main menu style and position', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"type" => "info"),
		
		"menu_main" => array( 
					"title" => esc_html__('Select main menu',  'yreg-estate'),
					"desc" => wp_kses_data( __('Select main menu for the current page',  'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "default",
					"options" => yreg_estate_get_options_param('list_menus'),
					"type" => "select"),


		"menu_animation_in" => array( 
					"title" => esc_html__('Submenu show animation', 'yreg-estate'),
					"desc" => wp_kses_data( __('Select animation to show submenu ', 'yreg-estate') ),
					"std" => "fadeIn",
					"type" => "select",
					"options" => yreg_estate_get_options_param('list_animations_in')),

		"menu_animation_out" => array( 
					"title" => esc_html__('Submenu hide animation', 'yreg-estate'),
					"desc" => wp_kses_data( __('Select animation to hide submenu ', 'yreg-estate') ),
					"std" => "fadeOut",
					"type" => "select",
					"options" => yreg_estate_get_options_param('list_animations_out')),
		
		"menu_relayout" => array( 
					"title" => esc_html__('Main menu relayout', 'yreg-estate'),
					"desc" => wp_kses_data( __('Allow relayout main menu if window width less then this value', 'yreg-estate') ),
					"std" => 960,
					"min" => 320,
					"max" => 1024,
					"type" => "spinner"),
		
		"menu_responsive" => array( 
					"title" => esc_html__('Main menu responsive', 'yreg-estate'),
					"desc" => wp_kses_data( __('Allow responsive version for the main menu if window width less then this value', 'yreg-estate') ),
					"std" => 640,
					"min" => 320,
					"max" => 1024,
					"type" => "spinner"),
		
		"menu_width" => array( 
					"title" => esc_html__('Submenu width', 'yreg-estate'),
					"desc" => wp_kses_data( __('Width for dropdown menus in main menu', 'yreg-estate') ),
					"step" => 5,
					"std" => "",
					"min" => 180,
					"max" => 300,
					"mask" => "?999",
					"type" => "spinner"),

		
		
		'info_header_5' => array(
					"title" => esc_html__('Main logo', 'yreg-estate'),
					"desc" => wp_kses_data( __("Select or upload logos for the site's header and select it position", 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"type" => "info"
					),

		'favicon' => array(
					"title" => esc_html__('Favicon', 'yreg-estate'),
					"desc" => wp_kses_data( __("Upload a 16px x 16px image that will represent your website's favicon.<br /><em>To ensure cross-browser compatibility, we recommend converting the favicon into .ico format before uploading. (<a href='http://www.favicon.cc/'>www.favicon.cc</a>)</em>", 'yreg-estate') ),
					"std" => "",
					"type" => "media"
					),

		'logo' => array(
					"title" => esc_html__('Logo image', 'yreg-estate'),
					"desc" => wp_kses_data( __('Main logo image', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "",
					"type" => "media"
					),

//		'logo_retina' => array(
//					"title" => esc_html__('Logo image for Retina', 'yreg-estate'),
//					"desc" => wp_kses_data( __('Main logo image used on Retina display', 'yreg-estate') ),
//					"override" => "category,services_group,post,page",
//					"std" => "",
//					"type" => "media"
//					),
		
//		'logo_fixed' => array(
//					"title" => esc_html__('Logo image (fixed header)', 'yreg-estate'),
//					"desc" => wp_kses_data( __('Logo image for the header (if menu is fixed after the page is scrolled)', 'yreg-estate') ),
//					"override" => "category,services_group,post,page",
//					"divider" => false,
//					"std" => "",
//					"type" => "media"
//					),

		'logo_text' => array(
					"title" => esc_html__('Logo text', 'yreg-estate'),
					"desc" => wp_kses_data( __('Logo text - display it after logo image', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => '',
					"type" => "text"
					),

//		'logo_height' => array(
//					"title" => esc_html__('Logo height', 'yreg-estate'),
//					"desc" => wp_kses_data( __('Height for the logo in the header area', 'yreg-estate') ),
//					"override" => "category,services_group,post,page",
//					"step" => 1,
//					"std" => '',
//					"min" => 10,
//					"max" => 300,
//					"mask" => "?999",
//					"type" => "spinner"
//					),
//
//		'logo_offset' => array(
//					"title" => esc_html__('Logo top offset', 'yreg-estate'),
//					"desc" => wp_kses_data( __('Top offset for the logo in the header area', 'yreg-estate') ),
//					"override" => "category,services_group,post,page",
//					"step" => 1,
//					"std" => '',
//					"min" => 0,
//					"max" => 99,
//					"mask" => "?99",
//					"type" => "spinner"
//					),
		
		
		
		
		
		
		
		// Customization -> Slider
		//-------------------------------------------------
		
		"customization_slider" => array( 
					"title" => esc_html__('Slider', 'yreg-estate'),
					"icon" => "iconadmin-picture",
					"override" => "category,services_group,page",
					"type" => "tab"),
		
		"info_slider_1" => array(
					"title" => esc_html__('Main slider parameters', 'yreg-estate'),
					"desc" => wp_kses_data( __('Select parameters for main slider (you can override it in each category and page)', 'yreg-estate') ),
					"override" => "category,services_group,page",
					"type" => "info"),
					
		"show_slider" => array(
					"title" => esc_html__('Show Slider', 'yreg-estate'),
					"desc" => wp_kses_data( __('Do you want to show slider on each page (post)', 'yreg-estate') ),
					"override" => "category,services_group,page",
					"std" => "no",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
					
		"slider_display" => array(
					"title" => esc_html__('Slider display', 'yreg-estate'),
					"desc" => wp_kses_data( __('How display slider: boxed (fixed width and height), fullwide (fixed height) or fullscreen', 'yreg-estate') ),
					"override" => "category,services_group,page",
					"dependency" => array(
						'show_slider' => array('yes')
					),
					"std" => "fullwide",
					"options" => array(
						"boxed"=>esc_html__("Boxed", 'yreg-estate'),
						"fullwide"=>esc_html__("Fullwide", 'yreg-estate'),
						"fullscreen"=>esc_html__("Fullscreen", 'yreg-estate')
					),
					"type" => "checklist"),
		
		"slider_height" => array(
					"title" => esc_html__("Height (in pixels)", 'yreg-estate'),
					"desc" => wp_kses_data( __("Slider height (in pixels) - only if slider display with fixed height.", 'yreg-estate') ),
					"override" => "category,services_group,page",
					"dependency" => array(
						'show_slider' => array('yes')
					),
					"std" => '',
					"min" => 100,
					"step" => 10,
					"type" => "spinner"),
		
		"slider_engine" => array(
					"title" => esc_html__('Slider engine', 'yreg-estate'),
					"desc" => wp_kses_data( __('What engine use to show slider?', 'yreg-estate') ),
					"override" => "category,services_group,page",
					"dependency" => array(
						'show_slider' => array('yes')
					),
					"std" => "swiper",
					"options" => yreg_estate_get_options_param('list_sliders'),
					"type" => "radio"),
		
		"slider_category" => array(
					"title" => esc_html__('Posts Slider: Category to show', 'yreg-estate'),
					"desc" => wp_kses_data( __('Select category to show in Flexslider (ignored for Revolution and Royal sliders)', 'yreg-estate') ),
					"override" => "category,services_group,page",
					"dependency" => array(
						'show_slider' => array('yes'),
						'slider_engine' => array('swiper')
					),
					"std" => "",
					"options" => yreg_estate_array_merge(array(0 => esc_html__('- Select category -', 'yreg-estate')), yreg_estate_get_options_param('list_categories')),
					"type" => "select",
					"multiple" => true,
					"style" => "list"),
		
		"slider_posts" => array(
					"title" => esc_html__('Posts Slider: Number posts or comma separated posts list',  'yreg-estate'),
					"desc" => wp_kses_data( __("How many recent posts display in slider or comma separated list of posts ID (in this case selected category ignored)", 'yreg-estate') ),
					"override" => "category,services_group,page",
					"dependency" => array(
						'show_slider' => array('yes'),
						'slider_engine' => array('swiper')
					),
					"std" => "5",
					"type" => "text"),
		
		"slider_orderby" => array(
					"title" => esc_html__("Posts Slider: Posts order by",  'yreg-estate'),
					"desc" => wp_kses_data( __("Posts in slider ordered by date (default), comments, views, author rating, users rating, random or alphabetically", 'yreg-estate') ),
					"override" => "category,services_group,page",
					"dependency" => array(
						'show_slider' => array('yes'),
						'slider_engine' => array('swiper')
					),
					"std" => "date",
					"options" => yreg_estate_get_options_param('list_sorting'),
					"type" => "select"),
		
		"slider_order" => array(
					"title" => esc_html__("Posts Slider: Posts order", 'yreg-estate'),
					"desc" => wp_kses_data( __('Select the desired ordering method for posts', 'yreg-estate') ),
					"override" => "category,services_group,page",
					"dependency" => array(
						'show_slider' => array('yes'),
						'slider_engine' => array('swiper')
					),
					"std" => "desc",
					"options" => yreg_estate_get_options_param('list_ordering'),
					"size" => "big",
					"type" => "switch"),
					
		"slider_interval" => array(
					"title" => esc_html__("Posts Slider: Slide change interval", 'yreg-estate'),
					"desc" => wp_kses_data( __("Interval (in ms) for slides change in slider", 'yreg-estate') ),
					"override" => "category,services_group,page",
					"dependency" => array(
						'show_slider' => array('yes'),
						'slider_engine' => array('swiper')
					),
					"std" => 7000,
					"min" => 100,
					"step" => 100,
					"type" => "spinner"),
		
		"slider_pagination" => array(
					"title" => esc_html__("Posts Slider: Pagination", 'yreg-estate'),
					"desc" => wp_kses_data( __("Choose pagination style for the slider", 'yreg-estate') ),
					"override" => "category,services_group,page",
					"dependency" => array(
						'show_slider' => array('yes'),
						'slider_engine' => array('swiper')
					),
					"std" => "no",
					"options" => array(
						'no'   => esc_html__('None', 'yreg-estate'),
						'yes'  => esc_html__('Dots', 'yreg-estate'), 
						'over' => esc_html__('Titles', 'yreg-estate')
					),
					"type" => "checklist"),
		
		"slider_infobox" => array(
					"title" => esc_html__("Posts Slider: Show infobox", 'yreg-estate'),
					"desc" => wp_kses_data( __("Do you want to show post's title, reviews rating and description on slides in slider", 'yreg-estate') ),
					"override" => "category,services_group,page",
					"dependency" => array(
						'show_slider' => array('yes'),
						'slider_engine' => array('swiper')
					),
					"std" => "slide",
					"options" => array(
						'no'    => esc_html__('None',  'yreg-estate'),
						'slide' => esc_html__('Slide', 'yreg-estate'), 
						'fixed' => esc_html__('Fixed', 'yreg-estate')
					),
					"type" => "checklist"),
					
		"slider_info_category" => array(
					"title" => esc_html__("Posts Slider: Show post's category", 'yreg-estate'),
					"desc" => wp_kses_data( __("Do you want to show post's category on slides in slider", 'yreg-estate') ),
					"override" => "category,services_group,page",
					"dependency" => array(
						'show_slider' => array('yes'),
						'slider_engine' => array('swiper')
					),
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
					
//		"slider_info_reviews" => array(
//					"title" => esc_html__("Posts Slider: Show post's reviews rating", 'yreg-estate'),
//					"desc" => wp_kses_data( __("Do you want to show post's reviews rating on slides in slider", 'yreg-estate') ),
//					"override" => "category,services_group,page",
//					"dependency" => array(
//						'show_slider' => array('yes'),
//						'slider_engine' => array('swiper')
//					),
//					"std" => "no",
//					"options" => yreg_estate_get_options_param('list_yes_no'),
//					"type" => "switch"),
					
		"slider_info_descriptions" => array(
					"title" => esc_html__("Posts Slider: Show post's descriptions", 'yreg-estate'),
					"desc" => wp_kses_data( __("How many characters show in the post's description in slider. 0 - no descriptions", 'yreg-estate') ),
					"override" => "category,services_group,page",
					"dependency" => array(
						'show_slider' => array('yes'),
						'slider_engine' => array('swiper')
					),
					"std" => 0,
					"min" => 0,
					"step" => 10,
					"type" => "spinner"),
		
		
		
		
		
		// Customization -> Sidebars
		//-------------------------------------------------
		
		"customization_sidebars" => array( 
					"title" => esc_html__('Sidebars', 'yreg-estate'),
					"icon" => "iconadmin-indent-right",
					"override" => "category,services_group,post,page",
					"type" => "tab"),
		
		"info_sidebars_1" => array( 
					"title" => esc_html__('Custom sidebars', 'yreg-estate'),
					"desc" => wp_kses_data( __('In this section you can create unlimited sidebars. You can fill them with widgets in the menu Appearance - Widgets', 'yreg-estate') ),
					"type" => "info"),
		
		"custom_sidebars" => array(
					"title" => esc_html__('Custom sidebars',  'yreg-estate'),
					"desc" => wp_kses_data( __('Manage custom sidebars. You can use it with each category (page, post) independently',  'yreg-estate') ),
					"std" => "",
					"cloneable" => true,
					"type" => "text"),
		
		"info_sidebars_2" => array(
					"title" => esc_html__('Main sidebar', 'yreg-estate'),
					"desc" => wp_kses_data( __('Show / Hide and select main sidebar', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"type" => "info"),
		
		'show_sidebar_main' => array( 
					"title" => esc_html__('Show main sidebar',  'yreg-estate'),
					"desc" => wp_kses_data( __('Select position for the main sidebar or hide it',  'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "right",
					"options" => yreg_estate_get_options_param('list_positions'),
					"dir" => "horizontal",
					"type" => "checklist"),

		"sidebar_main_scheme" => array(
					"title" => esc_html__("Color scheme", 'yreg-estate'),
					"desc" => wp_kses_data( __('Select predefined color scheme for the main sidebar', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"dependency" => array(
						'show_sidebar_main' => array('left', 'right')
					),
					"std" => "original",
					"dir" => "horizontal",
					"options" => yreg_estate_get_options_param('list_color_schemes'),
					"type" => "checklist"),
		
		"sidebar_main" => array( 
					"title" => esc_html__('Select main sidebar',  'yreg-estate'),
					"desc" => wp_kses_data( __('Select main sidebar content',  'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"dependency" => array(
						'show_sidebar_main' => array('left', 'right')
					),
					"std" => "sidebar_main",
					"options" => yreg_estate_get_options_param('list_sidebars'),
					"type" => "select"),
		/*
		"info_sidebars_3" => array(
					"title" => esc_html__('Outer sidebar', 'yreg-estate'),
					"desc" => wp_kses_data( __('Show / Hide and select outer sidebar (sidemenu, logo, etc.', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"type" => "info"),
		
		'show_sidebar_outer' => array( 
					"title" => esc_html__('Show outer sidebar',  'yreg-estate'),
					"desc" => wp_kses_data( __('Select position for the outer sidebar or hide it',  'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "hide",
					"options" => yreg_estate_get_options_param('list_positions'),
					"dir" => "horizontal",
					"type" => "checklist"),

		"sidebar_outer_scheme" => array(
					"title" => esc_html__("Color scheme", 'yreg-estate'),
					"desc" => wp_kses_data( __('Select predefined color scheme for the outer sidebar', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"dependency" => array(
						'show_sidebar_outer' => array('left', 'right')
					),
					"std" => "original",
					"dir" => "horizontal",
					"options" => yreg_estate_get_options_param('list_color_schemes'),
					"type" => "checklist"),
		
		"sidebar_outer_show_logo" => array( 
					"title" => esc_html__('Show Logo', 'yreg-estate'),
					"desc" => wp_kses_data( __('Show Logo in the outer sidebar', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"dependency" => array(
						'show_sidebar_outer' => array('left', 'right')
					),
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
		
		"sidebar_outer_show_socials" => array( 
					"title" => esc_html__('Show Social icons', 'yreg-estate'),
					"desc" => wp_kses_data( __('Show Social icons in the outer sidebar', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"dependency" => array(
						'show_sidebar_outer' => array('left', 'right')
					),
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
		
		"sidebar_outer_show_menu" => array( 
					"title" => esc_html__('Show Menu', 'yreg-estate'),
					"desc" => wp_kses_data( __('Show Menu in the outer sidebar', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"dependency" => array(
						'show_sidebar_outer' => array('left', 'right')
					),
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
			
		"menu_side" => array(
					"title" => esc_html__('Select menu',  'yreg-estate'),
					"desc" => wp_kses_data( __('Select menu for the outer sidebar',  'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"dependency" => array(
						'show_sidebar_outer' => array('left', 'right'),
						'sidebar_outer_show_menu' => array('yes')
					),
					"std" => "default",
					"options" => yreg_estate_get_options_param('list_menus'),
					"type" => "select"),
		
		"sidebar_outer_show_widgets" => array( 
					"title" => esc_html__('Show Widgets', 'yreg-estate'),
					"desc" => wp_kses_data( __('Show Widgets in the outer sidebar', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"dependency" => array(
						'show_sidebar_outer' => array('left', 'right')
					),
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),

		"sidebar_outer" => array( 
					"title" => esc_html__('Select outer sidebar',  'yreg-estate'),
					"desc" => wp_kses_data( __('Select outer sidebar content',  'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"dependency" => array(
						'sidebar_outer_show_widgets' => array('yes'),
						'show_sidebar_outer' => array('left', 'right')
					),
					"std" => "sidebar_outer",
					"options" => yreg_estate_get_options_param('list_sidebars'),
					"type" => "select"),
		*/
		
		
		
		// Customization -> Footer
		//-------------------------------------------------
		
		'customization_footer' => array(
					"title" => esc_html__("Footer", 'yreg-estate'),
					"override" => "category,services_group,post,page",
					"icon" => 'iconadmin-window',
					"type" => "tab"),
		
		
		"info_footer_1" => array(
					"title" => esc_html__("Footer components", 'yreg-estate'),
					"desc" => wp_kses_data( __("Select components of the footer, set style and put the content for the user's footer area", 'yreg-estate') ),
					"override" => "category,services_group,page,post",
					"type" => "info"),
		
		"show_sidebar_footer" => array(
					"title" => esc_html__('Show footer sidebar', 'yreg-estate'),
					"desc" => wp_kses_data( __('Select style for the footer sidebar or hide it', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),

		"sidebar_footer_scheme" => array(
					"title" => esc_html__("Color scheme", 'yreg-estate'),
					"desc" => wp_kses_data( __('Select predefined color scheme for the footer', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"dependency" => array(
						'show_sidebar_footer' => array('yes')
					),
					"std" => "original",
					"dir" => "horizontal",
					"options" => yreg_estate_get_options_param('list_color_schemes'),
					"type" => "checklist"),
		
		"sidebar_footer" => array( 
					"title" => esc_html__('Select footer sidebar',  'yreg-estate'),
					"desc" => wp_kses_data( __('Select footer sidebar for the blog page',  'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"dependency" => array(
						'show_sidebar_footer' => array('yes')
					),
					"std" => "sidebar_footer",
					"options" => yreg_estate_get_options_param('list_sidebars'),
					"type" => "select"),
		
		"sidebar_footer_columns" => array( 
					"title" => esc_html__('Footer sidebar columns',  'yreg-estate'),
					"desc" => wp_kses_data( __('Select columns number for the footer sidebar',  'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"dependency" => array(
						'show_sidebar_footer' => array('yes')
					),
					"std" => 4,
					"min" => 1,
					"max" => 6,
					"type" => "spinner"),
		
			
				"info_footer_2" => array(
					"title" => esc_html__('Custom shortcode box', 'yreg-estate'),
					"desc" => wp_kses_data( __('', 'yreg-estate') ),
					"override" => "category,services_group,page,post",
					"type" => "info"),
			
				"show_custom_shortcode_box_in_footer" => array(
					"title" => esc_html__('Show shortcode box', 'yreg-estate'),
					"desc" => wp_kses_data( __('', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "no",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
			
				'custom_shortcode_box' => array(
					"title" => esc_html__('Your custom HTML/JS code',  'yreg-estate'),
					"desc" => wp_kses_data( __('',  'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"dependency" => array(
						'show_custom_shortcode_box_in_footer' => array('yes')
					),
					"cols" => 80,
					"rows" => 10,
					"std" => "",
					"allow_html" => true,
					"allow_js" => true,
					"type" => "textarea"),
			
			
			
			
//		"show_emailer_in_footer" => array(
//					"title" => esc_html__('Show Emailer in footer', 'yreg-estate'),
//					"desc" => wp_kses_data( __('Show Emailer in footer."', 'yreg-estate') ),
//					"override" => "category,services_group,post,page",
//					"std" => "no",
//					"options" => yreg_estate_get_options_param('list_yes_no'),
//					"type" => "switch"),
//			"emailer_in_footer_title" => array(
//					"title" => esc_html__('Title',  'yreg-estate'),
//					"desc" => wp_kses_data( __("zZzZzZz", 'yreg-estate') ),
//					"override" => "category,services_group,page,post",
//					"dependency" => array(
//						'show_emailer_in_footer' => array('yes')
//					),
//					"std" => "Sign up for updates",
//					"type" => "text"),
			
			
			
			
				"info_footer_3" => array(
					"title" => esc_html__('Emailer in Footer', 'yreg-estate'),
					"desc" => wp_kses_data( __('Select parameters for Emailer in the Footer', 'yreg-estate') ),
					"override" => "category,services_group,page,post",
					"type" => "info"),
			
				"show_emailer_in_footer" => array(
					"title" => esc_html__('Show Emailer in footer', 'yreg-estate'),
					"desc" => wp_kses_data( __('Show Emailer in footer."', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "no",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
				"emailer_in_footer_title" => array(
					"title" => esc_html__('Title',  'yreg-estate'),
					"desc" => wp_kses_data( __("Insert your title here", 'yreg-estate') ),
					"override" => "category,services_group,page,post",
					"dependency" => array(
						'show_emailer_in_footer' => array('yes')
					),
					"std" => "Sign up for updates",
					"type" => "text"),
			
			
			
		/*
		"info_footer_2" => array(
					"title" => esc_html__('Testimonials in Footer', 'yreg-estate'),
					"desc" => wp_kses_data( __('Select parameters for Testimonials in the Footer', 'yreg-estate') ),
					"override" => "category,services_group,page,post",
					"type" => "info"),

		"show_testimonials_in_footer" => array(
					"title" => esc_html__('Show Testimonials in footer', 'yreg-estate'),
					"desc" => wp_kses_data( __('Show Testimonials slider in footer. For correct operation of the slider (and shortcode testimonials) you must fill out Testimonials posts on the menu "Testimonials"', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "no",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),

		"testimonials_scheme" => array(
					"title" => esc_html__("Color scheme", 'yreg-estate'),
					"desc" => wp_kses_data( __('Select predefined color scheme for the testimonials area', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"dependency" => array(
						'show_testimonials_in_footer' => array('yes')
					),
					"std" => "original",
					"dir" => "horizontal",
					"options" => yreg_estate_get_options_param('list_color_schemes'),
					"type" => "checklist"),

		"testimonials_count" => array( 
					"title" => esc_html__('Testimonials count', 'yreg-estate'),
					"desc" => wp_kses_data( __('Number testimonials to show', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"dependency" => array(
						'show_testimonials_in_footer' => array('yes')
					),
					"std" => 3,
					"step" => 1,
					"min" => 1,
					"max" => 10,
					"type" => "spinner"),
		*/
			
		/*
		"info_footer_3" => array(
					"title" => esc_html__('Twitter in Footer', 'yreg-estate'),
					"desc" => wp_kses_data( __('Select parameters for Twitter stream in the Footer (you can override it in each category and page)', 'yreg-estate') ),
					"override" => "category,services_group,page,post",
					"type" => "info"),

		"show_twitter_in_footer" => array(
					"title" => esc_html__('Show Twitter in footer', 'yreg-estate'),
					"desc" => wp_kses_data( __('Show Twitter slider in footer. For correct operation of the slider (and shortcode twitter) you must fill out the Twitter API keys on the menu "Appearance - Theme Options - Socials"', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "no",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),

		"twitter_scheme" => array(
					"title" => esc_html__("Color scheme", 'yreg-estate'),
					"desc" => wp_kses_data( __('Select predefined color scheme for the twitter area', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"dependency" => array(
						'show_twitter_in_footer' => array('yes')
					),
					"std" => "original",
					"dir" => "horizontal",
					"options" => yreg_estate_get_options_param('list_color_schemes'),
					"type" => "checklist"),

		"twitter_count" => array( 
					"title" => esc_html__('Twitter count', 'yreg-estate'),
					"desc" => wp_kses_data( __('Number twitter to show', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"dependency" => array(
						'show_twitter_in_footer' => array('yes')
					),
					"std" => 3,
					"step" => 1,
					"min" => 1,
					"max" => 10,
					"type" => "spinner"),
		*/

			
		/*
		"info_footer_4" => array(
					"title" => esc_html__('Google map parameters', 'yreg-estate'),
					"desc" => wp_kses_data( __('Select parameters for Google map (you can override it in each category and page)', 'yreg-estate') ),
					"override" => "category,services_group,page,post",
					"type" => "info"),
					
		"show_googlemap" => array(
					"title" => esc_html__('Show Google Map', 'yreg-estate'),
					"desc" => wp_kses_data( __('Do you want to show Google map on each page (post)', 'yreg-estate') ),
					"override" => "category,services_group,page,post",
					"std" => "no",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
		
		"googlemap_height" => array(
					"title" => esc_html__("Map height", 'yreg-estate'),
					"desc" => wp_kses_data( __("Map height (default - in pixels, allows any CSS units of measure)", 'yreg-estate') ),
					"override" => "category,services_group,page",
					"dependency" => array(
						'show_googlemap' => array('yes')
					),
					"std" => 400,
					"min" => 100,
					"step" => 10,
					"type" => "spinner"),
		
		"googlemap_address" => array(
					"title" => esc_html__('Address to show on map',  'yreg-estate'),
					"desc" => wp_kses_data( __("Enter address to show on map center", 'yreg-estate') ),
					"override" => "category,services_group,page,post",
					"dependency" => array(
						'show_googlemap' => array('yes')
					),
					"std" => "",
					"type" => "text"),
		
		"googlemap_latlng" => array(
					"title" => esc_html__('Latitude and Longitude to show on map',  'yreg-estate'),
					"desc" => wp_kses_data( __("Enter coordinates (separated by comma) to show on map center (instead of address)", 'yreg-estate') ),
					"override" => "category,services_group,page,post",
					"dependency" => array(
						'show_googlemap' => array('yes')
					),
					"std" => "",
					"type" => "text"),
		
		"googlemap_title" => array(
					"title" => esc_html__('Title to show on map',  'yreg-estate'),
					"desc" => wp_kses_data( __("Enter title to show on map center", 'yreg-estate') ),
					"override" => "category,services_group,page,post",
					"dependency" => array(
						'show_googlemap' => array('yes')
					),
					"std" => "",
					"type" => "text"),
		
		"googlemap_description" => array(
					"title" => esc_html__('Description to show on map',  'yreg-estate'),
					"desc" => wp_kses_data( __("Enter description to show on map center", 'yreg-estate') ),
					"override" => "category,services_group,page,post",
					"dependency" => array(
						'show_googlemap' => array('yes')
					),
					"std" => "",
					"type" => "text"),
		
		"googlemap_zoom" => array(
					"title" => esc_html__('Google map initial zoom',  'yreg-estate'),
					"desc" => wp_kses_data( __("Enter desired initial zoom for Google map", 'yreg-estate') ),
					"override" => "category,services_group,page,post",
					"dependency" => array(
						'show_googlemap' => array('yes')
					),
					"std" => 16,
					"min" => 1,
					"max" => 20,
					"step" => 1,
					"type" => "spinner"),
		
		"googlemap_style" => array(
					"title" => esc_html__('Google map style',  'yreg-estate'),
					"desc" => wp_kses_data( __("Select style to show Google map", 'yreg-estate') ),
					"override" => "category,services_group,page,post",
					"dependency" => array(
						'show_googlemap' => array('yes')
					),
					"std" => 'style1',
					"options" => yreg_estate_get_options_param('list_gmap_styles'),
					"type" => "select"),
		
		"googlemap_marker" => array(
					"title" => esc_html__('Google map marker',  'yreg-estate'),
					"desc" => wp_kses_data( __("Select or upload png-image with Google map marker", 'yreg-estate') ),
					"dependency" => array(
						'show_googlemap' => array('yes')
					),
					"std" => '',
					"type" => "media"),
		*/
		
		
			
		"info_footer_5" => array(
					"title" => esc_html__("Contacts area", 'yreg-estate'),
					"desc" => wp_kses_data( __("Show/Hide contacts area in the footer", 'yreg-estate') ),
					"override" => "category,services_group,page,post",
					"type" => "info"),
		
		"show_contacts_in_footer" => array(
					"title" => esc_html__('Show Contacts in footer', 'yreg-estate'),
					"desc" => wp_kses_data( __('Show contact information area in footer: site logo, contact info and large social icons', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "no",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
		
		"show_logo_footer" => array(
					"title" => esc_html__('Show Logo in footer',  'yreg-estate'),
					"desc" => wp_kses_data( __("Show Logo area in footer", 'yreg-estate') ),
					"override" => "category,services_group,page,post",
					"dependency" => array(
						'show_contacts_in_footer' => array('yes')
					),
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
			
			'logo_footer' => array(
					"title" => esc_html__('Logo image for footer', 'yreg-estate'),
					"desc" => wp_kses_data( __('Logo image in the footer (in the contacts area)', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"dependency" => array(
						'show_contacts_in_footer' => array('yes'),
						'show_logo_footer' => array('yes')
					),
					"divider" => false,
					"std" => "",
					"type" => "media"
					),
			
			"show_textbox_footer" => array(
					"title" => esc_html__('Show text in footer',  'yreg-estate'),
					"desc" => wp_kses_data( __("Show text area in footer", 'yreg-estate') ),
					"override" => "category,services_group,page,post",
					"dependency" => array(
						'show_contacts_in_footer' => array('yes')
					),
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
			
			"title_textbox_footer" => array(
					"title" => esc_html__('Title to text in footer',  'yreg-estate'),
					"desc" => wp_kses_data( __("Enter title to text in footer", 'yreg-estate') ),
					"override" => "category,services_group,page,post",
					"dependency" => array(
						'show_contacts_in_footer' => array('yes'),
						'show_textbox_footer' => array('yes')
					),
					"divider" => false,
					"std" => "about us",
					"type" => "text"),
			
			"content_textbox_footer" => array(
					"title" => esc_html__('Footer text',  'yreg-estate'),
					"desc" => wp_kses_data( __("Copyright text to show in footer area (bottom of site)", 'yreg-estate') ),
					"override" => "category,services_group,page,post",
					"dependency" => array(
						'show_contacts_in_footer' => array('yes'),
						'show_textbox_footer' => array('yes')
					),
					"allow_html" => true,
					"divider" => false,
					"std" => "We are the leading real estate and rental marketplace dedicated to empowering consumers with data, inspiration and knowledge around the place they call home, and connecting them with the best local professionals who can help.",
					"rows" => "10",
					"type" => "editor"),
			
			"show_socbox_footer" => array(
					"title" => esc_html__('Show socials in footer',  'yreg-estate'),
					"desc" => wp_kses_data( __("Show socials area in footer", 'yreg-estate') ),
					"override" => "category,services_group,page,post",
					"dependency" => array(
						'show_contacts_in_footer' => array('yes')
					),
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
			
			"title_socbox_footer" => array(
					"title" => esc_html__('Title to socials in footer',  'yreg-estate'),
					"desc" => wp_kses_data( __("Enter title to socials in footer", 'yreg-estate') ),
					"override" => "category,services_group,page,post",
					"dependency" => array(
						'show_contacts_in_footer' => array('yes'),
						'show_socbox_footer' => array('yes')
					),
					"divider" => false,
					"std" => "follow us",
					"type" => "text"),
			
		
		
		"info_footer_6" => array(
					"title" => esc_html__("Copyright and footer menu", 'yreg-estate'),
					"desc" => wp_kses_data( __("Show/Hide copyright area in the footer", 'yreg-estate') ),
					"override" => "category,services_group,page,post",
					"type" => "info"),

		"show_copyright_in_footer" => array(
					"title" => esc_html__('Show Copyright area in footer', 'yreg-estate'),
					"desc" => wp_kses_data( __('Show area with copyright information, footer menu and small social icons in footer', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "plain",
					"options" => array(
						'none' => esc_html__('Hide', 'yreg-estate'),
						'text' => esc_html__('Text', 'yreg-estate'),
						'menu' => esc_html__('Text and menu', 'yreg-estate'),
						'socials' => esc_html__('Text and Social icons', 'yreg-estate')
					),
					"type" => "checklist"),

		"copyright_scheme" => array(
					"title" => esc_html__("Color scheme", 'yreg-estate'),
					"desc" => wp_kses_data( __('Select predefined color scheme for the copyright area', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"dependency" => array(
						'show_copyright_in_footer' => array('text', 'menu', 'socials')
					),
					"std" => "original",
					"dir" => "horizontal",
					"options" => yreg_estate_get_options_param('list_color_schemes'),
					"type" => "checklist"),
		
		"menu_footer" => array( 
					"title" => esc_html__('Select footer menu',  'yreg-estate'),
					"desc" => wp_kses_data( __('Select footer menu for the current page',  'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "default",
					"dependency" => array(
						'show_copyright_in_footer' => array('menu')
					),
					"options" => yreg_estate_get_options_param('list_menus'),
					"type" => "select"),

		"footer_copyright" => array(
					"title" => esc_html__('Footer copyright text',  'yreg-estate'),
					"desc" => wp_kses_data( __("Copyright text to show in footer area (bottom of site)", 'yreg-estate') ),
					"override" => "category,services_group,page,post",
					"dependency" => array(
						'show_copyright_in_footer' => array('text', 'menu', 'socials')
					),
					"allow_html" => true,
					"std" => "ESTATE &copy; 2016 All Rights Reserved ",
					"rows" => "10",
					"type" => "editor"),




		// Customization -> Other
		//-------------------------------------------------
		
		'customization_other' => array(
					"title" => esc_html__('Other', 'yreg-estate'),
					"override" => "category,services_group,page,post",
					"icon" => 'iconadmin-cog',
					"type" => "tab"
					),

		'info_other_1' => array(
					"title" => esc_html__('Theme customization other parameters', 'yreg-estate'),
					"desc" => wp_kses_data( __('Animation parameters and responsive layouts for the small screens', 'yreg-estate') ),
					"type" => "info"
					),

		'show_theme_customizer' => array(
					"title" => esc_html__('Show Theme customizer', 'yreg-estate'),
					"desc" => wp_kses_data( __('Do you want to show theme customizer in the right panel? Your website visitors will be able to customise it yourself.', 'yreg-estate') ),
					"std" => "no",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"
					),

		"customizer_demo" => array(
					"title" => esc_html__('Theme customizer panel demo time', 'yreg-estate'),
					"desc" => wp_kses_data( __('Timer for demo mode for the customizer panel (in milliseconds: 1000ms = 1s). If 0 - no demo.', 'yreg-estate') ),
					"dependency" => array(
						'show_theme_customizer' => array('yes')
					),
					"std" => "0",
					"min" => 0,
					"max" => 10000,
					"step" => 500,
					"type" => "spinner"),
		
		'css_animation' => array(
					"title" => esc_html__('Extended CSS animations', 'yreg-estate'),
					"desc" => wp_kses_data( __('Do you want use extended animations effects on your site?', 'yreg-estate') ),
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"
					),
		
		'animation_on_mobile' => array(
					"title" => esc_html__('Allow CSS animations on mobile', 'yreg-estate'),
					"desc" => wp_kses_data( __('Do you allow extended animations effects on mobile devices?', 'yreg-estate') ),
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"
					),

		'remember_visitors_settings' => array(
					"title" => esc_html__("Remember visitor's settings", 'yreg-estate'),
					"desc" => wp_kses_data( __('To remember the settings that were made by the visitor, when navigating to other pages or to limit their effect only within the current page', 'yreg-estate') ),
					"std" => "no",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"
					),
					
		'responsive_layouts' => array(
					"title" => esc_html__('Responsive Layouts', 'yreg-estate'),
					"desc" => wp_kses_data( __('Do you want use responsive layouts on small screen or still use main layout?', 'yreg-estate') ),
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"
					),
		
		'page_preloader' => array(
					"title" => esc_html__('Show page preloader',  'yreg-estate'),
					"desc" => wp_kses_data( __('Do you want show animated page preloader?',  'yreg-estate') ),
					"std" => "",
					"type" => "media"
					),


		'info_other_2' => array(
					"title" => esc_html__('Google fonts parameters', 'yreg-estate'),
					"desc" => wp_kses_data( __('Specify additional parameters, used to load Google fonts', 'yreg-estate') ),
					"type" => "info"
					),
		
		"fonts_subset" => array(
					"title" => esc_html__('Characters subset', 'yreg-estate'),
					"desc" => wp_kses_data( __('Select subset, included into used Google fonts', 'yreg-estate') ),
					"std" => "latin,latin-ext",
					"options" => array(
						'latin' => esc_html__('Latin', 'yreg-estate'),
						'latin-ext' => esc_html__('Latin Extended', 'yreg-estate'),
						'greek' => esc_html__('Greek', 'yreg-estate'),
						'greek-ext' => esc_html__('Greek Extended', 'yreg-estate'),
						'cyrillic' => esc_html__('Cyrillic', 'yreg-estate'),
						'cyrillic-ext' => esc_html__('Cyrillic Extended', 'yreg-estate'),
						'vietnamese' => esc_html__('Vietnamese', 'yreg-estate')
					),
					"size" => "medium",
					"dir" => "vertical",
					"multiple" => true,
					"type" => "checklist"),


		'info_other_3' => array(
					"title" => esc_html__('Additional CSS and HTML/JS code', 'yreg-estate'),
					"desc" => wp_kses_data( __('Put here your custom CSS and JS code', 'yreg-estate') ),
					"override" => "category,services_group,page,post",
					"type" => "info"
					),
					
		'custom_css_html' => array(
					"title" => esc_html__('Use custom CSS/HTML/JS', 'yreg-estate'),
					"desc" => wp_kses_data( __('Do you want use custom HTML/CSS/JS code in your site? For example: custom styles, Google Analitics code, etc.', 'yreg-estate') ),
					"std" => "no",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"
					),
		
		"gtm_code" => array(
					"title" => esc_html__('Google tags manager or Google analitics code',  'yreg-estate'),
					"desc" => wp_kses_data( __('Put here Google Tags Manager (GTM) code from your account: Google analitics, remarketing, etc. This code will be placed after open body tag.',  'yreg-estate') ),
					"dependency" => array(
						'custom_css_html' => array('yes')
					),
					"cols" => 80,
					"rows" => 20,
					"std" => "",
					"allow_html" => true,
					"allow_js" => true,
					"type" => "textarea"),
		
		"gtm_code2" => array(
					"title" => esc_html__('Google remarketing code',  'yreg-estate'),
					"desc" => wp_kses_data( __('Put here Google Remarketing code from your account. This code will be placed before close body tag.',  'yreg-estate') ),
					"dependency" => array(
						'custom_css_html' => array('yes')
					),
					"divider" => false,
					"cols" => 80,
					"rows" => 20,
					"std" => "",
					"allow_html" => true,
					"allow_js" => true,
					"type" => "textarea"),
		
		'custom_code' => array(
					"title" => esc_html__('Your custom HTML/JS code',  'yreg-estate'),
					"desc" => wp_kses_data( __('Put here your invisible html/js code: Google analitics, counters, etc',  'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"dependency" => array(
						'custom_css_html' => array('yes')
					),
					"cols" => 80,
					"rows" => 20,
					"std" => "",
					"allow_html" => true,
					"allow_js" => true,
					"type" => "textarea"
					),
		
		'custom_css' => array(
					"title" => esc_html__('Your custom CSS code',  'yreg-estate'),
					"desc" => wp_kses_data( __('Put here your css code to correct main theme styles',  'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"dependency" => array(
						'custom_css_html' => array('yes')
					),
					"divider" => false,
					"cols" => 80,
					"rows" => 20,
					"std" => "",
					"type" => "textarea"
					),
		
		
		
		
		
		
		
		
		
		//###############################
		//#### Blog and Single pages #### 
		//###############################
		"partition_blog" => array(
					"title" => esc_html__('Blog &amp; Single', 'yreg-estate'),
					"icon" => "iconadmin-docs",
					"override" => "category,services_group,post,page",
					"type" => "partition"),
		
		
		
		// Blog -> Stream page
		//-------------------------------------------------
		
		'blog_tab_stream' => array(
					"title" => esc_html__('Stream page', 'yreg-estate'),
					"start" => 'blog_tabs',
					"icon" => "iconadmin-docs",
					"override" => "category,services_group,post,page",
					"type" => "tab"),
		
		"info_blog_1" => array(
					"title" => esc_html__('Blog streampage parameters', 'yreg-estate'),
					"desc" => wp_kses_data( __('Select desired blog streampage parameters (you can override it in each category)', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"type" => "info"),
		
		"blog_style" => array(
					"title" => esc_html__('Blog style', 'yreg-estate'),
					"desc" => wp_kses_data( __('Select desired blog style', 'yreg-estate') ),
					"override" => "category,services_group,page",
					"std" => "excerpt",
					"options" => yreg_estate_get_options_param('list_blog_styles'),
					"type" => "select"),
		
		"hover_style" => array(
					"title" => esc_html__('Hover style', 'yreg-estate'),
					"desc" => wp_kses_data( __('Select desired hover style (only for Blog style = Portfolio)', 'yreg-estate') ),
					"override" => "category,services_group,page",
					"dependency" => array(
						'blog_style' => array('portfolio','grid','square','colored')
					),
					"std" => "square effect_shift",
					"options" => yreg_estate_get_options_param('list_hovers'),
					"type" => "select"),
		
		"hover_dir" => array(
					"title" => esc_html__('Hover dir', 'yreg-estate'),
					"desc" => wp_kses_data( __('Select hover direction (only for Blog style = Portfolio and Hover style = Circle or Square)', 'yreg-estate') ),
					"override" => "category,services_group,page",
					"dependency" => array(
						'blog_style' => array('portfolio','grid','square','colored'),
						'hover_style' => array('square','circle')
					),
					"std" => "left_to_right",
					"options" => yreg_estate_get_options_param('list_hovers_dir'),
					"type" => "select"),
		
		"article_style" => array(
					"title" => esc_html__('Article style', 'yreg-estate'),
					"desc" => wp_kses_data( __('Select article display method: boxed or stretch', 'yreg-estate') ),
					"override" => "category,services_group,page",
					"std" => "stretch",
					"options" => yreg_estate_get_options_param('list_article_styles'),
					"size" => "medium",
					"type" => "switch"),
		
		"dedicated_location" => array(
					"title" => esc_html__('Dedicated location', 'yreg-estate'),
					"desc" => wp_kses_data( __('Select location for the dedicated content or featured image in the "excerpt" blog style', 'yreg-estate') ),
					"override" => "category,services_group,page,post",
					"dependency" => array(
						'blog_style' => array('excerpt')
					),
					"std" => "default",
					"options" => yreg_estate_get_options_param('list_locations'),
					"type" => "select"),
		
		"show_filters" => array(
					"title" => esc_html__('Show filters', 'yreg-estate'),
					"desc" => wp_kses_data( __('What taxonomy use for filter buttons', 'yreg-estate') ),
					"override" => "category,services_group,page",
					"dependency" => array(
						'blog_style' => array('portfolio','grid','square','colored')
					),
					"std" => "hide",
					"options" => yreg_estate_get_options_param('list_filters'),
					"type" => "checklist"),
		
		"blog_sort" => array(
					"title" => esc_html__('Blog posts sorted by', 'yreg-estate'),
					"desc" => wp_kses_data( __('Select the desired sorting method for posts', 'yreg-estate') ),
					"override" => "category,services_group,page",
					"std" => "date",
					"options" => yreg_estate_get_options_param('list_sorting'),
					"dir" => "vertical",
					"type" => "radio"),
		
		"blog_order" => array(
					"title" => esc_html__('Blog posts order', 'yreg-estate'),
					"desc" => wp_kses_data( __('Select the desired ordering method for posts', 'yreg-estate') ),
					"override" => "category,services_group,page",
					"std" => "desc",
					"options" => yreg_estate_get_options_param('list_ordering'),
					"size" => "big",
					"type" => "switch"),
		
		"posts_per_page" => array(
					"title" => esc_html__('Blog posts per page',  'yreg-estate'),
					"desc" => wp_kses_data( __('How many posts display on blog pages for selected style. If empty or 0 - inherit system wordpress settings',  'yreg-estate') ),
					"override" => "category,services_group,page",
					"std" => "12",
					"mask" => "?99",
					"type" => "text"),
		
		"post_excerpt_maxlength" => array(
					"title" => esc_html__('Excerpt maxlength for streampage',  'yreg-estate'),
					"desc" => wp_kses_data( __('How many characters from post excerpt are display in blog streampage (only for Blog style = Excerpt). 0 - do not trim excerpt.',  'yreg-estate') ),
					"override" => "category,services_group,page",
					"dependency" => array(
						'blog_style' => array('excerpt', 'portfolio', 'grid', 'square', 'related')
					),
					"std" => "250",
					"mask" => "?9999",
					"type" => "text"),
		
		"post_excerpt_maxlength_masonry" => array(
					"title" => esc_html__('Excerpt maxlength for classic and masonry',  'yreg-estate'),
					"desc" => wp_kses_data( __('How many characters from post excerpt are display in blog streampage (only for Blog style = Classic or Masonry). 0 - do not trim excerpt.',  'yreg-estate') ),
					"override" => "category,services_group,page",
					"dependency" => array(
						'blog_style' => array('masonry', 'classic')
					),
					"std" => "80",
					"mask" => "?9999",
					"type" => "text"),
		
		
		
		
		// Blog -> Single page
		//-------------------------------------------------
		
		'blog_tab_single' => array(
					"title" => esc_html__('Single page', 'yreg-estate'),
					"icon" => "iconadmin-doc",
					"override" => "category,services_group,post,page",
					"type" => "tab"),
		
		
		"info_single_1" => array(
					"title" => esc_html__('Single (detail) pages parameters', 'yreg-estate'),
					"desc" => wp_kses_data( __('Select desired parameters for single (detail) pages (you can override it in each category and single post (page))', 'yreg-estate') ),
					"override" => "category,services_group,page,post",
					"type" => "info"),
		
		"single_style" => array(
					"title" => esc_html__('Single page style', 'yreg-estate'),
					"desc" => wp_kses_data( __('Select desired style for single page', 'yreg-estate') ),
					"override" => "category,services_group,page,post",
					"std" => "single-standard",
					"options" => yreg_estate_get_options_param('list_single_styles'),
					"dir" => "horizontal",
					"type" => "radio"),

		"icon" => array(
					"title" => esc_html__('Select post icon', 'yreg-estate'),
					"desc" => wp_kses_data( __('Select icon for output before post/category name in some layouts', 'yreg-estate') ),
					"override" => "services_group,page,post",
					"std" => "",
					"options" => yreg_estate_get_options_param('list_icons'),
					"style" => "select",
					"type" => "icons"
					),

		"alter_thumb_size" => array(
					"title" => esc_html__('Alter thumb size (WxH)',  'yreg-estate'),
					"override" => "page,post",
					"desc" => wp_kses_data( __("Select thumb size for the alternative portfolio layout (number items horizontally x number items vertically)", 'yreg-estate') ),
					"class" => "",
					"std" => "1_1",
					"type" => "radio",
					"options" => yreg_estate_get_options_param('list_alter_sizes')
					),
		
		"show_featured_image" => array(
					"title" => esc_html__('Show featured image before post',  'yreg-estate'),
					"desc" => wp_kses_data( __("Show featured image (if selected) before post content on single pages", 'yreg-estate') ),
					"override" => "category,services_group,page,post",
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
		
		"show_post_title" => array(
					"title" => esc_html__('Show post title', 'yreg-estate'),
					"desc" => wp_kses_data( __('Show area with post title on single pages', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
		
//			"show_post_property_info" => array(
//					"title" => esc_html__('Show post property info', 'yreg-estate'),
//					"desc" => wp_kses_data( __('', 'yreg-estate') ),
//					"override" => "category,services_group,post,page",
//					"std" => "yes",
//					"options" => yreg_estate_get_options_param('list_yes_no'),
//					"type" => "switch"),
			
		"show_post_title_on_quotes" => array(
					"title" => esc_html__('Show post title on links, chat, quote, status', 'yreg-estate'),
					"desc" => wp_kses_data( __('Show area with post title on single and blog pages in specific post formats: links, chat, quote, status', 'yreg-estate') ),
					"override" => "category,services_group,page",
					"std" => "no",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
		
		"show_post_info" => array(
					"title" => esc_html__('Show post info', 'yreg-estate'),
					"desc" => wp_kses_data( __('Show area with post info on single pages', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
		
		"show_text_before_readmore" => array(
					"title" => esc_html__('Show text before "Read more" tag', 'yreg-estate'),
					"desc" => wp_kses_data( __('Show text before "Read more" tag on single pages', 'yreg-estate') ),
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
					
		"show_post_author" => array(
					"title" => esc_html__('Show post author details',  'yreg-estate'),
					"desc" => wp_kses_data( __("Show post author information block on single post page", 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
		
		"show_post_tags" => array(
					"title" => esc_html__('Show post tags',  'yreg-estate'),
					"desc" => wp_kses_data( __("Show tags block on single post page", 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
		
//		"show_post_related" => array(
//					"title" => esc_html__('Show related posts',  'yreg-estate'),
//					"desc" => wp_kses_data( __("Show related posts block on single post page", 'yreg-estate') ),
//					"override" => "category,services_group,post,page",
//					"std" => "no",
//					"options" => yreg_estate_get_options_param('list_yes_no'),
//					"type" => "switch"),
//
//		"post_related_count" => array(
//					"title" => esc_html__('Related posts number',  'yreg-estate'),
//					"desc" => wp_kses_data( __("How many related posts showed on single post page", 'yreg-estate') ),
//					"dependency" => array(
//						'show_post_related' => array('yes')
//					),
//					"override" => "category,services_group,post,page",
//					"std" => "2",
//					"step" => 1,
//					"min" => 2,
//					"max" => 8,
//					"type" => "spinner"),
//
//		"post_related_columns" => array(
//					"title" => esc_html__('Related posts columns',  'yreg-estate'),
//					"desc" => wp_kses_data( __("How many columns used to show related posts on single post page. 1 - use scrolling to show all related posts", 'yreg-estate') ),
//					"override" => "category,services_group,post,page",
//					"dependency" => array(
//						'show_post_related' => array('yes')
//					),
//					"std" => "2",
//					"step" => 1,
//					"min" => 1,
//					"max" => 4,
//					"type" => "spinner"),
//		
//		"post_related_sort" => array(
//					"title" => esc_html__('Related posts sorted by', 'yreg-estate'),
//					"desc" => wp_kses_data( __('Select the desired sorting method for related posts', 'yreg-estate') ),
//		//			"override" => "category,services_group,page",
//					"dependency" => array(
//						'show_post_related' => array('yes')
//					),
//					"std" => "date",
//					"options" => yreg_estate_get_options_param('list_sorting'),
//					"type" => "select"),
//		
//		"post_related_order" => array(
//					"title" => esc_html__('Related posts order', 'yreg-estate'),
//					"desc" => wp_kses_data( __('Select the desired ordering method for related posts', 'yreg-estate') ),
//		//			"override" => "category,services_group,page",
//					"dependency" => array(
//						'show_post_related' => array('yes')
//					),
//					"std" => "desc",
//					"options" => yreg_estate_get_options_param('list_ordering'),
//					"size" => "big",
//					"type" => "switch"),
		
		"show_post_comments" => array(
					"title" => esc_html__('Show comments',  'yreg-estate'),
					"desc" => wp_kses_data( __("Show comments block on single post page", 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
		
		
		
		// Blog -> Other parameters
		//-------------------------------------------------
		
		'blog_tab_other' => array(
					"title" => esc_html__('Other parameters', 'yreg-estate'),
					"icon" => "iconadmin-newspaper",
					"override" => "category,services_group,page",
					"type" => "tab"),
		
		"info_blog_other_1" => array(
					"title" => esc_html__('Other Blog parameters', 'yreg-estate'),
					"desc" => wp_kses_data( __('Select excluded categories, substitute parameters, etc.', 'yreg-estate') ),
					"type" => "info"),
		
		"exclude_cats" => array(
					"title" => esc_html__('Exclude categories', 'yreg-estate'),
					"desc" => wp_kses_data( __('Select categories, which posts are exclude from blog page', 'yreg-estate') ),
					"std" => "",
					"options" => yreg_estate_get_options_param('list_categories'),
					"multiple" => true,
					"style" => "list",
					"type" => "select"),
		
		"blog_pagination" => array(
					"title" => esc_html__('Blog pagination', 'yreg-estate'),
					"desc" => wp_kses_data( __('Select type of the pagination on blog streampages', 'yreg-estate') ),
					"std" => "pages",
					"override" => "category,services_group,page",
					"options" => array(
						'pages'    => esc_html__('Standard page numbers', 'yreg-estate'),
//						'slider'   => esc_html__('Slider with page numbers', 'yreg-estate'),
						'viewmore' => esc_html__('"View more" button', 'yreg-estate'),
						'infinite' => esc_html__('Infinite scroll', 'yreg-estate')
					),
					"dir" => "vertical",
					"type" => "radio"),
		
		"blog_counters" => array(
					"title" => esc_html__('Blog counters', 'yreg-estate'),
					"desc" => wp_kses_data( __('Select counters, displayed near the post title', 'yreg-estate') ),
					"override" => "category,services_group,page",
					"std" => "views",
					"options" => yreg_estate_get_options_param('list_blog_counters'),
					"dir" => "vertical",
					"multiple" => true,
					"type" => "checklist"),
		
		"close_category" => array(
					"title" => esc_html__("Post's category announce", 'yreg-estate'),
					"desc" => wp_kses_data( __('What category display in announce block (over posts thumb) - original or nearest parental', 'yreg-estate') ),
					"override" => "category,services_group,page",
					"std" => "parental",
					"options" => array(
						'parental' => esc_html__('Nearest parental category', 'yreg-estate'),
						'original' => esc_html__("Original post's category", 'yreg-estate')
					),
					"dir" => "vertical",
					"type" => "radio"),
		
		"show_date_after" => array(
					"title" => esc_html__('Show post date after', 'yreg-estate'),
					"desc" => wp_kses_data( __('Show post date after N days (before - show post age)', 'yreg-estate') ),
					"override" => "category,services_group,page",
					"std" => "30",
					"mask" => "?99",
					"type" => "text"),
		
		
		
		
		
		//###############################
		//#### Reviews               #### 
		//###############################
			/*
		"partition_reviews" => array(
					"title" => esc_html__('Reviews', 'yreg-estate'),
					"icon" => "iconadmin-newspaper",
					"override" => "category,services_group,services_group",
					"type" => "partition"),
		
		"info_reviews_1" => array(
					"title" => esc_html__('Reviews criterias', 'yreg-estate'),
					"desc" => wp_kses_data( __('Set up list of reviews criterias. You can override it in any category.', 'yreg-estate') ),
					"override" => "category,services_group,services_group",
					"type" => "info"),
		
		"show_reviews" => array(
					"title" => esc_html__('Show reviews block',  'yreg-estate'),
					"desc" => wp_kses_data( __("Show reviews block on single post page and average reviews rating after post's title in stream pages", 'yreg-estate') ),
					"override" => "category,services_group,services_group",
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
		
		"reviews_max_level" => array(
					"title" => esc_html__('Max reviews level',  'yreg-estate'),
					"desc" => wp_kses_data( __("Maximum level for reviews marks", 'yreg-estate') ),
					"std" => "5",
					"options" => array(
						'5'=>esc_html__('5 stars', 'yreg-estate'), 
						'10'=>esc_html__('10 stars', 'yreg-estate'), 
						'100'=>esc_html__('100%', 'yreg-estate')
					),
					"type" => "radio",
					),
		
		"reviews_style" => array(
					"title" => esc_html__('Show rating as',  'yreg-estate'),
					"desc" => wp_kses_data( __("Show rating marks as text or as stars/progress bars.", 'yreg-estate') ),
					"std" => "stars",
					"options" => array(
						'text' => esc_html__('As text (for example: 7.5 / 10)', 'yreg-estate'), 
						'stars' => esc_html__('As stars or bars', 'yreg-estate')
					),
					"dir" => "vertical",
					"type" => "radio"),
		
		"reviews_criterias_levels" => array(
					"title" => esc_html__('Reviews Criterias Levels', 'yreg-estate'),
					"desc" => wp_kses_data( __('Words to mark criterials levels. Just write the word and press "Enter". Also you can arrange words.', 'yreg-estate') ),
					"std" => esc_html__("bad,poor,normal,good,great", 'yreg-estate'),
					"type" => "tags"),
		
		"reviews_first" => array(
					"title" => esc_html__('Show first reviews',  'yreg-estate'),
					"desc" => wp_kses_data( __("What reviews will be displayed first: by author or by visitors. Also this type of reviews will display under post's title.", 'yreg-estate') ),
					"std" => "author",
					"options" => array(
						'author' => esc_html__('By author', 'yreg-estate'),
						'users' => esc_html__('By visitors', 'yreg-estate')
						),
					"dir" => "horizontal",
					"type" => "radio"),
		
		"reviews_second" => array(
					"title" => esc_html__('Hide second reviews',  'yreg-estate'),
					"desc" => wp_kses_data( __("Do you want hide second reviews tab in widgets and single posts?", 'yreg-estate') ),
					"std" => "show",
					"options" => yreg_estate_get_options_param('list_show_hide'),
					"size" => "medium",
					"type" => "switch"),
		
		"reviews_can_vote" => array(
					"title" => esc_html__('What visitors can vote',  'yreg-estate'),
					"desc" => wp_kses_data( __("What visitors can vote: all or only registered", 'yreg-estate') ),
					"std" => "all",
					"options" => array(
						'all'=>esc_html__('All visitors', 'yreg-estate'), 
						'registered'=>esc_html__('Only registered', 'yreg-estate')
					),
					"dir" => "horizontal",
					"type" => "radio"),
		
		"reviews_criterias" => array(
					"title" => esc_html__('Reviews criterias',  'yreg-estate'),
					"desc" => wp_kses_data( __('Add default reviews criterias.',  'yreg-estate') ),
					"override" => "category,services_group,services_group",
					"std" => "",
					"cloneable" => true,
					"type" => "text"),

		// Don't remove this parameter - it used in admin for store marks
		"reviews_marks" => array(
					"std" => "",
					"type" => "hidden"),
		*/





		//###############################
		//#### Media                #### 
		//###############################
		"partition_media" => array(
					"title" => esc_html__('Media', 'yreg-estate'),
					"icon" => "iconadmin-picture",
					"override" => "category,services_group,post,page",
					"type" => "partition"),
		
		"info_media_1" => array(
					"title" => esc_html__('Media settings', 'yreg-estate'),
					"desc" => wp_kses_data( __('Set up parameters to show images, galleries, audio and video posts', 'yreg-estate') ),
					"override" => "category,services_group,services_group",
					"type" => "info"),
					
		"retina_ready" => array(
					"title" => esc_html__('Image dimensions', 'yreg-estate'),
					"desc" => wp_kses_data( __('What dimensions use for uploaded image: Original or "Retina ready" (twice enlarged)', 'yreg-estate') ),
					"std" => "1",
					"size" => "medium",
					"options" => array(
						"1" => esc_html__("Original", 'yreg-estate'), 
						"2" => esc_html__("Retina", 'yreg-estate')
					),
					"type" => "switch"),
		
		"images_quality" => array(
					"title" => esc_html__('Quality for cropped images', 'yreg-estate'),
					"desc" => wp_kses_data( __('Quality (1-100) to save cropped images', 'yreg-estate') ),
					"std" => "70",
					"min" => 1,
					"max" => 100,
					"type" => "spinner"),
		
		"substitute_gallery" => array(
					"title" => esc_html__('Substitute standard Wordpress gallery', 'yreg-estate'),
					"desc" => wp_kses_data( __('Substitute standard Wordpress gallery with our slider on the single pages', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "no",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
		
		"gallery_instead_image" => array(
					"title" => esc_html__('Show gallery instead featured image', 'yreg-estate'),
					"desc" => wp_kses_data( __('Show slider with gallery instead featured image on blog streampage and in the related posts section for the gallery posts', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
		
		"gallery_max_slides" => array(
					"title" => esc_html__('Max images number in the slider', 'yreg-estate'),
					"desc" => wp_kses_data( __('Maximum images number from gallery into slider', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"dependency" => array(
						'gallery_instead_image' => array('yes')
					),
					"std" => "5",
					"min" => 2,
					"max" => 10,
					"type" => "spinner"),
		
		"popup_engine" => array(
					"title" => esc_html__('Popup engine to zoom images', 'yreg-estate'),
					"desc" => wp_kses_data( __('Select engine to show popup windows with images and galleries', 'yreg-estate') ),
					"std" => "magnific",
					"options" => yreg_estate_get_options_param('list_popups'),
					"type" => "select"),
		
		"substitute_audio" => array(
					"title" => esc_html__('Substitute audio tags', 'yreg-estate'),
					"desc" => wp_kses_data( __('Substitute audio tag with source from soundcloud to embed player', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
		
		"substitute_video" => array(
					"title" => esc_html__('Substitute video tags', 'yreg-estate'),
					"desc" => wp_kses_data( __('Substitute video tags with embed players or leave video tags unchanged (if you use third party plugins for the video tags)', 'yreg-estate') ),
					"override" => "category,services_group,post,page",
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
		
		"use_mediaelement" => array(
					"title" => esc_html__('Use Media Element script for audio and video tags', 'yreg-estate'),
					"desc" => wp_kses_data( __('Do you want use the Media Element script for all audio and video tags on your site or leave standard HTML5 behaviour?', 'yreg-estate') ),
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
		
		
		
		
		//###############################
		//#### Socials               #### 
		//###############################
		"partition_socials" => array(
					"title" => esc_html__('Socials', 'yreg-estate'),
					"icon" => "iconadmin-users",
					"override" => "category,services_group,page",
					"type" => "partition"),
		
		"info_socials_1" => array(
					"title" => esc_html__('Social networks', 'yreg-estate'),
					"desc" => wp_kses_data( __("Social networks list for site footer and Social widget", 'yreg-estate') ),
					"type" => "info"),
		
		"social_icons" => array(
					"title" => esc_html__('Social networks',  'yreg-estate'),
					"desc" => wp_kses_data( __('Select icon and write URL to your profile in desired social networks.',  'yreg-estate') ),
					"std" => array(array('url'=>'', 'icon'=>'')),
					"cloneable" => true,
					"size" => "small",
					"style" => $socials_type,
					"options" => $socials_type=='images' ? yreg_estate_get_options_param('list_socials') : yreg_estate_get_options_param('list_icons'),
					"type" => "socials"),
		
		"info_socials_2" => array(
					"title" => esc_html__('Share buttons', 'yreg-estate'),
					"desc" => wp_kses_data( __("Add button's code for each social share network.<br>
					In share url you can use next macro:<br>
					<b>{url}</b> - share post (page) URL,<br>
					<b>{title}</b> - post title,<br>
					<b>{image}</b> - post image,<br>
					<b>{descr}</b> - post description (if supported)<br>
					For example:<br>
					<b>Facebook</b> share string: <em>http://www.facebook.com/sharer.php?u={link}&amp;t={title}</em><br>
					<b>Delicious</b> share string: <em>http://delicious.com/save?url={link}&amp;title={title}&amp;note={descr}</em>", 'yreg-estate') ),
					"override" => "category,services_group,page",
					"type" => "info"),
		
		"show_share" => array(
					"title" => esc_html__('Show social share buttons',  'yreg-estate'),
					"desc" => wp_kses_data( __("Show social share buttons block", 'yreg-estate') ),
					"override" => "category,services_group,page",
					"std" => "horizontal",
					"options" => array(
						'hide'		=> esc_html__('Hide', 'yreg-estate'),
						'vertical'	=> esc_html__('Vertical', 'yreg-estate'),
						'horizontal'=> esc_html__('Horizontal', 'yreg-estate')
					),
					"type" => "checklist"),

		"show_share_counters" => array(
					"title" => esc_html__('Show share counters',  'yreg-estate'),
					"desc" => wp_kses_data( __("Show share counters after social buttons", 'yreg-estate') ),
					"override" => "category,services_group,page",
					"dependency" => array(
						'show_share' => array('vertical', 'horizontal')
					),
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),

		"share_caption" => array(
					"title" => esc_html__('Share block caption',  'yreg-estate'),
					"desc" => wp_kses_data( __('Caption for the block with social share buttons',  'yreg-estate') ),
					"override" => "category,services_group,page",
					"dependency" => array(
						'show_share' => array('vertical', 'horizontal')
					),
					"std" => esc_html__('Share:', 'yreg-estate'),
					"type" => "text"),
		
		"share_buttons" => array(
					"title" => esc_html__('Share buttons',  'yreg-estate'),
					"desc" => wp_kses_data( __('Select icon and write share URL for desired social networks.<br><b>Important!</b> If you leave text field empty - internal theme link will be used (if present).',  'yreg-estate') ),
					"dependency" => array(
						'show_share' => array('vertical', 'horizontal')
					),
					"std" => array(array('url'=>'', 'icon'=>'')),
					"cloneable" => true,
					"size" => "small",
					"style" => $socials_type,
					"options" => $socials_type=='images' ? yreg_estate_get_options_param('list_socials') : yreg_estate_get_options_param('list_icons'),
					"type" => "socials"),
		
		
		"info_socials_3" => array(
					"title" => esc_html__('Twitter API keys', 'yreg-estate'),
					"desc" => wp_kses_data( __("Put to this section Twitter API 1.1 keys.<br>You can take them after registration your application in <strong>https://apps.twitter.com/</strong>", 'yreg-estate') ),
					"type" => "info"),
		
		"twitter_username" => array(
					"title" => esc_html__('Twitter username',  'yreg-estate'),
					"desc" => wp_kses_data( __('Your login (username) in Twitter',  'yreg-estate') ),
					"divider" => false,
					"std" => "",
					"type" => "text"),
		
		"twitter_consumer_key" => array(
					"title" => esc_html__('Consumer Key',  'yreg-estate'),
					"desc" => wp_kses_data( __('Twitter API Consumer key',  'yreg-estate') ),
					"divider" => false,
					"std" => "",
					"type" => "text"),
		
		"twitter_consumer_secret" => array(
					"title" => esc_html__('Consumer Secret',  'yreg-estate'),
					"desc" => wp_kses_data( __('Twitter API Consumer secret',  'yreg-estate') ),
					"divider" => false,
					"std" => "",
					"type" => "text"),
		
		"twitter_token_key" => array(
					"title" => esc_html__('Token Key',  'yreg-estate'),
					"desc" => wp_kses_data( __('Twitter API Token key',  'yreg-estate') ),
					"divider" => false,
					"std" => "",
					"type" => "text"),
		
		"twitter_token_secret" => array(
					"title" => esc_html__('Token Secret',  'yreg-estate'),
					"desc" => wp_kses_data( __('Twitter API Token secret',  'yreg-estate') ),
					"divider" => false,
					"std" => "",
					"type" => "text"),
		
		
		
		
		
		//###############################
		//#### Contact info          #### 
		//###############################
		"partition_contacts" => array(
					"title" => esc_html__('Contact info', 'yreg-estate'),
					"icon" => "iconadmin-mail",
					"type" => "partition"),
		
		"info_contact_1" => array(
					"title" => esc_html__('Contact information', 'yreg-estate'),
					"desc" => wp_kses_data( __('Company address, phones and e-mail', 'yreg-estate') ),
					"type" => "info"),
		
		"contact_info" => array(
					"title" => esc_html__('Contacts in the header', 'yreg-estate'),
					"desc" => wp_kses_data( __('String with contact info in the left side of the site header', 'yreg-estate') ),
					"std" => "",
					"before" => array('icon'=>'iconadmin-home'),
					"allow_html" => true,
					"type" => "text"),
		
		"contact_open_hours" => array(
					"title" => esc_html__('Open hours in the header', 'yreg-estate'),
					"desc" => wp_kses_data( __('String with open hours in the site header', 'yreg-estate') ),
					"std" => "",
					"before" => array('icon'=>'iconadmin-clock'),
					"allow_html" => true,
					"type" => "text"),
		
		"contact_email" => array(
					"title" => esc_html__('Contact form email', 'yreg-estate'),
					"desc" => wp_kses_data( __('E-mail for send contact form and user registration data', 'yreg-estate') ),
					"std" => "contact@yoursite.com",
					"before" => array('icon'=>'iconadmin-mail'),
					"type" => "text"),
		
		"contact_address_1" => array(
					"title" => esc_html__('Company address (part 1)', 'yreg-estate'),
					"desc" => wp_kses_data( __('Company country, post code and city', 'yreg-estate') ),
					"std" => "121 King Street, NY, USA",
					"before" => array('icon'=>'iconadmin-home'),
					"type" => "text"),
		
		"contact_address_2" => array(
					"title" => esc_html__('Company address (part 2)', 'yreg-estate'),
					"desc" => wp_kses_data( __('Street and house number', 'yreg-estate') ),
					"std" => "",
					"before" => array('icon'=>'iconadmin-home'),
					"type" => "text"),
		
		"contact_phone" => array(
					"title" => esc_html__('Phone', 'yreg-estate'),
					"desc" => wp_kses_data( __('Phone number', 'yreg-estate') ),
					"std" => "<i>800</i> 123 45 67",
					"before" => array('icon'=>'iconadmin-phone'),
					"allow_html" => true,
					"type" => "text"),
		
		"contact_fax" => array(
					"title" => esc_html__('Fax', 'yreg-estate'),
					"desc" => wp_kses_data( __('Fax number', 'yreg-estate') ),
					"std" => "",
					"before" => array('icon'=>'iconadmin-phone'),
					"allow_html" => true,
					"type" => "text"),
		
		"info_contact_2" => array(
					"title" => esc_html__('Contact and Comments form', 'yreg-estate'),
					"desc" => wp_kses_data( __('Maximum length of the messages in the contact form shortcode and in the comments form', 'yreg-estate') ),
					"type" => "info"),
		
		"message_maxlength_contacts" => array(
					"title" => esc_html__('Contact form message', 'yreg-estate'),
					"desc" => wp_kses_data( __("Message's maxlength in the contact form shortcode", 'yreg-estate') ),
					"std" => "1000",
					"min" => 0,
					"max" => 10000,
					"step" => 100,
					"type" => "spinner"),
		
		"message_maxlength_comments" => array(
					"title" => esc_html__('Comments form message', 'yreg-estate'),
					"desc" => wp_kses_data( __("Message's maxlength in the comments form", 'yreg-estate') ),
					"std" => "1000",
					"min" => 0,
					"max" => 10000,
					"step" => 100,
					"type" => "spinner"),
		
		"info_contact_3" => array(
					"title" => esc_html__('Default mail function', 'yreg-estate'),
					"desc" => wp_kses_data( __('What function you want to use for sending mail: the built-in Wordpress wp_mail() or standard PHP mail() function? Attention! Some plugins may not work with one of them and you always have the ability to switch to alternative.', 'yreg-estate') ),
					"type" => "info"),
		
		"mail_function" => array(
					"title" => esc_html__("Mail function", 'yreg-estate'),
					"desc" => wp_kses_data( __("What function you want to use for sending mail?", 'yreg-estate') ),
					"std" => "wp_mail",
					"size" => "medium",
					"options" => array(
						'wp_mail' => esc_html__('WP mail', 'yreg-estate'),
						'mail' => esc_html__('PHP mail', 'yreg-estate')
					),
					"type" => "switch"),
		
		
		
		
		
		
		
		//###############################
		//#### Search parameters     #### 
		//###############################
		"partition_search" => array(
					"title" => esc_html__('Search', 'yreg-estate'),
					"icon" => "iconadmin-search",
					"type" => "partition"),
		
		"info_search_1" => array(
					"title" => esc_html__('Search parameters', 'yreg-estate'),
					"desc" => wp_kses_data( __('Enable/disable AJAX search and output settings for it', 'yreg-estate') ),
					"type" => "info"),
		
		"show_search" => array(
					"title" => esc_html__('Show search field', 'yreg-estate'),
					"desc" => wp_kses_data( __('Show search field in the top area and side menus', 'yreg-estate') ),
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
		
		"use_ajax_search" => array(
					"title" => esc_html__('Enable AJAX search', 'yreg-estate'),
					"desc" => wp_kses_data( __('Use incremental AJAX search for the search field in top of page', 'yreg-estate') ),
					"dependency" => array(
						'show_search' => array('yes')
					),
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
		
		"ajax_search_min_length" => array(
					"title" => esc_html__('Min search string length',  'yreg-estate'),
					"desc" => wp_kses_data( __('The minimum length of the search string',  'yreg-estate') ),
					"dependency" => array(
						'show_search' => array('yes'),
						'use_ajax_search' => array('yes')
					),
					"std" => 4,
					"min" => 3,
					"type" => "spinner"),
		
		"ajax_search_delay" => array(
					"title" => esc_html__('Delay before search (in ms)',  'yreg-estate'),
					"desc" => wp_kses_data( __('How much time (in milliseconds, 1000 ms = 1 second) must pass after the last character before the start search',  'yreg-estate') ),
					"dependency" => array(
						'show_search' => array('yes'),
						'use_ajax_search' => array('yes')
					),
					"std" => 500,
					"min" => 300,
					"max" => 1000,
					"step" => 100,
					"type" => "spinner"),
		
		"ajax_search_types" => array(
					"title" => esc_html__('Search area', 'yreg-estate'),
					"desc" => wp_kses_data( __('Select post types, what will be include in search results. If not selected - use all types.', 'yreg-estate') ),
					"dependency" => array(
						'show_search' => array('yes'),
						'use_ajax_search' => array('yes')
					),
					"std" => "",
					"options" => yreg_estate_get_options_param('list_posts_types'),
					"multiple" => true,
					"style" => "list",
					"type" => "select"),
		
		"ajax_search_posts_count" => array(
					"title" => esc_html__('Posts number in output',  'yreg-estate'),
					"dependency" => array(
						'show_search' => array('yes'),
						'use_ajax_search' => array('yes')
					),
					"desc" => wp_kses_data( __('Number of the posts to show in search results',  'yreg-estate') ),
					"std" => 5,
					"min" => 1,
					"max" => 10,
					"type" => "spinner"),
		
		"ajax_search_posts_image" => array(
					"title" => esc_html__("Show post's image", 'yreg-estate'),
					"dependency" => array(
						'show_search' => array('yes'),
						'use_ajax_search' => array('yes')
					),
					"desc" => wp_kses_data( __("Show post's thumbnail in the search results", 'yreg-estate') ),
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
		
		"ajax_search_posts_date" => array(
					"title" => esc_html__("Show post's date", 'yreg-estate'),
					"dependency" => array(
						'show_search' => array('yes'),
						'use_ajax_search' => array('yes')
					),
					"desc" => wp_kses_data( __("Show post's publish date in the search results", 'yreg-estate') ),
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
		
		"ajax_search_posts_author" => array(
					"title" => esc_html__("Show post's author", 'yreg-estate'),
					"dependency" => array(
						'show_search' => array('yes'),
						'use_ajax_search' => array('yes')
					),
					"desc" => wp_kses_data( __("Show post's author in the search results", 'yreg-estate') ),
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
		
		"ajax_search_posts_counters" => array(
					"title" => esc_html__("Show post's counters", 'yreg-estate'),
					"dependency" => array(
						'show_search' => array('yes'),
						'use_ajax_search' => array('yes')
					),
					"desc" => wp_kses_data( __("Show post's counters (views, comments, likes) in the search results", 'yreg-estate') ),
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
		
		
		
		
		
		//###############################
		//#### Service               #### 
		//###############################
		
		"partition_service" => array(
					"title" => esc_html__('Service', 'yreg-estate'),
					"icon" => "iconadmin-wrench",
					"type" => "partition"),
		
		"info_service_1" => array(
					"title" => esc_html__('Theme functionality', 'yreg-estate'),
					"desc" => wp_kses_data( __('Basic theme functionality settings', 'yreg-estate') ),
					"type" => "info"),
		
		"notify_about_new_registration" => array(
					"title" => esc_html__('Notify about new registration', 'yreg-estate'),
					"desc" => wp_kses_data( __('Send E-mail with new registration data to the contact email or to site admin e-mail (if contact email is empty)', 'yreg-estate') ),
					"divider" => false,
					"std" => "no",
					"options" => array(
						'no'    => esc_html__('No', 'yreg-estate'),
						'both'  => esc_html__('Both', 'yreg-estate'),
						'admin' => esc_html__('Admin', 'yreg-estate'),
						'user'  => esc_html__('User', 'yreg-estate')
					),
					"dir" => "horizontal",
					"type" => "checklist"),
		
		"use_ajax_views_counter" => array(
					"title" => esc_html__('Use AJAX post views counter', 'yreg-estate'),
					"desc" => wp_kses_data( __('Use javascript for post views count (if site work under the caching plugin) or increment views count in single page template', 'yreg-estate') ),
					"std" => "no",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
		
		"allow_editor" => array(
					"title" => esc_html__('Frontend editor',  'yreg-estate'),
					"desc" => wp_kses_data( __("Allow authors to edit their posts in frontend area)", 'yreg-estate') ),
					"std" => "no",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),

		"admin_add_filters" => array(
					"title" => esc_html__('Additional filters in the admin panel', 'yreg-estate'),
					"desc" => wp_kses_data( __('Show additional filters (on post formats, tags and categories) in admin panel page "Posts". <br>Attention! If you have more than 2.000-3.000 posts, enabling this option may cause slow load of the "Posts" page! If you encounter such slow down, simply open Appearance - Theme Options - Service and set "No" for this option.', 'yreg-estate') ),
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),

		"show_overriden_taxonomies" => array(
					"title" => esc_html__('Show overriden options for taxonomies', 'yreg-estate'),
					"desc" => wp_kses_data( __('Show extra column in categories list, where changed (overriden) theme options are displayed.', 'yreg-estate') ),
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),

		"show_overriden_posts" => array(
					"title" => esc_html__('Show overriden options for posts and pages', 'yreg-estate'),
					"desc" => wp_kses_data( __('Show extra column in posts and pages list, where changed (overriden) theme options are displayed.', 'yreg-estate') ),
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
		
		"admin_dummy_data" => array(
					"title" => esc_html__('Enable Dummy Data Installer', 'yreg-estate'),
					"desc" => wp_kses_data( __('Show "Install Dummy Data" in the menu "Appearance". <b>Attention!</b> When you install dummy data all content of your site will be replaced!', 'yreg-estate') ),
					"std" => "yes",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),

		"admin_dummy_timeout" => array(
					"title" => esc_html__('Dummy Data Installer Timeout',  'yreg-estate'),
					"desc" => wp_kses_data( __('Web-servers set the time limit for the execution of php-scripts. By default, this is 30 sec. Therefore, the import process will be split into parts. Upon completion of each part - the import will resume automatically! The import process will try to increase this limit to the time, specified in this field.',  'yreg-estate') ),
					"std" => 120,
					"min" => 30,
					"max" => 1800,
					"type" => "spinner"),
		
		"admin_emailer" => array(
					"title" => esc_html__('Enable Emailer in the admin panel', 'yreg-estate'),
					"desc" => wp_kses_data( __('Allow to use yregEstate Emailer for mass-volume e-mail distribution and management of mailing lists in "Appearance - Emailer"', 'yreg-estate') ),
					"std" => "no",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),

		"admin_po_composer" => array(
					"title" => esc_html__('Enable PO Composer in the admin panel', 'yreg-estate'),
					"desc" => wp_kses_data( __('Allow to use "PO Composer" for edit language files in this theme (in the "Appearance - PO Composer")', 'yreg-estate') ),
					"std" => "no",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
		
		"debug_mode" => array(
					"title" => esc_html__('Debug mode', 'yreg-estate'),
					"desc" => wp_kses_data( __('In debug mode we are using unpacked scripts and styles, else - using minified scripts and styles (if present). <b>Attention!</b> If you have modified the source code in the js or css files, regardless of this option will be used latest (modified) version stylesheets and scripts. You can re-create minified versions of files using on-line services or utilities', 'yreg-estate') ),
					"std" => "no",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
		
		
		"info_service_2" => array(
					"title" => esc_html__('Wordpress cache', 'yreg-estate'),
					"desc" => wp_kses_data( __('For example, it recommended after activating the WPML plugin - in the cache are incorrect data about the structure of categories and your site may display "white screen". After clearing the cache usually the performance of the site is restored.', 'yreg-estate') ),
					"type" => "info"),
		
		"use_menu_cache" => array(
					"title" => esc_html__('Use menu cache', 'yreg-estate'),
					"desc" => wp_kses_data( __('Use cache for any menu (increase theme speed, decrease queries number). Attention! Please, clear cache after change permalink settings!', 'yreg-estate') ),
					"std" => "no",
					"options" => yreg_estate_get_options_param('list_yes_no'),
					"type" => "switch"),
		
		"clear_cache" => array(
					"title" => esc_html__('Clear cache', 'yreg-estate'),
					"desc" => wp_kses_data( __('Clear Wordpress cache data', 'yreg-estate') ),
					"divider" => false,
					"icon" => "iconadmin-trash",
					"action" => "clear_cache",
					"type" => "button"),
			
			
			
		//###############################
		//#### Property              #### 
		//###############################
			"partition_property" => array(
				"title" => esc_html__('Property', 'yreg-estate'),
				"icon" => "iconadmin-home",
				"type" => "partition"),
			
			"info_property_1" => array(
				"title" => esc_html__('Property functionality', 'yreg-estate'),
				"desc" => wp_kses_data( __('Basic property functionality settings', 'yreg-estate') ),
				"type" => "info"),

			"property_location_list" => array(
				"title" => esc_html__('Property location list', 'yreg-estate'),
				"desc" => wp_kses_data( __('', 'yreg-estate') ),
				"std" => esc_html__("Upper East Side,Upper West Side,Midtown East,Midtown West,Downtown,Upper Manhattan,Brooklyn,Queens,Bronx,Staten Island", 'yreg-estate'),
				"type" => "tags"),
			
			"property_type_list" => array(
				"title" => esc_html__('Property type list', 'yreg-estate'),
				"desc" => wp_kses_data( __('', 'yreg-estate') ),
				"std" => esc_html__("Cooperative,Condominium,Cond-op,House,Investment,Commercial,Professional / Medical,New Development,Townhouse,Vacant Lot / Land", 'yreg-estate'),
				"type" => "tags"),
			
			"property_style_list" => array(
				"title" => esc_html__('Property style list', 'yreg-estate'),
				"desc" => wp_kses_data( __('', 'yreg-estate') ),
				"std" => esc_html__("Pre-war,Post-war,Loft,Townhouse Apartment,Penthouse", 'yreg-estate'),
				"type" => "tags"),
			
			"property_amenities_list" => array(
				"title" => esc_html__('Property amenities list', 'yreg-estate'),
				"desc" => wp_kses_data( __('', 'yreg-estate') ),
				"std" => esc_html__("Attended Lobby,Concierge,Fireplace,Gym,Outdoor Space,Parking,Pet Friendly,Pool,Views,Washer / Drye", 'yreg-estate'),
				"type" => "tags"),
			
			"property_options_list" => array(
				"title" => esc_html__('Property options list', 'yreg-estate'),
				"desc" => wp_kses_data( __('', 'yreg-estate') ),
				"std" => esc_html__("New Listings Only,Open Houses,Sponsor Units,Show Listings In Contract", 'yreg-estate'),
				"type" => "tags"),
			
			"property_price_sign_list" => array(
				"title" => esc_html__('Currency Available', 'yreg-estate'),
				"desc" => wp_kses_data( __('Add your currecy and press Enter/Return Button', 'yreg-estate') ),
				"std" => esc_html__("$,&euro;", 'yreg-estate'),
				"type" => "tags"),
			
			"property_price_per_list" => array(
				"title" => esc_html__('Dettagli Affitasi', 'yreg-estate'),
				"desc" => wp_kses_data( __('Add available rent term here and press  Enter/Return Button', 'yreg-estate') ),
				"std" => esc_html__("year,month,week,day", 'yreg-estate'),
				"type" => "tags"),

			"property_search_area_max" => array(
					"title" => esc_html__('Property search area max', 'yreg-estate'),
					"desc" => wp_kses_data( __('', 'yreg-estate') ),
					"std" => "10000",
					"type" => "text"),
			
			"property_search_price_max" => array(
					"title" => esc_html__('Property search price max', 'yreg-estate'),
					"desc" => wp_kses_data( __('', 'yreg-estate') ),
					"std" => "10000000",
					"type" => "text"),
			
		));
		
		
		
		//###############################################
		//#### Hidden fields (for internal use only) #### 
		//###############################################
		/*
		yreg_estate_storage_set_array('options', "custom_stylesheet_file", array(
			"title" => esc_html__('Custom stylesheet file', 'yreg-estate'),
			"desc" => wp_kses_data( __('Path to the custom stylesheet (stored in the uploads folder)', 'yreg-estate') ),
			"std" => "",
			"type" => "hidden"
			)
		);
		
		yreg_estate_storage_set_array('options', "custom_stylesheet_url", array(
			"title" => esc_html__('Custom stylesheet url', 'yreg-estate'),
			"desc" => wp_kses_data( __('URL to the custom stylesheet (stored in the uploads folder)', 'yreg-estate') ),
			"std" => "",
			"type" => "hidden"
			)
		);
		*/

	}
}


// Update all temporary vars (start with $yreg_estate_) in the Theme Options with actual lists
if ( !function_exists( 'yreg_estate_options_settings_theme_setup2' ) ) {
	add_action( 'yreg_estate_action_after_init_theme', 'yreg_estate_options_settings_theme_setup2', 1 );
	function yreg_estate_options_settings_theme_setup2() {
		if (yreg_estate_options_is_used()) {
			// Replace arrays with actual parameters
			$lists = array();
			$tmp = yreg_estate_storage_get('options');
			if (is_array($tmp) && count($tmp) > 0) {
				$prefix = '$yreg_estate_';
				$prefix_len = yreg_estate_strlen($prefix);
				foreach ($tmp as $k=>$v) {
					if (isset($v['options']) && is_array($v['options']) && count($v['options']) > 0) {
						foreach ($v['options'] as $k1=>$v1) {
							if (yreg_estate_substr($k1, 0, $prefix_len) == $prefix || yreg_estate_substr($v1, 0, $prefix_len) == $prefix) {
								$list_func = yreg_estate_substr(yreg_estate_substr($k1, 0, $prefix_len) == $prefix ? $k1 : $v1, 1);
								unset($tmp[$k]['options'][$k1]);
								if (isset($lists[$list_func]))
									$tmp[$k]['options'] = yreg_estate_array_merge($tmp[$k]['options'], $lists[$list_func]);
								else {
									if (function_exists($list_func)) {
										$tmp[$k]['options'] = $lists[$list_func] = yreg_estate_array_merge($tmp[$k]['options'], $list_func == 'yreg_estate_get_list_menus' ? $list_func(true) : $list_func());
								   	} else
								   		dfl(sprintf(esc_html__('Wrong function name %s in the theme options array', 'yreg-estate'), $list_func));
								}
							}
						}
					}
				}
				yreg_estate_storage_set('options', $tmp);
			}
		}
	}
}

// Reset old Theme Options while theme first run
if ( !function_exists( 'yreg_estate_options_reset' ) ) {
	function yreg_estate_options_reset($clear=true) {
		$theme_data = wp_get_theme();
		$slug = str_replace(' ', '_', trim(yreg_estate_strtolower((string) $theme_data->get('Name'))));
		$option_name = 'yreg_estate_'.strip_tags($slug).'_options_reset';
		if ( get_option($option_name, false) === false ) {	// && (string) $theme_data->get('Version') == '1.0'
			if ($clear) {
				// Remove Theme Options from WP Options
				global $wpdb;
				$wpdb->query('delete from '.esc_sql($wpdb->options).' where option_name like "yreg_estate_%"');
				// Add Templates Options
				if (file_exists(yreg_estate_get_file_dir('demo/templates_options.txt'))) {
					$txt = yreg_estate_fgc(yreg_estate_get_file_dir('demo/templates_options.txt'));
					$data = yreg_estate_unserialize($txt);
					// Replace upload url in options
					if (is_array($data) && count($data) > 0) {
						foreach ($data as $k=>$v) {
							if (is_array($v) && count($v) > 0) {
								foreach ($v as $k1=>$v1) {
									$v[$k1] = yreg_estate_replace_uploads_url(yreg_estate_replace_uploads_url($v1, 'uploads'), 'imports');
								}
							}
							add_option( $k, $v, '', 'yes' );
						}
					}
				}
			}
			add_option($option_name, 1, '', 'yes');
		}
	}
}
?>