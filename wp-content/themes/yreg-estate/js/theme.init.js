/* global jQuery:false */
/* global YREG_ESTATE_STORAGE:false */


// Theme-specific first load actions
//==============================================
function yreg_estate_theme_ready_actions() {
	"use strict";
	// Put here your init code for the theme-specific actions
	// It will be called before core actions
	date_time();
}


// Theme-specific scroll actions
//==============================================
function yreg_estate_theme_scroll_actions() {
	"use strict";
	// Put here your theme-specific code for scroll actions
	// It will be called when page is scrolled (before core actions)
}


// Theme-specific resize actions
//==============================================
function yreg_estate_theme_resize_actions() {
	"use strict";
	// Put here your theme-specific code for resize actions
	// It will be called when window is resized (before core actions)
	
	estate_max_height();
	maxh();
}


// Theme-specific shortcodes init
//=====================================================
function yreg_estate_theme_sc_init(cont) {
	"use strict";
	// Put here your theme-specific code for init shortcodes
	// It will be called before core init shortcodes
	// @param cont - jQuery-container with shortcodes (init only inside this container)
}


// Theme-specific post-formats init
//=====================================================
function yreg_estate_theme_init_post_formats() {
	"use strict";
	// Put here your theme-specific code for init post-formats
	// It will be called before core init post_formats when page is loaded or after 'Load more' or 'Infonite scroll' actions
}





function estate_max_height() {
	"use strict";
	jQuery('.eMaxHBox .eMaxH').each(function(){
		jQuery(this).css('height', 'inherit');
	});
	var maxH = 0;
	jQuery('.eMaxHBox .eMaxH').each(function(){
		//console.log(1);
		if ( jQuery(this).height() > maxH ) {
			maxH = jQuery(this).height();
		}
	});
	jQuery('.eMaxHBox .eMaxH').height(maxH);
	
}


jQuery(document).ready(function(){  
    jQuery(".estateCheckBox").change(function(){
		
        if(jQuery(this).is(":checked")){  //если чекбокс отмечен, то
            jQuery(this).parent("label").addClass("estateLabelCheckBoxSelected"); //добавляем класс LabelSelected ярлыку 
        }else{   
            jQuery(this).parent("label").removeClass("estateLabelCheckBoxSelected");  //иначе удаляем его
        }
		
    });  
});  


jQuery(function() {
	"use strict";
	
	var js_area_min = parseInt(jQuery( ".ps_area_min" ).val());
	var js_area_max = parseInt(jQuery( ".ps_area_max" ).val());
	var js_area_big = parseInt(jQuery( ".ps_area_big" ).val());
	jQuery( "#slider-range-area" ).slider({
		range: true,
		min: 0,
		max: js_area_big,
		values: [ js_area_min, js_area_max ],
		slide: function( event, ui ) {
			
			jQuery( ".ps_area .ps_area_min" ).val(ui.values[0]);
			jQuery( ".ps_area .ps_area_max" ).val(ui.values[1]);
			
			if ( (ui.values[0]==0) && (ui.values[1]==js_area_big) ) {
				jQuery( ".ps_area_info_value" ).html( 'Any' );
			} else {
				jQuery( ".ps_area_info_value" ).html( ui.values[0] + " - " + ui.values[1] + " SqFt" );
			}
		}
	});
	if ( (js_area_min==0) && (js_area_max==js_area_big) ) {
		jQuery( ".ps_area_info_value" ).html( 'Any' );
	} else {
		jQuery( ".ps_area_info_value" ).html( js_area_min + " - " + js_area_max + " SqFt" );
	}
});


jQuery(function() {
	"use strict";
	
	var js_price_min = parseInt(jQuery( ".ps_price_min" ).val());
	var js_price_max = parseInt(jQuery( ".ps_price_max" ).val());
	var js_price_big = parseInt(jQuery( ".ps_price_big" ).val());
	jQuery( "#slider-range-price" ).slider({
		range: true,
		min: 0,
		max: js_price_big,
		values: [ js_price_min, js_price_max ],
		slide: function( event, ui ) {
			
			jQuery( ".ps_price .ps_price_min" ).val(ui.values[0]);
			jQuery( ".ps_price .ps_price_max" ).val(ui.values[1]);
			
			if ( (ui.values[0]==0) && (ui.values[1]==js_price_big) ) {
				jQuery( ".ps_price_info_value" ).html( 'Any' );
			} else {
				jQuery( ".ps_price_info_value" ).html( "$" + ui.values[0] + " - $" + ui.values[1] );
			}
		}
	});
	if ( (js_price_min==0) && (js_price_max==js_price_big) ) {
		jQuery( ".ps_price_info_value" ).html( 'Any' );
	} else {
		jQuery( ".ps_price_info_value" ).html( "$" + js_price_min + " - $" + js_price_max );
	}
});



function maxh() {
	"use strict";
	if (jQuery('.maxHBox').length>0) {
		jQuery('.maxHBox').each(function (){
			"use strict";
			var maxH = 0;
			jQuery(this).find('.maxHBoxItem').each(function (){
				"use strict";
				if ( jQuery(this).height() > maxH ) {
					maxH = jQuery(this).height();
				}
				console.log(maxH);
			});
			jQuery(this).find('.maxHBoxItem').height(maxH);
			console.log('xXx');
		});
	}
}


function date_time() {
	"use strict";
	jQuery('.custom_form_slider_item .cfs_date').before('<div class="cfs_date_time"></div>');
	jQuery('.custom_form_slider_item .cfs_date').appendTo('.custom_form_slider_item .cfs_date_time');
	jQuery('.custom_form_slider_item .cfs_time').appendTo('.custom_form_slider_item .cfs_date_time');
	
}