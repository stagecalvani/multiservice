<?php
/* Booked Appointments support functions
------------------------------------------------------------------------------- */

// Theme init
if (!function_exists('yreg_estate_booked_theme_setup')) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_booked_theme_setup', 1 );
	function yreg_estate_booked_theme_setup() {
		// Register shortcode in the shortcodes list
		if (yreg_estate_exists_booked()) {
			add_action('yreg_estate_action_add_styles', 					'yreg_estate_booked_frontend_scripts');
			add_action('yreg_estate_action_shortcodes_list',				'yreg_estate_booked_reg_shortcodes');
			if (function_exists('yreg_estate_exists_visual_composer') && yreg_estate_exists_visual_composer())
				add_action('yreg_estate_action_shortcodes_list_vc',		'yreg_estate_booked_reg_shortcodes_vc');
			if (is_admin()) {
				add_filter( 'yreg_estate_filter_importer_options',			'yreg_estate_booked_importer_set_options' );
			}
		}
		if (is_admin()) {
			add_filter( 'yreg_estate_filter_importer_required_plugins',	'yreg_estate_booked_importer_required_plugins', 10, 2);
			add_filter( 'yreg_estate_filter_required_plugins',				'yreg_estate_booked_required_plugins' );
		}
	}
}


// Check if plugin installed and activated
if ( !function_exists( 'yreg_estate_exists_booked' ) ) {
	function yreg_estate_exists_booked() {
		return class_exists('booked_plugin');
	}
}

// Filter to add in the required plugins list
if ( !function_exists( 'yreg_estate_booked_required_plugins' ) ) {
	//add_filter('yreg_estate_filter_required_plugins',	'yreg_estate_booked_required_plugins');
	function yreg_estate_booked_required_plugins($list=array()) {
		if (in_array('booked', yreg_estate_storage_get('required_plugins'))) {
			$path = yreg_estate_get_file_dir('plugins/install/booked.zip');
			if (file_exists($path)) {
				$list[] = array(
					'name' 		=> 'Booked',
					'slug' 		=> 'booked',
					'source'	=> $path,
					'required' 	=> false
					);
			}
		}
		return $list;
	}
}

// Enqueue custom styles
if ( !function_exists( 'yreg_estate_booked_frontend_scripts' ) ) {
	//add_action( 'yreg_estate_action_add_styles', 'yreg_estate_booked_frontend_scripts' );
	function yreg_estate_booked_frontend_scripts() {
		if (file_exists(yreg_estate_get_file_dir('css/plugin.booked.css')))
			yreg_estate_enqueue_style( 'yreg_estate-plugin.booked-style',  yreg_estate_get_file_url('css/plugin.booked.css'), array(), null );
	}
}



// One-click import support
//------------------------------------------------------------------------

// Check in the required plugins
if ( !function_exists( 'yreg_estate_booked_importer_required_plugins' ) ) {
	//add_filter( 'yreg_estate_filter_importer_required_plugins',	'yreg_estate_booked_importer_required_plugins', 10, 2);
	function yreg_estate_booked_importer_required_plugins($not_installed='', $list='') {
		//if (in_array('booked', yreg_estate_storage_get('required_plugins')) && !yreg_estate_exists_booked() )
		if (yreg_estate_strpos($list, 'booked')!==false && !yreg_estate_exists_booked() )
			$not_installed .= '<br>Booked Appointments';
		return $not_installed;
	}
}

// Set options for one-click importer
if ( !function_exists( 'yreg_estate_booked_importer_set_options' ) ) {
	//add_filter( 'yreg_estate_filter_importer_options',	'yreg_estate_booked_importer_set_options', 10, 1 );
	function yreg_estate_booked_importer_set_options($options=array()) {
		if (in_array('booked', yreg_estate_storage_get('required_plugins')) && yreg_estate_exists_booked()) {
			$options['additional_options'][] = 'booked_%';		// Add slugs to export options for this plugin
		}
		return $options;
	}
}


// Lists
//------------------------------------------------------------------------

// Return booked calendars list, prepended inherit (if need)
if ( !function_exists( 'yreg_estate_get_list_booked_calendars' ) ) {
	function yreg_estate_get_list_booked_calendars($prepend_inherit=false) {
		return yreg_estate_exists_booked() ? yreg_estate_get_list_terms($prepend_inherit, 'booked_custom_calendars') : array();
	}
}



// Register plugin's shortcodes
//------------------------------------------------------------------------

// Register shortcode in the shortcodes list
if (!function_exists('yreg_estate_booked_reg_shortcodes')) {
	//add_filter('yreg_estate_action_shortcodes_list',	'yreg_estate_booked_reg_shortcodes');
	function yreg_estate_booked_reg_shortcodes() {
		if (yreg_estate_storage_isset('shortcodes')) {

			$booked_cals = yreg_estate_get_list_booked_calendars();

			yreg_estate_sc_map('booked-appointments', array(
				"title" => esc_html__("Booked Appointments", 'yreg-estate'),
				"desc" => esc_html__("Display the currently logged in user's upcoming appointments", 'yreg-estate'),
				"decorate" => true,
				"container" => false,
				"params" => array()
				)
			);

			yreg_estate_sc_map('booked-calendar', array(
				"title" => esc_html__("Booked Calendar", 'yreg-estate'),
				"desc" => esc_html__("Insert booked calendar", 'yreg-estate'),
				"decorate" => true,
				"container" => false,
				"params" => array(
					"calendar" => array(
						"title" => esc_html__("Calendar", 'yreg-estate'),
						"desc" => esc_html__("Select booked calendar to display", 'yreg-estate'),
						"value" => "0",
						"type" => "select",
						"options" => yreg_estate_array_merge(array(0 => esc_html__('- Select calendar -', 'yreg-estate')), $booked_cals)
					),
					"year" => array(
						"title" => esc_html__("Year", 'yreg-estate'),
						"desc" => esc_html__("Year to display on calendar by default", 'yreg-estate'),
						"value" => date("Y"),
						"min" => date("Y"),
						"max" => date("Y")+10,
						"type" => "spinner"
					),
					"month" => array(
						"title" => esc_html__("Month", 'yreg-estate'),
						"desc" => esc_html__("Month to display on calendar by default", 'yreg-estate'),
						"value" => date("m"),
						"min" => 1,
						"max" => 12,
						"type" => "spinner"
					)
				)
			));
		}
	}
}


// Register shortcode in the VC shortcodes list
if (!function_exists('yreg_estate_booked_reg_shortcodes_vc')) {
	//add_filter('yreg_estate_action_shortcodes_list_vc',	'yreg_estate_booked_reg_shortcodes_vc');
	function yreg_estate_booked_reg_shortcodes_vc() {

		$booked_cals = yreg_estate_get_list_booked_calendars();

		// Booked Appointments
		vc_map( array(
				"base" => "booked-appointments",
				"name" => esc_html__("Booked Appointments", 'yreg-estate'),
				"description" => esc_html__("Display the currently logged in user's upcoming appointments", 'yreg-estate'),
				"category" => esc_html__('Content', 'yreg-estate'),
				'icon' => 'icon_trx_booked',
				"class" => "trx_sc_single trx_sc_booked_appointments",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => false,
				"params" => array()
			) );
			
		class WPBakeryShortCode_Booked_Appointments extends YREG_ESTATE_VC_ShortCodeSingle {}

		// Booked Calendar
		vc_map( array(
				"base" => "booked-calendar",
				"name" => esc_html__("Booked Calendar", 'yreg-estate'),
				"description" => esc_html__("Insert booked calendar", 'yreg-estate'),
				"category" => esc_html__('Content', 'yreg-estate'),
				'icon' => 'icon_trx_booked',
				"class" => "trx_sc_single trx_sc_booked_calendar",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "calendar",
						"heading" => esc_html__("Calendar", 'yreg-estate'),
						"description" => esc_html__("Select booked calendar to display", 'yreg-estate'),
						"admin_label" => true,
						"class" => "",
						"std" => "0",
						"value" => array_flip(yreg_estate_array_merge(array(0 => esc_html__('- Select calendar -', 'yreg-estate')), $booked_cals)),
						"type" => "dropdown"
					),
					array(
						"param_name" => "year",
						"heading" => esc_html__("Year", 'yreg-estate'),
						"description" => esc_html__("Year to display on calendar by default", 'yreg-estate'),
						"admin_label" => true,
						"class" => "",
						"std" => date("Y"),
						"value" => date("Y"),
						"type" => "textfield"
					),
					array(
						"param_name" => "month",
						"heading" => esc_html__("Month", 'yreg-estate'),
						"description" => esc_html__("Month to display on calendar by default", 'yreg-estate'),
						"admin_label" => true,
						"class" => "",
						"std" => date("m"),
						"value" => date("m"),
						"type" => "textfield"
					)
				)
			) );
			
		class WPBakeryShortCode_Booked_Calendar extends YREG_ESTATE_VC_ShortCodeSingle {}

	}
}
?>