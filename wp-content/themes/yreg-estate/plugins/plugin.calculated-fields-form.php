<?php
/* Calculated fields form support functions
------------------------------------------------------------------------------- */

// Theme init
if (!function_exists('yreg_estate_calcfields_form_theme_setup')) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_calcfields_form_theme_setup', 1 );
	function yreg_estate_calcfields_form_theme_setup() {
		// Register shortcode in the shortcodes list
		if (yreg_estate_exists_calcfields_form()) {
			add_action('yreg_estate_action_shortcodes_list',				'yreg_estate_calcfields_form_reg_shortcodes');
			if (function_exists('yreg_estate_exists_visual_composer') && yreg_estate_exists_visual_composer())
				add_action('yreg_estate_action_shortcodes_list_vc',		'yreg_estate_calcfields_form_reg_shortcodes_vc');
			if (is_admin()) {
				add_filter( 'yreg_estate_filter_importer_options',			'yreg_estate_calcfields_form_importer_set_options', 10, 1 );
				add_action( 'yreg_estate_action_importer_params',			'yreg_estate_calcfields_form_importer_show_params', 10, 1 );
				add_action( 'yreg_estate_action_importer_import',			'yreg_estate_calcfields_form_importer_import', 10, 2 );
				add_action( 'yreg_estate_action_importer_import_fields',	'yreg_estate_calcfields_form_importer_import_fields', 10, 1 );
				add_action( 'yreg_estate_action_importer_export',			'yreg_estate_calcfields_form_importer_export', 10, 1 );
				add_action( 'yreg_estate_action_importer_export_fields',	'yreg_estate_calcfields_form_importer_export_fields', 10, 1 );
			}
			add_action('wp_enqueue_scripts', 							'yreg_estate_calcfields_form_frontend_scripts');
		}
		if (is_admin()) {
			add_filter( 'yreg_estate_filter_importer_required_plugins',	'yreg_estate_calcfields_form_importer_required_plugins', 10, 2 );
			add_filter( 'yreg_estate_filter_required_plugins',				'yreg_estate_calcfields_form_required_plugins' );
		}
	}
}

// Check if plugin installed and activated
if ( !function_exists( 'yreg_estate_exists_calcfields_form' ) ) {
	function yreg_estate_exists_calcfields_form() {
		return defined('CP_SCHEME');
	}
}

// Filter to add in the required plugins list
if ( !function_exists( 'yreg_estate_calcfields_form_required_plugins' ) ) {
	//add_filter('yreg_estate_filter_required_plugins',	'yreg_estate_calcfields_form_required_plugins');
	function yreg_estate_calcfields_form_required_plugins($list=array()) {
		if (in_array('calcfields', yreg_estate_storage_get('required_plugins')))
			$list[] = array(
					'name' 		=> 'Calculated Fields Form',
					'slug' 		=> 'calculated-fields-form',
					'required' 	=> false
					);
		return $list;
	}
}

// Remove jquery_ui from frontend
if ( !function_exists( 'yreg_estate_calcfields_form_frontend_scripts' ) ) {
	function yreg_estate_calcfields_form_frontend_scripts() {
		// Disable loading JQuery UI CSS
		//global $wp_styles, $wp_scripts;
		//$wp_styles->done[] = 'cpcff_jquery_ui';
	}
}


// One-click import support
//------------------------------------------------------------------------

// Check in the required plugins
if ( !function_exists( 'yreg_estate_calcfields_form_importer_required_plugins' ) ) {
	//add_filter( 'yreg_estate_filter_importer_required_plugins',	'yreg_estate_calcfields_form_importer_required_plugins', 10, 2 );
	function yreg_estate_calcfields_form_importer_required_plugins($not_installed='', $list='') {
		//if (in_array('calcfields', yreg_estate_storage_get('required_plugins')) && !yreg_estate_exists_calcfields_form() )
		if (yreg_estate_strpos($list, 'calcfields')!==false && !yreg_estate_exists_calcfields_form() )
			$not_installed .= '<br>Calculated Fields Form';
		return $not_installed;
	}
}

// Set options for one-click importer
if ( !function_exists( 'yreg_estate_calcfields_form_importer_set_options' ) ) {
	//add_filter( 'yreg_estate_filter_importer_options',	'yreg_estate_calcfields_form_importer_set_options', 10, 1 );
	function yreg_estate_calcfields_form_importer_set_options($options=array()) {
		if ( in_array('calcfields', yreg_estate_storage_get('required_plugins')) && yreg_estate_exists_calcfields_form() ) {
			$options['file_with_calcfields_form'] = 'demo/calcfields_form.txt';			// Name of the file with Calculated Fields Form data
		}
		return $options;
	}
}

// Add checkbox to the one-click importer
if ( !function_exists( 'yreg_estate_calcfields_form_importer_show_params' ) ) {
	//add_action( 'yreg_estate_action_importer_params',	'yreg_estate_calcfields_form_importer_show_params', 10, 1 );
	function yreg_estate_calcfields_form_importer_show_params($importer) {
		?>
		<input type="checkbox" <?php echo in_array('calcfields', yreg_estate_storage_get('required_plugins')) && $importer->options['plugins_initial_state'] 
											? 'checked="checked"' 
											: ''; ?> value="1" name="import_calcfields_form" id="import_calcfields_form" /> <label for="import_calcfields_form"><?php esc_html_e('Import Calculated Fields Form', 'yreg-estate'); ?></label><br>
		<?php
	}
}

// Import posts
if ( !function_exists( 'yreg_estate_calcfields_form_importer_import' ) ) {
	//add_action( 'yreg_estate_action_importer_import',	'yreg_estate_calcfields_form_importer_import', 10, 2 );
	function yreg_estate_calcfields_form_importer_import($importer, $action) {
		if ( $action == 'import_calcfields_form' ) {
			$importer->import_dump('calcfields_form', esc_html__('Calculated Fields Form', 'yreg-estate'));
		}
	}
}

// Display import progress
if ( !function_exists( 'yreg_estate_calcfields_form_importer_import_fields' ) ) {
	//add_action( 'yreg_estate_action_importer_import_fields',	'yreg_estate_calcfields_form_importer_import_fields', 10, 1 );
	function yreg_estate_calcfields_form_importer_import_fields($importer) {
		?>
		<tr class="import_calcfields_form">
			<td class="import_progress_item"><?php esc_html_e('Calculated Fields Form', 'yreg-estate'); ?></td>
			<td class="import_progress_status"></td>
		</tr>
		<?php
	}
}

// Export posts
if ( !function_exists( 'yreg_estate_calcfields_form_importer_export' ) ) {
	//add_action( 'yreg_estate_action_importer_export',	'yreg_estate_calcfields_form_importer_export', 10, 1 );
	function yreg_estate_calcfields_form_importer_export($importer) {
		yreg_estate_storage_set('export_calcfields_form', serialize( array(
			CP_CALCULATEDFIELDSF_FORMS_TABLE => $importer->export_dump(CP_CALCULATEDFIELDSF_FORMS_TABLE)
			) )
		);
	}
}

// Display exported data in the fields
if ( !function_exists( 'yreg_estate_calcfields_form_importer_export_fields' ) ) {
	//add_action( 'yreg_estate_action_importer_export_fields',	'yreg_estate_calcfields_form_importer_export_fields', 10, 1 );
	function yreg_estate_calcfields_form_importer_export_fields($importer) {
		?>
		<tr>
			<th align="left"><?php esc_html_e('Calculated Fields Form', 'yreg-estate'); ?></th>
			<td><?php yreg_estate_fpc(yreg_estate_get_file_dir('core/core.importer/export/calcfields_form.txt'), yreg_estate_storage_get('export_calcfields_form')); ?>
				<a download="calcfields_form.txt" href="<?php echo esc_url(yreg_estate_get_file_url('core/core.importer/export/calcfields_form.txt')); ?>"><?php esc_html_e('Download', 'yreg-estate'); ?></a>
			</td>
		</tr>
		<?php
	}
}


// Lists
//------------------------------------------------------------------------

// Return Calculated forms list list, prepended inherit (if need)
if ( !function_exists( 'yreg_estate_get_list_calcfields_form' ) ) {
	function yreg_estate_get_list_calcfields_form($prepend_inherit=false) {
		if (($list = yreg_estate_storage_get('list_calcfields_form'))=='') {
			$list = array();
			if (yreg_estate_exists_calcfields_form()) {
				global $wpdb;
				$rows = $wpdb->get_results( "SELECT id, form_name FROM " . esc_sql($wpdb->prefix . CP_CALCULATEDFIELDSF_FORMS_TABLE) );
				if (is_array($rows) && count($rows) > 0) {
					foreach ($rows as $row) {
						$list[$row->id] = $row->form_name;
					}
				}
			}
			$list = apply_filters('yreg_estate_filter_list_calcfields_form', $list);
			if (yreg_estate_get_theme_setting('use_list_cache')) yreg_estate_storage_set('list_calcfields_form', $list); 
		}
		return $prepend_inherit ? yreg_estate_array_merge(array('inherit' => esc_html__("Inherit", 'yreg-estate')), $list) : $list;
	}
}



// Shortcodes
//------------------------------------------------------------------------

// Register shortcode in the shortcodes list
if (!function_exists('yreg_estate_calcfields_form_reg_shortcodes')) {
	//add_filter('yreg_estate_action_shortcodes_list',	'yreg_estate_calcfields_form_reg_shortcodes');
	function yreg_estate_calcfields_form_reg_shortcodes() {
		if (yreg_estate_storage_isset('shortcodes')) {

			$forms_list = yreg_estate_get_list_calcfields_form();

			yreg_estate_sc_map_after( 'trx_button', 'CP_CALCULATED_FIELDS', array(
					"title" => esc_html__("Calculated fields form", 'yreg-estate'),
					"desc" => esc_html__("Insert calculated fields form", 'yreg-estate'),
					"decorate" => true,
					"container" => false,
					"params" => array(
						"id" => array(
							"title" => esc_html__("Form ID", 'yreg-estate'),
							"desc" => esc_html__("Select Form to insert into current page", 'yreg-estate'),
							"value" => "",
							"size" => "medium",
							"options" => $forms_list,
							"type" => "select"
							)
						)
					)
			);
		}
	}
}


// Register shortcode in the VC shortcodes list
if (!function_exists('yreg_estate_calcfields_form_reg_shortcodes_vc')) {
	//add_filter('yreg_estate_action_shortcodes_list_vc',	'yreg_estate_calcfields_form_reg_shortcodes_vc');
	function yreg_estate_calcfields_form_reg_shortcodes_vc() {

		$forms_list = yreg_estate_get_list_calcfields_form();

		// Calculated fields form
		vc_map( array(
				"base" => "CP_CALCULATED_FIELDS",
				"name" => esc_html__("Calculated fields form", 'yreg-estate'),
				"description" => esc_html__("Insert calculated fields form", 'yreg-estate'),
				"category" => esc_html__('Content', 'yreg-estate'),
				'icon' => 'icon_trx_calcfields',
				"class" => "trx_sc_single trx_sc_calcfields",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "id",
						"heading" => esc_html__("Form ID", 'yreg-estate'),
						"description" => esc_html__("Select Form to insert into current page", 'yreg-estate'),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip($forms_list),
						"type" => "dropdown"
					)
				)
			) );
			
		class WPBakeryShortCode_Cp_Calculated_Fields extends YREG_ESTATE_VC_ShortCodeSingle {}

	}
}
?>