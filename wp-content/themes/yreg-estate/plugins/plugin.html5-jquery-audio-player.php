<?php
/* HTML5 jQuery Audio Player support functions
------------------------------------------------------------------------------- */

// Theme init
if (!function_exists('yreg_estate_html5_jquery_audio_player_theme_setup')) {
    add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_html5_jquery_audio_player_theme_setup' );
    function yreg_estate_html5_jquery_audio_player_theme_setup() {
        // Add shortcode in the shortcodes list
        if (yreg_estate_exists_html5_jquery_audio_player()) {
			add_action('yreg_estate_action_add_styles',					'yreg_estate_html5_jquery_audio_player_frontend_scripts' );
            add_action('yreg_estate_action_shortcodes_list',				'yreg_estate_html5_jquery_audio_player_reg_shortcodes');
			if (function_exists('yreg_estate_exists_visual_composer') && yreg_estate_exists_visual_composer())
	            add_action('yreg_estate_action_shortcodes_list_vc',		'yreg_estate_html5_jquery_audio_player_reg_shortcodes_vc');
            if (is_admin()) {
                add_filter( 'yreg_estate_filter_importer_options',			'yreg_estate_html5_jquery_audio_player_importer_set_options', 10, 1 );
                add_action( 'yreg_estate_action_importer_params',			'yreg_estate_html5_jquery_audio_player_importer_show_params', 10, 1 );
                add_action( 'yreg_estate_action_importer_import',			'yreg_estate_html5_jquery_audio_player_importer_import', 10, 2 );
				add_action( 'yreg_estate_action_importer_import_fields',	'yreg_estate_html5_jquery_audio_player_importer_import_fields', 10, 1 );
                add_action( 'yreg_estate_action_importer_export',			'yreg_estate_html5_jquery_audio_player_importer_export', 10, 1 );
                add_action( 'yreg_estate_action_importer_export_fields',	'yreg_estate_html5_jquery_audio_player_importer_export_fields', 10, 1 );
            }
        }
        if (is_admin()) {
            add_filter( 'yreg_estate_filter_importer_required_plugins',	'yreg_estate_html5_jquery_audio_player_importer_required_plugins', 10, 2 );
            add_filter( 'yreg_estate_filter_required_plugins',				'yreg_estate_html5_jquery_audio_player_required_plugins' );
        }
    }
}

// Check if plugin installed and activated
if ( !function_exists( 'yreg_estate_exists_html5_jquery_audio_player' ) ) {
	function yreg_estate_exists_html5_jquery_audio_player() {
		return function_exists('hmp_db_create');
	}
}

// Filter to add in the required plugins list
if ( !function_exists( 'yreg_estate_html5_jquery_audio_player_required_plugins' ) ) {
	//add_filter('yreg_estate_filter_required_plugins',	'yreg_estate_html5_jquery_audio_player_required_plugins');
	function yreg_estate_html5_jquery_audio_player_required_plugins($list=array()) {
		if (in_array('html5_jquery_audio_player', yreg_estate_storage_get('required_plugins')))
		$list[] = array(
					'name' 		=> 'HTML5 jQuery Audio Player',
					'slug' 		=> 'html5-jquery-audio-player',
					'required' 	=> false
				);
		return $list;
	}
}

// Enqueue custom styles
if ( !function_exists( 'yreg_estate_html5_jquery_audio_player_frontend_scripts' ) ) {
	//add_action( 'yreg_estate_action_add_styles', 'yreg_estate_html5_jquery_audio_player_frontend_scripts' );
	function yreg_estate_html5_jquery_audio_player_frontend_scripts() {
		if (file_exists(yreg_estate_get_file_dir('css/plugin.html5-jquery-audio-player.css'))) {
			yreg_estate_enqueue_style( 'yreg_estate-plugin.html5-jquery-audio-player-style',  yreg_estate_get_file_url('css/plugin.html5-jquery-audio-player.css'), array(), null );
		}
	}
}



// One-click import support
//------------------------------------------------------------------------

// Check HTML5 jQuery Audio Player in the required plugins
if ( !function_exists( 'yreg_estate_html5_jquery_audio_player_importer_required_plugins' ) ) {
	//add_filter( 'yreg_estate_filter_importer_required_plugins',	'yreg_estate_html5_jquery_audio_player_importer_required_plugins', 10, 2 );
	function yreg_estate_html5_jquery_audio_player_importer_required_plugins($not_installed='', $importer=null) {
		//if ($importer && in_array('html5_jquery_audio_player', $importer->options['required_plugins']) && !yreg_estate_exists_html5_jquery_audio_player() )
		if (yreg_estate_strpos($list, 'html5_jquery_audio_player')!==false && !yreg_estate_exists_html5_jquery_audio_player() )
			$not_installed .= '<br>HTML5 jQuery Audio Player';
		return $not_installed;
	}
}


// Set options for one-click importer
if ( !function_exists( 'yreg_estate_html5_jquery_audio_player_importer_set_options' ) ) {
    //add_filter( 'yreg_estate_filter_importer_options',	'yreg_estate_html5_jquery_audio_player_importer_set_options', 10, 1 );
    function yreg_estate_html5_jquery_audio_player_importer_set_options($options=array()) {
		if ( in_array('html5_jquery_audio_player', yreg_estate_storage_get('required_plugins')) && yreg_estate_exists_html5_jquery_audio_player() ) {
            $options['file_with_html5_jquery_audio_player'] = 'demo/html5_jquery_audio_player.txt';			// Name of the file with HTML5 jQuery Audio Player data
            $options['additional_options'][] = 'showbuy';		// Add slugs to export options for this plugin
            $options['additional_options'][] = 'buy_text';		// Add slugs to export options for this plugin
            $options['additional_options'][] = 'showlist';		// Add slugs to export options for this plugin
            $options['additional_options'][] = 'autoplay';		// Add slugs to export options for this plugin
            $options['additional_options'][] = 'tracks';		// Add slugs to export options for this plugin
            $options['additional_options'][] = 'currency';		// Add slugs to export options for this plugin
            $options['additional_options'][] = 'color';		    // Add slugs to export options for this plugin
            $options['additional_options'][] = 'tcolor';		// Add slugs to export options for this plugin
        }
        return $options;
    }
}

// Add checkbox to the one-click importer
if ( !function_exists( 'yreg_estate_html5_jquery_audio_player_importer_show_params' ) ) {
    //add_action( 'yreg_estate_action_importer_params',	'yreg_estate_html5_jquery_audio_player_importer_show_params', 10, 1 );
    function yreg_estate_html5_jquery_audio_player_importer_show_params($importer) {
        ?>
        <input type="checkbox" <?php echo in_array('html5_jquery_audio_player', yreg_estate_storage_get('required_plugins')) && $importer->options['plugins_initial_state']
											? 'checked="checked"' 
											: ''; ?> value="1" name="import_html5_jquery_audio_player" id="import_html5_jquery_audio_player" /> <label for="import_html5_jquery_audio_player"><?php esc_html_e('Import HTML5 jQuery Audio Player', 'yreg-estate'); ?></label><br>
    <?php
    }
}


// Import posts
if ( !function_exists( 'yreg_estate_html5_jquery_audio_player_importer_import' ) ) {
    //add_action( 'yreg_estate_action_importer_import',	'yreg_estate_html5_jquery_audio_player_importer_import', 10, 2 );
    function yreg_estate_html5_jquery_audio_player_importer_import($importer, $action) {
		if ( $action == 'import_html5_jquery_audio_player' ) {
            $importer->import_dump('html5_jquery_audio_player', esc_html__('HTML5 jQuery Audio Player', 'yreg-estate'));
        }
    }
}

// Display import progress
if ( !function_exists( 'yreg_estate_html5_jquery_audio_player_importer_import_fields' ) ) {
	//add_action( 'yreg_estate_action_importer_import_fields',	'yreg_estate_html5_jquery_audio_player_importer_import_fields', 10, 1 );
	function yreg_estate_html5_jquery_audio_player_importer_import_fields($importer) {
		?>
		<tr class="import_html5_jquery_audio_player">
			<td class="import_progress_item"><?php esc_html_e('HTML5 jQuery Audio Player', 'yreg-estate'); ?></td>
			<td class="import_progress_status"></td>
		</tr>
		<?php
	}
}


// Export posts
if ( !function_exists( 'yreg_estate_html5_jquery_audio_player_importer_export' ) ) {
    //add_action( 'yreg_estate_action_importer_export',	'yreg_estate_html5_jquery_audio_player_importer_export', 10, 1 );
    function yreg_estate_html5_jquery_audio_player_importer_export($importer) {
		yreg_estate_storage_set('export_html5_jquery_audio_player', serialize( array(
			'hmp_playlist'	=> $importer->export_dump('hmp_playlist'),
			'hmp_rating'	=> $importer->export_dump('hmp_rating')
			) )
		);
    }
}


// Display exported data in the fields
if ( !function_exists( 'yreg_estate_html5_jquery_audio_player_importer_export_fields' ) ) {
    //add_action( 'yreg_estate_action_importer_export_fields',	'yreg_estate_html5_jquery_audio_player_importer_export_fields', 10, 1 );
    function yreg_estate_html5_jquery_audio_player_importer_export_fields($importer) {
        ?>
        <tr>
            <th align="left"><?php esc_html_e('HTML5 jQuery Audio Player', 'yreg-estate'); ?></th>
            <td><?php yreg_estate_fpc(yreg_estate_get_file_dir('core/core.importer/export/html5_jquery_audio_player.txt'), yreg_estate_storage_get('export_html5_jquery_audio_player')); ?>
                <a download="html5_jquery_audio_player.txt" href="<?php echo esc_url(yreg_estate_get_file_url('core/core.importer/export/html5_jquery_audio_player.txt')); ?>"><?php esc_html_e('Download', 'yreg-estate'); ?></a>
            </td>
        </tr>
    <?php
    }
}





// Shortcodes
//------------------------------------------------------------------------

// Register shortcode in the shortcodes list
if (!function_exists('yreg_estate_html5_jquery_audio_player_reg_shortcodes')) {
    //add_filter('yreg_estate_action_shortcodes_list',	'yreg_estate_html5_jquery_audio_player_reg_shortcodes');
    function yreg_estate_html5_jquery_audio_player_reg_shortcodes() {
		if (yreg_estate_storage_isset('shortcodes')) {
			yreg_estate_sc_map_after('trx_audio', 'hmp_player', array(
                "title" => esc_html__("HTML5 jQuery Audio Player", 'yreg-estate'),
                "desc" => esc_html__("Insert HTML5 jQuery Audio Player", 'yreg-estate'),
                "decorate" => true,
                "container" => false,
				"params" => array()
				)
            );
        }
    }
}


// Register shortcode in the VC shortcodes list
if (!function_exists('yreg_estate_hmp_player_reg_shortcodes_vc')) {
    add_filter('yreg_estate_action_shortcodes_list_vc',	'yreg_estate_hmp_player_reg_shortcodes_vc');
    function yreg_estate_hmp_player_reg_shortcodes_vc() {

        // YREG_ESTATE HTML5 jQuery Audio Player
        vc_map( array(
            "base" => "hmp_player",
            "name" => esc_html__("HTML5 jQuery Audio Player", 'yreg-estate'),
            "description" => esc_html__("Insert HTML5 jQuery Audio Player", 'yreg-estate'),
            "category" => esc_html__('Content', 'yreg-estate'),
            'icon' => 'icon_trx_audio',
            "class" => "trx_sc_single trx_sc_hmp_player",
            "content_element" => true,
            "is_container" => false,
            "show_settings_on_create" => false,
            "params" => array()
        ) );

        class WPBakeryShortCode_Hmp_Player extends YREG_ESTATE_VC_ShortCodeSingle {}

    }
}
?>