<?php
/* Instagram Widget support functions
------------------------------------------------------------------------------- */

// Theme init
if (!function_exists('yreg_estate_instagram_widget_theme_setup')) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_instagram_widget_theme_setup', 1 );
	function yreg_estate_instagram_widget_theme_setup() {
		if (yreg_estate_exists_instagram_widget()) {
			add_action( 'yreg_estate_action_add_styles', 						'yreg_estate_instagram_widget_frontend_scripts' );
		}
		if (is_admin()) {
			add_filter( 'yreg_estate_filter_importer_required_plugins',		'yreg_estate_instagram_widget_importer_required_plugins', 10, 2 );
			add_filter( 'yreg_estate_filter_required_plugins',					'yreg_estate_instagram_widget_required_plugins' );
		}
	}
}

// Check if Instagram Widget installed and activated
if ( !function_exists( 'yreg_estate_exists_instagram_widget' ) ) {
	function yreg_estate_exists_instagram_widget() {
		return function_exists('wpiw_init');
	}
}

// Filter to add in the required plugins list
if ( !function_exists( 'yreg_estate_instagram_widget_required_plugins' ) ) {
	//add_filter('yreg_estate_filter_required_plugins',	'yreg_estate_instagram_widget_required_plugins');
	function yreg_estate_instagram_widget_required_plugins($list=array()) {
		if (in_array('instagram_widget', yreg_estate_storage_get('required_plugins')))
			$list[] = array(
					'name' 		=> 'Instagram Widget',
					'slug' 		=> 'wp-instagram-widget',
					'required' 	=> false
				);
		return $list;
	}
}

// Enqueue custom styles
if ( !function_exists( 'yreg_estate_instagram_widget_frontend_scripts' ) ) {
	//add_action( 'yreg_estate_action_add_styles', 'yreg_estate_instagram_widget_frontend_scripts' );
	function yreg_estate_instagram_widget_frontend_scripts() {
		if (file_exists(yreg_estate_get_file_dir('css/plugin.instagram-widget.css')))
			yreg_estate_enqueue_style( 'yreg_estate-plugin.instagram-widget-style',  yreg_estate_get_file_url('css/plugin.instagram-widget.css'), array(), null );
	}
}



// One-click import support
//------------------------------------------------------------------------

// Check Instagram Widget in the required plugins
if ( !function_exists( 'yreg_estate_instagram_widget_importer_required_plugins' ) ) {
	//add_filter( 'yreg_estate_filter_importer_required_plugins',	'yreg_estate_instagram_widget_importer_required_plugins', 10, 2 );
	function yreg_estate_instagram_widget_importer_required_plugins($not_installed='', $list='') {
		//if (in_array('instagram_widget', yreg_estate_storage_get('required_plugins')) && !yreg_estate_exists_instagram_widget() )
		if (yreg_estate_strpos($list, 'instagram_widget')!==false && !yreg_estate_exists_instagram_widget() )
			$not_installed .= '<br>WP Instagram Widget';
		return $not_installed;
	}
}
?>