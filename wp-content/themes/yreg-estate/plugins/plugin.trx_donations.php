<?php
/* YREG_ESTATE Donations support functions
------------------------------------------------------------------------------- */

// Theme init
if (!function_exists('yreg_estate_trx_donations_theme_setup')) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_trx_donations_theme_setup', 1 );
	function yreg_estate_trx_donations_theme_setup() {

		// Register shortcode in the shortcodes list
		if (yreg_estate_exists_trx_donations()) {
			// Detect current page type, taxonomy and title (for custom post_types use priority < 10 to fire it handles early, than for standard post types)
			add_filter('yreg_estate_filter_get_blog_type',			'yreg_estate_trx_donations_get_blog_type', 9, 2);
			add_filter('yreg_estate_filter_get_blog_title',		'yreg_estate_trx_donations_get_blog_title', 9, 2);
			add_filter('yreg_estate_filter_get_current_taxonomy',	'yreg_estate_trx_donations_get_current_taxonomy', 9, 2);
			add_filter('yreg_estate_filter_is_taxonomy',			'yreg_estate_trx_donations_is_taxonomy', 9, 2);
			add_filter('yreg_estate_filter_get_stream_page_title',	'yreg_estate_trx_donations_get_stream_page_title', 9, 2);
			add_filter('yreg_estate_filter_get_stream_page_link',	'yreg_estate_trx_donations_get_stream_page_link', 9, 2);
			add_filter('yreg_estate_filter_get_stream_page_id',	'yreg_estate_trx_donations_get_stream_page_id', 9, 2);
			add_filter('yreg_estate_filter_query_add_filters',		'yreg_estate_trx_donations_query_add_filters', 9, 2);
			add_filter('yreg_estate_filter_detect_inheritance_key','yreg_estate_trx_donations_detect_inheritance_key', 9, 1);
			add_filter('yreg_estate_filter_list_post_types',		'yreg_estate_trx_donations_list_post_types');
			// Register shortcodes in the list
			add_action('yreg_estate_action_shortcodes_list',		'yreg_estate_trx_donations_reg_shortcodes');
			if (function_exists('yreg_estate_exists_visual_composer') && yreg_estate_exists_visual_composer())
				add_action('yreg_estate_action_shortcodes_list_vc','yreg_estate_trx_donations_reg_shortcodes_vc');
			if (is_admin()) {
				add_filter( 'yreg_estate_filter_importer_options',				'yreg_estate_trx_donations_importer_set_options' );
			}
		}
		if (is_admin()) {
			add_filter( 'yreg_estate_filter_importer_required_plugins',	'yreg_estate_trx_donations_importer_required_plugins', 10, 2 );
			add_filter( 'yreg_estate_filter_required_plugins',				'yreg_estate_trx_donations_required_plugins' );
		}
	}
}

if ( !function_exists( 'yreg_estate_trx_donations_settings_theme_setup2' ) ) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_trx_donations_settings_theme_setup2', 3 );
	function yreg_estate_trx_donations_settings_theme_setup2() {
		// Add Donations post type and taxonomy into theme inheritance list
		if (yreg_estate_exists_trx_donations()) {
			yreg_estate_add_theme_inheritance( array('donations' => array(
				'stream_template' => 'blog-donations',
				'single_template' => 'single-donation',
				'taxonomy' => array(YREG_ESTATE_Donations::TAXONOMY),
				'taxonomy_tags' => array(),
				'post_type' => array(YREG_ESTATE_Donations::POST_TYPE),
				'override' => 'page'
				) )
			);
		}
	}
}

// Check if YREG_ESTATE Donations installed and activated
if ( !function_exists( 'yreg_estate_exists_trx_donations' ) ) {
	function yreg_estate_exists_trx_donations() {
		return class_exists('YREG_ESTATE_Donations');
	}
}


// Return true, if current page is donations page
if ( !function_exists( 'yreg_estate_is_trx_donations_page' ) ) {
	function yreg_estate_is_trx_donations_page() {
		$is = false;
		if (yreg_estate_exists_trx_donations()) {
			$is = in_array(yreg_estate_storage_get('page_template'), array('blog-donations', 'single-donation'));
			if (!$is) {
				if (!yreg_estate_storage_empty('pre_query'))
					$is = (yreg_estate_storage_call_obj_method('pre_query', 'is_single') && yreg_estate_storage_call_obj_method('pre_query', 'get', 'post_type') == YREG_ESTATE_Donations::POST_TYPE) 
							|| yreg_estate_storage_call_obj_method('pre_query', 'is_post_type_archive', YREG_ESTATE_Donations::POST_TYPE) 
							|| yreg_estate_storage_call_obj_method('pre_query', 'is_tax', YREG_ESTATE_Donations::TAXONOMY);
				else
					$is = (is_single() && get_query_var('post_type') == YREG_ESTATE_Donations::POST_TYPE) 
							|| is_post_type_archive(YREG_ESTATE_Donations::POST_TYPE) 
							|| is_tax(YREG_ESTATE_Donations::TAXONOMY);
			}
		}
		return $is;
	}
}

// Filter to detect current page inheritance key
if ( !function_exists( 'yreg_estate_trx_donations_detect_inheritance_key' ) ) {
	//add_filter('yreg_estate_filter_detect_inheritance_key',	'yreg_estate_trx_donations_detect_inheritance_key', 9, 1);
	function yreg_estate_trx_donations_detect_inheritance_key($key) {
		if (!empty($key)) return $key;
		return yreg_estate_is_trx_donations_page() ? 'donations' : '';
	}
}

// Filter to detect current page slug
if ( !function_exists( 'yreg_estate_trx_donations_get_blog_type' ) ) {
	//add_filter('yreg_estate_filter_get_blog_type',	'yreg_estate_trx_donations_get_blog_type', 9, 2);
	function yreg_estate_trx_donations_get_blog_type($page, $query=null) {
		if (!empty($page)) return $page;
		if ($query && $query->is_tax(YREG_ESTATE_Donations::TAXONOMY) || is_tax(YREG_ESTATE_Donations::TAXONOMY))
			$page = 'donations_category';
		else if ($query && $query->get('post_type')==YREG_ESTATE_Donations::POST_TYPE || get_query_var('post_type')==YREG_ESTATE_Donations::POST_TYPE)
			$page = $query && $query->is_single() || is_single() ? 'donations_item' : 'donations';
		return $page;
	}
}

// Filter to detect current page title
if ( !function_exists( 'yreg_estate_trx_donations_get_blog_title' ) ) {
	//add_filter('yreg_estate_filter_get_blog_title',	'yreg_estate_trx_donations_get_blog_title', 9, 2);
	function yreg_estate_trx_donations_get_blog_title($title, $page) {
		if (!empty($title)) return $title;
		if ( yreg_estate_strpos($page, 'donations')!==false ) {
			if ( $page == 'donations_category' ) {
				$term = get_term_by( 'slug', get_query_var( YREG_ESTATE_Donations::TAXONOMY ), YREG_ESTATE_Donations::TAXONOMY, OBJECT);
				$title = $term->name;
			} else if ( $page == 'donations_item' ) {
				$title = yreg_estate_get_post_title();
			} else {
				$title = esc_html__('All donations', 'yreg-estate');
			}
		}

		return $title;
	}
}

// Filter to detect stream page title
if ( !function_exists( 'yreg_estate_trx_donations_get_stream_page_title' ) ) {
	//add_filter('yreg_estate_filter_get_stream_page_title',	'yreg_estate_trx_donations_get_stream_page_title', 9, 2);
	function yreg_estate_trx_donations_get_stream_page_title($title, $page) {
		if (!empty($title)) return $title;
		if (yreg_estate_strpos($page, 'donations')!==false) {
			if (($page_id = yreg_estate_trx_donations_get_stream_page_id(0, $page=='donations' ? 'blog-donations' : $page)) > 0)
				$title = yreg_estate_get_post_title($page_id);
			else
				$title = esc_html__('All donations', 'yreg-estate');				
		}
		return $title;
	}
}

// Filter to detect stream page ID
if ( !function_exists( 'yreg_estate_trx_donations_get_stream_page_id' ) ) {
	//add_filter('yreg_estate_filter_get_stream_page_id',	'yreg_estate_trx_donations_get_stream_page_id', 9, 2);
	function yreg_estate_trx_donations_get_stream_page_id($id, $page) {
		if (!empty($id)) return $id;
		if (yreg_estate_strpos($page, 'donations')!==false) $id = yreg_estate_get_template_page_id('blog-donations');
		return $id;
	}
}

// Filter to detect stream page URL
if ( !function_exists( 'yreg_estate_trx_donations_get_stream_page_link' ) ) {
	//add_filter('yreg_estate_filter_get_stream_page_link',	'yreg_estate_trx_donations_get_stream_page_link', 9, 2);
	function yreg_estate_trx_donations_get_stream_page_link($url, $page) {
		if (!empty($url)) return $url;
		if (yreg_estate_strpos($page, 'donations')!==false) {
			$id = yreg_estate_get_template_page_id('blog-donations');
			if ($id) $url = get_permalink($id);
		}
		return $url;
	}
}

// Filter to detect current taxonomy
if ( !function_exists( 'yreg_estate_trx_donations_get_current_taxonomy' ) ) {
	//add_filter('yreg_estate_filter_get_current_taxonomy',	'yreg_estate_trx_donations_get_current_taxonomy', 9, 2);
	function yreg_estate_trx_donations_get_current_taxonomy($tax, $page) {
		if (!empty($tax)) return $tax;
		if ( yreg_estate_strpos($page, 'donations')!==false ) {
			$tax = YREG_ESTATE_Donations::TAXONOMY;
		}
		return $tax;
	}
}

// Return taxonomy name (slug) if current page is this taxonomy page
if ( !function_exists( 'yreg_estate_trx_donations_is_taxonomy' ) ) {
	//add_filter('yreg_estate_filter_is_taxonomy',	'yreg_estate_trx_donations_is_taxonomy', 9, 2);
	function yreg_estate_trx_donations_is_taxonomy($tax, $query=null) {
		if (!empty($tax))
			return $tax;
		else 
			return $query && $query->get(YREG_ESTATE_Donations::TAXONOMY)!='' || is_tax(YREG_ESTATE_Donations::TAXONOMY) ? YREG_ESTATE_Donations::TAXONOMY : '';
	}
}

// Add custom post type and/or taxonomies arguments to the query
if ( !function_exists( 'yreg_estate_trx_donations_query_add_filters' ) ) {
	//add_filter('yreg_estate_filter_query_add_filters',	'yreg_estate_trx_donations_query_add_filters', 9, 2);
	function yreg_estate_trx_donations_query_add_filters($args, $filter) {
		if ($filter == 'donations') {
			$args['post_type'] = YREG_ESTATE_Donations::POST_TYPE;
		}
		return $args;
	}
}

// Add custom post type to the list
if ( !function_exists( 'yreg_estate_trx_donations_list_post_types' ) ) {
	//add_filter('yreg_estate_filter_list_post_types',		'yreg_estate_trx_donations_list_post_types');
	function yreg_estate_trx_donations_list_post_types($list) {
		$list[YREG_ESTATE_Donations::POST_TYPE] = esc_html__('Donations', 'yreg-estate');
		return $list;
	}
}


// Register shortcode in the shortcodes list
if (!function_exists('yreg_estate_trx_donations_reg_shortcodes')) {
	//add_filter('yreg_estate_action_shortcodes_list',	'yreg_estate_trx_donations_reg_shortcodes');
	function yreg_estate_trx_donations_reg_shortcodes() {
		if (yreg_estate_storage_isset('shortcodes')) {

			$plugin = YREG_ESTATE_Donations::get_instance();
			$donations_groups = yreg_estate_get_list_terms(false, YREG_ESTATE_Donations::TAXONOMY);

			yreg_estate_sc_map_before('trx_dropcaps', array(

				// YREG_ESTATE Donations form
				"trx_donations_form" => array(
					"title" => esc_html__("Donations form", 'yreg-estate'),
					"desc" => esc_html__("Insert yregEstate Donations form", 'yreg-estate'),
					"decorate" => true,
					"container" => false,
					"params" => array(
						"title" => array(
							"title" => esc_html__("Title", 'yreg-estate'),
							"desc" => esc_html__("Title for the donations form", 'yreg-estate'),
							"value" => "",
							"type" => "text"
						),
						"subtitle" => array(
							"title" => esc_html__("Subtitle", 'yreg-estate'),
							"desc" => esc_html__("Subtitle for the donations form", 'yreg-estate'),
							"value" => "",
							"type" => "text"
						),
						"description" => array(
							"title" => esc_html__("Description", 'yreg-estate'),
							"desc" => esc_html__("Short description for the donations form", 'yreg-estate'),
							"value" => "",
							"type" => "textarea"
						),
						"align" => array(
							"title" => esc_html__("Alignment", 'yreg-estate'),
							"desc" => esc_html__("Alignment of the donations form", 'yreg-estate'),
							"divider" => true,
							"value" => "",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => yreg_estate_get_sc_param('align')
						),
						"account" => array(
							"title" => esc_html__("PayPal account", 'yreg-estate'),
							"desc" => esc_html__("PayPal account's e-mail. If empty - used from yregEstate Donations settings", 'yreg-estate'),
							"divider" => true,
							"value" => "",
							"type" => "text"
						),
						"sandbox" => array(
							"title" => esc_html__("Sandbox mode", 'yreg-estate'),
							"desc" => esc_html__("Use PayPal sandbox to test payments", 'yreg-estate'),
							"dependency" => array(
								'account' => array('not_empty')
							),
							"value" => "yes",
							"type" => "switch",
							"options" => yreg_estate_get_sc_param('yes_no')
						),
						"amount" => array(
							"title" => esc_html__("Default amount", 'yreg-estate'),
							"desc" => esc_html__("Specify amount, initially selected in the form", 'yreg-estate'),
							"dependency" => array(
								'account' => array('not_empty')
							),
							"value" => 5,
							"min" => 1,
							"step" => 5,
							"type" => "spinner"
						),
						"currency" => array(
							"title" => esc_html__("Currency", 'yreg-estate'),
							"desc" => esc_html__("Select payment's currency", 'yreg-estate'),
							"dependency" => array(
								'account' => array('not_empty')
							),
							"divider" => true,
							"value" => "",
							"type" => "select",
							"style" => "list",
							"multiple" => true,
							"options" => yreg_estate_array_merge(array(0 => esc_html__('- Select currency -', 'yreg-estate')), $plugin->currency_codes)
						),
						"width" => yreg_estate_shortcodes_width(),
						"top" => yreg_estate_get_sc_param('top'),
						"bottom" => yreg_estate_get_sc_param('bottom'),
						"left" => yreg_estate_get_sc_param('left'),
						"right" => yreg_estate_get_sc_param('right'),
						"id" => yreg_estate_get_sc_param('id'),
						"class" => yreg_estate_get_sc_param('class'),
						"css" => yreg_estate_get_sc_param('css')
					)
				),
				
				
				// YREG_ESTATE Donations form
				"trx_donations_list" => array(
					"title" => esc_html__("Donations list", 'yreg-estate'),
					"desc" => esc_html__("Insert yregEstate Doantions list", 'yreg-estate'),
					"decorate" => true,
					"container" => false,
					"params" => array(
						"title" => array(
							"title" => esc_html__("Title", 'yreg-estate'),
							"desc" => esc_html__("Title for the donations list", 'yreg-estate'),
							"value" => "",
							"type" => "text"
						),
						"subtitle" => array(
							"title" => esc_html__("Subtitle", 'yreg-estate'),
							"desc" => esc_html__("Subtitle for the donations list", 'yreg-estate'),
							"value" => "",
							"type" => "text"
						),
						"description" => array(
							"title" => esc_html__("Description", 'yreg-estate'),
							"desc" => esc_html__("Short description for the donations list", 'yreg-estate'),
							"value" => "",
							"type" => "textarea"
						),
						"link" => array(
							"title" => esc_html__("Button URL", 'yreg-estate'),
							"desc" => esc_html__("Link URL for the button at the bottom of the block", 'yreg-estate'),
							"divider" => true,
							"value" => "",
							"type" => "text"
						),
						"link_caption" => array(
							"title" => esc_html__("Button caption", 'yreg-estate'),
							"desc" => esc_html__("Caption for the button at the bottom of the block", 'yreg-estate'),
							"value" => "",
							"type" => "text"
						),
						"style" => array(
							"title" => esc_html__("List style", 'yreg-estate'),
							"desc" => esc_html__("Select style to display donations", 'yreg-estate'),
							"value" => "excerpt",
							"type" => "select",
							"options" => array(
								'excerpt' => esc_html__('Excerpt', 'yreg-estate')
							)
						),
						"readmore" => array(
							"title" => esc_html__("Read more text", 'yreg-estate'),
							"desc" => esc_html__("Text of the 'Read more' link", 'yreg-estate'),
							"value" => esc_html__('Read more', 'yreg-estate'),
							"type" => "text"
						),
						"cat" => array(
							"title" => esc_html__("Categories", 'yreg-estate'),
							"desc" => esc_html__("Select categories (groups) to show donations. If empty - select donations from any category (group) or from IDs list", 'yreg-estate'),
							"divider" => true,
							"value" => "",
							"type" => "select",
							"style" => "list",
							"multiple" => true,
							"options" => yreg_estate_array_merge(array(0 => esc_html__('- Select category -', 'yreg-estate')), $donations_groups)
						),
						"count" => array(
							"title" => esc_html__("Number of donations", 'yreg-estate'),
							"desc" => esc_html__("How many donations will be displayed? If used IDs - this parameter ignored.", 'yreg-estate'),
							"value" => 3,
							"min" => 1,
							"max" => 100,
							"type" => "spinner"
						),
						"columns" => array(
							"title" => esc_html__("Columns", 'yreg-estate'),
							"desc" => esc_html__("How many columns use to show donations list", 'yreg-estate'),
							"value" => 3,
							"min" => 2,
							"max" => 6,
							"step" => 1,
							"type" => "spinner"
						),
						"offset" => array(
							"title" => esc_html__("Offset before select posts", 'yreg-estate'),
							"desc" => esc_html__("Skip posts before select next part.", 'yreg-estate'),
							"dependency" => array(
								'custom' => array('no')
							),
							"value" => 0,
							"min" => 0,
							"type" => "spinner"
						),
						"orderby" => array(
							"title" => esc_html__("Donadions order by", 'yreg-estate'),
							"desc" => esc_html__("Select desired sorting method", 'yreg-estate'),
							"value" => "date",
							"type" => "select",
							"options" => yreg_estate_get_sc_param('sorting')
						),
						"order" => array(
							"title" => esc_html__("Donations order", 'yreg-estate'),
							"desc" => esc_html__("Select donations order", 'yreg-estate'),
							"value" => "desc",
							"type" => "switch",
							"size" => "big",
							"options" => yreg_estate_get_sc_param('ordering')
						),
						"ids" => array(
							"title" => esc_html__("Donations IDs list", 'yreg-estate'),
							"desc" => esc_html__("Comma separated list of donations ID. If set - parameters above are ignored!", 'yreg-estate'),
							"value" => "",
							"type" => "text"
						),
						"top" => yreg_estate_get_sc_param('top'),
						"bottom" => yreg_estate_get_sc_param('bottom'),
						"id" => yreg_estate_get_sc_param('id'),
						"class" => yreg_estate_get_sc_param('class'),
						"css" => yreg_estate_get_sc_param('css')
					)
				)

			));
		}
	}
}


// Register shortcode in the VC shortcodes list
if (!function_exists('yreg_estate_trx_donations_reg_shortcodes_vc')) {
	//add_filter('yreg_estate_action_shortcodes_list_vc',	'yreg_estate_trx_donations_reg_shortcodes_vc');
	function yreg_estate_trx_donations_reg_shortcodes_vc() {

		$plugin = YREG_ESTATE_Donations::get_instance();
		$donations_groups = yreg_estate_get_list_terms(false, YREG_ESTATE_Donations::TAXONOMY);

		// YREG_ESTATE Donations form
		vc_map( array(
				"base" => "trx_donations_form",
				"name" => esc_html__("Donations form", 'yreg-estate'),
				"description" => esc_html__("Insert yregEstate Donations form", 'yreg-estate'),
				"category" => esc_html__('Content', 'yreg-estate'),
				'icon' => 'icon_trx_donations_form',
				"class" => "trx_sc_single trx_sc_donations_form",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "title",
						"heading" => esc_html__("Title", 'yreg-estate'),
						"description" => esc_html__("Title for the donations form", 'yreg-estate'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "subtitle",
						"heading" => esc_html__("Subtitle", 'yreg-estate'),
						"description" => esc_html__("Subtitle for the donations form", 'yreg-estate'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "description",
						"heading" => esc_html__("Description", 'yreg-estate'),
						"description" => esc_html__("Description for the donations form", 'yreg-estate'),
						"class" => "",
						"value" => "",
						"type" => "textarea"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Alignment", 'yreg-estate'),
						"description" => esc_html__("Alignment of the donations form", 'yreg-estate'),
						"class" => "",
						"value" => array_flip(yreg_estate_get_sc_param('align')),
						"type" => "dropdown"
					),
					array(
						"param_name" => "account",
						"heading" => esc_html__("PayPal account", 'yreg-estate'),
						"description" => esc_html__("PayPal account's e-mail. If empty - used from yregEstate Donations settings", 'yreg-estate'),
						"admin_label" => true,
						"group" => esc_html__('PayPal', 'yreg-estate'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "sandbox",
						"heading" => esc_html__("Sandbox mode", 'yreg-estate'),
						"description" => esc_html__("Use PayPal sandbox to test payments", 'yreg-estate'),
						"admin_label" => true,
						"group" => esc_html__('PayPal', 'yreg-estate'),
						'dependency' => array(
							'element' => 'account',
							'not_empty' => true
						),
						"class" => "",
						"value" => array("Sandbox mode" => "yes" ),
						"type" => "checkbox"
					),
					array(
						"param_name" => "amount",
						"heading" => esc_html__("Default amount", 'yreg-estate'),
						"description" => esc_html__("Specify amount, initially selected in the form", 'yreg-estate'),
						"admin_label" => true,
						"group" => esc_html__('PayPal', 'yreg-estate'),
						"class" => "",
						"value" => "5",
						"type" => "textfield"
					),
					array(
						"param_name" => "currency",
						"heading" => esc_html__("Currency", 'yreg-estate'),
						"description" => esc_html__("Select payment's currency", 'yreg-estate'),
						"class" => "",
						"value" => array_flip(yreg_estate_array_merge(array(0 => esc_html__('- Select currency -', 'yreg-estate')), $plugin->currency_codes)),
						"type" => "dropdown"
					),
					yreg_estate_get_vc_param('id'),
					yreg_estate_get_vc_param('class'),
					yreg_estate_get_vc_param('css'),
					yreg_estate_vc_width(),
					yreg_estate_get_vc_param('margin_top'),
					yreg_estate_get_vc_param('margin_bottom'),
					yreg_estate_get_vc_param('margin_left'),
					yreg_estate_get_vc_param('margin_right')
				)
			) );
			
		class WPBakeryShortCode_Trx_Donations_Form extends YREG_ESTATE_VC_ShortCodeSingle {}



		// YREG_ESTATE Donations list
		vc_map( array(
				"base" => "trx_donations_list",
				"name" => esc_html__("Donations list", 'yreg-estate'),
				"description" => esc_html__("Insert yregEstate Donations list", 'yreg-estate'),
				"category" => esc_html__('Content', 'yreg-estate'),
				'icon' => 'icon_trx_donations_list',
				"class" => "trx_sc_single trx_sc_donations_list",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "style",
						"heading" => esc_html__("List style", 'yreg-estate'),
						"description" => esc_html__("Select style to display donations", 'yreg-estate'),
						"class" => "",
						"value" => array(
							esc_html__('Excerpt', 'yreg-estate') => 'excerpt'
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "title",
						"heading" => esc_html__("Title", 'yreg-estate'),
						"description" => esc_html__("Title for the donations form", 'yreg-estate'),
						"group" => esc_html__('Captions', 'yreg-estate'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "subtitle",
						"heading" => esc_html__("Subtitle", 'yreg-estate'),
						"description" => esc_html__("Subtitle for the donations form", 'yreg-estate'),
						"group" => esc_html__('Captions', 'yreg-estate'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "description",
						"heading" => esc_html__("Description", 'yreg-estate'),
						"description" => esc_html__("Description for the donations form", 'yreg-estate'),
						"group" => esc_html__('Captions', 'yreg-estate'),
						"class" => "",
						"value" => "",
						"type" => "textarea"
					),
					array(
						"param_name" => "link",
						"heading" => esc_html__("Button URL", 'yreg-estate'),
						"description" => esc_html__("Link URL for the button at the bottom of the block", 'yreg-estate'),
						"group" => esc_html__('Captions', 'yreg-estate'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "link_caption",
						"heading" => esc_html__("Button caption", 'yreg-estate'),
						"description" => esc_html__("Caption for the button at the bottom of the block", 'yreg-estate'),
						"group" => esc_html__('Captions', 'yreg-estate'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "readmore",
						"heading" => esc_html__("Read more text", 'yreg-estate'),
						"description" => esc_html__("Text of the 'Read more' link", 'yreg-estate'),
						"group" => esc_html__('Captions', 'yreg-estate'),
						"class" => "",
						"value" => esc_html__('Read more', 'yreg-estate'),
						"type" => "textfield"
					),
					array(
						"param_name" => "cat",
						"heading" => esc_html__("Categories", 'yreg-estate'),
						"description" => esc_html__("Select category to show donations. If empty - select donations from any category (group) or from IDs list", 'yreg-estate'),
						"group" => esc_html__('Query', 'yreg-estate'),
						"class" => "",
						"value" => array_flip(yreg_estate_array_merge(array(0 => esc_html__('- Select category -', 'yreg-estate')), $donations_groups)),
						"type" => "dropdown"
					),
					array(
						"param_name" => "columns",
						"heading" => esc_html__("Columns", 'yreg-estate'),
						"description" => esc_html__("How many columns use to show donations", 'yreg-estate'),
						"group" => esc_html__('Query', 'yreg-estate'),
						"admin_label" => true,
						"class" => "",
						"value" => "3",
						"type" => "textfield"
					),
					array(
						"param_name" => "count",
						"heading" => esc_html__("Number of posts", 'yreg-estate'),
						"description" => esc_html__("How many posts will be displayed? If used IDs - this parameter ignored.", 'yreg-estate'),
						"group" => esc_html__('Query', 'yreg-estate'),
						"class" => "",
						"value" => "3",
						"type" => "textfield"
					),
					array(
						"param_name" => "offset",
						"heading" => esc_html__("Offset before select posts", 'yreg-estate'),
						"description" => esc_html__("Skip posts before select next part.", 'yreg-estate'),
						"group" => esc_html__('Query', 'yreg-estate'),
						"class" => "",
						"value" => "0",
						"type" => "textfield"
					),
					array(
						"param_name" => "orderby",
						"heading" => esc_html__("Post sorting", 'yreg-estate'),
						"description" => esc_html__("Select desired posts sorting method", 'yreg-estate'),
						"group" => esc_html__('Query', 'yreg-estate'),
						"class" => "",
						"value" => array_flip(yreg_estate_get_sc_param('sorting')),
						"type" => "dropdown"
					),
					array(
						"param_name" => "order",
						"heading" => esc_html__("Post order", 'yreg-estate'),
						"description" => esc_html__("Select desired posts order", 'yreg-estate'),
						"group" => esc_html__('Query', 'yreg-estate'),
						"class" => "",
						"value" => array_flip(yreg_estate_get_sc_param('ordering')),
						"type" => "dropdown"
					),
					array(
						"param_name" => "ids",
						"heading" => esc_html__("client's IDs list", 'yreg-estate'),
						"description" => esc_html__("Comma separated list of donation's ID. If set - parameters above (category, count, order, etc.)  are ignored!", 'yreg-estate'),
						"group" => esc_html__('Query', 'yreg-estate'),
						'dependency' => array(
							'element' => 'cats',
							'is_empty' => true
						),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),

					yreg_estate_get_vc_param('id'),
					yreg_estate_get_vc_param('class'),
					yreg_estate_get_vc_param('css'),
					yreg_estate_get_vc_param('margin_top'),
					yreg_estate_get_vc_param('margin_bottom')
				)
			) );
			
		class WPBakeryShortCode_Trx_Donations_List extends YREG_ESTATE_VC_ShortCodeSingle {}

	}
}

// Filter to add in the required plugins list
if ( !function_exists( 'yreg_estate_trx_donations_required_plugins' ) ) {
	//add_filter('yreg_estate_filter_required_plugins',	'yreg_estate_trx_donations_required_plugins');
	function yreg_estate_trx_donations_required_plugins($list=array()) {
		if (in_array('trx_donations', yreg_estate_storage_get('required_plugins'))) {
			$path = yreg_estate_get_file_dir('plugins/install/trx_donations.zip');
			if (file_exists($path)) {
				$list[] = array(
					'name' 		=> 'YREG_ESTATE Donations',
					'slug' 		=> 'trx_donations',
					'source'	=> $path,
					'required' 	=> false
					);
			}
		}
		return $list;
	}
}



// One-click import support
//------------------------------------------------------------------------

// Check in the required plugins
if ( !function_exists( 'yreg_estate_trx_donations_importer_required_plugins' ) ) {
	//add_filter( 'yreg_estate_filter_importer_required_plugins',	'yreg_estate_trx_donations_importer_required_plugins', 10, 2 );
	function yreg_estate_trx_donations_importer_required_plugins($not_installed='', $list='') {
		//if (in_array('trx_donations', yreg_estate_storage_get('required_plugins')) && !yreg_estate_exists_trx_donations() )
		if (yreg_estate_strpos($list, 'trx_donations')!==false && !yreg_estate_exists_trx_donations() )
			$not_installed .= '<br>YREG_ESTATE Donations';
		return $not_installed;
	}
}

// Set options for one-click importer
if ( !function_exists( 'yreg_estate_trx_donations_importer_set_options' ) ) {
	//add_filter( 'yreg_estate_filter_importer_options',	'yreg_estate_trx_donations_importer_set_options' );
	function yreg_estate_trx_donations_importer_set_options($options=array()) {
		if ( in_array('trx_donations', yreg_estate_storage_get('required_plugins')) && yreg_estate_exists_trx_donations() ) {
			$options['additional_options'][] = 'trx_donations_options';		// Add slugs to export options for this plugin

		}
		return $options;
	}
}
?>