<?php
/**
 * YREG_ESTATE Framework: Property support
 *
 * @package	yreg_estate
 * @since	yreg_estate 1.0
 */

// Theme init
if (!function_exists('yreg_estate_property_theme_setup')) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_property_theme_setup', 1 );
	function yreg_estate_property_theme_setup() {

		// Detect current page type, taxonomy and title (for custom post_types use priority < 10 to fire it handles early, than for standard post types)
		add_filter('yreg_estate_filter_get_blog_type',			'yreg_estate_property_get_blog_type', 9, 2);
		add_filter('yreg_estate_filter_get_blog_title',		'yreg_estate_property_get_blog_title', 9, 2);
		add_filter('yreg_estate_filter_get_current_taxonomy',	'yreg_estate_property_get_current_taxonomy', 9, 2);
		add_filter('yreg_estate_filter_is_taxonomy',			'yreg_estate_property_is_taxonomy', 9, 2);
		add_filter('yreg_estate_filter_get_stream_page_title',	'yreg_estate_property_get_stream_page_title', 9, 2);
		add_filter('yreg_estate_filter_get_stream_page_link',	'yreg_estate_property_get_stream_page_link', 9, 2);
		add_filter('yreg_estate_filter_get_stream_page_id',	'yreg_estate_property_get_stream_page_id', 9, 2);
		add_filter('yreg_estate_filter_query_add_filters',		'yreg_estate_property_query_add_filters', 9, 2);
		add_filter('yreg_estate_filter_detect_inheritance_key','yreg_estate_property_detect_inheritance_key', 9, 1);
		add_filter('yreg_estate_filter_post_save_custom_options', 'yreg_estate_property_post_save_custom_options', 10, 3);

		// Extra column for property lists
		if (yreg_estate_get_theme_option('show_overriden_posts')=='yes') {
			add_filter('manage_edit-property_columns',			'yreg_estate_post_add_options_column', 9);
			add_filter('manage_property_posts_custom_column',	'yreg_estate_post_fill_options_column', 9, 2);
		}

		// Registar shortcodes [trx_property] and [trx_property_item] in the shortcodes list
		add_action('yreg_estate_action_shortcodes_list',		'yreg_estate_property_reg_shortcodes');
		if (function_exists('yreg_estate_exists_visual_composer') && yreg_estate_exists_visual_composer())
			add_action('yreg_estate_action_shortcodes_list_vc','yreg_estate_property_reg_shortcodes_vc');
		
		// Add supported data types
		yreg_estate_theme_support_pt('property');
		yreg_estate_theme_support_tx('property_group');
	}
}

if ( !function_exists( 'yreg_estate_property_settings_theme_setup2' ) ) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_property_settings_theme_setup2', 3 );
	function yreg_estate_property_settings_theme_setup2() {
		// Add post type 'property' and taxonomy 'property_group' into theme inheritance list
		yreg_estate_add_theme_inheritance( array('property' => array(
			'stream_template' => 'blog-property',
			'single_template' => 'single-property',
			'taxonomy' => array('property_group'),
			'taxonomy_tags' => array(),
			'post_type' => array('property'),
			'override' => 'page'
			) )
		);
	}
}



// Return property options list in array
if ( !function_exists( 'yreg_estate_get_property_list' ) ) {
	function yreg_estate_get_property_list($property_options) {
		$options = yreg_estate_get_custom_option($property_options);
		$list = array(); $_list = array();
		if (!empty($options)) {
			$_list = explode(',',$options);
			foreach ($_list as $value) {
				$list[$value] = $value;
			}
		}
		return $list;
	}
}

// Return property options list in array
if ( !function_exists( 'yreg_estate_property_list_save_custom_fields' ) ) {
	function yreg_estate_property_list_save_custom_fields($post_id, $options, $text) {
		$list1 = yreg_estate_get_property_list($text);
		$list2 = array();
		if (!empty($options)) {
			$list2 = explode(',', $options);
			sort($list2);
		}
		$t = 'yreg_estate_'.$text;
		/*
		$meta = get_post_meta($post_id, $t);
		foreach ( $list1 as $v) {
			if (in_array($v, $meta) && !in_array($v, $list2))
				delete_post_meta($post_id, $t, $v);
		}
		foreach ( $list2 as $v ) {
			if (in_array($v, $list1) && !in_array($v, $meta))
				add_post_meta($post_id, $t, $v);
		}
		*/
		$v = '|';
		foreach ( $list2 as $val ) {
			if (in_array($val, $list1))
				$v .= $val . '|';
		}
		update_post_meta($post_id, $t, $v);
	}
}



if (!function_exists('yreg_estate_property_after_theme_setup')) {
	add_action( 'yreg_estate_action_after_init_theme', 'yreg_estate_property_after_theme_setup' );
	function yreg_estate_property_after_theme_setup() {
		// Update fields in the meta box
		if (yreg_estate_storage_get_array('post_meta_box', 'page')=='property') {
			// Meta box fields
			yreg_estate_storage_set_array('post_meta_box', 'title', esc_html__('Property Options', 'yreg-estate'));
			yreg_estate_storage_set_array('post_meta_box', 'fields', array(
				"mb_partition_property" => array(
					"title" => esc_html__('Property details', 'yreg-estate'),
					"override" => "page,post",
					"divider" => false,
					"icon" => "iconadmin-home",
					"type" => "partition"),
				"mb_info_property_1" => array(
					"title" => esc_html__('Property details', 'yreg-estate'),
					"override" => "page,post",
					"divider" => false,
					"desc" => wp_kses_data( __('In this section you can put details for this property', 'yreg-estate') ),
					"class" => "property_meta",
					"type" => "info"),
				
				"property_id_web" => array(
					"title" => esc_html__('Property ID',  'yreg-estate'),
					"desc" => wp_kses_data( __("Add unique propert ID", 'yreg-estate') ),
					"override" => "page,post",
					"std" => '',
					"type" => "text"),
				
				"property_status" => array(
					"title" => esc_html__('Property status',  'yreg-estate'),
					"desc" => wp_kses_data( __("Select property status. Default: sale.", 'yreg-estate') ),
					"override" => "page,post",
					"std" => "sale",
					"options" => array(
						"sale" => esc_html__("Sale", 'yreg-estate'), 
						"rent" => esc_html__("Rent", 'yreg-estate')
					),
					"type" => "checklist"),
				
				"property_price_per" => array( 
					"title" => esc_html__('Rent period',  'yreg-estate'),
					"desc" => wp_kses_data( __('Select rent period here. Default: month.',  'yreg-estate') ),
					"override" => "post,page",
					"dependency" => array(
						'property_status' => array('rent')
					),
					"std" => "month",
					"options" => yreg_estate_get_property_list('property_price_per_list'),
					"type" => "select"),
				
				"property_price" => array(
					"title" => esc_html__('Property price',  'yreg-estate'),
					"desc" => wp_kses_data( __("Set your property price here (no currency sign required)", 'yreg-estate') ),
					"override" => "page,post",
					"std" => '0',
					"type" => "text"),
				
				"property_price_sign" => array( 
					"title" => esc_html__('Currecny',  'yreg-estate'),
					"desc" => wp_kses_data( __('Select your currency. Default: $.',  'yreg-estate') ),
					"override" => "post,page",
					"std" => "$",
					"options" => yreg_estate_get_property_list('property_price_sign_list'),
					"type" => "checklist"),
				
				"property_location" => array( 
					"title" => esc_html__('Property location',  'yreg-estate'),
					"desc" => wp_kses_data( __('Select property location.',  'yreg-estate') ),
					"override" => "post,page",
					"std" => "default",
					"options" => yreg_estate_get_property_list('property_location_list'),
					"type" => "select"),
				
				"property_address_1" => array(
					"title" => esc_html__('Property address 1',  'yreg-estate'),
					"desc" => wp_kses_data( __("Street and house number. Example: 96 Shore Rd APT 4A", 'yreg-estate') ),
					"override" => "page,post",
					"std" => '0',
					"type" => "text"),
				
				"property_address_2" => array(
					"title" => esc_html__('Property address 2',  'yreg-estate'),
					"desc" => wp_kses_data( __("Example: Brooklyn, NY 11209", 'yreg-estate') ),
					"override" => "page,post",
					"std" => '0',
					"type" => "text"),
				
				"property_type" => array( 
					"title" => esc_html__('Property type',  'yreg-estate'),
					"desc" => wp_kses_data( __('Select property type.',  'yreg-estate') ),
					"override" => "post,page",
					"std" => "",
					"options" => yreg_estate_get_property_list('property_type_list'),
					"type" => "select"),
				
				"property_style" => array( 
					"title" => esc_html__('Property style',  'yreg-estate'),
					"desc" => wp_kses_data( __('Select property style.',  'yreg-estate') ),
					"override" => "post,page",
					"std" => "",
					"options" => yreg_estate_get_property_list('property_style_list'),
					"multiple" => true,
					"style" => "list",
					"type" => "select"),
				
				"property_amenities" => array( 
					"title" => esc_html__('Property amenities',  'yreg-estate'),
					"desc" => wp_kses_data( __('Select property amenities.',  'yreg-estate') ),
					"override" => "post,page",
					"std" => "",
					"options" => yreg_estate_get_property_list('property_amenities_list'),
					"multiple" => true,
					"style" => "list",
					"type" => "select"),
				
				"property_options" => array( 
					"title" => esc_html__('Property options',  'yreg-estate'),
					"desc" => wp_kses_data( __('Select property options.',  'yreg-estate') ),
					"override" => "post,page",
					"std" => "",
					"options" => yreg_estate_get_property_list('property_options_list'),
					"multiple" => true,
					"style" => "list",
					"type" => "select"),
				
				"property_area" => array(
					"title" => esc_html__('Property area',  'yreg-estate'),
					"desc" => wp_kses_data( __("Property area (Sq Ft)", 'yreg-estate') ),
					"override" => "page,post",
					"class" => "property_area",
					"std" => '0',
					"type" => "text"),
				
				"property_rooms" => array(
					"title" => esc_html__("How many rooms?", 'yreg-estate'),
					"desc" => wp_kses_data( __("Set rooms number", 'yreg-estate') ),
					"override" => "page,post",
					"std" => 0,
					"min" => 0,
					"max" => 50,
					"step" => 1,
					"type" => "spinner"),
				
				"property_bedrooms" => array(
					"title" => esc_html__("How many bedrooms?", 'yreg-estate'),
					"desc" => wp_kses_data( __("Set bedrooms number", 'yreg-estate') ),
					"override" => "page,post",
					"std" => 0,
					"min" => 0,
					"max" => 50,
					"step" => 1,
					"type" => "spinner"),
				
				"property_bathrooms" => array(
					"title" => esc_html__("How many bathrooms?", 'yreg-estate'),
					"desc" => wp_kses_data( __("Set bathrooms number", 'yreg-estate') ),
					"override" => "page,post",
					"std" => 0,
					"min" => 0,
					"max" => 50,
					"step" => 1,
					"type" => "spinner"),
				
				"property_garages" => array(
					"title" => esc_html__("How many garages?", 'yreg-estate'),
					"desc" => wp_kses_data( __("Set garages number", 'yreg-estate') ),
					"override" => "page,post",
					"std" => 0,
					"min" => 0,
					"max" => 50,
					"step" => 1,
					"type" => "spinner"),
				
				"property_build" => array(
					"title" => esc_html__("Build year", 'yreg-estate'),
					"desc" => wp_kses_data( __("Set build year", 'yreg-estate') ),
					"override" => "page,post",
					"std" => 0,
					"min" => 0,
					"max" => 3000,
					"step" => 1,
					"type" => "spinner"),
				
				)
			);
		}
	}
}


// Return true, if current page is property page
if ( !function_exists( 'yreg_estate_is_property_page' ) ) {
	function yreg_estate_is_property_page() {
		$is = in_array(yreg_estate_storage_get('page_template'), array('blog-property', 'single-property'));
		if (!$is) {
			if (!yreg_estate_storage_empty('pre_query'))
				$is = yreg_estate_storage_call_obj_method('pre_query', 'get', 'post_type')=='property'
						|| yreg_estate_storage_call_obj_method('pre_query', 'is_tax', 'property_group') 
						|| (yreg_estate_storage_call_obj_method('pre_query', 'is_page') 
							&& ($id=yreg_estate_get_template_page_id('blog-property')) > 0 
							&& $id==yreg_estate_storage_get_obj_property('pre_query', 'queried_object_id', 0)
							);
			else
				$is = get_query_var('post_type')=='property' 
						|| is_tax('property_group') 
						|| (is_page() && ($id=yreg_estate_get_template_page_id('blog-property')) > 0 && $id==get_the_ID());
		}
		return $is;
	}
}

// Filter to detect current page inheritance key
if ( !function_exists( 'yreg_estate_property_detect_inheritance_key' ) ) {
	//add_filter('yreg_estate_filter_detect_inheritance_key',	'yreg_estate_property_detect_inheritance_key', 9, 1);
	function yreg_estate_property_detect_inheritance_key($key) {
		if (!empty($key)) return $key;
		return yreg_estate_is_property_page() ? 'property' : '';
	}
}

// Filter to detect current page slug
if ( !function_exists( 'yreg_estate_property_get_blog_type' ) ) {
	//add_filter('yreg_estate_filter_get_blog_type',	'yreg_estate_property_get_blog_type', 9, 2);
	function yreg_estate_property_get_blog_type($page, $query=null) {
		if (!empty($page)) return $page;
		if ($query && $query->is_tax('property_group') || is_tax('property_group'))
			$page = 'property_category';
		else if ($query && $query->get('post_type')=='property' || get_query_var('post_type')=='property')
			$page = $query && $query->is_single() || is_single() ? 'property_item' : 'property';
		return $page;
	}
}

// Filter to detect current page title
if ( !function_exists( 'yreg_estate_property_get_blog_title' ) ) {
	//add_filter('yreg_estate_filter_get_blog_title',	'yreg_estate_property_get_blog_title', 9, 2);
	function yreg_estate_property_get_blog_title($title, $page) {
		if (!empty($title)) return $title;
		if ( yreg_estate_strpos($page, 'property')!==false ) {
			if ( $page == 'property_category' ) {
				$term = get_term_by( 'slug', get_query_var( 'property_group' ), 'property_group', OBJECT);
				$title = $term->name;
			} else if ( $page == 'property_item' ) {
				$title = yreg_estate_get_post_title();
			} else {
				$title = esc_html__('All property', 'yreg-estate');
			}
		}
		return $title;
	}
}

// Filter to detect stream page title
if ( !function_exists( 'yreg_estate_property_get_stream_page_title' ) ) {
	//add_filter('yreg_estate_filter_get_stream_page_title',	'yreg_estate_property_get_stream_page_title', 9, 2);
	function yreg_estate_property_get_stream_page_title($title, $page) {
		if (!empty($title)) return $title;
		if (yreg_estate_strpos($page, 'property')!==false) {
			if (($page_id = yreg_estate_property_get_stream_page_id(0, $page=='property' ? 'blog-property' : $page)) > 0)
				$title = yreg_estate_get_post_title($page_id);
			else
				$title = esc_html__('All property', 'yreg-estate');				
		}
		return $title;
	}
}

// Filter to detect stream page ID
if ( !function_exists( 'yreg_estate_property_get_stream_page_id' ) ) {
	//add_filter('yreg_estate_filter_get_stream_page_id',	'yreg_estate_property_get_stream_page_id', 9, 2);
	function yreg_estate_property_get_stream_page_id($id, $page) {
		if (!empty($id)) return $id;
		if (yreg_estate_strpos($page, 'property')!==false) $id = yreg_estate_get_template_page_id('blog-property');
		return $id;
	}
}

// Filter to detect stream page URL
if ( !function_exists( 'yreg_estate_property_get_stream_page_link' ) ) {
	//add_filter('yreg_estate_filter_get_stream_page_link',	'yreg_estate_property_get_stream_page_link', 9, 2);
	function yreg_estate_property_get_stream_page_link($url, $page) {
		if (!empty($url)) return $url;
		if (yreg_estate_strpos($page, 'property')!==false) {
			$id = yreg_estate_get_template_page_id('blog-property');
			if ($id) $url = get_permalink($id);
		}
		return $url;
	}
}

// Filter to detect current taxonomy
if ( !function_exists( 'yreg_estate_property_get_current_taxonomy' ) ) {
	//add_filter('yreg_estate_filter_get_current_taxonomy',	'yreg_estate_property_get_current_taxonomy', 9, 2);
	function yreg_estate_property_get_current_taxonomy($tax, $page) {
		if (!empty($tax)) return $tax;
		if ( yreg_estate_strpos($page, 'property')!==false ) {
			$tax = 'property_group';
		}
		return $tax;
	}
}

// Return taxonomy name (slug) if current page is this taxonomy page
if ( !function_exists( 'yreg_estate_property_is_taxonomy' ) ) {
	//add_filter('yreg_estate_filter_is_taxonomy',	'yreg_estate_property_is_taxonomy', 9, 2);
	function yreg_estate_property_is_taxonomy($tax, $query=null) {
		if (!empty($tax))
			return $tax;
		else 
			return $query && $query->get('property_group')!='' || is_tax('property_group') ? 'property_group' : '';
	}
}

// Add custom post type and/or taxonomies arguments to the query
if ( !function_exists( 'yreg_estate_property_query_add_filters' ) ) {
	//add_filter('yreg_estate_filter_query_add_filters',	'yreg_estate_property_query_add_filters', 9, 2);
	function yreg_estate_property_query_add_filters($args, $filter) {
		if ($filter == 'property') {
			$args['post_type'] = 'property';
		}
		$j = array();
		$a = yreg_estate_storage_get('blog_filters_property');
		if ( !empty($a) ) {

			if ( isset($a['keyword']) ) {
				$args['s'] = $a['keyword'];
			}
			
			
			if ( isset($a['status']) ) {
				$j[] = 'STATUS='.$a['status'];
			}		
//			if ( isset($a['status']) ) {
//				$args['meta_query'][] = array (
//					'key' => 'yreg_estate_property_status',
//					'value' => $a['status'],
//					'compare' => '='
//				);
//			}
			
			if ( isset($a['location']) ) {
				$j[] = 'LOCATION='.$a['location'];
			}
//			if ( isset($a['location']) ) {
//				$args['meta_query'][] = array (
//					'key' => 'yreg_estate_property_location',
//					'value' => $a['location'],
//					'compare' => '='
//				);
//			}
			
			if ( isset($a['type']) ) {
				$j[] = 'TYPE='.$a['type'];
			}
//			if ( isset($a['type']) ) {
//				$args['meta_query'][] = array (
//					'key' => 'yreg_estate_property_type',
//					'value' => $a['type'],
//					'compare' => '='
//				);
//			}
			
			if ( isset($a['style']) ) {
				$j[] = 'STYLE='.$a['style'];
			}
//			if ( isset($a['style']) ) {
//				$args['meta_query'][] = array (
//					'key' => 'yreg_estate_property_style_list',
//					'value' => '%'.$a['style'].'%',
//					'compare' => 'LIKE'
//				);
//			}
			
			
			if ( isset($a['rooms']) ) {
				$j[] = 'PR'.$a['rooms'];
			}
			if ( isset($a['bedrooms']) ) {
				$j[] = 'BE'.$a['bedrooms'];
			}
			if ( isset($a['bathrooms']) ) {
				$j[] = 'BA'.$a['bathrooms'];
			}
			if ( isset($a['garages']) ) {
				$j[] = 'CP'.$a['garages'];
			}
//			if ( isset($a['rooms']) ) {
//				$args['meta_query'][] = array (
//					'key' => 'yreg_estate_property_rooms',
//					'value' => $a['rooms'],
//					'compare' => '>=',
//					'type' => 'NUMERIC'
//				);
//			}
//			
//			if ( isset($a['bedrooms']) ) {
//				$args['meta_query'][] = array (
//					'key' => 'yreg_estate_property_bedrooms',
//					'value' => $a['bedrooms'],
//					'compare' => '>=',
//					'type' => 'NUMERIC'
//				);
//			}
//			
//			if ( isset($a['bathrooms']) ) {
//				$args['meta_query'][] = array (
//					'key' => 'yreg_estate_property_bathrooms',
//					'value' => $a['bathrooms'],
//					'compare' => '>=',
//					'type' => 'NUMERIC'
//				);
//			}
//			
//			if ( isset($a['garages']) ) {
//				$args['meta_query'][] = array (
//					'key' => 'yreg_estate_property_garages',
//					'value' => $a['garages'],
//					'compare' => '>=',
//					'type' => 'NUMERIC'
//				);
//			}
			
			
			if ( isset($a['area_min']) and isset($a['area_max']) ) {
				$args['meta_query'][] = array (
					'key' => 'yreg_estate_property_area',
                                        'value' => array( $a['area_min'], $a['area_max'] ),
					'compare' => 'BETWEEN',
					'type' => 'NUMERIC'
				);
			} elseif ( isset($a['area_min']) ) {
				$args['meta_query'][] = array (
					'key' => 'yreg_estate_property_area',
					'value' => $a['area_min'],
					'compare' => '>=',
					'type' => 'NUMERIC'
				);
			} elseif ( isset($a['area_max']) ) {
				$args['meta_query'][] = array (
					'key' => 'yreg_estate_property_area',
					'value' => $a['area_max'],
					'compare' => '<=',
					'type' => 'NUMERIC'
				);
			}
			
			if ( isset($a['price_min']) and isset($a['price_max']) ) {
				$args['meta_query'][] = array (
					'key' => 'yreg_estate_property_price',
					'value' => array( $a['price_min'], $a['price_max'] ),
					'compare' => 'BETWEEN',
					'type' => 'NUMERIC'
				);
			} elseif ( isset($a['price_min']) ) {
				$args['meta_query'][] = array (
					'key' => 'yreg_estate_property_price',
					'value' => $a['price_min'],
					'compare' => '>=',
					'type' => 'NUMERIC'
				);
			} elseif ( isset($a['price_max']) ) {
				$args['meta_query'][] = array (
					'key' => 'yreg_estate_property_price',
					'value' => $a['price_max'],
					'compare' => '<=',
					'type' => 'NUMERIC'
				);
			}

			
			if ( isset($a['amenities']) ) {
				$arr = array_keys($a['amenities']);
				sort($arr);
				$v = join('%', $arr);
				$args['meta_query'][] = array (
					'key' => 'yreg_estate_property_amenities_list',
					'value' => $v,
					'compare' => 'LIKE'
				);
			}

			if ( isset($a['options']) ) {
				$arr = array_keys($a['options']);
				sort($arr);
				$v = join('%', $arr);
				$args['meta_query'][] = array (
					'key' => 'yreg_estate_property_options_list',
					'value' => $v,
					'compare' => 'LIKE'
				);
			}
			
			if (!empty($j)) {
				$v = join('%', $j);
				$args['meta_query'][] = array (
					'key' => 'yreg_estate_property_sco',
					'value' => $v,
					'compare' => 'LIKE'
				);
			}
			
		}
		return $args;
	}
}


// Before save custom options - calc and save average rating
if (!function_exists('yreg_estate_property_post_save_custom_options')) {
	//add_filter('yreg_estate_filter_post_save_custom_options',	'yreg_estate_property_post_save_custom_options', 10, 3);
	function yreg_estate_property_post_save_custom_options($custom_options, $post_type, $post_id) {
		
		
		$s_c_o = '|';
		// Save custom options
		// ###################
		
		// property_id_web
		if ( isset($custom_options['property_id_web']) ) {
			$temp = htmlspecialchars(trim($custom_options['property_id_web']));
			if ( $temp == 'inherit' ) { $temp = '';	}
			update_post_meta($post_id, 'yreg_estate_property_id_web', $temp);
		}
		
		// property_status
		if ( isset($custom_options['property_status']) ) {
			$temp = htmlspecialchars(trim($custom_options['property_status']));
			if ( $temp == 'inherit' ) { $temp = 'sale';	}
			update_post_meta($post_id, 'yreg_estate_property_status', $temp);
			$s_c_o .= 'STATUS='.$temp.'|';
		}
		
		// property_price_per
		if ( isset($custom_options['property_price_per']) ) {
			$temp = htmlspecialchars(trim($custom_options['property_price_per']));
			if ( $temp == 'inherit' ) { $temp = 'month';	}
			update_post_meta($post_id, 'yreg_estate_property_price_per', $temp);
		}
		
		// property_price
		if ( isset($custom_options['property_price']) ) {
			$temp = (int) str_replace(" ", "", htmlspecialchars(trim($custom_options['property_price'])));
			update_post_meta($post_id, 'yreg_estate_property_price', $temp);
		}
		
		// property_price_sign
		if ( isset($custom_options['property_price_sign']) ) {
			$temp = htmlspecialchars(trim($custom_options['property_price_sign']));
			if ( $temp == 'inherit' ) { $temp = '$';	}
			update_post_meta($post_id, 'yreg_estate_property_price_sign', $temp);
		}
		
		// property_location
		if ( isset($custom_options['property_location']) ) {
			$temp = htmlspecialchars(trim($custom_options['property_location']));
			if ( $temp == 'inherit' ) { $temp = '';	}
			update_post_meta($post_id, 'yreg_estate_property_location', $temp);
			$s_c_o .= 'LOCATION='.$temp.'|';
		}
		
		// property_address_1
		if ( isset($custom_options['property_address_1']) ) {
			$temp = htmlspecialchars(trim($custom_options['property_address_1']));
			if ( $temp == 'inherit' ) { $temp = '';	}
			update_post_meta($post_id, 'yreg_estate_property_address_1', $temp);
		}
		
		// property_address_2
		if ( isset($custom_options['property_address_2']) ) {
			$temp = htmlspecialchars(trim($custom_options['property_address_2']));
			if ( $temp == 'inherit' ) { $temp = '';	}
			update_post_meta($post_id, 'yreg_estate_property_address_2', $temp);
		}
		
		// property_type
		if ( isset($custom_options['property_type']) ) {
			$temp = htmlspecialchars(trim($custom_options['property_type']));
			if ( $temp == 'inherit' ) { $temp = '';	}
			update_post_meta($post_id, 'yreg_estate_property_type', $temp);
			$s_c_o .= 'TYPE='.$temp.'|';
		}
		
		// property_style
		if ( isset($custom_options['property_style']) ) {
			$temp = htmlspecialchars(trim($custom_options['property_style']));
			yreg_estate_property_list_save_custom_fields($post_id, $temp, 'property_style_list');
			
			$list1 = yreg_estate_get_property_list('property_style_list');
			$list2 = array();
			if (!empty($temp)) {
				$list2 = explode(',', $temp);
				sort($list2);
			}
			foreach ( $list2 as $val ) {
				if (in_array($val, $list1))
					$s_c_o .= 'STYLE='.$val . '|';
			}
		}
		
		// property_amenities
		if ( isset($custom_options['property_amenities']) ) {
			$temp = htmlspecialchars(trim($custom_options['property_amenities']));
			yreg_estate_property_list_save_custom_fields($post_id, $temp, 'property_amenities_list');
		}
		
		// property_options
		if ( isset($custom_options['property_options']) ) {
			$temp = htmlspecialchars(trim($custom_options['property_options']));
			yreg_estate_property_list_save_custom_fields($post_id, $temp, 'property_options_list');
		}
		
		// property_area
		if ( isset($custom_options['property_area']) ) {
			$temp = (int) str_replace(" ", "", htmlspecialchars(trim($custom_options['property_area'])));
			update_post_meta($post_id, 'yreg_estate_property_area', $temp);
		}
		
		// property_rooms
		if ( isset($custom_options['property_rooms']) ) {
			$temp = (int) str_replace(" ", "", htmlspecialchars(trim($custom_options['property_rooms'])));
			update_post_meta($post_id, 'yreg_estate_property_rooms', $temp);
			for ( $i=1; $i<=$temp; $i++ ) {
				$s_c_o .= 'PR'.$i.'|';
			}
		}
		
		// property_bedrooms
		if ( isset($custom_options['property_bedrooms']) ) {
			$temp = (int) str_replace(" ", "", htmlspecialchars(trim($custom_options['property_bedrooms'])));
			update_post_meta($post_id, 'yreg_estate_property_bedrooms', $temp);
			for ( $i=1; $i<=$temp; $i++ ) {
				$s_c_o .= 'BE'.$i.'|';
			}
		}
		
		// property_bathrooms
		if ( isset($custom_options['property_bathrooms']) ) {
			$temp = (int) str_replace(" ", "", htmlspecialchars(trim($custom_options['property_bathrooms'])));
			update_post_meta($post_id, 'yreg_estate_property_bathrooms', $temp);
			for ( $i=1; $i<=$temp; $i++ ) {
				$s_c_o .= 'BA'.$i.'|';
			}
		}
		
		// property_garages
		if ( isset($custom_options['property_garages']) ) {
			$temp = (int) str_replace(" ", "", htmlspecialchars(trim($custom_options['property_garages'])));
			update_post_meta($post_id, 'yreg_estate_property_garages', $temp);
			for ( $i=1; $i<=$temp; $i++ ) {
				$s_c_o .= 'CP'.$i.'|';
			}
		}
		
		// property_build
		if ( isset($custom_options['property_build']) ) {
			$temp = (int) str_replace(" ", "", htmlspecialchars(trim($custom_options['property_build'])));
			update_post_meta($post_id, 'yreg_estate_property_build', $temp);
		}
		
		update_post_meta($post_id, 'yreg_estate_property_sco', $s_c_o);
		return $custom_options;
	}
}


// Return property status text
if ( !function_exists( 'yreg_estate_property_status_text' ) ) {
	function yreg_estate_property_status_text($text) {
		if ( !empty($text) ) {
			if ( $text == 'sale' ) {
				$text = esc_html__('sale', 'yreg-estate');
			} elseif ( $text == 'rent' ) {
				$text = esc_html__('rent', 'yreg-estate');
			}
			
		} else { $text = ''; }
		return $text;
	}
}


// Return property price text
if ( !function_exists( 'yreg_estate_property_delimiter_text' ) ) {
	function yreg_estate_property_delimiter_text($text, $separator) {
		if ( empty($separator) ) { $separator = ','; }
		$text_int = (int) $text;
		$text = number_format($text_int, 0, ',', $separator);
		return $text;
	}
}



// ---------------------------------- [trx_property] ---------------------------------------

/*
[trx_property id="unique_id" columns="3" style="property-1|property-2|..."]
	[trx_property_item name="property name" position="director" image="url"]Description text[/trx_property_item]
	...
[/trx_property]
*/
yreg_estate_storage_set('sc_property_busy', false);
if ( !function_exists( 'yreg_estate_sc_property' ) ) {
	function yreg_estate_sc_property($atts, $content=null){	
		if (yreg_estate_in_shortcode_property(true)) return '';
		extract(yreg_estate_html_decode(shortcode_atts(array(
			// Individual params
			"style" => "property-1",
			"columns" => 3,
			"slider" => "no",
			"slides_space" => 0,
			"controls" => "no",
			"interval" => "",
			"autoheight" => "no",
			"custom" => "no",
			"ids" => "",
			"status" => "none",
			"cat" => "",
			"count" => 3,
			"offset" => "",
			"orderby" => "date",
			"order" => "desc",
			"title" => "",
			"subtitle" => "",
			"description" => "",
			"link_caption" => esc_html__('Learn more', 'yreg-estate'),
			"link" => '',
			"scheme" => '',
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"width" => "",
			"height" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		
		yreg_estate_storage_set('sc_ps', '1');

		if (empty($id)) $id = "sc_property_".str_replace('.', '', mt_rand());
		if (empty($width)) $width = "100%";
		if (!empty($height) && yreg_estate_param_is_on($autoheight)) $autoheight = "no";
		if (empty($interval)) $interval = mt_rand(5000, 10000);

		$class .= ($class ? ' ' : '') . yreg_estate_get_css_position_as_classes($top, $right, $bottom, $left);

		$ws = yreg_estate_get_css_dimensions_from_values($width);
		$hs = yreg_estate_get_css_dimensions_from_values('', $height);
		$css .= ($hs) . ($ws);

		if (yreg_estate_param_is_on($slider)) yreg_estate_enqueue_slider('swiper');
	
		$columns = max(1, min(12, $columns));
		$count = max(1, (int) $count);
		if (yreg_estate_param_is_off($custom) && $count < $columns) $columns = $count;
		yreg_estate_storage_set('sc_property_data', array(
			'id'=>$id,
            'style'=>$style,
            'counter'=>0,
            'columns'=>$columns,
            'slider'=>$slider,
            'css_wh'=>$ws . $hs
            )
        );

		$output = '<div' . ($id ? ' id="'.esc_attr($id).'_wrap"' : '') 
						. ' class="sc_property_wrap'
						. ($scheme && !yreg_estate_param_is_off($scheme) && !yreg_estate_param_is_inherit($scheme) ? ' scheme_'.esc_attr($scheme) : '') 
						.'">'
					. '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
						. ' class="sc_property sc_property_style_'.esc_attr($style)
							. ' ' . esc_attr(yreg_estate_get_template_property($style, 'container_classes'))
							. ' ' . esc_attr(yreg_estate_get_slider_controls_classes($controls))
							. (!empty($class) ? ' '.esc_attr($class) : '')
							. (yreg_estate_param_is_on($slider)
								? ' sc_slider_swiper swiper-slider-container'
									. (yreg_estate_param_is_on($autoheight) ? ' sc_slider_height_auto' : '')
									. ($hs ? ' sc_slider_height_fixed' : '')
								: '')
						.'"'
						. (!empty($width) && yreg_estate_strpos($width, '%')===false ? ' data-old-width="' . esc_attr($width) . '"' : '')
						. (!empty($height) && yreg_estate_strpos($height, '%')===false ? ' data-old-height="' . esc_attr($height) . '"' : '')
						. ((int) $interval > 0 ? ' data-interval="'.esc_attr($interval).'"' : '')
						. ($columns > 1 ? ' data-slides-per-view="' . esc_attr($columns) . '"' : '')
						. ($slides_space > 0 ? ' data-slides-space="' . esc_attr($slides_space) . '"' : '')
						. ($style!='property-1' ? ' data-slides-min-width="250"' : '')
						. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
						. (!yreg_estate_param_is_off($animation) ? ' data-animation="'.esc_attr(yreg_estate_get_animation_classes($animation)).'"' : '')
					. '>'
					. (!empty($subtitle) ? '<h6 class="sc_property_subtitle sc_item_subtitle">' . trim(yreg_estate_strmacros($subtitle)) . '</h6>' : '')
					. (!empty($title) ? '<h2 class="sc_property_title sc_item_title">' . trim(yreg_estate_strmacros($title)) . '</h2>' : '')
					. (!empty($description) ? '<div class="sc_property_descr sc_item_descr">' . trim(yreg_estate_strmacros($description)) . '</div>' : '')
					. (yreg_estate_param_is_on($slider) 
						? '<div class="slides swiper-wrapper">' 
						: ($columns > 1 
							? '<div class="sc_columns columns_wrap">' 
							: '')
						);
	
		$content = do_shortcode($content);
		yreg_estate_storage_set('sc_property_busy', true);
	
		if (yreg_estate_param_is_on($custom) && $content) {
			$output .= $content;
		} else {
			global $post;
	
			if (!empty($ids)) {
				$posts = explode(',', $ids);
				$count = count($posts);
			}
			
			$args = array(
				'post_type' => 'property',
				'post_status' => 'publish',
				'posts_per_page' => $count,
				'ignore_sticky_posts' => true,
				'order' => $order=='asc' ? 'asc' : 'desc',
			);
		
			if ($offset > 0 && empty($ids)) {
				$args['offset'] = $offset;
			}
			
			$args = yreg_estate_query_add_sort_order($args, $orderby, $order);
			$args = yreg_estate_query_add_posts_and_cats($args, $ids, 'property', $cat, 'property_group');
			
			
			if ( $status != 'none' ) {
				$bfp = array();
				$bfp['status'] = $status;
				yreg_estate_storage_set('blog_filters_property', $bfp);
				$args = yreg_estate_query_add_filters($args, yreg_estate_storage_get('blog_filters_property'));
			}
			
			$query = new WP_Query( $args );
	
			$post_number = 0;

			while ( $query->have_posts() ) { 
				$query->the_post();
				$post_number++;
				$args = array(
					'layout' => $style,
					'show' => false,
					'number' => $post_number,
					'posts_on_page' => ($count > 0 ? $count : $query->found_posts),
					"descr" => yreg_estate_get_custom_option('post_excerpt_maxlength'.($columns > 1 ? '_masonry' : '')),
					"orderby" => $orderby,
					'content' => false,
					'terms_list' => false,
					'columns_count' => $columns,
					'slider' => $slider,
					'tag_id' => $id ? $id . '_' . $post_number : '',
					'tag_class' => '',
					'tag_animation' => '',
					'tag_css' => '',
					'tag_css_wh' => $ws . $hs
				);
				$post_data = yreg_estate_get_post_data($args);
				$post_meta = get_post_meta($post_data['post_id'], 'yreg_estate_post_options', true);
				$thumb_sizes = yreg_estate_get_thumb_sizes(array('layout' => $style));
//				$args['property_name'] = $post_meta['property_name'];
//				$args['property_position'] = $post_meta['property_position'];
				$args['property_image'] = $post_data['post_thumb'];
				$args['property_link'] = yreg_estate_param_is_on('property_show_link')
					? (!empty($post_meta['property_link']) ? $post_meta['property_link'] : $post_data['post_link'])
					: '';
				$output .= yreg_estate_show_post_layout($args, $post_data);
			}
			wp_reset_postdata();
		}
	
		if (yreg_estate_param_is_on($slider)) {
			$output .= '</div>'
				. '<div class="sc_slider_controls_wrap"><a class="sc_slider_prev" href="#"></a><a class="sc_slider_next" href="#"></a></div>'
				. '<div class="sc_slider_pagination_wrap"></div>';
		} else if ($columns > 1) {
			$output .= '</div>';
		}

		$output .= (!empty($link) ? '<div class="sc_property_button sc_item_button">'.yreg_estate_do_shortcode('[trx_button link="'.esc_url($link).'" icon="icon-right"]'.esc_html($link_caption).'[/trx_button]').'</div>' : '')
				. '</div><!-- /.sc_property -->'
			. '</div><!-- /.sc_property_wrap -->';
	
		// Add template specific scripts and styles
		do_action('yreg_estate_action_blog_scripts', $style);
		
		yreg_estate_storage_set('sc_property_busy', false);
		return apply_filters('yreg_estate_shortcode_output', $output, 'trx_property', $atts, $content);
	}
	yreg_estate_require_shortcode('trx_property', 'yreg_estate_sc_property');
}


if ( !function_exists( 'yreg_estate_sc_property_item' ) ) {
	function yreg_estate_sc_property_item($atts, $content=null) {
		if (yreg_estate_in_shortcode_blogger()) return '';
		extract(yreg_estate_html_decode(shortcode_atts( array(
			// Individual params
			"name" => "",
			"position" => "",
			"image" => "",
			"link" => "",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => ""
		), $atts)));
	
		yreg_estate_storage_inc_array('sc_property_data', 'counter');
	
		$id = $id ? $id : (yreg_estate_storage_get_array('sc_property_data', 'id') ? yreg_estate_storage_get_array('sc_property_data', 'id') . '_' . yreg_estate_storage_get_array('sc_property_data', 'counter') : '');
	
		$descr = trim(chop(do_shortcode($content)));
	
		$thumb_sizes = yreg_estate_get_thumb_sizes(array('layout' => yreg_estate_storage_get_array('sc_property_data', 'style')));

		if ($image > 0) {
			$attach = wp_get_attachment_image_src( $image, 'full' );
			if (isset($attach[0]) && $attach[0]!='')
				$image = $attach[0];
		}
		$image = yreg_estate_get_resized_image_tag($image, $thumb_sizes['w'], $thumb_sizes['h']);

		$post_data = array(
			'post_title' => $name,
			'post_excerpt' => $descr
		);
		$args = array(
			'layout' => yreg_estate_storage_get_array('sc_property_data', 'style'),
			'number' => yreg_estate_storage_get_array('sc_property_data', 'counter'),
			'columns_count' => yreg_estate_storage_get_array('sc_property_data', 'columns'),
			'slider' => yreg_estate_storage_get_array('sc_property_data', 'slider'),
			'show' => false,
			'descr'  => 0,
			'tag_id' => $id,
			'tag_class' => $class,
			'tag_animation' => $animation,
			'tag_css' => $css,
			'tag_css_wh' => yreg_estate_storage_get_array('sc_property_data', 'css_wh'),
			'property_position' => $position,
			'property_link' => $link,
			'property_image' => $image
		);
		$output = yreg_estate_show_post_layout($args, $post_data);
		return apply_filters('yreg_estate_shortcode_output', $output, 'trx_property_item', $atts, $content);
	}
	yreg_estate_require_shortcode('trx_property_item', 'yreg_estate_sc_property_item');
}
// ---------------------------------- [/trx_property] ---------------------------------------



// Add [trx_property] and [trx_property_item] in the shortcodes list
if (!function_exists('yreg_estate_property_reg_shortcodes')) {
	//add_filter('yreg_estate_action_shortcodes_list',	'yreg_estate_property_reg_shortcodes');
	function yreg_estate_property_reg_shortcodes() {
		if (yreg_estate_storage_isset('shortcodes')) {

			$users = yreg_estate_get_list_users();
			$members = yreg_estate_get_list_posts(false, array(
				'post_type'=>'property',
				'orderby'=>'title',
				'order'=>'asc',
				'return'=>'title'
				)
			);
			$property_groups = yreg_estate_get_list_terms(false, 'property_group');
			$property_styles = yreg_estate_get_list_templates('property');
			$controls 		= yreg_estate_get_list_slider_controls();

			yreg_estate_sc_map_after('trx_chat', array(

				// Property
				"trx_property" => array(
					"title" => esc_html__("Property", 'yreg-estate'),
					"desc" => wp_kses_data( __("Insert property list in your page (post)", 'yreg-estate') ),
					"decorate" => true,
					"container" => false,
					"params" => array(
						"style" => array(
							"title" => esc_html__("Property style", 'yreg-estate'),
							"desc" => wp_kses_data( __("Select style to display property list", 'yreg-estate') ),
							"value" => "property-1",
							"type" => "select",
							"options" => $property_styles
						),
						"columns" => array(
							"title" => esc_html__("Columns", 'yreg-estate'),
							"desc" => wp_kses_data( __("How many columns use to show property", 'yreg-estate') ),
							"value" => 3,
							"min" => 1,
							"max" => 6,
							"step" => 1,
							"type" => "spinner"
						),
						"slider" => array(
							"title" => esc_html__("Slider", 'yreg-estate'),
							"desc" => wp_kses_data( __("Use slider to show property", 'yreg-estate') ),
							"value" => "no",
							"type" => "switch",
							"options" => yreg_estate_get_sc_param('yes_no')
						),
						"controls" => array(
							"title" => esc_html__("Controls", 'yreg-estate'),
							"desc" => wp_kses_data( __("Slider controls style and position", 'yreg-estate') ),
							"dependency" => array(
								'slider' => array('yes')
							),
							"divider" => true,
							"value" => "no",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $controls
						),
						"slides_space" => array(
							"title" => esc_html__("Space between slides", 'yreg-estate'),
							"desc" => wp_kses_data( __("Size of space (in px) between slides", 'yreg-estate') ),
							"dependency" => array(
								'slider' => array('yes')
							),
							"value" => 0,
							"min" => 0,
							"max" => 100,
							"step" => 10,
							"type" => "spinner"
						),
						"interval" => array(
							"title" => esc_html__("Slides change interval", 'yreg-estate'),
							"desc" => wp_kses_data( __("Slides change interval (in milliseconds: 1000ms = 1s)", 'yreg-estate') ),
							"dependency" => array(
								'slider' => array('yes')
							),
							"value" => 7000,
							"step" => 500,
							"min" => 0,
							"type" => "spinner"
						),
						"autoheight" => array(
							"title" => esc_html__("Autoheight", 'yreg-estate'),
							"desc" => wp_kses_data( __("Change whole slider's height (make it equal current slide's height)", 'yreg-estate') ),
							"dependency" => array(
								'slider' => array('yes')
							),
							"value" => "no",
							"type" => "switch",
							"options" => yreg_estate_get_sc_param('yes_no')
						),
						"custom" => array(
							"title" => esc_html__("Custom", 'yreg-estate'),
							"desc" => wp_kses_data( __("Allow get team members from inner shortcodes (custom) or get it from specified group (cat)", 'yreg-estate') ),
							"divider" => true,
							"value" => "no",
							"type" => "switch",
							"options" => yreg_estate_get_sc_param('yes_no')
						),
						"status" => array(
							"title" => esc_html__("Property status", 'yreg-estate'),
							"desc" => wp_kses_data( __("Select property status", 'yreg-estate') ),
							"dependency" => array(
								'custom' => array('no')
							),
							"divider" => true,
							"value" => "none",
							"type" => "select",
							"style" => "list",
							"options" => array(
								'none' => esc_html__('All property', 'yreg-estate'),
								'sale' => esc_html__('Sale', 'yreg-estate'),
								'rent' => esc_html__('Rent', 'yreg-estate')
							)
						),
						"cat" => array(
							"title" => esc_html__("Categories", 'yreg-estate'),
							"desc" => wp_kses_data( __("Select categories (groups) to show team members. If empty - select team members from any category (group) or from IDs list", 'yreg-estate') ),
							"dependency" => array(
								'custom' => array('no')
							),
							"divider" => true,
							"value" => "",
							"type" => "select",
							"style" => "list",
							"multiple" => true,
							"options" => yreg_estate_array_merge(array(0 => esc_html__('- Select category -', 'yreg-estate')), $property_groups)
						),
						"count" => array(
							"title" => esc_html__("Number of posts", 'yreg-estate'),
							"desc" => wp_kses_data( __("How many posts will be displayed? If used IDs - this parameter ignored.", 'yreg-estate') ),
							"dependency" => array(
								'custom' => array('no')
							),
							"value" => 3,
							"min" => 1,
							"max" => 100,
							"type" => "spinner"
						),
						"offset" => array(
							"title" => esc_html__("Offset before select posts", 'yreg-estate'),
							"desc" => wp_kses_data( __("Skip posts before select next part.", 'yreg-estate') ),
							"dependency" => array(
								'custom' => array('no')
							),
							"value" => 0,
							"min" => 0,
							"type" => "spinner"
						),
						"orderby" => array(
							"title" => esc_html__("Post order by", 'yreg-estate'),
							"desc" => wp_kses_data( __("Select desired posts sorting method", 'yreg-estate') ),
							"dependency" => array(
								'custom' => array('no')
							),
							"value" => "title",
							"type" => "select",
							"options" => yreg_estate_get_sc_param('sorting')
						),
						"order" => array(
							"title" => esc_html__("Post order", 'yreg-estate'),
							"desc" => wp_kses_data( __("Select desired posts order", 'yreg-estate') ),
							"dependency" => array(
								'custom' => array('no')
							),
							"value" => "asc",
							"type" => "switch",
							"size" => "big",
							"options" => yreg_estate_get_sc_param('ordering')
						),
						"ids" => array(
							"title" => esc_html__("Post IDs list", 'yreg-estate'),
							"desc" => wp_kses_data( __("Comma separated list of posts ID. If set - parameters above are ignored!", 'yreg-estate') ),
							"dependency" => array(
								'custom' => array('no')
							),
							"value" => "",
							"type" => "text"
						),
						"width" => yreg_estate_shortcodes_width(),
						"height" => yreg_estate_shortcodes_height(),
						"top" => yreg_estate_get_sc_param('top'),
						"bottom" => yreg_estate_get_sc_param('bottom'),
						"left" => yreg_estate_get_sc_param('left'),
						"right" => yreg_estate_get_sc_param('right'),
						"id" => yreg_estate_get_sc_param('id'),
						"class" => yreg_estate_get_sc_param('class'),
						"animation" => yreg_estate_get_sc_param('animation'),
						"css" => yreg_estate_get_sc_param('css')
					),
					
				)

			));
		}
	}
}


// Add [trx_property] and [trx_property_item] in the VC shortcodes list
if (!function_exists('yreg_estate_property_reg_shortcodes_vc')) {
	//add_filter('yreg_estate_action_shortcodes_list_vc',	'yreg_estate_property_reg_shortcodes_vc');
	function yreg_estate_property_reg_shortcodes_vc() {

		$property_groups = yreg_estate_get_list_terms(false, 'property_group');
		$property_styles = yreg_estate_get_list_templates('property');
		$controls		= yreg_estate_get_list_slider_controls();

		// Property
		vc_map( array(
				"base" => "trx_property",
				"name" => esc_html__("Property", 'yreg-estate'),
				"description" => wp_kses_data( __("Insert property list", 'yreg-estate') ),
				"category" => esc_html__('Content', 'yreg-estate'),
				'icon' => 'icon_trx_property',
				"class" => "trx_sc_columns trx_sc_property",
				"content_element" => true,
				"is_container" => true,
				"show_settings_on_create" => true,
				"as_parent" => array('only' => 'trx_property_item'),
				"params" => array(
					array(
						"param_name" => "style",
						"heading" => esc_html__("Property style", 'yreg-estate'),
						"description" => wp_kses_data( __("Select style to display property list", 'yreg-estate') ),
						"class" => "",
						"admin_label" => true,
						"value" => array_flip($property_styles),
						"type" => "dropdown"
					),
					array(
						"param_name" => "slider",
						"heading" => esc_html__("Slider", 'yreg-estate'),
						"description" => wp_kses_data( __("Use slider to show testimonials", 'yreg-estate') ),
						"admin_label" => true,
						"group" => esc_html__('Slider', 'yreg-estate'),
						"class" => "",
						"std" => "no",
						"value" => array_flip(yreg_estate_get_sc_param('yes_no')),
						"type" => "dropdown"
					),
					array(
						"param_name" => "controls",
						"heading" => esc_html__("Controls", 'yreg-estate'),
						"description" => wp_kses_data( __("Slider controls style and position", 'yreg-estate') ),
						"admin_label" => true,
						"group" => esc_html__('Slider', 'yreg-estate'),
						'dependency' => array(
							'element' => 'slider',
							'value' => 'yes'
						),
						"class" => "",
						"std" => "no",
						"value" => array_flip($controls),
						"type" => "dropdown"
					),
					array(
						"param_name" => "slides_space",
						"heading" => esc_html__("Space between slides", 'yreg-estate'),
						"description" => wp_kses_data( __("Size of space (in px) between slides", 'yreg-estate') ),
						"admin_label" => true,
						"group" => esc_html__('Slider', 'yreg-estate'),
						'dependency' => array(
							'element' => 'slider',
							'value' => 'yes'
						),
						"class" => "",
						"value" => "0",
						"type" => "textfield"
					),
					array(
						"param_name" => "interval",
						"heading" => esc_html__("Slides change interval", 'yreg-estate'),
						"description" => wp_kses_data( __("Slides change interval (in milliseconds: 1000ms = 1s)", 'yreg-estate') ),
						"group" => esc_html__('Slider', 'yreg-estate'),
						'dependency' => array(
							'element' => 'slider',
							'value' => 'yes'
						),
						"class" => "",
						"value" => "7000",
						"type" => "textfield"
					),
					array(
						"param_name" => "autoheight",
						"heading" => esc_html__("Autoheight", 'yreg-estate'),
						"description" => wp_kses_data( __("Change whole slider's height (make it equal current slide's height)", 'yreg-estate') ),
						"group" => esc_html__('Slider', 'yreg-estate'),
						'dependency' => array(
							'element' => 'slider',
							'value' => 'yes'
						),
						"class" => "",
						"value" => array("Autoheight" => "yes" ),
						"type" => "checkbox"
					),
					array(
						"param_name" => "custom",
						"heading" => esc_html__("Custom", 'yreg-estate'),
						"description" => wp_kses_data( __("Allow get property from inner shortcodes (custom) or get it from specified group (cat)", 'yreg-estate') ),
						"class" => "",
						"value" => array("Custom property" => "yes" ),
						"type" => "checkbox"
					),
					array(
						"param_name" => "status",
						"heading" => esc_html__("Property status", 'yreg-estate'),
						"description" => wp_kses_data( __("Select property status", 'yreg-estate') ),
						"group" => esc_html__('Query', 'yreg-estate'),
						"admin_label" => true,
						"std" => "none",
						"value" => array(
							esc_html__('All property', 'yreg-estate') => 'none',
							esc_html__('Sale', 'yreg-estate') => 'sale',
							esc_html__('Rent', 'yreg-estate') => 'rent'
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "cat",
						"heading" => esc_html__("Categories", 'yreg-estate'),
						"description" => wp_kses_data( __("Select category to show property. If empty - select property from any category (group) or from IDs list", 'yreg-estate') ),
						"group" => esc_html__('Query', 'yreg-estate'),
						'dependency' => array(
							'element' => 'custom',
							'is_empty' => true
						),
						"class" => "",
						"value" => array_flip(yreg_estate_array_merge(array(0 => esc_html__('- Select category -', 'yreg-estate')), $property_groups)),
						"type" => "dropdown"
					),
					array(
						"param_name" => "columns",
						"heading" => esc_html__("Columns", 'yreg-estate'),
						"description" => wp_kses_data( __("How many columns use to show property", 'yreg-estate') ),
						"group" => esc_html__('Query', 'yreg-estate'),
						"admin_label" => true,
						"class" => "",
						"value" => "3",
						"type" => "textfield"
					),
					array(
						"param_name" => "count",
						"heading" => esc_html__("Number of posts", 'yreg-estate'),
						"description" => wp_kses_data( __("How many posts will be displayed? If used IDs - this parameter ignored.", 'yreg-estate') ),
						"group" => esc_html__('Query', 'yreg-estate'),
						'dependency' => array(
							'element' => 'custom',
							'is_empty' => true
						),
						"class" => "",
						"value" => "3",
						"type" => "textfield"
					),
					array(
						"param_name" => "offset",
						"heading" => esc_html__("Offset before select posts", 'yreg-estate'),
						"description" => wp_kses_data( __("Skip posts before select next part.", 'yreg-estate') ),
						"group" => esc_html__('Query', 'yreg-estate'),
						'dependency' => array(
							'element' => 'custom',
							'is_empty' => true
						),
						"class" => "",
						"value" => "0",
						"type" => "textfield"
					),
					array(
						"param_name" => "orderby",
						"heading" => esc_html__("Post sorting", 'yreg-estate'),
						"description" => wp_kses_data( __("Select desired posts sorting method", 'yreg-estate') ),
						"group" => esc_html__('Query', 'yreg-estate'),
						'dependency' => array(
							'element' => 'custom',
							'is_empty' => true
						),
						"class" => "",
						"value" => array_flip(yreg_estate_get_sc_param('sorting')),
						"type" => "dropdown"
					),
					array(
						"param_name" => "order",
						"heading" => esc_html__("Post order", 'yreg-estate'),
						"description" => wp_kses_data( __("Select desired posts order", 'yreg-estate') ),
						"group" => esc_html__('Query', 'yreg-estate'),
						'dependency' => array(
							'element' => 'custom',
							'is_empty' => true
						),
						"class" => "",
						"value" => array_flip(yreg_estate_get_sc_param('ordering')),
						"type" => "dropdown"
					),
					array(
						"param_name" => "ids",
						"heading" => esc_html__("property's IDs list", 'yreg-estate'),
						"description" => wp_kses_data( __("Comma separated list of property's ID. If set - parameters above (category, count, order, etc.)  are ignored!", 'yreg-estate') ),
						"group" => esc_html__('Query', 'yreg-estate'),
						'dependency' => array(
							'element' => 'custom',
							'is_empty' => true
						),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
//					array(
//						"param_name" => "link",
//						"heading" => esc_html__("Button URL", 'yreg-estate'),
//						"description" => wp_kses_data( __("Link URL for the button at the bottom of the block", 'yreg-estate') ),
//						"group" => esc_html__('Captions', 'yreg-estate'),
//						"class" => "",
//						"value" => "",
//						"type" => "textfield"
//					),
//					array(
//						"param_name" => "link_caption",
//						"heading" => esc_html__("Button caption", 'yreg-estate'),
//						"description" => wp_kses_data( __("Caption for the button at the bottom of the block", 'yreg-estate') ),
//						"group" => esc_html__('Captions', 'yreg-estate'),
//						"class" => "",
//						"value" => "",
//						"type" => "textfield"
//					),
					yreg_estate_vc_width(),
					yreg_estate_vc_height(),
					yreg_estate_get_vc_param('margin_top'),
					yreg_estate_get_vc_param('margin_bottom'),
					yreg_estate_get_vc_param('margin_left'),
					yreg_estate_get_vc_param('margin_right'),
					yreg_estate_get_vc_param('id'),
					yreg_estate_get_vc_param('class'),
					yreg_estate_get_vc_param('animation'),
					yreg_estate_get_vc_param('css')
				),
				'js_view' => 'VcTrxColumnsView'
			) );
			
			
//		vc_map( array(
//				"base" => "trx_property_item",
//				"name" => esc_html__("Property", 'yreg-estate'),
//				"description" => wp_kses_data( __("Property - all data pull out from it account on your site", 'yreg-estate') ),
//				"show_settings_on_create" => true,
//				"class" => "trx_sc_collection trx_sc_column_item trx_sc_property_item",
//				"content_element" => true,
//				"is_container" => true,
//				'icon' => 'icon_trx_property_item',
//				"as_child" => array('only' => 'trx_property'),
//				"as_parent" => array('except' => 'trx_property'),
//				"params" => array(
//					array(
//						"param_name" => "name",
//						"heading" => esc_html__("Name", 'yreg-estate'),
//						"description" => wp_kses_data( __("Property's name", 'yreg-estate') ),
//						"admin_label" => true,
//						"class" => "",
//						"value" => "",
//						"type" => "textfield"
//					),
//					array(
//						"param_name" => "position",
//						"heading" => esc_html__("Position", 'yreg-estate'),
//						"description" => wp_kses_data( __("Property's position", 'yreg-estate') ),
//						"admin_label" => true,
//						"class" => "",
//						"value" => "",
//						"type" => "textfield"
//					),
//					array(
//						"param_name" => "link",
//						"heading" => esc_html__("Link", 'yreg-estate'),
//						"description" => wp_kses_data( __("Link on property's personal page", 'yreg-estate') ),
//						"class" => "",
//						"value" => "",
//						"type" => "textfield"
//					),
//					array(
//						"param_name" => "image",
//						"heading" => esc_html__("Property's image", 'yreg-estate'),
//						"description" => wp_kses_data( __("Property's image", 'yreg-estate') ),
//						"class" => "",
//						"value" => "",
//						"type" => "attach_image"
//					),
//					yreg_estate_get_vc_param('id'),
//					yreg_estate_get_vc_param('class'),
//					yreg_estate_get_vc_param('animation'),
//					yreg_estate_get_vc_param('css')
//				),
//				'js_view' => 'VcTrxColumnItemView'
//			) );
			
		class WPBakeryShortCode_Trx_Property extends YREG_ESTATE_VC_ShortCodeColumns {}
//		class WPBakeryShortCode_Trx_Property_Item extends YREG_ESTATE_VC_ShortCodeCollection {}

	}
}
?>