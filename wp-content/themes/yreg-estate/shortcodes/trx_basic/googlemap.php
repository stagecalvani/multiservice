<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('yreg_estate_sc_googlemap_theme_setup')) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_sc_googlemap_theme_setup' );
	function yreg_estate_sc_googlemap_theme_setup() {
		add_action('yreg_estate_action_shortcodes_list', 		'yreg_estate_sc_googlemap_reg_shortcodes');
		if (function_exists('yreg_estate_exists_visual_composer') && yreg_estate_exists_visual_composer())
			add_action('yreg_estate_action_shortcodes_list_vc','yreg_estate_sc_googlemap_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

//[trx_googlemap id="unique_id" width="width_in_pixels_or_percent" height="height_in_pixels"]
//	[trx_googlemap_marker address="your_address"]
//[/trx_googlemap]

if (!function_exists('yreg_estate_sc_googlemap')) {	
	function yreg_estate_sc_googlemap($atts, $content = null) {
		if (yreg_estate_in_shortcode_blogger()) return '';
		extract(yreg_estate_html_decode(shortcode_atts(array(
			// Individual params
			"zoom" => 18,
			"style" => 'default',
			"scheme" => "",
			// Common params
			"id" => "",
			"class" => "",
			"css" => "",
			"animation" => "",
			"width" => "100%",
			"height" => "400",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$class .= ($class ? ' ' : '') . yreg_estate_get_css_position_as_classes($top, $right, $bottom, $left);
		$css .= yreg_estate_get_css_dimensions_from_values($width, $height);
		if (empty($id)) $id = 'sc_googlemap_'.str_replace('.', '', mt_rand());
		if (empty($style)) $style = yreg_estate_get_custom_option('googlemap_style');
		yreg_estate_enqueue_script( 'googlemap', yreg_estate_get_protocol().'://maps.google.com/maps/api/js?sensor=false', array(), null, true );
		yreg_estate_enqueue_script( 'yreg_estate-googlemap-script', yreg_estate_get_file_url('js/core.googlemap.js'), array(), null, true );
		yreg_estate_storage_set('sc_googlemap_markers', array());
		$content = do_shortcode($content);
		$output = '';
		$markers = yreg_estate_storage_get('sc_googlemap_markers');
		if (count($markers) == 0) {
			$markers[] = array(
				'title' => yreg_estate_get_custom_option('googlemap_title'),
				'description' => yreg_estate_strmacros(yreg_estate_get_custom_option('googlemap_description')),
				'latlng' => yreg_estate_get_custom_option('googlemap_latlng'),
				'address' => yreg_estate_get_custom_option('googlemap_address'),
				'point' => yreg_estate_get_custom_option('googlemap_marker')
			);
		}
		$output .= 
			($content ? '<div id="'.esc_attr($id).'_wrap" class="sc_googlemap_wrap'
					. ($scheme && !yreg_estate_param_is_off($scheme) && !yreg_estate_param_is_inherit($scheme) ? ' scheme_'.esc_attr($scheme) : '') 
					. '">' : '')
			. '<div id="'.esc_attr($id).'"'
				. ' class="sc_googlemap'. (!empty($class) ? ' '.esc_attr($class) : '').'"'
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
				. (!yreg_estate_param_is_off($animation) ? ' data-animation="'.esc_attr(yreg_estate_get_animation_classes($animation)).'"' : '')
				. ' data-zoom="'.esc_attr($zoom).'"'
				. ' data-style="'.esc_attr($style).'"'
				. '>';
		$cnt = 0;
		foreach ($markers as $marker) {
			$cnt++;
			if (empty($marker['id'])) $marker['id'] = $id.'_'.intval($cnt);
			$output .= '<div id="'.esc_attr($marker['id']).'" class="sc_googlemap_marker"'
				. ' data-title="'.esc_attr($marker['title']).'"'
				. ' data-description="'.esc_attr(yreg_estate_strmacros($marker['description'])).'"'
				. ' data-address="'.esc_attr($marker['address']).'"'
				. ' data-latlng="'.esc_attr($marker['latlng']).'"'
				. ' data-point="'.esc_attr($marker['point']).'"'
				. '></div>';
		}
		$output .= '</div>'
			. ($content ? '<div class="sc_googlemap_content">' . trim($content) . '</div></div>' : '');
			
		return apply_filters('yreg_estate_shortcode_output', $output, 'trx_googlemap', $atts, $content);
	}
	yreg_estate_require_shortcode("trx_googlemap", "yreg_estate_sc_googlemap");
}


if (!function_exists('yreg_estate_sc_googlemap_marker')) {	
	function yreg_estate_sc_googlemap_marker($atts, $content = null) {
		if (yreg_estate_in_shortcode_blogger()) return '';
		extract(yreg_estate_html_decode(shortcode_atts(array(
			// Individual params
			"title" => "",
			"address" => "",
			"latlng" => "",
			"point" => "",
			// Common params
			"id" => ""
		), $atts)));
		if (!empty($point)) {
			if ($point > 0) {
				$attach = wp_get_attachment_image_src( $point, 'full' );
				if (isset($attach[0]) && $attach[0]!='')
					$point = $attach[0];
			}
		}
		yreg_estate_storage_set_array('sc_googlemap_markers', '', array(
			'id' => $id,
			'title' => $title,
			'description' => do_shortcode($content),
			'latlng' => $latlng,
			'address' => $address,
			'point' => $point ? $point : yreg_estate_get_custom_option('googlemap_marker')
			)
		);
		return '';
	}
	yreg_estate_require_shortcode("trx_googlemap_marker", "yreg_estate_sc_googlemap_marker");
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_googlemap_reg_shortcodes' ) ) {
	//add_action('yreg_estate_action_shortcodes_list', 'yreg_estate_sc_googlemap_reg_shortcodes');
	function yreg_estate_sc_googlemap_reg_shortcodes() {
	
		yreg_estate_sc_map("trx_googlemap", array(
			"title" => esc_html__("Google map", 'yreg-estate'),
			"desc" => wp_kses_data( __("Insert Google map with specified markers", 'yreg-estate') ),
			"decorate" => false,
			"container" => true,
			"params" => array(
				"zoom" => array(
					"title" => esc_html__("Zoom", 'yreg-estate'),
					"desc" => wp_kses_data( __("Map zoom factor", 'yreg-estate') ),
					"divider" => true,
					"value" => 18,
					"min" => 1,
					"max" => 20,
					"type" => "spinner"
				),
				"style" => array(
					"title" => esc_html__("Map style", 'yreg-estate'),
					"desc" => wp_kses_data( __("Select map style", 'yreg-estate') ),
					"value" => "default",
					"type" => "checklist",
					"options" => yreg_estate_get_sc_param('googlemap_styles')
				),
				"scheme" => array(
					"title" => esc_html__("Color scheme", 'yreg-estate'),
					"desc" => wp_kses_data( __("Select color scheme for this block", 'yreg-estate') ),
					"value" => "",
					"type" => "checklist",
					"options" => yreg_estate_get_sc_param('schemes')
				),
				"width" => yreg_estate_shortcodes_width('100%'),
				"height" => yreg_estate_shortcodes_height(240),
				"top" => yreg_estate_get_sc_param('top'),
				"bottom" => yreg_estate_get_sc_param('bottom'),
				"left" => yreg_estate_get_sc_param('left'),
				"right" => yreg_estate_get_sc_param('right'),
				"id" => yreg_estate_get_sc_param('id'),
				"class" => yreg_estate_get_sc_param('class'),
				"animation" => yreg_estate_get_sc_param('animation'),
				"css" => yreg_estate_get_sc_param('css')
			),
			"children" => array(
				"name" => "trx_googlemap_marker",
				"title" => esc_html__("Google map marker", 'yreg-estate'),
				"desc" => wp_kses_data( __("Google map marker", 'yreg-estate') ),
				"decorate" => false,
				"container" => true,
				"params" => array(
					"address" => array(
						"title" => esc_html__("Address", 'yreg-estate'),
						"desc" => wp_kses_data( __("Address of this marker", 'yreg-estate') ),
						"value" => "",
						"type" => "text"
					),
					"latlng" => array(
						"title" => esc_html__("Latitude and Longitude", 'yreg-estate'),
						"desc" => wp_kses_data( __("Comma separated marker's coorditanes (instead Address)", 'yreg-estate') ),
						"value" => "",
						"type" => "text"
					),
					"point" => array(
						"title" => esc_html__("URL for marker image file", 'yreg-estate'),
						"desc" => wp_kses_data( __("Select or upload image or write URL from other site for this marker. If empty - use default marker", 'yreg-estate') ),
						"readonly" => false,
						"value" => "",
						"type" => "media"
					),
					"title" => array(
						"title" => esc_html__("Title", 'yreg-estate'),
						"desc" => wp_kses_data( __("Title for this marker", 'yreg-estate') ),
						"value" => "",
						"type" => "text"
					),
					"_content_" => array(
						"title" => esc_html__("Description", 'yreg-estate'),
						"desc" => wp_kses_data( __("Description for this marker", 'yreg-estate') ),
						"rows" => 4,
						"value" => "",
						"type" => "textarea"
					),
					"id" => yreg_estate_get_sc_param('id')
				)
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_googlemap_reg_shortcodes_vc' ) ) {
	//add_action('yreg_estate_action_shortcodes_list_vc', 'yreg_estate_sc_googlemap_reg_shortcodes_vc');
	function yreg_estate_sc_googlemap_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_googlemap",
			"name" => esc_html__("Google map", 'yreg-estate'),
			"description" => wp_kses_data( __("Insert Google map with desired address or coordinates", 'yreg-estate') ),
			"category" => esc_html__('Content', 'yreg-estate'),
			'icon' => 'icon_trx_googlemap',
			"class" => "trx_sc_collection trx_sc_googlemap",
			"content_element" => true,
			"is_container" => true,
			"as_parent" => array('only' => 'trx_googlemap_marker,trx_form,trx_section,trx_block,trx_promo'),
			"show_settings_on_create" => true,
			"params" => array(
				array(
					"param_name" => "zoom",
					"heading" => esc_html__("Zoom", 'yreg-estate'),
					"description" => wp_kses_data( __("Map zoom factor", 'yreg-estate') ),
					"admin_label" => true,
					"class" => "",
					"value" => "18",
					"type" => "textfield"
				),
				array(
					"param_name" => "style",
					"heading" => esc_html__("Style", 'yreg-estate'),
					"description" => wp_kses_data( __("Map custom style", 'yreg-estate') ),
					"admin_label" => true,
					"class" => "",
					"value" => array_flip(yreg_estate_get_sc_param('googlemap_styles')),
					"type" => "dropdown"
				),
				array(
					"param_name" => "scheme",
					"heading" => esc_html__("Color scheme", 'yreg-estate'),
					"description" => wp_kses_data( __("Select color scheme for this block", 'yreg-estate') ),
					"class" => "",
					"value" => array_flip(yreg_estate_get_sc_param('schemes')),
					"type" => "dropdown"
				),
				yreg_estate_get_vc_param('id'),
				yreg_estate_get_vc_param('class'),
				yreg_estate_get_vc_param('animation'),
				yreg_estate_get_vc_param('css'),
				yreg_estate_vc_width('100%'),
				yreg_estate_vc_height(240),
				yreg_estate_get_vc_param('margin_top'),
				yreg_estate_get_vc_param('margin_bottom'),
				yreg_estate_get_vc_param('margin_left'),
				yreg_estate_get_vc_param('margin_right')
			)
		) );
		
		vc_map( array(
			"base" => "trx_googlemap_marker",
			"name" => esc_html__("Googlemap marker", 'yreg-estate'),
			"description" => wp_kses_data( __("Insert new marker into Google map", 'yreg-estate') ),
			"class" => "trx_sc_collection trx_sc_googlemap_marker",
			'icon' => 'icon_trx_googlemap_marker',
			//"allowed_container_element" => 'vc_row',
			"show_settings_on_create" => true,
			"content_element" => true,
			"is_container" => true,
			"as_child" => array('only' => 'trx_googlemap'), // Use only|except attributes to limit parent (separate multiple values with comma)
			"params" => array(
				array(
					"param_name" => "address",
					"heading" => esc_html__("Address", 'yreg-estate'),
					"description" => wp_kses_data( __("Address of this marker", 'yreg-estate') ),
					"admin_label" => true,
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "latlng",
					"heading" => esc_html__("Latitude and Longitude", 'yreg-estate'),
					"description" => wp_kses_data( __("Comma separated marker's coorditanes (instead Address)", 'yreg-estate') ),
					"admin_label" => true,
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "title",
					"heading" => esc_html__("Title", 'yreg-estate'),
					"description" => wp_kses_data( __("Title for this marker", 'yreg-estate') ),
					"admin_label" => true,
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "point",
					"heading" => esc_html__("URL for marker image file", 'yreg-estate'),
					"description" => wp_kses_data( __("Select or upload image or write URL from other site for this marker. If empty - use default marker", 'yreg-estate') ),
					"class" => "",
					"value" => "",
					"type" => "attach_image"
				),
				yreg_estate_get_vc_param('id')
			)
		) );
		
		class WPBakeryShortCode_Trx_Googlemap extends YREG_ESTATE_VC_ShortCodeCollection {}
		class WPBakeryShortCode_Trx_Googlemap_Marker extends YREG_ESTATE_VC_ShortCodeCollection {}
	}
}
?>