<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('yreg_estate_sc_hide_theme_setup')) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_sc_hide_theme_setup' );
	function yreg_estate_sc_hide_theme_setup() {
		add_action('yreg_estate_action_shortcodes_list', 		'yreg_estate_sc_hide_reg_shortcodes');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_hide selector="unique_id"]
*/

if (!function_exists('yreg_estate_sc_hide')) {	
	function yreg_estate_sc_hide($atts, $content=null){	
		if (yreg_estate_in_shortcode_blogger()) return '';
		extract(yreg_estate_html_decode(shortcode_atts(array(
			// Individual params
			"selector" => "",
			"hide" => "on",
			"delay" => 0
		), $atts)));
		$selector = trim(chop($selector));
		$output = $selector == '' ? '' : 
			'<script type="text/javascript">
				jQuery(document).ready(function() {
					'.($delay>0 ? 'setTimeout(function() {' : '').'
					jQuery("'.esc_attr($selector).'").' . ($hide=='on' ? 'hide' : 'show') . '();
					'.($delay>0 ? '},'.($delay).');' : '').'
				});
			</script>';
		return apply_filters('yreg_estate_shortcode_output', $output, 'trx_hide', $atts, $content);
	}
	yreg_estate_require_shortcode('trx_hide', 'yreg_estate_sc_hide');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_hide_reg_shortcodes' ) ) {
	//add_action('yreg_estate_action_shortcodes_list', 'yreg_estate_sc_hide_reg_shortcodes');
	function yreg_estate_sc_hide_reg_shortcodes() {
	
		yreg_estate_sc_map("trx_hide", array(
			"title" => esc_html__("Hide/Show any block", 'yreg-estate'),
			"desc" => wp_kses_data( __("Hide or Show any block with desired CSS-selector", 'yreg-estate') ),
			"decorate" => false,
			"container" => false,
			"params" => array(
				"selector" => array(
					"title" => esc_html__("Selector", 'yreg-estate'),
					"desc" => wp_kses_data( __("Any block's CSS-selector", 'yreg-estate') ),
					"value" => "",
					"type" => "text"
				),
				"hide" => array(
					"title" => esc_html__("Hide or Show", 'yreg-estate'),
					"desc" => wp_kses_data( __("New state for the block: hide or show", 'yreg-estate') ),
					"value" => "yes",
					"size" => "small",
					"options" => yreg_estate_get_sc_param('yes_no'),
					"type" => "switch"
				)
			)
		));
	}
}
?>