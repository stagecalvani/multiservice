<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('yreg_estate_sc_price_block_theme_setup')) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_sc_price_block_theme_setup' );
	function yreg_estate_sc_price_block_theme_setup() {
		add_action('yreg_estate_action_shortcodes_list', 		'yreg_estate_sc_price_block_reg_shortcodes');
		if (function_exists('yreg_estate_exists_visual_composer') && yreg_estate_exists_visual_composer())
			add_action('yreg_estate_action_shortcodes_list_vc','yreg_estate_sc_price_block_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

if (!function_exists('yreg_estate_sc_price_block')) {	
	function yreg_estate_sc_price_block($atts, $content=null){	
		if (yreg_estate_in_shortcode_blogger()) return '';
		extract(yreg_estate_html_decode(shortcode_atts(array(
			// Individual params
			"style" => 1,
			"title" => "",
			"link" => "",
			"link_text" => "",
			"icon" => "",
			"money" => "",
			"currency" => "$",
			"period" => "",
			"align" => "",
			"scheme" => "",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"width" => "",
			"height" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$output = '';
		$class .= ($class ? ' ' : '') . yreg_estate_get_css_position_as_classes($top, $right, $bottom, $left);
		$css .= yreg_estate_get_css_dimensions_from_values($width, $height);
		if ($money) $money = do_shortcode('[trx_price money="'.esc_attr($money).'" period="'.esc_attr($period).'"'.($currency ? ' currency="'.esc_attr($currency).'"' : '').']');
		$content = do_shortcode(yreg_estate_sc_clear_around($content));
		$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
					. ' class="sc_price_block sc_price_block_style_'.max(1, min(3, $style))
						. (!empty($class) ? ' '.esc_attr($class) : '')
						. ($scheme && !yreg_estate_param_is_off($scheme) && !yreg_estate_param_is_inherit($scheme) ? ' scheme_'.esc_attr($scheme) : '') 
						. ($align && $align!='none' ? ' align'.esc_attr($align) : '') 
						. '"'
					. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
					. (!yreg_estate_param_is_off($animation) ? ' data-animation="'.esc_attr(yreg_estate_get_animation_classes($animation)).'"' : '')
					. '>'
				. (!empty($title) ? '<div class="sc_price_block_title"><span>'.($title).'</span></div>' : '')
				. '<div class="sc_price_block_money">'
					. (!empty($icon) ? '<div class="sc_price_block_icon '.esc_attr($icon).'"></div>' : '')
					. ($money)
				. '</div>'
				. (!empty($content) ? '<div class="sc_price_block_description">'.($content).'</div>' : '')
				. (!empty($link_text) ? '<div class="sc_price_block_link">'.do_shortcode('[trx_button style="style3" link="'.($link ? esc_url($link) : '#').'"]'.($link_text).'[/trx_button]').'</div>' : '')
			. '</div>';
		return apply_filters('yreg_estate_shortcode_output', $output, 'trx_price_block', $atts, $content);
	}
	yreg_estate_require_shortcode('trx_price_block', 'yreg_estate_sc_price_block');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_price_block_reg_shortcodes' ) ) {
	//add_action('yreg_estate_action_shortcodes_list', 'yreg_estate_sc_price_block_reg_shortcodes');
	function yreg_estate_sc_price_block_reg_shortcodes() {
	
		yreg_estate_sc_map("trx_price_block", array(
			"title" => esc_html__("Price block", 'yreg-estate'),
			"desc" => wp_kses_data( __("Insert price block with title, price and description", 'yreg-estate') ),
			"decorate" => false,
			"container" => true,
			"params" => array(
				"style" => array(
					"title" => esc_html__("Block style", 'yreg-estate'),
					"desc" => wp_kses_data( __("Select style for this price block", 'yreg-estate') ),
					"value" => 1,
					"options" => yreg_estate_get_list_styles(1, 3),
					"type" => "checklist"
				),
				"title" => array(
					"title" => esc_html__("Title", 'yreg-estate'),
					"desc" => wp_kses_data( __("Block title", 'yreg-estate') ),
					"value" => "",
					"type" => "text"
				),
				"link" => array(
					"title" => esc_html__("Link URL", 'yreg-estate'),
					"desc" => wp_kses_data( __("URL for link from button (at bottom of the block)", 'yreg-estate') ),
					"value" => "",
					"type" => "text"
				),
				"link_text" => array(
					"title" => esc_html__("Link text", 'yreg-estate'),
					"desc" => wp_kses_data( __("Text (caption) for the link button (at bottom of the block). If empty - button not showed", 'yreg-estate') ),
					"value" => "",
					"type" => "text"
				),
				"icon" => array(
					"title" => esc_html__("Icon",  'yreg-estate'),
					"desc" => wp_kses_data( __('Select icon from Fontello icons set (placed before/instead price)',  'yreg-estate') ),
					"value" => "",
					"type" => "icons",
					"options" => yreg_estate_get_sc_param('icons')
				),
				"money" => array(
					"title" => esc_html__("Money", 'yreg-estate'),
					"desc" => wp_kses_data( __("Money value (dot or comma separated)", 'yreg-estate') ),
					"divider" => true,
					"value" => "",
					"type" => "text"
				),
				"currency" => array(
					"title" => esc_html__("Currency", 'yreg-estate'),
					"desc" => wp_kses_data( __("Currency character", 'yreg-estate') ),
					"value" => "$",
					"type" => "text"
				),
				"period" => array(
					"title" => esc_html__("Period", 'yreg-estate'),
					"desc" => wp_kses_data( __("Period text (if need). For example: monthly, daily, etc.", 'yreg-estate') ),
					"value" => "",
					"type" => "text"
				),
				"scheme" => array(
					"title" => esc_html__("Color scheme", 'yreg-estate'),
					"desc" => wp_kses_data( __("Select color scheme for this block", 'yreg-estate') ),
					"value" => "",
					"type" => "checklist",
					"options" => yreg_estate_get_sc_param('schemes')
				),
				"align" => array(
					"title" => esc_html__("Alignment", 'yreg-estate'),
					"desc" => wp_kses_data( __("Align price to left or right side", 'yreg-estate') ),
					"divider" => true,
					"value" => "",
					"type" => "checklist",
					"dir" => "horizontal",
					"options" => yreg_estate_get_sc_param('float')
				), 
				"_content_" => array(
					"title" => esc_html__("Description", 'yreg-estate'),
					"desc" => wp_kses_data( __("Description for this price block", 'yreg-estate') ),
					"divider" => true,
					"rows" => 4,
					"value" => "",
					"type" => "textarea"
				),
				"width" => yreg_estate_shortcodes_width(),
				"height" => yreg_estate_shortcodes_height(),
				"top" => yreg_estate_get_sc_param('top'),
				"bottom" => yreg_estate_get_sc_param('bottom'),
				"left" => yreg_estate_get_sc_param('left'),
				"right" => yreg_estate_get_sc_param('right'),
				"id" => yreg_estate_get_sc_param('id'),
				"class" => yreg_estate_get_sc_param('class'),
				"animation" => yreg_estate_get_sc_param('animation'),
				"css" => yreg_estate_get_sc_param('css')
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_price_block_reg_shortcodes_vc' ) ) {
	//add_action('yreg_estate_action_shortcodes_list_vc', 'yreg_estate_sc_price_block_reg_shortcodes_vc');
	function yreg_estate_sc_price_block_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_price_block",
			"name" => esc_html__("Price block", 'yreg-estate'),
			"description" => wp_kses_data( __("Insert price block with title, price and description", 'yreg-estate') ),
			"category" => esc_html__('Content', 'yreg-estate'),
			'icon' => 'icon_trx_price_block',
			"class" => "trx_sc_single trx_sc_price_block",
			"content_element" => true,
			"is_container" => false,
			"show_settings_on_create" => true,
			"params" => array(
				array(
					"param_name" => "style",
					"heading" => esc_html__("Block style", 'yreg-estate'),
					"desc" => wp_kses_data( __("Select style of this price block", 'yreg-estate') ),
					"admin_label" => true,
					"class" => "",
					"std" => 1,
					"value" => array_flip(yreg_estate_get_list_styles(1, 3)),
					"type" => "dropdown"
				),
				array(
					"param_name" => "title",
					"heading" => esc_html__("Title", 'yreg-estate'),
					"description" => wp_kses_data( __("Block title", 'yreg-estate') ),
					"admin_label" => true,
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "link",
					"heading" => esc_html__("Link URL", 'yreg-estate'),
					"description" => wp_kses_data( __("URL for link from button (at bottom of the block)", 'yreg-estate') ),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "link_text",
					"heading" => esc_html__("Link text", 'yreg-estate'),
					"description" => wp_kses_data( __("Text (caption) for the link button (at bottom of the block). If empty - button not showed", 'yreg-estate') ),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "icon",
					"heading" => esc_html__("Icon", 'yreg-estate'),
					"description" => wp_kses_data( __("Select icon from Fontello icons set (placed before/instead price)", 'yreg-estate') ),
					"class" => "",
					"value" => yreg_estate_get_sc_param('icons'),
					"type" => "dropdown"
				),
				array(
					"param_name" => "money",
					"heading" => esc_html__("Money", 'yreg-estate'),
					"description" => wp_kses_data( __("Money value (dot or comma separated)", 'yreg-estate') ),
					"admin_label" => true,
					"group" => esc_html__('Money', 'yreg-estate'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "currency",
					"heading" => esc_html__("Currency symbol", 'yreg-estate'),
					"description" => wp_kses_data( __("Currency character", 'yreg-estate') ),
					"admin_label" => true,
					"group" => esc_html__('Money', 'yreg-estate'),
					"class" => "",
					"value" => "$",
					"type" => "textfield"
				),
				array(
					"param_name" => "period",
					"heading" => esc_html__("Period", 'yreg-estate'),
					"description" => wp_kses_data( __("Period text (if need). For example: monthly, daily, etc.", 'yreg-estate') ),
					"admin_label" => true,
					"group" => esc_html__('Money', 'yreg-estate'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "scheme",
					"heading" => esc_html__("Color scheme", 'yreg-estate'),
					"description" => wp_kses_data( __("Select color scheme for this block", 'yreg-estate') ),
					"group" => esc_html__('Colors and Images', 'yreg-estate'),
					"class" => "",
					"value" => array_flip(yreg_estate_get_sc_param('schemes')),
					"type" => "dropdown"
				),
				array(
					"param_name" => "align",
					"heading" => esc_html__("Alignment", 'yreg-estate'),
					"description" => wp_kses_data( __("Align price to left or right side", 'yreg-estate') ),
					"admin_label" => true,
					"class" => "",
					"value" => array_flip(yreg_estate_get_sc_param('float')),
					"type" => "dropdown"
				),
				array(
					"param_name" => "content",
					"heading" => esc_html__("Description", 'yreg-estate'),
					"description" => wp_kses_data( __("Description for this price block", 'yreg-estate') ),
					"class" => "",
					"value" => "",
					"type" => "textarea_html"
				),
				yreg_estate_get_vc_param('id'),
				yreg_estate_get_vc_param('class'),
				yreg_estate_get_vc_param('animation'),
				yreg_estate_get_vc_param('css'),
				yreg_estate_vc_width(),
				yreg_estate_vc_height(),
				yreg_estate_get_vc_param('margin_top'),
				yreg_estate_get_vc_param('margin_bottom'),
				yreg_estate_get_vc_param('margin_left'),
				yreg_estate_get_vc_param('margin_right')
			),
			'js_view' => 'VcTrxTextView'
		) );
		
		class WPBakeryShortCode_Trx_PriceBlock extends YREG_ESTATE_VC_ShortCodeSingle {}
	}
}
?>