<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('yreg_estate_sc_quote_theme_setup')) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_sc_quote_theme_setup' );
	function yreg_estate_sc_quote_theme_setup() {
		add_action('yreg_estate_action_shortcodes_list', 		'yreg_estate_sc_quote_reg_shortcodes');
		if (function_exists('yreg_estate_exists_visual_composer') && yreg_estate_exists_visual_composer())
			add_action('yreg_estate_action_shortcodes_list_vc','yreg_estate_sc_quote_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_quote id="unique_id" cite="url" title=""]Et adipiscing integer, scelerisque pid, augue mus vel tincidunt porta[/quote]
*/

if (!function_exists('yreg_estate_sc_quote')) {	
	function yreg_estate_sc_quote($atts, $content=null){	
		if (yreg_estate_in_shortcode_blogger()) return '';
		extract(yreg_estate_html_decode(shortcode_atts(array(
			// Individual params
			"title" => "",
			"cite" => "",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"width" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$class .= ($class ? ' ' : '') . yreg_estate_get_css_position_as_classes($top, $right, $bottom, $left);
		$css .= yreg_estate_get_css_dimensions_from_values($width);
		$cite_param = $cite != '' ? ' cite="'.esc_attr($cite).'"' : '';
		$title = $title=='' ? $cite : $title;
		$content = do_shortcode($content);
		if (yreg_estate_substr($content, 0, 2)!='<p') $content = '<p>' . ($content) . '</p>';
		$output = '<blockquote' 
			. ($id ? ' id="'.esc_attr($id).'"' : '') . ($cite_param) 
			. ' class="sc_quote'. (!empty($class) ? ' '.esc_attr($class) : '').'"' 
			. (!yreg_estate_param_is_off($animation) ? ' data-animation="'.esc_attr(yreg_estate_get_animation_classes($animation)).'"' : '')
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
			. '>'
				. ($content)
				. ($title == '' ? '' : ('<p class="sc_quote_title">' . ($cite!='' ? '<a href="'.esc_url($cite).'">' : '') . ($title) . ($cite!='' ? '</a>' : '') . '</p>'))
			.'</blockquote>';
		return apply_filters('yreg_estate_shortcode_output', $output, 'trx_quote', $atts, $content);
	}
	yreg_estate_require_shortcode('trx_quote', 'yreg_estate_sc_quote');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_quote_reg_shortcodes' ) ) {
	//add_action('yreg_estate_action_shortcodes_list', 'yreg_estate_sc_quote_reg_shortcodes');
	function yreg_estate_sc_quote_reg_shortcodes() {
	
		yreg_estate_sc_map("trx_quote", array(
			"title" => esc_html__("Quote", 'yreg-estate'),
			"desc" => wp_kses_data( __("Quote text", 'yreg-estate') ),
			"decorate" => false,
			"container" => true,
			"params" => array(
				"cite" => array(
					"title" => esc_html__("Quote cite", 'yreg-estate'),
					"desc" => wp_kses_data( __("URL for quote cite", 'yreg-estate') ),
					"value" => "",
					"type" => "text"
				),
				"title" => array(
					"title" => esc_html__("Title (author)", 'yreg-estate'),
					"desc" => wp_kses_data( __("Quote title (author name)", 'yreg-estate') ),
					"value" => "",
					"type" => "text"
				),
				"_content_" => array(
					"title" => esc_html__("Quote content", 'yreg-estate'),
					"desc" => wp_kses_data( __("Quote content", 'yreg-estate') ),
					"rows" => 4,
					"value" => "",
					"type" => "textarea"
				),
				"width" => yreg_estate_shortcodes_width(),
				"top" => yreg_estate_get_sc_param('top'),
				"bottom" => yreg_estate_get_sc_param('bottom'),
				"left" => yreg_estate_get_sc_param('left'),
				"right" => yreg_estate_get_sc_param('right'),
				"id" => yreg_estate_get_sc_param('id'),
				"class" => yreg_estate_get_sc_param('class'),
				"animation" => yreg_estate_get_sc_param('animation'),
				"css" => yreg_estate_get_sc_param('css')
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_quote_reg_shortcodes_vc' ) ) {
	//add_action('yreg_estate_action_shortcodes_list_vc', 'yreg_estate_sc_quote_reg_shortcodes_vc');
	function yreg_estate_sc_quote_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_quote",
			"name" => esc_html__("Quote", 'yreg-estate'),
			"description" => wp_kses_data( __("Quote text", 'yreg-estate') ),
			"category" => esc_html__('Content', 'yreg-estate'),
			'icon' => 'icon_trx_quote',
			"class" => "trx_sc_single trx_sc_quote",
			"content_element" => true,
			"is_container" => false,
			"show_settings_on_create" => true,
			"params" => array(
				array(
					"param_name" => "cite",
					"heading" => esc_html__("Quote cite", 'yreg-estate'),
					"description" => wp_kses_data( __("URL for the quote cite link", 'yreg-estate') ),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "title",
					"heading" => esc_html__("Title (author)", 'yreg-estate'),
					"description" => wp_kses_data( __("Quote title (author name)", 'yreg-estate') ),
					"admin_label" => true,
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "content",
					"heading" => esc_html__("Quote content", 'yreg-estate'),
					"description" => wp_kses_data( __("Quote content", 'yreg-estate') ),
					"class" => "",
					"value" => "",
					"type" => "textarea_html"
				),
				yreg_estate_get_vc_param('id'),
				yreg_estate_get_vc_param('class'),
				yreg_estate_get_vc_param('animation'),
				yreg_estate_get_vc_param('css'),
				yreg_estate_vc_width(),
				yreg_estate_get_vc_param('margin_top'),
				yreg_estate_get_vc_param('margin_bottom'),
				yreg_estate_get_vc_param('margin_left'),
				yreg_estate_get_vc_param('margin_right')
			),
			'js_view' => 'VcTrxTextView'
		) );
		
		class WPBakeryShortCode_Trx_Quote extends YREG_ESTATE_VC_ShortCodeSingle {}
	}
}
?>