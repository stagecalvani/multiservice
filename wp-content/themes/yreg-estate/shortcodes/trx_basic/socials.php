<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('yreg_estate_sc_socials_theme_setup')) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_sc_socials_theme_setup' );
	function yreg_estate_sc_socials_theme_setup() {
		add_action('yreg_estate_action_shortcodes_list', 		'yreg_estate_sc_socials_reg_shortcodes');
		if (function_exists('yreg_estate_exists_visual_composer') && yreg_estate_exists_visual_composer())
			add_action('yreg_estate_action_shortcodes_list_vc','yreg_estate_sc_socials_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_socials id="unique_id" size="small"]
	[trx_social_item name="facebook" url="profile url" icon="path for the icon"]
	[trx_social_item name="twitter" url="profile url"]
[/trx_socials]
*/

if (!function_exists('yreg_estate_sc_socials')) {	
	function yreg_estate_sc_socials($atts, $content=null){	
		if (yreg_estate_in_shortcode_blogger()) return '';
		extract(yreg_estate_html_decode(shortcode_atts(array(
			// Individual params
			"size" => "small",		// tiny | small | medium | large
			"shape" => "square",	// round | square
			"type" => yreg_estate_get_theme_setting('socials_type'),	// icons | images
			"socials" => "",
			"custom" => "no",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$class .= ($class ? ' ' : '') . yreg_estate_get_css_position_as_classes($top, $right, $bottom, $left);
		yreg_estate_storage_set('sc_social_data', array(
			'icons' => false,
            'type' => $type
            )
        );
		if (!empty($socials)) {
			$allowed = explode('|', $socials);
			$list = array();
			for ($i=0; $i<count($allowed); $i++) {
				$s = explode('=', $allowed[$i]);
				if (!empty($s[1])) {
					$list[] = array(
						'icon'	=> $type=='images' ? yreg_estate_get_socials_url($s[0]) : 'icon-'.trim($s[0]),
						'url'	=> $s[1]
						);
				}
			}
			if (count($list) > 0) yreg_estate_storage_set_array('sc_social_data', 'icons', $list);
		} else if (yreg_estate_param_is_off($custom))
			$content = do_shortcode($content);
		if (yreg_estate_storage_get_array('sc_social_data', 'icons')===false) yreg_estate_storage_set_array('sc_social_data', 'icons', yreg_estate_get_custom_option('social_icons'));
		$output = yreg_estate_prepare_socials(yreg_estate_storage_get_array('sc_social_data', 'icons'));
		$output = $output
			? '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
				. ' class="sc_socials sc_socials_type_' . esc_attr($type) . ' sc_socials_shape_' . esc_attr($shape) . ' sc_socials_size_' . esc_attr($size) . (!empty($class) ? ' '.esc_attr($class) : '') . '"' 
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
				. (!yreg_estate_param_is_off($animation) ? ' data-animation="'.esc_attr(yreg_estate_get_animation_classes($animation)).'"' : '')
				. '>' 
				. ($output)
				. '</div>'
			: '';
		return apply_filters('yreg_estate_shortcode_output', $output, 'trx_socials', $atts, $content);
	}
	yreg_estate_require_shortcode('trx_socials', 'yreg_estate_sc_socials');
}


if (!function_exists('yreg_estate_sc_social_item')) {	
	function yreg_estate_sc_social_item($atts, $content=null){	
		if (yreg_estate_in_shortcode_blogger()) return '';
		extract(yreg_estate_html_decode(shortcode_atts(array(
			// Individual params
			"name" => "",
			"url" => "",
			"icon" => ""
		), $atts)));
		if (!empty($name) && empty($icon)) {
			$type = yreg_estate_storage_get_array('sc_social_data', 'type');
			if ($type=='images') {
				if (file_exists(yreg_estate_get_socials_dir($name.'.png')))
					$icon = yreg_estate_get_socials_url($name.'.png');
			} else
				$icon = 'icon-'.esc_attr($name);
		}
		if (!empty($icon) && !empty($url)) {
			if (yreg_estate_storage_get_array('sc_social_data', 'icons')===false) yreg_estate_storage_set_array('sc_social_data', 'icons', array());
			yreg_estate_storage_set_array2('sc_social_data', 'icons', '', array(
				'icon' => $icon,
				'url' => $url
				)
			);
		}
		return '';
	}
	yreg_estate_require_shortcode('trx_social_item', 'yreg_estate_sc_social_item');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_socials_reg_shortcodes' ) ) {
	//add_action('yreg_estate_action_shortcodes_list', 'yreg_estate_sc_socials_reg_shortcodes');
	function yreg_estate_sc_socials_reg_shortcodes() {
	
		yreg_estate_sc_map("trx_socials", array(
			"title" => esc_html__("Social icons", 'yreg-estate'),
			"desc" => wp_kses_data( __("List of social icons (with hovers)", 'yreg-estate') ),
			"decorate" => true,
			"container" => false,
			"params" => array(
				"type" => array(
					"title" => esc_html__("Icon's type", 'yreg-estate'),
					"desc" => wp_kses_data( __("Type of the icons - images or font icons", 'yreg-estate') ),
					"value" => yreg_estate_get_theme_setting('socials_type'),
					"options" => array(
						'icons' => esc_html__('Icons', 'yreg-estate'),
						'images' => esc_html__('Images', 'yreg-estate')
					),
					"type" => "checklist"
				), 
				"size" => array(
					"title" => esc_html__("Icon's size", 'yreg-estate'),
					"desc" => wp_kses_data( __("Size of the icons", 'yreg-estate') ),
					"value" => "small",
					"options" => yreg_estate_get_sc_param('sizes'),
					"type" => "checklist"
				), 
				"shape" => array(
					"title" => esc_html__("Icon's shape", 'yreg-estate'),
					"desc" => wp_kses_data( __("Shape of the icons", 'yreg-estate') ),
					"value" => "square",
					"options" => yreg_estate_get_sc_param('shapes'),
					"type" => "checklist"
				), 
				"socials" => array(
					"title" => esc_html__("Manual socials list", 'yreg-estate'),
					"desc" => wp_kses_data( __("Custom list of social networks. For example: twitter=http://twitter.com/my_profile|facebook=http://facebook.com/my_profile. If empty - use socials from Theme options.", 'yreg-estate') ),
					"divider" => true,
					"value" => "",
					"type" => "text"
				),
				"custom" => array(
					"title" => esc_html__("Custom socials", 'yreg-estate'),
					"desc" => wp_kses_data( __("Make custom icons from inner shortcodes (prepare it on tabs)", 'yreg-estate') ),
					"divider" => true,
					"value" => "no",
					"options" => yreg_estate_get_sc_param('yes_no'),
					"type" => "switch"
				),
				"top" => yreg_estate_get_sc_param('top'),
				"bottom" => yreg_estate_get_sc_param('bottom'),
				"left" => yreg_estate_get_sc_param('left'),
				"right" => yreg_estate_get_sc_param('right'),
				"id" => yreg_estate_get_sc_param('id'),
				"class" => yreg_estate_get_sc_param('class'),
				"animation" => yreg_estate_get_sc_param('animation'),
				"css" => yreg_estate_get_sc_param('css')
			),
			"children" => array(
				"name" => "trx_social_item",
				"title" => esc_html__("Custom social item", 'yreg-estate'),
				"desc" => wp_kses_data( __("Custom social item: name, profile url and icon url", 'yreg-estate') ),
				"decorate" => false,
				"container" => false,
				"params" => array(
					"name" => array(
						"title" => esc_html__("Social name", 'yreg-estate'),
						"desc" => wp_kses_data( __("Name (slug) of the social network (twitter, facebook, linkedin, etc.)", 'yreg-estate') ),
						"value" => "",
						"type" => "text"
					),
					"url" => array(
						"title" => esc_html__("Your profile URL", 'yreg-estate'),
						"desc" => wp_kses_data( __("URL of your profile in specified social network", 'yreg-estate') ),
						"value" => "",
						"type" => "text"
					),
					"icon" => array(
						"title" => esc_html__("URL (source) for icon file", 'yreg-estate'),
						"desc" => wp_kses_data( __("Select or upload image or write URL from other site for the current social icon", 'yreg-estate') ),
						"readonly" => false,
						"value" => "",
						"type" => "media"
					)
				)
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_socials_reg_shortcodes_vc' ) ) {
	//add_action('yreg_estate_action_shortcodes_list_vc', 'yreg_estate_sc_socials_reg_shortcodes_vc');
	function yreg_estate_sc_socials_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_socials",
			"name" => esc_html__("Social icons", 'yreg-estate'),
			"description" => wp_kses_data( __("Custom social icons", 'yreg-estate') ),
			"category" => esc_html__('Content', 'yreg-estate'),
			'icon' => 'icon_trx_socials',
			"class" => "trx_sc_collection trx_sc_socials",
			"content_element" => true,
			"is_container" => true,
			"show_settings_on_create" => true,
			"as_parent" => array('only' => 'trx_social_item'),
			"params" => array_merge(array(
				array(
					"param_name" => "type",
					"heading" => esc_html__("Icon's type", 'yreg-estate'),
					"description" => wp_kses_data( __("Type of the icons - images or font icons", 'yreg-estate') ),
					"class" => "",
					"std" => yreg_estate_get_theme_setting('socials_type'),
					"value" => array(
						esc_html__('Icons', 'yreg-estate') => 'icons',
						esc_html__('Images', 'yreg-estate') => 'images'
					),
					"type" => "dropdown"
				),
				array(
					"param_name" => "size",
					"heading" => esc_html__("Icon's size", 'yreg-estate'),
					"description" => wp_kses_data( __("Size of the icons", 'yreg-estate') ),
					"class" => "",
					"std" => "small",
					"value" => array_flip(yreg_estate_get_sc_param('sizes')),
					"type" => "dropdown"
				),
				array(
					"param_name" => "shape",
					"heading" => esc_html__("Icon's shape", 'yreg-estate'),
					"description" => wp_kses_data( __("Shape of the icons", 'yreg-estate') ),
					"class" => "",
					"std" => "square",
					"value" => array_flip(yreg_estate_get_sc_param('shapes')),
					"type" => "dropdown"
				),
				array(
					"param_name" => "socials",
					"heading" => esc_html__("Manual socials list", 'yreg-estate'),
					"description" => wp_kses_data( __("Custom list of social networks. For example: twitter=http://twitter.com/my_profile|facebook=http://facebook.com/my_profile. If empty - use socials from Theme options.", 'yreg-estate') ),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "custom",
					"heading" => esc_html__("Custom socials", 'yreg-estate'),
					"description" => wp_kses_data( __("Make custom icons from inner shortcodes (prepare it on tabs)", 'yreg-estate') ),
					"class" => "",
					"value" => array(esc_html__('Custom socials', 'yreg-estate') => 'yes'),
					"type" => "checkbox"
				),
				yreg_estate_get_vc_param('id'),
				yreg_estate_get_vc_param('class'),
				yreg_estate_get_vc_param('animation'),
				yreg_estate_get_vc_param('css'),
				yreg_estate_get_vc_param('margin_top'),
				yreg_estate_get_vc_param('margin_bottom'),
				yreg_estate_get_vc_param('margin_left'),
				yreg_estate_get_vc_param('margin_right')
			))
		) );
		
		
		vc_map( array(
			"base" => "trx_social_item",
			"name" => esc_html__("Custom social item", 'yreg-estate'),
			"description" => wp_kses_data( __("Custom social item: name, profile url and icon url", 'yreg-estate') ),
			"show_settings_on_create" => true,
			"content_element" => true,
			"is_container" => false,
			'icon' => 'icon_trx_social_item',
			"class" => "trx_sc_single trx_sc_social_item",
			"as_child" => array('only' => 'trx_socials'),
			"as_parent" => array('except' => 'trx_socials'),
			"params" => array(
				array(
					"param_name" => "name",
					"heading" => esc_html__("Social name", 'yreg-estate'),
					"description" => wp_kses_data( __("Name (slug) of the social network (twitter, facebook, linkedin, etc.)", 'yreg-estate') ),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "url",
					"heading" => esc_html__("Your profile URL", 'yreg-estate'),
					"description" => wp_kses_data( __("URL of your profile in specified social network", 'yreg-estate') ),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "icon",
					"heading" => esc_html__("URL (source) for icon file", 'yreg-estate'),
					"description" => wp_kses_data( __("Select or upload image or write URL from other site for the current social icon", 'yreg-estate') ),
					"admin_label" => true,
					"class" => "",
					"value" => "",
					"type" => "attach_image"
				)
			)
		) );
		
		class WPBakeryShortCode_Trx_Socials extends YREG_ESTATE_VC_ShortCodeCollection {}
		class WPBakeryShortCode_Trx_Social_Item extends YREG_ESTATE_VC_ShortCodeSingle {}
	}
}
?>