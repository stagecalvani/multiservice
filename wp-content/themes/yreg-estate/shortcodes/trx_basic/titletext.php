<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('yreg_estate_sc_titletext_theme_setup')) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_sc_titletext_theme_setup' );
	function yreg_estate_sc_titletext_theme_setup() {
		add_action('yreg_estate_action_shortcodes_list', 		'yreg_estate_sc_titletext_reg_shortcodes');
		if (function_exists('yreg_estate_exists_visual_composer') && yreg_estate_exists_visual_composer())
			add_action('yreg_estate_action_shortcodes_list_vc','yreg_estate_sc_titletext_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_titletext id="unique_id" style='regular|iconed' icon='' image='' background="on|off" type="1-6"]Et adipiscing integer, scelerisque pid, augue mus vel tincidunt porta[/trx_titletext]
*/

if (!function_exists('yreg_estate_sc_titletext')) {	
	function yreg_estate_sc_titletext($atts, $content=null){	
		if (yreg_estate_in_shortcode_blogger()) return '';
		extract(yreg_estate_html_decode(shortcode_atts(array(
			// Individual params
			"type" => "1",
			"style" => "regular",
			"subtitle" => "",
			"align" => "",
			"font_weight" => "",
			"font_size" => "",
			"color" => "",
			"icon" => "",
			"image" => "",
			"picture" => "",
			"image_size" => "small",
			"position" => "left",
			"snumber" => "",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"width" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$class .= ($class ? ' ' : '') . yreg_estate_get_css_position_as_classes($top, $right, $bottom, $left);
		$css .= yreg_estate_get_css_dimensions_from_values($width)
			.($align && $align!='none' && !yreg_estate_param_is_inherit($align) ? 'text-align:' . esc_attr($align) .';' : '')
			.($color ? 'color:' . esc_attr($color) .';' : '')
			.($font_weight && !yreg_estate_param_is_inherit($font_weight) ? 'font-weight:' . esc_attr($font_weight) .';' : '')
			.($font_size   ? 'font-size:' . esc_attr($font_size) .';' : '')
			;
		
		$output = '';
		
		$output .= '<div '
			. ($id ? ' id="'.esc_attr($id).'"' : '')
			. ' class="sc_titletext'
			. (!empty($class) ? ' '.esc_attr($class) : '')
			. '"'
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
			. '>'
			. (!empty($snumber) ? '<div class="sc_titletext_snumber">'.trim($snumber).'</div>' : '')
			. '<div class="sc_titletext_item">'
			. (!empty($subtitle) ? '<div class="sc_titletext_subtitle">'.trim($subtitle).'</div>' : '')
			. '<div class="sc_titletext_content">'.do_shortcode($content).'</div>'
			. '</div>'
			. '<div class="cL"></div>'	
			. '</div>';
		
		
		
//		$type = min(6, max(1, $type));
//		if ($picture > 0) {
//			$attach = wp_get_attachment_image_src( $picture, 'full' );
//			if (isset($attach[0]) && $attach[0]!='')
//				$picture = $attach[0];
//		}
//		$pic = $style!='iconed' 
//			? '' 
//			: '<span class="sc_title_icon sc_title_icon_'.esc_attr($position).'  sc_title_icon_'.esc_attr($image_size).($icon!='' && $icon!='none' ? ' '.esc_attr($icon) : '')
//				.(!empty($subtitle) ? ' sc_left' : '')
//				.'"'.'>'
//				.($picture ? '<img src="'.esc_url($picture).'" alt="" />' : '')
//				.(empty($picture) && $image && $image!='none' ? '<img src="'.esc_url(yreg_estate_strpos($image, 'http:')!==false ? $image : yreg_estate_get_file_url('images/icons/'.($image).'.png')).'" alt="" />' : '')
//				.'</span>';
//		$output = '<h' . esc_attr($type) . ($id ? ' id="'.esc_attr($id).'"' : '')
//				. ' class="sc_titletext sc_titletext_'.esc_attr($style)
//					.($align && $align!='none' && !yreg_estate_param_is_inherit($align) ? ' sc_align_' . esc_attr($align) : '')
//					.(!empty($class) ? ' '.esc_attr($class) : '')
//					.'"'
//				. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
//				. (!yreg_estate_param_is_off($animation) ? ' data-animation="'.esc_attr(yreg_estate_get_animation_classes($animation)).'"' : '')
//				. '>'
//					. ($pic)
//					.(!empty($subtitle) ? '<span class="sc_titletext_box">' : '')
//					. do_shortcode($content)
//					.(!empty($subtitle) ? '<span class="sc_titletext_subtitle">'.$subtitle.'</span></span>' : '')
//				. '</h' . esc_attr($type) . '>';
		
		
		
		
		return apply_filters('yreg_estate_shortcode_output', $output, 'trx_titletext', $atts, $content);
	}
	yreg_estate_require_shortcode('trx_titletext', 'yreg_estate_sc_titletext');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_titletext_reg_shortcodes' ) ) {
	//add_action('yreg_estate_action_shortcodes_list', 'yreg_estate_sc_titletext_reg_shortcodes');
	function yreg_estate_sc_titletext_reg_shortcodes() {
	
		yreg_estate_sc_map("trx_titletext", array(
			"title" => esc_html__("Title text", 'yreg-estate'),
			"desc" => wp_kses_data( __("", 'yreg-estate') ),
			"decorate" => false,
			"container" => true,
			"params" => array(
				"subtitle" => array(
					"title" => esc_html__("Title", 'yreg-estate'),
					"desc" => wp_kses_data( __("Title", 'yreg-estate') ),
					"value" => "",
					"type" => "text"
				),
				"_content_" => array(
					"title" => esc_html__("Title content", 'yreg-estate'),
					"desc" => wp_kses_data( __("Title content", 'yreg-estate') ),
					"rows" => 4,
					"value" => "",
					"type" => "textarea"
				),
				"snumber" => array(
					"title" => esc_html__("String number", 'yreg-estate'),
					"desc" => wp_kses_data( __("", 'yreg-estate') ),
					"value" => "",
					"type" => "text"
				),
				

				"top" => yreg_estate_get_sc_param('top'),
				"bottom" => yreg_estate_get_sc_param('bottom'),
				"left" => yreg_estate_get_sc_param('left'),
				"right" => yreg_estate_get_sc_param('right'),
				"id" => yreg_estate_get_sc_param('id'),
				"class" => yreg_estate_get_sc_param('class'),
				"animation" => yreg_estate_get_sc_param('animation'),
				"css" => yreg_estate_get_sc_param('css')
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_titletext_reg_shortcodes_vc' ) ) {
	//add_action('yreg_estate_action_shortcodes_list_vc', 'yreg_estate_sc_titletext_reg_shortcodes_vc');
	function yreg_estate_sc_titletext_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_titletext",
			"name" => esc_html__("Title text", 'yreg-estate'),
			"description" => wp_kses_data( __("", 'yreg-estate') ),
			"category" => esc_html__('Content', 'yreg-estate'),
			'icon' => 'icon_trx_title',
			"class" => "trx_sc_single trx_sc_titletext",
			"content_element" => true,
			"is_container" => false,
			"show_settings_on_create" => true,
			"params" => array(
				
				array(
					"param_name" => "subtitle",
					"heading" => esc_html__("Title", 'yreg-estate'),
					"description" => wp_kses_data( __("Title", 'yreg-estate') ),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				
				array(
					"param_name" => "content",
					"heading" => esc_html__("Title content", 'yreg-estate'),
					"description" => wp_kses_data( __("Title content", 'yreg-estate') ),
					"class" => "",
					"value" => "",
					"type" => "textarea_html"
				),
				
				array(
					"param_name" => "snumber",
					"heading" => esc_html__("String number", 'yreg-estate'),
					"description" => wp_kses_data( __("", 'yreg-estate') ),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),

				yreg_estate_get_vc_param('id'),
				yreg_estate_get_vc_param('class'),
				yreg_estate_get_vc_param('animation'),
				yreg_estate_get_vc_param('css'),
				yreg_estate_get_vc_param('margin_top'),
				yreg_estate_get_vc_param('margin_bottom'),
				yreg_estate_get_vc_param('margin_left'),
				yreg_estate_get_vc_param('margin_right')
			),
			'js_view' => 'VcTrxTextView'
		) );
		
		class WPBakeryShortCode_Trx_Titletext extends YREG_ESTATE_VC_ShortCodeSingle {}
	}
}
?>