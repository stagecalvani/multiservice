<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('yreg_estate_sc_button_theme_setup')) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_sc_button_theme_setup' );
	function yreg_estate_sc_button_theme_setup() {
		add_action('yreg_estate_action_shortcodes_list', 		'yreg_estate_sc_button_reg_shortcodes');
		if (function_exists('yreg_estate_exists_visual_composer') && yreg_estate_exists_visual_composer())
			add_action('yreg_estate_action_shortcodes_list_vc','yreg_estate_sc_button_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_button id="unique_id" type="square|round" fullsize="0|1" style="global|light|dark" size="mini|medium|big|huge|banner" icon="icon-name" link='#' target='']Button caption[/trx_button]
*/

if (!function_exists('yreg_estate_sc_button')) {	
	function yreg_estate_sc_button($atts, $content=null){	
		if (yreg_estate_in_shortcode_blogger()) return '';
		extract(yreg_estate_html_decode(shortcode_atts(array(
			// Individual params
			"type" => "box",
			"style" => "style1",
			"size" => "small",
			"icon" => "",
			"color" => "",
			"bg_color" => "",
			"link" => "",
			"target" => "",
			"align" => "",
			"rel" => "",
			"popup" => "no",
			// Common params
			"id" => "",
			"class" => "",
			"css" => "",
			"animation" => "",
			"width" => "",
			"height" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		
		if ( $type == 'text' ) {
			if ( $icon == '' ) {
				$icon = 'icon-right-dir';
			}
		}
		
		$class .= ($class ? ' ' : '') . yreg_estate_get_css_position_as_classes($top, $right, $bottom, $left);
		$css .= yreg_estate_get_css_dimensions_from_values($width, $height)
			. ($color !== '' ? 'color:' . esc_attr($color) .';' : '')
			. ($bg_color !== '' ? 'background-color:' . esc_attr($bg_color) . '; border-color:'. esc_attr($bg_color) .';' : '');
		if (yreg_estate_param_is_on($popup)) yreg_estate_enqueue_popup('magnific');
		$output = '<a href="' . (empty($link) ? '#' : $link) . '"'
			. (!empty($target) ? ' target="'.esc_attr($target).'"' : '')
			. (!empty($rel) ? ' rel="'.esc_attr($rel).'"' : '')
			. (!yreg_estate_param_is_off($animation) ? ' data-animation="'.esc_attr(yreg_estate_get_animation_classes($animation)).'"' : '')
			. ' class="sc_button sc_button_' . esc_attr($type) 
					. ' sc_button_style_' . esc_attr($style) 
					. ' sc_button_size_' . esc_attr($size)
					. ($align && $align!='none' ? ' align'.esc_attr($align) : '') 
					. (!empty($class) ? ' '.esc_attr($class) : '')
					. ($icon!='' ? '  sc_button_iconed '. esc_attr($icon) : '') 
					. (yreg_estate_param_is_on($popup) ? ' sc_popup_link' : '') 
					. '"'
			. ($id ? ' id="'.esc_attr($id).'"' : '') 
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
			. '>'
			. do_shortcode($content)
			. '</a>';
		return apply_filters('yreg_estate_shortcode_output', $output, 'trx_button', $atts, $content);
	}
	yreg_estate_require_shortcode('trx_button', 'yreg_estate_sc_button');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_button_reg_shortcodes' ) ) {
	//add_action('yreg_estate_action_shortcodes_list', 'yreg_estate_sc_button_reg_shortcodes');
	function yreg_estate_sc_button_reg_shortcodes() {
	
		yreg_estate_sc_map("trx_button", array(
			"title" => esc_html__("Button", 'yreg-estate'),
			"desc" => wp_kses_data( __("Button with link", 'yreg-estate') ),
			"decorate" => false,
			"container" => true,
			"params" => array(
				"_content_" => array(
					"title" => esc_html__("Caption", 'yreg-estate'),
					"desc" => wp_kses_data( __("Button caption", 'yreg-estate') ),
					"value" => "",
					"type" => "text"
				),
				"type" => array(
					"title" => esc_html__("Button's type", 'yreg-estate'),
					"desc" => wp_kses_data( __("Select button's type", 'yreg-estate') ),
					"value" => "box",
					"size" => "small",
					"options" => array(
						'box' => esc_html__('Box', 'yreg-estate'),
						'text' => esc_html__('Text', 'yreg-estate')
					),
					"type" => "switch"
				),
				"style" => array(
					"title" => esc_html__("Button's style", 'yreg-estate'),
					"desc" => wp_kses_data( __("Select button's style", 'yreg-estate') ),
					"value" => "style1",
					"dir" => "horizontal",
					"options" => array(
						'style1' => esc_html__('Style 1', 'yreg-estate'),
						'style2' => esc_html__('Style 2', 'yreg-estate'),
						'style3' => esc_html__('Style 3', 'yreg-estate')
					),
					"type" => "checklist"
				), 
				/*
				"size" => array(
					"title" => esc_html__("Button's size", 'yreg-estate'),
					"desc" => wp_kses_data( __("Select button's size", 'yreg-estate') ),
					"value" => "small",
					"dir" => "horizontal",
					"options" => array(
						'small' => esc_html__('Small', 'yreg-estate'),
						'medium' => esc_html__('Medium', 'yreg-estate'),
						'large' => esc_html__('Large', 'yreg-estate')
					),
					"type" => "checklist"
				), 
				*/
				"icon" => array(
					"title" => esc_html__("Button's icon",  'yreg-estate'),
					"desc" => wp_kses_data( __('Select icon for the title from Fontello icons set',  'yreg-estate') ),
					"value" => "",
					"type" => "icons",
					"options" => yreg_estate_get_sc_param('icons')
				),
				"color" => array(
					"title" => esc_html__("Button's text color", 'yreg-estate'),
					"desc" => wp_kses_data( __("Any color for button's caption", 'yreg-estate') ),
					"std" => "",
					"value" => "",
					"type" => "color"
				),
				"bg_color" => array(
					"title" => esc_html__("Button's backcolor", 'yreg-estate'),
					"desc" => wp_kses_data( __("Any color for button's background", 'yreg-estate') ),
					"value" => "",
					"type" => "color"
				),
				"align" => array(
					"title" => esc_html__("Button's alignment", 'yreg-estate'),
					"desc" => wp_kses_data( __("Align button to left, center or right", 'yreg-estate') ),
					"value" => "none",
					"type" => "checklist",
					"dir" => "horizontal",
					"options" => yreg_estate_get_sc_param('align')
				), 
				"link" => array(
					"title" => esc_html__("Link URL", 'yreg-estate'),
					"desc" => wp_kses_data( __("URL for link on button click", 'yreg-estate') ),
					"divider" => true,
					"value" => "",
					"type" => "text"
				),
				"target" => array(
					"title" => esc_html__("Link target", 'yreg-estate'),
					"desc" => wp_kses_data( __("Target for link on button click", 'yreg-estate') ),
					"dependency" => array(
						'link' => array('not_empty')
					),
					"value" => "",
					"type" => "text"
				),
				"popup" => array(
					"title" => esc_html__("Open link in popup", 'yreg-estate'),
					"desc" => wp_kses_data( __("Open link target in popup window", 'yreg-estate') ),
					"dependency" => array(
						'link' => array('not_empty')
					),
					"value" => "no",
					"type" => "switch",
					"options" => yreg_estate_get_sc_param('yes_no')
				), 
				"rel" => array(
					"title" => esc_html__("Rel attribute", 'yreg-estate'),
					"desc" => wp_kses_data( __("Rel attribute for button's link (if need)", 'yreg-estate') ),
					"dependency" => array(
						'link' => array('not_empty')
					),
					"value" => "",
					"type" => "text"
				),
				"width" => yreg_estate_shortcodes_width(),
				"height" => yreg_estate_shortcodes_height(),
				"top" => yreg_estate_get_sc_param('top'),
				"bottom" => yreg_estate_get_sc_param('bottom'),
				"left" => yreg_estate_get_sc_param('left'),
				"right" => yreg_estate_get_sc_param('right'),
				"id" => yreg_estate_get_sc_param('id'),
				"class" => yreg_estate_get_sc_param('class'),
				"animation" => yreg_estate_get_sc_param('animation'),
				"css" => yreg_estate_get_sc_param('css')
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_button_reg_shortcodes_vc' ) ) {
	//add_action('yreg_estate_action_shortcodes_list_vc', 'yreg_estate_sc_button_reg_shortcodes_vc');
	function yreg_estate_sc_button_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_button",
			"name" => esc_html__("Button", 'yreg-estate'),
			"description" => wp_kses_data( __("Button with link", 'yreg-estate') ),
			"category" => esc_html__('Content', 'yreg-estate'),
			'icon' => 'icon_trx_button',
			"class" => "trx_sc_single trx_sc_button",
			"content_element" => true,
			"is_container" => false,
			"show_settings_on_create" => true,
			"params" => array(
				array(
					"param_name" => "content",
					"heading" => esc_html__("Caption", 'yreg-estate'),
					"description" => wp_kses_data( __("Button caption", 'yreg-estate') ),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "type",
					"heading" => esc_html__("Button's type", 'yreg-estate'),
					"description" => wp_kses_data( __("Select button's type", 'yreg-estate') ),
					"class" => "",
					"value" => array(
						esc_html__('Box', 'yreg-estate') => 'box',
						esc_html__('Text', 'yreg-estate') => 'text'
					),
					"type" => "dropdown"
				),
				array(
					"param_name" => "style",
					"heading" => esc_html__("Button's style", 'yreg-estate'),
					"description" => wp_kses_data( __("Select button's style", 'yreg-estate') ),
					"class" => "",
					"value" => array(
						esc_html__('Style 1', 'yreg-estate') => 'style1',
						esc_html__('Style 2', 'yreg-estate') => 'style2',
						esc_html__('Style 3', 'yreg-estate') => 'style3'
					),
					"type" => "dropdown"
				),
				/*
				array(
					"param_name" => "size",
					"heading" => esc_html__("Button's size", 'yreg-estate'),
					"description" => wp_kses_data( __("Select button's size", 'yreg-estate') ),
					"admin_label" => true,
					"class" => "",
					"value" => array(
						esc_html__('Small', 'yreg-estate') => 'small',
						esc_html__('Medium', 'yreg-estate') => 'medium',
						esc_html__('Large', 'yreg-estate') => 'large'
					),
					"type" => "dropdown"
				),
				*/
				array(
					"param_name" => "icon",
					"heading" => esc_html__("Button's icon", 'yreg-estate'),
					"description" => wp_kses_data( __("Select icon for the title from Fontello icons set", 'yreg-estate') ),
					"class" => "",
					"value" => yreg_estate_get_sc_param('icons'),
					"type" => "dropdown"
				),
				array(
					"param_name" => "color",
					"heading" => esc_html__("Button's text color", 'yreg-estate'),
					"description" => wp_kses_data( __("Any color for button's caption", 'yreg-estate') ),
					"class" => "",
					"value" => "",
					"type" => "colorpicker"
				),
				array(
					"param_name" => "bg_color",
					"heading" => esc_html__("Button's backcolor", 'yreg-estate'),
					"description" => wp_kses_data( __("Any color for button's background", 'yreg-estate') ),
					"class" => "",
					"value" => "",
					"type" => "colorpicker"
				),
				array(
					"param_name" => "align",
					"heading" => esc_html__("Button's alignment", 'yreg-estate'),
					"description" => wp_kses_data( __("Align button to left, center or right", 'yreg-estate') ),
					"class" => "",
					"value" => array_flip(yreg_estate_get_sc_param('align')),
					"type" => "dropdown"
				),
				array(
					"param_name" => "link",
					"heading" => esc_html__("Link URL", 'yreg-estate'),
					"description" => wp_kses_data( __("URL for the link on button click", 'yreg-estate') ),
					"class" => "",
					"group" => esc_html__('Link', 'yreg-estate'),
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "target",
					"heading" => esc_html__("Link target", 'yreg-estate'),
					"description" => wp_kses_data( __("Target for the link on button click", 'yreg-estate') ),
					"class" => "",
					"group" => esc_html__('Link', 'yreg-estate'),
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "popup",
					"heading" => esc_html__("Open link in popup", 'yreg-estate'),
					"description" => wp_kses_data( __("Open link target in popup window", 'yreg-estate') ),
					"class" => "",
					"group" => esc_html__('Link', 'yreg-estate'),
					"value" => array(esc_html__('Open in popup', 'yreg-estate') => 'yes'),
					"type" => "checkbox"
				),
				array(
					"param_name" => "rel",
					"heading" => esc_html__("Rel attribute", 'yreg-estate'),
					"description" => wp_kses_data( __("Rel attribute for the button's link (if need", 'yreg-estate') ),
					"class" => "",
					"group" => esc_html__('Link', 'yreg-estate'),
					"value" => "",
					"type" => "textfield"
				),
				yreg_estate_get_vc_param('id'),
				yreg_estate_get_vc_param('class'),
				yreg_estate_get_vc_param('animation'),
				yreg_estate_get_vc_param('css'),
				yreg_estate_vc_width(),
				yreg_estate_vc_height(),
				yreg_estate_get_vc_param('margin_top'),
				yreg_estate_get_vc_param('margin_bottom'),
				yreg_estate_get_vc_param('margin_left'),
				yreg_estate_get_vc_param('margin_right')
			),
			'js_view' => 'VcTrxTextView'
		) );
		
		class WPBakeryShortCode_Trx_Button extends YREG_ESTATE_VC_ShortCodeSingle {}
	}
}
?>