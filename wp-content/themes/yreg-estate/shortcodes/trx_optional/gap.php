<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('yreg_estate_sc_gap_theme_setup')) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_sc_gap_theme_setup' );
	function yreg_estate_sc_gap_theme_setup() {
		add_action('yreg_estate_action_shortcodes_list', 		'yreg_estate_sc_gap_reg_shortcodes');
		if (function_exists('yreg_estate_exists_visual_composer') && yreg_estate_exists_visual_composer())
			add_action('yreg_estate_action_shortcodes_list_vc','yreg_estate_sc_gap_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

//[trx_gap]Fullwidth content[/trx_gap]

if (!function_exists('yreg_estate_sc_gap')) {	
	function yreg_estate_sc_gap($atts, $content = null) {
		if (yreg_estate_in_shortcode_blogger()) return '';
		$output = yreg_estate_gap_start() . do_shortcode($content) . yreg_estate_gap_end();
		return apply_filters('yreg_estate_shortcode_output', $output, 'trx_gap', $atts, $content);
	}
	yreg_estate_require_shortcode("trx_gap", "yreg_estate_sc_gap");
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_gap_reg_shortcodes' ) ) {
	//add_action('yreg_estate_action_shortcodes_list', 'yreg_estate_sc_gap_reg_shortcodes');
	function yreg_estate_sc_gap_reg_shortcodes() {
	
		yreg_estate_sc_map("trx_gap", array(
			"title" => esc_html__("Gap", 'yreg-estate'),
			"desc" => wp_kses_data( __("Insert gap (fullwidth area) in the post content. Attention! Use the gap only in the posts (pages) without left or right sidebar", 'yreg-estate') ),
			"decorate" => true,
			"container" => true,
			"params" => array(
				"_content_" => array(
					"title" => esc_html__("Gap content", 'yreg-estate'),
					"desc" => wp_kses_data( __("Gap inner content", 'yreg-estate') ),
					"rows" => 4,
					"value" => "",
					"type" => "textarea"
				)
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_gap_reg_shortcodes_vc' ) ) {
	//add_action('yreg_estate_action_shortcodes_list_vc', 'yreg_estate_sc_gap_reg_shortcodes_vc');
	function yreg_estate_sc_gap_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_gap",
			"name" => esc_html__("Gap", 'yreg-estate'),
			"description" => wp_kses_data( __("Insert gap (fullwidth area) in the post content", 'yreg-estate') ),
			"category" => esc_html__('Structure', 'yreg-estate'),
			'icon' => 'icon_trx_gap',
			"class" => "trx_sc_collection trx_sc_gap",
			"content_element" => true,
			"is_container" => true,
			"show_settings_on_create" => false,
			"params" => array(
				/*
				array(
					"param_name" => "content",
					"heading" => esc_html__("Gap content", 'yreg-estate'),
					"description" => wp_kses_data( __("Gap inner content", 'yreg-estate') ),
					"class" => "",
					"value" => "",
					"type" => "textarea_html"
				)
				*/
			)
		) );
		
		class WPBakeryShortCode_Trx_Gap extends YREG_ESTATE_VC_ShortCodeCollection {}
	}
}
?>