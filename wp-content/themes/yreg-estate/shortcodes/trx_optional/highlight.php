<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('yreg_estate_sc_highlight_theme_setup')) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_sc_highlight_theme_setup' );
	function yreg_estate_sc_highlight_theme_setup() {
		add_action('yreg_estate_action_shortcodes_list', 		'yreg_estate_sc_highlight_reg_shortcodes');
		if (function_exists('yreg_estate_exists_visual_composer') && yreg_estate_exists_visual_composer())
			add_action('yreg_estate_action_shortcodes_list_vc','yreg_estate_sc_highlight_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_highlight id="unique_id" color="fore_color's_name_or_#rrggbb" backcolor="back_color's_name_or_#rrggbb" style="custom_style"]Et adipiscing integer, scelerisque pid, augue mus vel tincidunt porta[/trx_highlight]
*/

if (!function_exists('yreg_estate_sc_highlight')) {	
	function yreg_estate_sc_highlight($atts, $content=null){	
		if (yreg_estate_in_shortcode_blogger()) return '';
		extract(yreg_estate_html_decode(shortcode_atts(array(
			// Individual params
			"color" => "",
			"bg_color" => "",
			"font_size" => "",
			"type" => "1",
			// Common params
			"id" => "",
			"class" => "",
			"css" => ""
		), $atts)));
		$css .= ($color != '' ? 'color:' . esc_attr($color) . ';' : '')
			.($bg_color != '' ? 'background-color:' . esc_attr($bg_color) . ';' : '')
			.($font_size != '' ? 'font-size:' . esc_attr(yreg_estate_prepare_css_value($font_size)) . '; line-height: 1em;' : '');
		$output = '<span' . ($id ? ' id="'.esc_attr($id).'"' : '') 
				. ' class="sc_highlight'.($type>0 ? ' sc_highlight_style_'.esc_attr($type) : ''). (!empty($class) ? ' '.esc_attr($class) : '').'"'
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
				. '>' 
				. do_shortcode($content) 
				. '</span>';
		return apply_filters('yreg_estate_shortcode_output', $output, 'trx_highlight', $atts, $content);
	}
	yreg_estate_require_shortcode('trx_highlight', 'yreg_estate_sc_highlight');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_highlight_reg_shortcodes' ) ) {
	//add_action('yreg_estate_action_shortcodes_list', 'yreg_estate_sc_highlight_reg_shortcodes');
	function yreg_estate_sc_highlight_reg_shortcodes() {
	
		yreg_estate_sc_map("trx_highlight", array(
			"title" => esc_html__("Highlight text", 'yreg-estate'),
			"desc" => wp_kses_data( __("Highlight text with selected color, background color and other styles", 'yreg-estate') ),
			"decorate" => false,
			"container" => true,
			"params" => array(
				"type" => array(
					"title" => esc_html__("Type", 'yreg-estate'),
					"desc" => wp_kses_data( __("Highlight type", 'yreg-estate') ),
					"value" => "1",
					"type" => "checklist",
					"options" => array(
						0 => esc_html__('Custom', 'yreg-estate'),
						1 => esc_html__('Type 1', 'yreg-estate'),
						2 => esc_html__('Type 2', 'yreg-estate'),
						3 => esc_html__('Type 3', 'yreg-estate')
					)
				),
				"color" => array(
					"title" => esc_html__("Color", 'yreg-estate'),
					"desc" => wp_kses_data( __("Color for the highlighted text", 'yreg-estate') ),
					"divider" => true,
					"value" => "",
					"type" => "color"
				),
				"bg_color" => array(
					"title" => esc_html__("Background color", 'yreg-estate'),
					"desc" => wp_kses_data( __("Background color for the highlighted text", 'yreg-estate') ),
					"value" => "",
					"type" => "color"
				),
				"font_size" => array(
					"title" => esc_html__("Font size", 'yreg-estate'),
					"desc" => wp_kses_data( __("Font size of the highlighted text (default - in pixels, allows any CSS units of measure)", 'yreg-estate') ),
					"value" => "",
					"type" => "text"
				),
				"_content_" => array(
					"title" => esc_html__("Highlighting content", 'yreg-estate'),
					"desc" => wp_kses_data( __("Content for highlight", 'yreg-estate') ),
					"divider" => true,
					"rows" => 4,
					"value" => "",
					"type" => "textarea"
				),
				"id" => yreg_estate_get_sc_param('id'),
				"class" => yreg_estate_get_sc_param('class'),
				"css" => yreg_estate_get_sc_param('css')
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_highlight_reg_shortcodes_vc' ) ) {
	//add_action('yreg_estate_action_shortcodes_list_vc', 'yreg_estate_sc_highlight_reg_shortcodes_vc');
	function yreg_estate_sc_highlight_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_highlight",
			"name" => esc_html__("Highlight text", 'yreg-estate'),
			"description" => wp_kses_data( __("Highlight text with selected color, background color and other styles", 'yreg-estate') ),
			"category" => esc_html__('Content', 'yreg-estate'),
			'icon' => 'icon_trx_highlight',
			"class" => "trx_sc_single trx_sc_highlight",
			"content_element" => true,
			"is_container" => false,
			"show_settings_on_create" => true,
			"params" => array(
				array(
					"param_name" => "type",
					"heading" => esc_html__("Type", 'yreg-estate'),
					"description" => wp_kses_data( __("Highlight type", 'yreg-estate') ),
					"admin_label" => true,
					"class" => "",
					"value" => array(
							esc_html__('Custom', 'yreg-estate') => 0,
							esc_html__('Type 1', 'yreg-estate') => 1,
							esc_html__('Type 2', 'yreg-estate') => 2,
							esc_html__('Type 3', 'yreg-estate') => 3
						),
					"type" => "dropdown"
				),
				array(
					"param_name" => "color",
					"heading" => esc_html__("Text color", 'yreg-estate'),
					"description" => wp_kses_data( __("Color for the highlighted text", 'yreg-estate') ),
					"class" => "",
					"value" => "",
					"type" => "colorpicker"
				),
				array(
					"param_name" => "bg_color",
					"heading" => esc_html__("Background color", 'yreg-estate'),
					"description" => wp_kses_data( __("Background color for the highlighted text", 'yreg-estate') ),
					"class" => "",
					"value" => "",
					"type" => "colorpicker"
				),
				array(
					"param_name" => "font_size",
					"heading" => esc_html__("Font size", 'yreg-estate'),
					"description" => wp_kses_data( __("Font size for the highlighted text (default - in pixels, allows any CSS units of measure)", 'yreg-estate') ),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "content",
					"heading" => esc_html__("Highlight text", 'yreg-estate'),
					"description" => wp_kses_data( __("Content for highlight", 'yreg-estate') ),
					"class" => "",
					"value" => "",
					"type" => "textarea_html"
				),
				yreg_estate_get_vc_param('id'),
				yreg_estate_get_vc_param('class'),
				yreg_estate_get_vc_param('css')
			),
			'js_view' => 'VcTrxTextView'
		) );
		
		class WPBakeryShortCode_Trx_Highlight extends YREG_ESTATE_VC_ShortCodeSingle {}
	}
}
?>