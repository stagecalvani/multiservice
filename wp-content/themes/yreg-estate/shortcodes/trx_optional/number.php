<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('yreg_estate_sc_number_theme_setup')) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_sc_number_theme_setup' );
	function yreg_estate_sc_number_theme_setup() {
		add_action('yreg_estate_action_shortcodes_list', 		'yreg_estate_sc_number_reg_shortcodes');
		if (function_exists('yreg_estate_exists_visual_composer') && yreg_estate_exists_visual_composer())
			add_action('yreg_estate_action_shortcodes_list_vc','yreg_estate_sc_number_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_number id="unique_id" value="400"]
*/

if (!function_exists('yreg_estate_sc_number')) {	
	function yreg_estate_sc_number($atts, $content=null){	
		if (yreg_estate_in_shortcode_blogger()) return '';
		extract(yreg_estate_html_decode(shortcode_atts(array(
			// Individual params
			"value" => "",
			"align" => "",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$class .= ($class ? ' ' : '') . yreg_estate_get_css_position_as_classes($top, $right, $bottom, $left);
		$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
				. ' class="sc_number' 
					. (!empty($align) ? ' align'.esc_attr($align) : '') 
					. (!empty($class) ? ' '.esc_attr($class) : '') 
					. '"'
				. (!yreg_estate_param_is_off($animation) ? ' data-animation="'.esc_attr(yreg_estate_get_animation_classes($animation)).'"' : '')
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
				. '>';
		for ($i=0; $i < yreg_estate_strlen($value); $i++) {
			$output .= '<span class="sc_number_item">' . trim(yreg_estate_substr($value, $i, 1)) . '</span>';
		}
		$output .= '</div>';
		return apply_filters('yreg_estate_shortcode_output', $output, 'trx_number', $atts, $content);
	}
	yreg_estate_require_shortcode('trx_number', 'yreg_estate_sc_number');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_number_reg_shortcodes' ) ) {
	//add_action('yreg_estate_action_shortcodes_list', 'yreg_estate_sc_number_reg_shortcodes');
	function yreg_estate_sc_number_reg_shortcodes() {
	
		yreg_estate_sc_map("trx_number", array(
			"title" => esc_html__("Number", 'yreg-estate'),
			"desc" => wp_kses_data( __("Insert number or any word as set separate characters", 'yreg-estate') ),
			"decorate" => false,
			"container" => false,
			"params" => array(
				"value" => array(
					"title" => esc_html__("Value", 'yreg-estate'),
					"desc" => wp_kses_data( __("Number or any word", 'yreg-estate') ),
					"value" => "",
					"type" => "text"
				),
				"align" => array(
					"title" => esc_html__("Align", 'yreg-estate'),
					"desc" => wp_kses_data( __("Select block alignment", 'yreg-estate') ),
					"value" => "none",
					"type" => "checklist",
					"dir" => "horizontal",
					"options" => yreg_estate_get_sc_param('align')
				),
				"top" => yreg_estate_get_sc_param('top'),
				"bottom" => yreg_estate_get_sc_param('bottom'),
				"left" => yreg_estate_get_sc_param('left'),
				"right" => yreg_estate_get_sc_param('right'),
				"id" => yreg_estate_get_sc_param('id'),
				"class" => yreg_estate_get_sc_param('class'),
				"animation" => yreg_estate_get_sc_param('animation'),
				"css" => yreg_estate_get_sc_param('css')
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'yreg_estate_sc_number_reg_shortcodes_vc' ) ) {
	//add_action('yreg_estate_action_shortcodes_list_vc', 'yreg_estate_sc_number_reg_shortcodes_vc');
	function yreg_estate_sc_number_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_number",
			"name" => esc_html__("Number", 'yreg-estate'),
			"description" => wp_kses_data( __("Insert number or any word as set of separated characters", 'yreg-estate') ),
			"category" => esc_html__('Content', 'yreg-estate'),
			"class" => "trx_sc_single trx_sc_number",
			'icon' => 'icon_trx_number',
			"content_element" => true,
			"is_container" => false,
			"show_settings_on_create" => true,
			"params" => array(
				array(
					"param_name" => "value",
					"heading" => esc_html__("Value", 'yreg-estate'),
					"description" => wp_kses_data( __("Number or any word to separate", 'yreg-estate') ),
					"admin_label" => true,
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "align",
					"heading" => esc_html__("Alignment", 'yreg-estate'),
					"description" => wp_kses_data( __("Select block alignment", 'yreg-estate') ),
					"class" => "",
					"value" => array_flip(yreg_estate_get_sc_param('align')),
					"type" => "dropdown"
				),
				yreg_estate_get_vc_param('id'),
				yreg_estate_get_vc_param('class'),
				yreg_estate_get_vc_param('animation'),
				yreg_estate_get_vc_param('css'),
				yreg_estate_get_vc_param('margin_top'),
				yreg_estate_get_vc_param('margin_bottom'),
				yreg_estate_get_vc_param('margin_left'),
				yreg_estate_get_vc_param('margin_right')
			)
		) );
		
		class WPBakeryShortCode_Trx_Number extends YREG_ESTATE_VC_ShortCodeSingle {}
	}
}
?>