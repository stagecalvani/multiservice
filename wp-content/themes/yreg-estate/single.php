<?php
/**
Template Name: Single post
 */
get_header(); 

$single_style = yreg_estate_storage_get('single_style');
if (empty($single_style)) $single_style = yreg_estate_get_custom_option('single_style');

while ( have_posts() ) { the_post();
	yreg_estate_show_post_layout(
		array(
			'layout' => $single_style,
			'sidebar' => !yreg_estate_param_is_off(yreg_estate_get_custom_option('show_sidebar_main')),
			'content' => yreg_estate_get_template_property($single_style, 'need_content'),
			'terms_list' => yreg_estate_get_template_property($single_style, 'need_terms')
		)
	);
}

get_footer();
?>