<?php
// Get template args
extract(yreg_estate_template_get_args('reviews-block'));

$reviews_markup = '';
if ($avg_author > 0 || $avg_users > 0) {
	$reviews_first_author = yreg_estate_get_theme_option('reviews_first')=='author';
	$reviews_second_hide = yreg_estate_get_theme_option('reviews_second')=='hide';
	$use_tabs = !$reviews_second_hide; // && $avg_author > 0 && $avg_users > 0;
	if ($use_tabs) yreg_estate_enqueue_script('jquery-ui-tabs', false, array('jquery','jquery-ui-core'), null, true);
	$max_level = max(5, (int) yreg_estate_get_custom_option('reviews_max_level'));
	$allow_user_marks = (!$reviews_first_author || !$reviews_second_hide) && (!isset($_COOKIE['yreg_estate_votes']) || yreg_estate_strpos($_COOKIE['yreg_estate_votes'], ','.($post_data['post_id']).',')===false) && (yreg_estate_get_theme_option('reviews_can_vote')=='all' || is_user_logged_in());
	$reviews_markup = '<div class="reviews_block'.($use_tabs ? ' sc_tabs sc_tabs_style_2' : '').'">';
	$output = $marks = $users = '';
	if ($use_tabs) {
		$author_tab = '<li class="sc_tabs_title"><a href="#author_marks" class="theme_button">'.esc_html__('Author', 'yreg-estate').'</a></li>';
		$users_tab = '<li class="sc_tabs_title"><a href="#users_marks" class="theme_button">'.esc_html__('Users', 'yreg-estate').'</a></li>';
		$output .= '<ul class="sc_tabs_titles">' . ($reviews_first_author ? ($author_tab) . ($users_tab) : ($users_tab) . ($author_tab)) . '</ul>';
	}
	// Criterias list
	$field = array(
		"options" => yreg_estate_get_theme_option('reviews_criterias')
	);
	if (!empty($post_data['post_terms'][$post_data['post_taxonomy']]->terms) && is_array($post_data['post_terms'][$post_data['post_taxonomy']]->terms)) {
		foreach ($post_data['post_terms'][$post_data['post_taxonomy']]->terms as $cat) {
			$id = (int) $cat->term_id;
			$prop = yreg_estate_taxonomy_get_inherited_property($post_data['post_taxonomy'], $id, 'reviews_criterias');
			if (!empty($prop) && !yreg_estate_is_inherit_option($prop)) {
				$field['options'] = $prop;
				break;
			}
		}
	}
	// Author marks
	if ($reviews_first_author || !$reviews_second_hide) {
		$field["id"] = "reviews_marks_author";
		$field["descr"] = strip_tags($post_data['post_excerpt']);
		$field["accept"] = false;
		$marks = yreg_estate_reviews_marks_to_display(yreg_estate_reviews_marks_prepare(yreg_estate_get_custom_option('reviews_marks'), count($field['options'])));
		$output .= '<div id="author_marks" class="sc_tabs_content">' . trim(yreg_estate_reviews_get_markup($field, $marks, false, false, $reviews_first_author)) . '</div>';
	}
	// Users marks
	if (!$reviews_first_author || !$reviews_second_hide) {
		$marks = yreg_estate_reviews_marks_to_display(yreg_estate_reviews_marks_prepare(get_post_meta($post_data['post_id'], 'yreg_estate_reviews_marks2', true), count($field['options'])));
		$users = max(0, get_post_meta($post_data['post_id'], 'yreg_estate_reviews_users', true));
		$field["id"] = "reviews_marks_users";
		$field["descr"] = wp_kses_data( sprintf(__("Summary rating from <b>%s</b> user's marks.", 'yreg-estate'), $users) 
									. ' ' 
                                    . ( !isset($_COOKIE['yreg_estate_votes']) || yreg_estate_strpos($_COOKIE['yreg_estate_votes'], ','.($post_data['post_id']).',')===false
											? __('You can set own marks for this article - just click on stars above and press "Accept".', 'yreg-estate')
                                            : __('Thanks for your vote!', 'yreg-estate')
                                      ) );
		$field["accept"] = $allow_user_marks;
		$output .= '<div id="users_marks" class="sc_tabs_content"'.(!$output ? ' style="display: block;"' : '') . '>' . trim(yreg_estate_reviews_get_markup($field, $marks, $allow_user_marks, false, !$reviews_first_author)) . '</div>';
	}
	$reviews_markup .= $output . '</div>';
	if ($allow_user_marks) {
		yreg_estate_enqueue_script('jquery-ui-draggable', false, array('jquery', 'jquery-ui-core'), null, true);
		$reviews_markup .= '
			<script type="text/javascript">
				jQuery(document).ready(function() {
					YREG_ESTATE_STORAGE["reviews_allow_user_marks"] = '.($allow_user_marks ? 'true' : 'false').';
					YREG_ESTATE_STORAGE["reviews_max_level"] = '.($max_level).';
					YREG_ESTATE_STORAGE["reviews_levels"] = "'.trim(yreg_estate_get_theme_option('reviews_criterias_levels')).'";
					YREG_ESTATE_STORAGE["reviews_vote"] = "'.(isset($_COOKIE['yreg_estate_votes']) ? $_COOKIE['yreg_estate_votes'] : '').'";
					YREG_ESTATE_STORAGE["reviews_marks"] = "'.($marks).'".split(",");
					YREG_ESTATE_STORAGE["reviews_users"] = '.max(0, $users).';
					YREG_ESTATE_STORAGE["post_id"] = '.($post_data['post_id']).';
				});
			</script>
		';
	}
}
yreg_estate_storage_set('reviews_markup', $reviews_markup);
?>