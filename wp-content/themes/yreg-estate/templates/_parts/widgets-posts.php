<?php
// Get template args
extract(yreg_estate_template_get_args('widgets-posts'));

global $post;
$post_id = $post->ID;
$post_date = yreg_estate_get_date_or_difference(apply_filters('yreg_estate_filter_post_date', $post->post_date, $post->ID, $post->post_type));
$post_title = $post->post_title;
$post_link = !isset($show_links) || $show_links ? get_permalink($post_id) : '';

$output = '<article class="post_item' . ($show_image == 0 ? ' no_thumb' : ' with_thumb') . ($post_number==1 ? ' first' : '') . '">';

if ($show_image) {
	$post_thumb = yreg_estate_get_resized_image_tag($post_id, 50, 50);
	if ($post_thumb) {
		$output .= '<div class="post_thumb">' . ($post_thumb) . '</div>';
	}
}

$output .= '<div class="post_content">'
			.'<div class="post_title">'
			.($post_link ? '<a href="' . esc_url($post_link) . '">' : '') . ($post_title) . ($post_link ? '</a>' : '')
			.'</div>';


if ($show_counters) {
	if (yreg_estate_strpos($show_counters, 'views')!==false) {
		$post_counters = yreg_estate_storage_isset('post_data_'.$post_id) && yreg_estate_storage_get_array('post_data_'.$post_id, 'post_options_counters') 
							? yreg_estate_storage_get_array('post_data_'.$post_id, 'post_views') 
							: yreg_estate_get_post_views($post_id);
		$post_counters_icon = 'post_counters_views icon-eye';
	} else if (yreg_estate_strpos($show_counters, 'likes')!==false) {
		$likes = isset($_COOKIE['yreg_estate_likes']) ? $_COOKIE['yreg_estate_likes'] : '';
		$allow = yreg_estate_strpos($likes, ','.($post_id).',')===false;
		$post_counters = yreg_estate_storage_isset('post_data_'.$post_id) && yreg_estate_storage_get_array('post_data_'.$post_id, 'post_options_counters') 
							? yreg_estate_storage_get_array('post_data_'.$post_id, 'post_likes') 
							: yreg_estate_get_post_likes($post_id);
		$post_counters_icon = 'post_counters_likes icon-heart '.($allow ? 'enabled' : 'disabled');
		yreg_estate_enqueue_messages();
	} else if (yreg_estate_strpos($show_counters, 'stars')!==false || yreg_estate_strpos($show_counters, 'rating')!==false) {
		$post_counters = yreg_estate_reviews_marks_to_display(yreg_estate_storage_isset('post_data_'.$post_id) && yreg_estate_storage_get_array('post_data_'.$post_id, 'post_options_reviews')
							? yreg_estate_storage_get_array('post_data_'.$post_id, $post_rating) 
							: get_post_meta($post_id, $post_rating, true));
		$post_counters_icon = 'post_counters_rating icon-star';
	} else {
		
		if (yreg_estate_strpos($show_counters, 'hide')!==false) {
			$post_counters = ''; $post_counters_icon = '';
		} else {
			
			$post_counters = yreg_estate_storage_isset('post_data_'.$post_id) && yreg_estate_storage_get_array('post_data_'.$post_id, 'post_options_counters') 
							? yreg_estate_storage_get_array('post_data_'.$post_id, 'post_comments') 
							: get_comments_number($post_id);
		$post_counters_icon = 'post_counters_comments icon-comment';
			
		}





		
		
		
	}

	if (yreg_estate_strpos($show_counters, 'stars')!==false && $post_counters > 0) {
		if (yreg_estate_strpos($post_counters, '.')===false) 
			$post_counters .= '.0';
		if (yreg_estate_get_custom_option('show_reviews')=='yes') {
			$output .= '<div class="post_rating reviews_summary blog_reviews">'
				. '<div class="criteria_summary criteria_row">' . trim(yreg_estate_reviews_get_summary_stars($post_counters, false, false, 5)) . '</div>'
				. '</div>';
		}
	}
}
if ($show_date || $show_counters || $show_author) {
	$output .= '<div class="post_info">';
	
	if ($show_author) {
		if (yreg_estate_storage_isset('post_data_'.$post_id)) {
			$post_author_id		= yreg_estate_storage_get_array('post_data_'.$post_id, 'post_author_id');
			$post_author_name	= yreg_estate_storage_get_array('post_data_'.$post_id, 'post_author');
			$post_author_url	= yreg_estate_storage_get_array('post_data_'.$post_id, 'post_author_url');
		} else {
			$post_author_id   = $post->post_author;
			$post_author_name = get_the_author_meta('display_name', $post_author_id);
			$post_author_url  = get_author_posts_url($post_author_id, '');
		}
		$output .= '<span class="post_info_item post_info_posted_by">' . esc_html__('by', 'yreg-estate') . ' ' . ($post_link ? '<a href="' . esc_url($post_author_url) . '" class="post_info_author">' : '') . ($post_author_name) . ($post_link ? '</a>' : '') . '</span> ';
	}
	
	if ($show_date) {
		$output .= '<span class="post_info_item post_info_posted">'.esc_html__('on', 'yreg-estate').($post_link ? ' <a href="' . esc_url($post_link) . '" class="post_info_date">' : '') . ($post_date) . ($post_link ? '</a>' : '').'</span>';
	}
	
	if ($show_counters && yreg_estate_strpos($show_counters, 'stars')===false) {
		$post_counters_link = yreg_estate_strpos($show_counters, 'comments')!==false 
									? get_comments_link( $post_id ) 
									: (yreg_estate_strpos($show_counters, 'likes')!==false
									    ? '#'
									    : $post_link
									    );
		$output .= '<span class="post_info_item post_info_counters">'
			. ($post_counters_link ? '<a href="' . esc_url($post_counters_link) . '"' : '<span') 
				. ' class="post_counters_item ' . esc_attr($post_counters_icon) . '"'
				. (yreg_estate_strpos($show_counters, 'likes')!==false
					? ' title="' . ($allow ? esc_attr__('Like', 'yreg-estate') : esc_attr__('Dislike', 'yreg-estate')) . '"'
						. ' data-postid="' . esc_attr($post_id) . '"'
                        . ' data-likes="' . esc_attr($post_counters) . '"'
                        . ' data-title-like="' . esc_attr__('Like', 'yreg-estate') . '"'
                        . ' data-title-dislike="' . esc_attr__('Dislike', 'yreg-estate') . '"'
					: ''
				)
				. '>'
			. '<span class="post_counters_number">' . ($post_counters) . '</span>'
			. ($post_counters_link ? '</a>' : '</span>')
			. '</span>';
	}
	$output .= '</div>';
}
$output .= '</div>'
		.'</article>';

// Return result
yreg_estate_storage_set('widgets_posts_output', $output);
?>