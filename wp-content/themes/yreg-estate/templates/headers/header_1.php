<?php

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }


/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'yreg_estate_template_header_1_theme_setup' ) ) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_template_header_1_theme_setup', 1 );
	function yreg_estate_template_header_1_theme_setup() {
		yreg_estate_add_template(array(
			'layout' => 'header_1',
			'mode'   => 'header',
			'title'  => esc_html__('Header 1', 'yreg-estate'),
			'icon'   => yreg_estate_get_file_url('templates/headers/images/1.jpg')
			));
	}
}

// Template output
if ( !function_exists( 'yreg_estate_template_header_1_output' ) ) {
	function yreg_estate_template_header_1_output($post_options, $post_data) {

		// WP custom header
		// Get custom image (for blog) or featured image (for single)
		$header_css = ''; $header_image = '';
//		if (is_singular()) {}
		$post_id = get_the_ID();
		$post_format = get_post_format();
		$post_icon = yreg_estate_get_custom_option('icon', yreg_estate_get_post_format_icon($post_format));
		$header_image1 = wp_get_attachment_url(get_post_thumbnail_id($post_id));
		$header_image2 = yreg_estate_get_custom_option('top_panel_image');
		$header_image3 = get_header_image();

		if (empty($header_image)) {
			if (isset($header_image2) and !empty($header_image2)) {$header_image = $header_image2;}
			if (empty($header_image)) {
				$header_image = $header_image1;
			}
		}
		
		

		
//		if (empty($header_image))
//			$header_image = yreg_estate_get_custom_option('top_panel_image');
		if (empty($header_image))
			$header_image = get_header_image();
		if (!empty($header_image)) {
			/* Uncomment next rows if you want crop image
			$thumb_sizes = yreg_estate_get_thumb_sizes(array(
				'layout' => $post_options['layout']
			));
			$header_image = yreg_estate_get_resized_image_url($header_image, $thumb_sizes['w'], $thumb_sizes['h'], null, false, false, true);
			*/
			$header_css = ' style="background-image: url('.esc_url($header_image).')"';
		}
		?>
		
		<div class="top_panel_fixed_wrap"></div>

		<header class="top_panel_wrap top_panel_style_1 scheme_<?php echo esc_attr($post_options['scheme']); ?>" <?php echo trim($header_css); ?>>
			<div class="header-bg">
			<div class="top_panel_wrap_inner top_panel_inner_style_1 top_panel_position_<?php echo esc_attr(yreg_estate_get_custom_option('top_panel_position')); ?>">
			
				<div class="content_wrap clearfix">
					<div class="top_panel_logo"><?php yreg_estate_show_logo(); ?></div>
					<div class="top_panel_contacts">
						<?php
						
						$contact_phone=trim(yreg_estate_get_custom_option('contact_phone'));
						$contact_email=trim(yreg_estate_get_custom_option('contact_email'));
						$contact_address_1=trim(yreg_estate_get_custom_option('contact_address_1'));
						$contact_address_2=trim(yreg_estate_get_custom_option('contact_address_2'));
						
						?>
						
						<div class="top_panel_contacts_left">
							<?php if ( !empty($contact_address_1) and (yreg_estate_get_custom_option('show_company_address_header')=='yes') ) { ?>
							<div class="contact_phone"><?php echo force_balance_tags($contact_address_1); ?></div>
							<?php } ?>
							<?php if ( !empty($contact_email) and (yreg_estate_get_custom_option('show_email_header')=='yes') ) { ?>
							<div class="contact_email"><?php echo force_balance_tags($contact_email); ?></div>
							<?php } ?>
						</div>
						<?php if ( !empty($contact_phone) and (yreg_estate_get_custom_option('show_phone_header')=='yes') ) { ?>
						<div class="top_panel_contacts_right"><?php esc_html_e('call us:', 'yreg-estate') ?> <strong><?php echo force_balance_tags($contact_phone); ?></strong></div>
						<?php } ?>
						<div class="cL"></div>
					</div>
					<div class="top_panel_menu">
						<a href="#" class="menu_main_responsive_button icon-down"><?php esc_html_e('Select menu item', 'yreg-estate'); ?></a>
						<nav class="menu_main_nav_area">
							<?php
							$menu_main = yreg_estate_get_nav_menu('menu_main');
							if (empty($menu_main)) $menu_main = yreg_estate_get_nav_menu();
							echo trim($menu_main);
							?>
						</nav>
						<?php // if (yreg_estate_get_custom_option('show_search')=='yes') echo trim(yreg_estate_sc_search(array())); ?>
					</div>
					<div class="cL"></div>
				</div>
				

				
				
				
				
				
			<?php if (yreg_estate_get_custom_option('show_top_panel_top')=='yes') { ?>
				<div class="top_panel_top">
					<div class="content_wrap clearfix">
						<?php
						yreg_estate_template_set_args('top-panel-top', array(
							'top_panel_top_components' => array('contact_info', 'open_hours', 'login', 'socials', 'currency', 'bookmarks')
						));
						get_template_part(yreg_estate_get_file_slug('templates/headers/_parts/top-panel-top.php'));
						?>
					</div>
				</div>
			<?php } ?>

			</div>
			
		<!--</header>-->

		<?php
	}
}
?>