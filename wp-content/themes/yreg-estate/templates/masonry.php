<?php

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }


/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'yreg_estate_template_masonry_theme_setup' ) ) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_template_masonry_theme_setup', 1 );
	function yreg_estate_template_masonry_theme_setup() {
		yreg_estate_add_template(array(
			'layout' => 'masonry_2',
			'template' => 'masonry',
			'mode'   => 'blog',
			'need_terms' => 'true',
			'need_isotope' => true,
			'title'  => esc_html__('Masonry tile (different height) /2 columns/', 'yreg-estate'),
			'thumb_title'  => esc_html__('Medium image', 'yreg-estate'),
			'w'		 => 370,
			'h_crop' => 209,
			'h'      => null
		));
		yreg_estate_add_template(array(
			'layout' => 'masonry_3',
			'template' => 'masonry',
			'mode'   => 'blog',
			'need_terms' => 'true',
			'need_isotope' => true,
			'title'  => esc_html__('Masonry tile /3 columns/', 'yreg-estate'),
			'thumb_title'  => esc_html__('Medium image', 'yreg-estate'),
			'w'		 => 370,
			'h_crop' => 209,
			'h'      => null
		));
		yreg_estate_add_template(array(
			'layout' => 'classic_2',
			'template' => 'masonry',
			'mode'   => 'blog',
			'need_terms' => 'true',
			'need_isotope' => true,
			'title'  => esc_html__('Classic tile (equal height) /2 columns/', 'yreg-estate'),
			'thumb_title'  => esc_html__('Medium-x image (crop)', 'yreg-estate'),
			'w'		 => 570,
			'h'		 => 340
		));
		yreg_estate_add_template(array(
			'layout' => 'classic_3',
			'template' => 'masonry',
			'need_terms' => 'true',
			'mode'   => 'blog',
			'need_isotope' => true,
			'title'  => esc_html__('Classic tile /3 columns/', 'yreg-estate'),
			'thumb_title'  => esc_html__('Medium image (crop)', 'yreg-estate'),
			'w'		 => 370,
			'h'		 => 209
		));
		// Add template specific scripts
		add_action('yreg_estate_action_blog_scripts', 'yreg_estate_template_masonry_add_scripts');
	}
}

// Add template specific scripts
if (!function_exists('yreg_estate_template_masonry_add_scripts')) {
	//add_action('yreg_estate_action_blog_scripts', 'yreg_estate_template_masonry_add_scripts');
	function yreg_estate_template_masonry_add_scripts($style) {
		if (in_array(yreg_estate_substr($style, 0, 8), array('classic_', 'masonry_'))) {
			yreg_estate_enqueue_script( 'isotope', yreg_estate_get_file_url('js/jquery.isotope.min.js'), array(), null, true );
		}
	}
}

// Template output
if ( !function_exists( 'yreg_estate_template_masonry_output' ) ) {
	function yreg_estate_template_masonry_output($post_options, $post_data) {
		$show_title = !in_array($post_data['post_format'], array('aside', 'chat', 'status', 'link', 'quote'));
		$parts = explode('_', $post_options['layout']);
		$style = $parts[0];
		$columns = max(1, min(12, empty($post_options['columns_count']) 
									? (empty($parts[1]) ? 1 : (int) $parts[1])
									: $post_options['columns_count']
									));
		$tag = yreg_estate_in_shortcode_blogger(true) ? 'div' : 'article';
		?>
		<div class="isotope_item isotope_item_<?php echo esc_attr($style); ?> isotope_item_<?php echo esc_attr($post_options['layout']); ?> isotope_column_<?php echo esc_attr($columns); ?>
					<?php
					if ($post_options['filters'] != '') {
						if ($post_options['filters']=='categories' && !empty($post_data['post_terms'][$post_data['post_taxonomy']]->terms_ids))
							echo ' flt_' . join(' flt_', $post_data['post_terms'][$post_data['post_taxonomy']]->terms_ids);
						else if ($post_options['filters']=='tags' && !empty($post_data['post_terms'][$post_data['post_taxonomy_tags']]->terms_ids))
							echo ' flt_' . join(' flt_', $post_data['post_terms'][$post_data['post_taxonomy_tags']]->terms_ids);
					}
					?>">
			<<?php echo trim($tag); ?> class="post_item post_item_<?php echo esc_attr($style); ?> post_item_<?php echo esc_attr($post_options['layout']); ?>
				 <?php echo ' post_format_'.esc_attr($post_data['post_format']) 
					. ($post_options['number']%2==0 ? ' even' : ' odd') 
					. ($post_options['number']==0 ? ' first' : '') 
					. ($post_options['number']==$post_options['posts_on_page'] ? ' last' : '');
				?>">
				
				<?php if ($post_data['post_video'] || $post_data['post_audio'] || $post_data['post_thumb'] ||  $post_data['post_gallery']) { ?>
					<div class="post_featured">
						<?php
						yreg_estate_template_set_args('post-featured', array(
							'post_options' => $post_options,
							'post_data' => $post_data
						));
						get_template_part(yreg_estate_get_file_slug('templates/_parts/post-featured.php'));
						?>
					</div>
				<?php } ?>

				<div class="post_content isotope_item_content">
					
					<?php

					if (!$post_data['post_protected'] && $post_options['info']) {
						$post_options['info_parts'] = array('date' => false, 'author' => false,  'terms' => true,  'counters' => false);
						yreg_estate_template_set_args('post-info', array(
							'post_options' => $post_options,
							'post_data' => $post_data
						));
						get_template_part(yreg_estate_get_file_slug('templates/_parts/post-info.php'));
					}

					if ($show_title) {
						if (!isset($post_options['links']) || $post_options['links']) {
							?>
							<h3 class="post_title"><a href="<?php echo esc_url($post_data['post_link']); ?>"><?php echo trim($post_data['post_title']); ?></a></h3>
							<?php
						} else {
							?>
							<h5 class="post_title"><?php echo trim($post_data['post_title']); ?></h5>
							<?php
						}
					}
					
					if (!$post_data['post_protected'] && $post_options['info']) {
						$post_options['info_parts'] = array('date' => true, 'author' => true,  'terms' => false,  'counters' => true);
						yreg_estate_template_set_args('post-info', array(
							'post_options' => $post_options,
							'post_data' => $post_data
						));
						get_template_part(yreg_estate_get_file_slug('templates/_parts/post-info.php'));
					}
					?>
	
					<div class="post_descr">
						<?php
						if ($post_data['post_protected']) {
							echo trim($post_data['post_excerpt']); 
						} else {
							if ($post_data['post_excerpt']) {
								echo in_array($post_data['post_format'], array('quote', 'link', 'chat', 'aside', 'status')) ? $post_data['post_excerpt'] : '<p>'.trim(yreg_estate_strshort($post_data['post_excerpt'], isset($post_options['descr']) ? $post_options['descr'] : yreg_estate_get_custom_option('post_excerpt_maxlength_masonry'))).'</p>';
							}
							if (empty($post_options['readmore'])) $post_options['readmore'] = esc_html__('Read more', 'yreg-estate');
							if (!yreg_estate_param_is_off($post_options['readmore']) && !in_array($post_data['post_format'], array('quote', 'link', 'chat', 'aside', 'status'))) {
								//echo trim(yreg_estate_sc_button(array('link'=>$post_data['post_link'], 'class'=>"post_readmore"), $post_options['readmore']));
								
							echo trim(yreg_estate_sc_button(array(
								'type' => 'text',
								'link' => $post_data['post_link']
								), esc_html__('View details', 'yreg-estate')));
							}
						}
						?>
					</div>

				</div>				<!-- /.post_content -->
			</<?php echo trim($tag); ?>>	<!-- /.post_item -->
		</div>						<!-- /.isotope_item -->
		<?php
	}
}
?>