<?php

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }

/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'yreg_estate_template_single_property_theme_setup' ) ) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_template_single_property_theme_setup', 1 );
	function yreg_estate_template_single_property_theme_setup() {
		yreg_estate_add_template(array(
			'layout' => 'single-property',
			'mode'   => 'property',
			'need_content' => true,
			'need_terms' => true,
			'title'  => esc_html__('Single property', 'yreg-estate'),
			'thumb_title'  => esc_html__('Fullwidth image (crop)', 'yreg-estate'),
			'w'		 => 1170,
			'h'		 => 659
		));
	}
}

// Template output
if ( !function_exists( 'yreg_estate_template_single_property_output' ) ) {
	function yreg_estate_template_single_property_output($post_options, $post_data) {
		$post_data['post_views']++;
		$avg_author = 0;
		$avg_users  = 0;
		if (!$post_data['post_protected'] && $post_options['reviews'] && yreg_estate_get_custom_option('show_reviews')=='yes') {
			$avg_author = $post_data['post_reviews_author'];
			$avg_users  = $post_data['post_reviews_users'];
		}
		$show_title = yreg_estate_get_custom_option('show_post_title')=='yes' && (yreg_estate_get_custom_option('show_post_title_on_quotes')=='yes' || !in_array($post_data['post_format'], array('aside', 'chat', 'status', 'link', 'quote')));
		$title_tag = yreg_estate_get_custom_option('show_page_title')=='yes' ? 'h3' : 'h1';

		$property_id_web = '';
		$property_id_web = get_post_meta( $post_data['post_id'], 'yreg_estate_property_id_web', true );
		
		$property_status = get_post_meta( $post_data['post_id'], 'yreg_estate_property_status', true );
		$property_status_text = yreg_estate_property_status_text($property_status);
		$ps_single_status = '<span class="ps_single_status">'.$property_status_text.'</span>';
		
		if ( $property_status == 'rent' ) {
			$property_price_per = get_post_meta( $post_data['post_id'], 'yreg_estate_property_price_per', true );
		}
		
		$property_price = get_post_meta( $post_data['post_id'], 'yreg_estate_property_price', true );
			$property_price_text = yreg_estate_property_delimiter_text($property_price, '');
		$property_price_sign = get_post_meta( $post_data['post_id'], 'yreg_estate_property_price_sign', true );
		$property_location = get_post_meta( $post_data['post_id'], 'yreg_estate_property_location', true );
		$property_address_1 = get_post_meta( $post_data['post_id'], 'yreg_estate_property_address_1', true );
		$property_address_2 = get_post_meta( $post_data['post_id'], 'yreg_estate_property_address_2', true );
		$property_type = get_post_meta( $post_data['post_id'], 'yreg_estate_property_type', true );
		$property_area = get_post_meta( $post_data['post_id'], 'yreg_estate_property_area', true );
			$property_area_text = yreg_estate_property_delimiter_text($property_area, '') . esc_html__(' sqft', 'yreg-estate');
		$property_rooms = (int) get_post_meta( $post_data['post_id'], 'yreg_estate_property_rooms', true );
		$property_bedrooms = (int) get_post_meta( $post_data['post_id'], 'yreg_estate_property_bedrooms', true );
		$property_bathrooms = (int) get_post_meta( $post_data['post_id'], 'yreg_estate_property_bathrooms', true );
		$property_garages = (int) get_post_meta( $post_data['post_id'], 'yreg_estate_property_garages', true );
		$property_build = '';
		$property_build = get_post_meta( $post_data['post_id'], 'yreg_estate_property_build', true );
		
		$property_style = '';
		$list_style = yreg_estate_get_property_list('property_style_list');
		foreach ($list_style as $key => $value) {
			$_property_style = (int) get_post_meta( $post_data['post_id'], 'yreg_estate_property_style_list_'.md5($value), true );
			
			if ( $_property_style == 1 ) {
				$property_style .= '.'.trim($value);
			}
					
		}
		
		
		
		$property_price_box = '';
		if ( (int) $property_price != 0 ) {
			$property_price_box .= '<div class="property_price_box">';
			if ( !empty($property_price_sign) ) {
				$property_price_box .= '<span class="property_price_box_sign">'. esc_html($property_price_sign) .'</span>';
			}
			if ( !empty($property_price_text) ) {
				$property_price_box .= '<span class="property_price_box_price">'. esc_html($property_price_text) .'</span>';
			}
			if ( !empty($property_price_per) and ( $property_status == 'rent' ) ) {
				$property_price_box .= '<span class="property_price_box_per">' . esc_html__('/', 'yreg-estate') . esc_html($property_price_per) . '</span>';
			}
			$property_price_box .= '</div>';
		}
		
		
		
		
//		echo '<hr><pre>';
//		var_dump($property_style);
//		echo '</pre><hr>';
		
		
		
		
		yreg_estate_open_wrapper('<article class="' 
				. join(' ', get_post_class('itemscope'
					. ' post_item post_item_single'
					. ' post_featured_' . esc_attr($post_options['post_class'])
					. ' post_format_' . esc_attr($post_data['post_format'])))
				. '"'
				. ' itemscope itemtype="http://schema.org/'.($avg_author > 0 || $avg_users > 0 ? 'Review' : 'Article')
				. '">');
		
		if ($show_title && $post_options['location'] == 'center' && yreg_estate_get_custom_option('show_page_title')=='no') {
			?>
			<<?php echo esc_html($title_tag); ?> itemprop="<?php echo (float) $avg_author > 0 || (float) $avg_users > 0 ? 'itemReviewed' : 'headline'; ?>" class="post_title entry-title"><span class="post_icon <?php echo esc_attr($post_data['post_icon']); ?>"></span><?php echo trim($post_data['post_title']); ?></<?php echo esc_html($title_tag); ?>>
		<?php 
		}

		if (!$post_data['post_protected'] && (
			!empty($post_options['dedicated']) ||
			(yreg_estate_get_custom_option('show_featured_image')=='yes' && $post_data['post_thumb'])	// && $post_data['post_format']!='gallery' && $post_data['post_format']!='image')
		)) {
			?>
			<section class="post_featured">
			<?php
			if (!empty($post_options['dedicated'])) {
				echo trim($post_options['dedicated']);
			} else {
				yreg_estate_enqueue_popup();
				?>
				<div class="post_thumb" data-image="<?php echo esc_url($post_data['post_attachment']); ?>" data-title="<?php echo esc_attr($post_data['post_title']); ?>">
					<a class="hover_icon hover_icon_view" href="<?php echo esc_url($post_data['post_attachment']); ?>" title="<?php echo esc_attr($post_data['post_title']); ?>"><?php echo trim($ps_single_status) . trim($post_data['post_thumb']); ?></a>
				</div>
				<?php 
			}
			?>
			</section>
			<?php
		}
			
		
		yreg_estate_open_wrapper('<section class="post_content'.(!$post_data['post_protected'] && $post_data['post_edit_enable'] ? ' '.esc_attr('post_content_editor_present') : '').'" itemprop="'.($avg_author > 0 || $avg_users > 0 ? 'reviewBody' : 'articleBody').'">');

		if (!$post_data['post_protected'] && yreg_estate_get_custom_option('show_post_info')=='yes') {
			$post_options['info_parts'] = array('snippets'=>true);
			yreg_estate_template_set_args('post-info', array(
				'post_options' => $post_options,
				'post_data' => $post_data
			));
			get_template_part(yreg_estate_get_file_slug('templates/_parts/post-info.php'));
		}
		
		
		if ($show_title && $post_options['location'] != 'center' && yreg_estate_get_custom_option('show_post_title')=='yes') {
			?>
			<<?php echo esc_html($title_tag); ?> itemprop="<?php echo (float) $avg_author > 0 || (float) $avg_users > 0 ? 'itemReviewed' : 'headline'; ?>" class="post_title entry-title"><?php echo trim($post_data['post_title']); ?></<?php echo esc_html($title_tag); ?>>
			
			<div class="ps_single_info">
				
				<?php if ( ($property_id_web != '') or ($property_type != '') or ($property_style != '') ) { ?>
				<div class="ps_single_info_descr">
					<?php
					
					if ($property_id_web != '') {
						echo esc_html__( 'Property ID: ', 'yreg-estate' ) . esc_html($property_id_web);
					}
					if ($property_type != '') {
						echo esc_html__( ', ', 'yreg-estate' ) . esc_html($property_type);
					}
					if ($property_style != '') {
						echo esc_html($property_style);
					}
					echo esc_html__( '.', 'yreg-estate' );
					?>
				</div>
				<?php } ?>
				
				<?php echo $property_price_box; ?>
				
				
				<?php
					if ( ( $property_area_text != 0 ) or ( $property_rooms != 0 ) or ( $property_bedrooms != 0 ) or ( $property_bathrooms != 0 ) or ( $property_garages != 0 ) or ( $property_build != 0 ) ) {
				?>
				<div class="sc_property_info_list">
					<?php
						if ( $property_area_text != 0 ) echo '<span class="icon-area_2">' . esc_html($property_area_text) . '</span>';
						if ( $property_rooms != 0 ) echo '<span class="icon-floor_plan">' . esc_html($property_rooms) . '</span>';
						if ( $property_bedrooms != 0 ) echo '<span class="icon-bed">' . esc_html($property_bedrooms) . '</span>';
						if ( $property_bathrooms != 0 ) echo '<span class="icon-bath">' . esc_html($property_bathrooms) . '</span>';
						if ( $property_garages != 0 ) echo '<span class="icon-warehouse">' . esc_html($property_garages) . '</span>';
						if ( $property_build != 0 ) echo '<span class="icon-crane">' . esc_html($property_build) . '</span>';
					?>
				</div>
				<?php } ?>
				
				
				<div class="cL"></div>
			</div>
			
			<?php 
		}

		
			
		// Post content
		if ($post_data['post_protected']) { 
			echo trim($post_data['post_excerpt']);
			echo get_the_password_form(); 
		} else {
			if (!yreg_estate_storage_empty('reviews_markup') && yreg_estate_strpos($post_data['post_content'], yreg_estate_get_reviews_placeholder())===false) 
				$post_data['post_content'] = yreg_estate_sc_reviews(array()) . ($post_data['post_content']);
			echo trim(yreg_estate_gap_wrapper(yreg_estate_reviews_wrapper($post_data['post_content'])));
			wp_link_pages( array( 
				'before' => '<nav class="pagination_single"><span class="pager_pages">' . esc_html__( 'Pages:', 'yreg-estate' ) . '</span>', 
				'after' => '</nav>',
				'link_before' => '<span class="pager_numbers">',
				'link_after' => '</span>'
				)
			); 
			if ( yreg_estate_get_custom_option('show_post_tags') == 'yes' && !empty($post_data['post_terms'][$post_data['post_taxonomy_tags']]->terms_links)) {
				?>
				<div class="post_info post_info_bottom">
					<span class="post_info_item post_info_tags"><?php esc_html_e('Tags:', 'yreg-estate'); ?> <?php echo join(', ', $post_data['post_terms'][$post_data['post_taxonomy_tags']]->terms_links); ?></span>
				</div>
				<?php 
			}
		} 

		// Prepare args for all rest template parts
		// This parts not pop args from storage!
		yreg_estate_template_set_args('single-footer', array(
			'post_options' => $post_options,
			'post_data' => $post_data
		));

		if (!$post_data['post_protected'] && $post_data['post_edit_enable']) {
			get_template_part(yreg_estate_get_file_slug('templates/_parts/editor-area.php'));
		}
			
		yreg_estate_close_wrapper();	// .post_content
			
		if (!$post_data['post_protected']) {
			get_template_part(yreg_estate_get_file_slug('templates/_parts/author-info.php'));
			get_template_part(yreg_estate_get_file_slug('templates/_parts/share.php'));
		}

		$sidebar_present = !yreg_estate_param_is_off(yreg_estate_get_custom_option('show_sidebar_main'));
		if (!$sidebar_present) yreg_estate_close_wrapper();	// .post_item
		get_template_part(yreg_estate_get_file_slug('templates/_parts/related-posts.php'));
		if ($sidebar_present) yreg_estate_close_wrapper();		// .post_item

		if (!$post_data['post_protected']) {
			get_template_part(yreg_estate_get_file_slug('templates/_parts/comments.php'));
		}

		// Manually pop args from storage
		// after all single footer templates
		yreg_estate_template_get_args('single-footer');
	}
}
?>