<?php

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }


/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'yreg_estate_template_property_2_theme_setup' ) ) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_template_property_2_theme_setup', 1 );
	function yreg_estate_template_property_2_theme_setup() {
		yreg_estate_add_template(array(
			'layout' => 'property-2',
			'template' => 'property-2',
			'mode'   => 'property',
			/*'container_classes' => 'sc_slider_nopagination sc_slider_controls sc_slider_controls_bottom',*/
			'title'  => esc_html__('Property / Info row', 'yreg-estate'),
		));
	}
}

// Template output
if ( !function_exists( 'yreg_estate_template_property_2_output' ) ) {
	function yreg_estate_template_property_2_output($post_options, $post_data) {
		$show_title = !empty($post_data['post_title']);
		$parts = explode('_', $post_options['layout']);
		$style = $parts[0];
		$columns = max(1, min(12, empty($parts[1]) ? $post_options['columns_count'] : (int) $parts[1]));

		
		if ( !isset($post_options['slider']) ) {
			$post_options['slider'] = 'no';
		}
		
		if ( !isset($post_options['tag_animation']) ) {
			$post_options['tag_animation'] = '';
		}
		
		if ( !isset($post_options['property_image']) ) {
			$post_options['property_image'] = $post_data['post_thumb'];
		}
		
		
		$property_status = get_post_meta( $post_data['post_id'], 'yreg_estate_property_status', true );
			$property_status_text = yreg_estate_property_status_text($property_status);
		
		if ( $property_status == 'rent' ) {
			$property_price_per = get_post_meta( $post_data['post_id'], 'yreg_estate_property_price_per', true );
		}
		
		$property_price = get_post_meta( $post_data['post_id'], 'yreg_estate_property_price', true );
			$property_price_text = yreg_estate_property_delimiter_text($property_price, '');
		$property_price_sign = get_post_meta( $post_data['post_id'], 'yreg_estate_property_price_sign', true );
		$property_location = get_post_meta( $post_data['post_id'], 'yreg_estate_property_location', true );
		$property_address_1 = get_post_meta( $post_data['post_id'], 'yreg_estate_property_address_1', true );
		$property_address_2 = get_post_meta( $post_data['post_id'], 'yreg_estate_property_address_2', true );
		$property_type = get_post_meta( $post_data['post_id'], 'yreg_estate_property_type', true );
		$property_area = get_post_meta( $post_data['post_id'], 'yreg_estate_property_area', true );
			$property_area_text = yreg_estate_property_delimiter_text($property_area, '') . esc_html__(' sqft', 'yreg-estate');
		$property_rooms = (int) get_post_meta( $post_data['post_id'], 'yreg_estate_property_rooms', true );
		$property_bedrooms = (int) get_post_meta( $post_data['post_id'], 'yreg_estate_property_bedrooms', true );
		$property_bathrooms = (int) get_post_meta( $post_data['post_id'], 'yreg_estate_property_bathrooms', true );
		$property_garages = (int) get_post_meta( $post_data['post_id'], 'yreg_estate_property_garages', true );
		$property_build = get_post_meta( $post_data['post_id'], 'yreg_estate_property_build', true );
		
		
		$property_price_box = '';
		if ( (int) $property_price != 0 ) {
			$property_price_box .= '<div class="property_price_box">';
			if ( !empty($property_price_sign) ) {
				$property_price_box .= '<span class="property_price_box_sign">'. esc_html($property_price_sign) .'</span>';
			}
			if ( !empty($property_price_text) ) {
				$property_price_box .= '<span class="property_price_box_price">'. esc_html($property_price_text) .'</span>';
			}
			if ( !empty($property_price_per) and ( $property_status == 'rent' ) ) {
				$property_price_box .= '<span class="property_price_box_per">' . esc_html__('/', 'yreg-estate') . esc_html($property_price_per) . '</span>';
			}
			$property_price_box .= '</div>';
		}
		
		
		
		?>
			<div<?php echo !empty($post_options['tag_id']) ? ' id="'.esc_attr($post_options['tag_id']).'"' : ''; ?>
				class="sc_property_item sc_property_item_<?php echo esc_attr($post_options['number']) . ($post_options['number'] % 2 == 1 ? ' odd' : ' even') . ($post_options['number'] == 1 ? ' first' : '') . (!empty($post_options['tag_class']) ? ' '.esc_attr($post_options['tag_class']) : ''); ?>"<?php echo (!empty($post_options['tag_css']) ? ' style="'.esc_attr($post_options['tag_css']).'"' : '') . (!yreg_estate_param_is_off($post_options['tag_animation']) ? ' data-animation="'.esc_attr(yreg_estate_get_animation_classes($post_options['tag_animation'])).'"' : ''); ?>>

				
				
			<div class="ps_single_info">
				
				<?php echo $property_price_box; ?>
				
				<div class="sc_property_info_list">
					
					<?php
						if ( $property_area_text != 0 ) echo '<span class="icon-area_2">' . esc_html($property_area_text) . '</span>';
//						if ( $property_rooms != 0 ) echo '<span class="icon-floor_plan">' . esc_html($property_rooms) . '</span>';
						if ( $property_bedrooms != 0 ) echo '<span class="icon-bed">' . esc_html($property_bedrooms) . '</span>';
						if ( $property_bathrooms != 0 ) echo '<span class="icon-bath">' . esc_html($property_bathrooms) . '</span>';
						if ( $property_garages != 0 ) echo '<span class="icon-warehouse">' . esc_html($property_garages) . '</span>';
//						if ( $property_build != '' ) echo '<span class="icon-crane">' . esc_html($property_build) . '</span>';
					?>
					
				</div>
				<div class="cL"></div>
			</div>
				
				
				
				
				
			</div>
		<?php
	}
}
?>