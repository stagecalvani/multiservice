<?php

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }


/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'yreg_estate_template_property_5_theme_setup' ) ) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_template_property_5_theme_setup', 1 );
	function yreg_estate_template_property_5_theme_setup() {
		yreg_estate_add_template(array(
			'layout' => 'property-5',
			'template' => 'property-5',
			'mode'   => 'property',
			'need_terms' => 'true',
			'need_columns' => 'true',
			/*'container_classes' => 'sc_slider_nopagination sc_slider_controls sc_slider_controls_bottom',*/
			'title'  => esc_html__('Property / Slider 2', 'yreg-estate')
		));
	}
}

// Template output
if ( !function_exists( 'yreg_estate_template_property_5_output' ) ) {
	function yreg_estate_template_property_5_output($post_options, $post_data) {
		$show_title = !empty($post_data['post_title']);
		$parts = explode('_', $post_options['layout']);
		$style = $parts[0];
		$columns = max(1, min(12, empty($parts[1]) ? (!empty($post_options['columns_count']) ? $post_options['columns_count'] : 1) : (int) $parts[1]));


		if ( !isset($post_options['slider']) ) {
			$post_options['slider'] = 'no';
		}
		
		if ( !isset($post_options['tag_animation']) ) {
			$post_options['tag_animation'] = '';
		}
		
		if ( !isset($post_options['property_image']) ) {
			$post_options['property_image'] = $post_data['post_thumb'];
		}
		
		
		$property_status = get_post_meta( $post_data['post_id'], 'yreg_estate_property_status', true );
			$property_status_text = yreg_estate_property_status_text($property_status);
		
		if ( $property_status == 'rent' ) {
			$property_price_per = get_post_meta( $post_data['post_id'], 'yreg_estate_property_price_per', true );
		}
		
		$property_price = get_post_meta( $post_data['post_id'], 'yreg_estate_property_price', true );
			$property_price_text = yreg_estate_property_delimiter_text($property_price, '');
		$property_price_sign = get_post_meta( $post_data['post_id'], 'yreg_estate_property_price_sign', true );
		$property_location = get_post_meta( $post_data['post_id'], 'yreg_estate_property_location', true );
		$property_address_1 = get_post_meta( $post_data['post_id'], 'yreg_estate_property_address_1', true );
		$property_address_2 = get_post_meta( $post_data['post_id'], 'yreg_estate_property_address_2', true );
		$property_type = get_post_meta( $post_data['post_id'], 'yreg_estate_property_type', true );
		$property_area = get_post_meta( $post_data['post_id'], 'yreg_estate_property_area', true );
			$property_area_text = yreg_estate_property_delimiter_text($property_area, '') . esc_html__(' sqft', 'yreg-estate');
		$property_rooms = (int) get_post_meta( $post_data['post_id'], 'yreg_estate_property_rooms', true );
		$property_bedrooms = (int) get_post_meta( $post_data['post_id'], 'yreg_estate_property_bedrooms', true );
		$property_bathrooms = (int) get_post_meta( $post_data['post_id'], 'yreg_estate_property_bathrooms', true );
		$property_garages = (int) get_post_meta( $post_data['post_id'], 'yreg_estate_property_garages', true );
		$property_build = get_post_meta( $post_data['post_id'], 'yreg_estate_property_build', true );
//		$property_gallery = get_post_meta( $post_data['post_id'], 'yreg_estate_property_gallery', true );
		
		
		$property_price_box = '';
		if ( (int) $property_price != 0 ) {
			$property_price_box .= '<div class="property_price_box">';
			if ( !empty($property_price_sign) ) {
				$property_price_box .= '<span class="property_price_box_sign">'. esc_html($property_price_sign) .'</span>';
			}
			if ( !empty($property_price_text) ) {
				$property_price_box .= '<span class="property_price_box_price">'. esc_html($property_price_text) .'</span>';
			}
			if ( !empty($property_price_per) and ( $property_status == 'rent' ) ) {
				$property_price_box .= '<span class="property_price_box_per">' . esc_html__('/', 'yreg-estate') . esc_html($property_price_per) . '</span>';
			}
			$property_price_box .= '</div>';
		}
		?>
			
	<div class="content_wrap">
		
		<div class="pr_info_list">
			<span class="icon-area_2"></span>
			<div class="pr_info_list_box">
				<?php echo '<div class="pr_info_list_var">'.esc_html($property_area).'</div>'; ?>
				<?php echo '<div class="pr_info_list_text">'.esc_html__('Square Feet', 'yreg-estate').'</div>'; ?>
			</div>
		</div>
		
		<div class="pr_info_list">
			<span class="icon-bed"></span>
			<div class="pr_info_list_box">
				<?php echo '<div class="pr_info_list_var">'.esc_html($property_bedrooms).'</div>'; ?>
				<?php echo '<div class="pr_info_list_text">'.esc_html__('bedrooms', 'yreg-estate').'</div>'; ?>
			</div>
		</div>
		
		<div class="pr_info_list">
			<span class="icon-bath"></span>
			<div class="pr_info_list_box">
				<?php echo '<div class="pr_info_list_var">'.esc_html($property_bathrooms).'</div>'; ?>
				<?php echo '<div class="pr_info_list_text">'.esc_html__('bathrooms', 'yreg-estate').'</div>'; ?>
			</div>
		</div>
		
		<div class="pr_info_list">
			<span class="icon-warehouse"></span>
			<div class="pr_info_list_box">
				<?php echo '<div class="pr_info_list_var">'.esc_html($property_garages).'</div>'; ?>
				<?php echo '<div class="pr_info_list_text">'.esc_html__('car parking', 'yreg-estate').'</div>'; ?>
			</div>
		</div>
		
		<div class="pr_info_list pr_info_list_build">
			<span class="icon-crane"></span>
			<div class="pr_info_list_box">
				<?php echo '<div class="pr_info_list_var">'.esc_html($property_build).'</div>'; ?>
				<?php echo '<div class="pr_info_list_text">'.esc_html__('year built', 'yreg-estate').'</div>'; ?>
			</div>
		</div>
		<div class="cL"></div>
	</div>





		<?php
	}
}
?>