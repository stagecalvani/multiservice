<?php

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }


/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'yreg_estate_template_property_6_theme_setup' ) ) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_template_property_6_theme_setup', 1 );
	function yreg_estate_template_property_6_theme_setup() {
		yreg_estate_add_template(array(
			'layout' => 'property-6',
			'template' => 'property-6',
			'mode'   => 'property',
			'need_terms' => 'true',
			'need_columns' => 'true',
			/*'container_classes' => 'sc_slider_nopagination sc_slider_controls sc_slider_controls_bottom',*/
			'title'  => esc_html__('Property / Slider 3', 'yreg-estate')
		));
	}
}

// Template output
if ( !function_exists( 'yreg_estate_template_property_6_output' ) ) {
	function yreg_estate_template_property_6_output($post_options, $post_data) {
		$show_title = !empty($post_data['post_title']);
		$parts = explode('_', $post_options['layout']);
		$style = $parts[0];


		if ( !isset($post_options['slider']) ) {
			$post_options['slider'] = 'no';
		}
		
		if ( !isset($post_options['tag_animation']) ) {
			$post_options['tag_animation'] = '';
		}
		
		if ( !isset($post_options['property_image']) ) {
			$post_options['property_image'] = $post_data['post_thumb'];
		}
		
		
		$property_status = get_post_meta( $post_data['post_id'], 'yreg_estate_property_status', true );
			$property_status_text = yreg_estate_property_status_text($property_status);
		if ( $property_status == 'rent' ) {
			$property_price_per = get_post_meta( $post_data['post_id'], 'yreg_estate_property_price_per', true );
		}
		$property_price = get_post_meta( $post_data['post_id'], 'yreg_estate_property_price', true );
			$property_price_text = yreg_estate_property_delimiter_text($property_price, '');
		$property_price_sign = get_post_meta( $post_data['post_id'], 'yreg_estate_property_price_sign', true );
		$property_address_1 = get_post_meta( $post_data['post_id'], 'yreg_estate_property_address_1', true );
		$property_address_2 = get_post_meta( $post_data['post_id'], 'yreg_estate_property_address_2', true );
		$property_type = get_post_meta( $post_data['post_id'], 'yreg_estate_property_type', true );
		
		$property_price_box = '';
		if ( (int) $property_price != 0 ) {
			$property_price_box .= '';
			if ( !empty($property_price_sign) ) {
				$property_price_box .= '<span>'. esc_html($property_price_sign) .'</span>';
			}
			if ( !empty($property_price_text) ) {
				$property_price_box .= ''. esc_html($property_price_text) .'';
			}
			if ( !empty($property_price_per) and ( $property_status == 'rent' ) ) {
				$property_price_box .= '<span>' . esc_html__('/', 'yreg-estate') . esc_html($property_price_per) . '</span>';
			}
			$property_price_box .= '';
		}
		?>


<div class="sc_property_item">
	
	<div class="sc_pr_h1"><div class="sc_pr_h2"><?php echo esc_html($property_type) . esc_html__(' for ', 'yreg-estate') . esc_html($property_status_text); ?></div></div>
	<div class="sc_pr_t1">
		<?php
		echo (!empty($post_data['post_link']) ? '<a href="'.esc_url($post_data['post_link']).'">'	: '');
		echo esc_html($property_address_1);
		echo (!empty($post_data['post_link']) ? '</a>' : '');
		?>
	</div>
	<div class="sc_pr_t2"><?php echo esc_html($property_address_2); ?></div>
	<div class="sc_pr_f1">
		<div class="sc_pr_f11"><div class="sc_pr_f111"><span><?php echo esc_html($property_type) . esc_html__(' for ', 'yreg-estate') . esc_html($property_status_text); ?></span></div></div>
		<!--<div class="sc_pr_f12"><span>$</span>745,000</div>-->
		<div class="sc_pr_f12"><?php echo $property_price_box; ?></div>
	</div>
	
</div>
			
	




		<?php
	}
}
?>