<?php

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }


/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'yreg_estate_template_services_2_theme_setup' ) ) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_template_services_2_theme_setup', 1 );
	function yreg_estate_template_services_2_theme_setup() {
		yreg_estate_add_template(array(
			'layout' => 'services-2',
			'template' => 'services-2',
			'mode'   => 'services',
			'need_columns' => true,
			'title'  => esc_html__('Services /Style 2/', 'yreg-estate'),
		));
	}
}

// Template output
if ( !function_exists( 'yreg_estate_template_services_2_output' ) ) {
	function yreg_estate_template_services_2_output($post_options, $post_data) {
		$show_title = !empty($post_data['post_title']);
		$parts = explode('_', $post_options['layout']);
		$style = $parts[0];
		$columns = max(1, min(12, empty($parts[1]) ? (!empty($post_options['columns_count']) ? $post_options['columns_count'] : 1) : (int) $parts[1]));
		if (yreg_estate_param_is_on($post_options['slider'])) {
			?><div class="swiper-slide" data-style="<?php echo esc_attr($post_options['tag_css_wh']); ?>" style="<?php echo esc_attr($post_options['tag_css_wh']); ?>"><div class="sc_services_item_wrap"><?php
		} else if ($columns > 1) {
			?><div class="column-1_<?php echo esc_attr($columns); ?> column_padding_bottom"><?php
		}
		?>
			<div<?php echo !empty($post_options['tag_id']) ? ' id="'.esc_attr($post_options['tag_id']).'"' : ''; ?>
				class="sc_services_item sc_services_item_<?php echo esc_attr($post_options['number']) . ($post_options['number'] % 2 == 1 ? ' odd' : ' even') . ($post_options['number'] == 1 ? ' first' : '') . (!empty($post_options['tag_class']) ? ' '.esc_attr($post_options['tag_class']) : ''); ?>"
				<?php echo (!empty($post_options['tag_css']) ? ' style="'.esc_attr($post_options['tag_css']).'"' : '') 
					. (!yreg_estate_param_is_off($post_options['tag_animation']) ? ' data-animation="'.esc_attr(yreg_estate_get_animation_classes($post_options['tag_animation'])).'"' : ''); ?>>
				
				
				<div class="sc_services_item_number">
					<?php echo esc_html(trim($post_options['number'])) . '.';  ?>
				</div>
				
				<div class="sc_services_item_content eMaxHBox">
					<?php
					if ($show_title) {
						if ((!isset($post_options['links']) || $post_options['links']) && !empty($post_data['post_link'])) {
							?><div class="sc_services_item_title"><a href="<?php echo esc_url($post_data['post_link']); ?>"><?php echo trim($post_data['post_title']); ?></a></div><?php
						} else {
							?><div class="sc_services_item_title"><?php echo trim($post_data['post_title']); ?></div><?php
						}
					}
					?>
					
					<div class="sc_services_item_description eMaxH">
						<?php
						if ($post_data['post_protected']) {
							echo trim($post_data['post_excerpt']); 
						} else {
							if ($post_data['post_excerpt']) {
								echo '<p>'.trim($post_data['post_excerpt']).'</p>';
								
//								echo in_array($post_data['post_format'], array('quote', 'link', 'chat', 'aside', 'status')) ? $post_data['post_excerpt'] : '<p>'.trim(yreg_estate_strshort($post_data['post_excerpt'], isset($post_options['descr']) ? $post_options['descr'] : yreg_estate_get_custom_option('post_excerpt_maxlength_masonry'))).'</p>';
								
							}
						}
						?>
					</div>
					
				</div>
				<div class="cL"></div>
				<div class="sc_services_item_button">
					<?php
					
					if (!empty($post_data['post_link']) && !yreg_estate_param_is_off($post_options['readmore'])) { ?>
					<a href="<?php echo esc_url($post_data['post_link']); ?>" class="sc_button sc_button_text sc_button_style_style1 sc_button_size_small margin_right_tiny  sc_button_iconed icon-right-dir" ><?php echo trim($post_options['readmore']); ?></a>
					<?php } ?>
				</div>
				
				
				
				
				
			</div>
		<?php
		if (yreg_estate_param_is_on($post_options['slider'])) {
			?></div></div><?php
		} else if ($columns > 1) {
			?></div><?php
		}
	}
}
?>