<?php

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }


/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'yreg_estate_template_team_2_theme_setup' ) ) {
	add_action( 'yreg_estate_action_before_init_theme', 'yreg_estate_template_team_2_theme_setup', 1 );
	function yreg_estate_template_team_2_theme_setup() {
		yreg_estate_add_template(array(
			'layout' => 'team-2',
			'template' => 'team-2',
			'mode'   => 'team',
			/*'container_classes' => 'sc_slider_pagination sc_slider_pagination_bottom sc_slider_nocontrols',*/
			'title'  => esc_html__('Team /Style 2/', 'yreg-estate'),
			'thumb_title'  => esc_html__('Medium square image (crop)', 'yreg-estate'),
			'w' => 370,
			'h' => 370
		));
	}
}

// Template output
if ( !function_exists( 'yreg_estate_template_team_2_output' ) ) {
	function yreg_estate_template_team_2_output($post_options, $post_data) {
		$show_title = true;
		$parts = explode('_', $post_options['layout']);
		$style = $parts[0];
		$columns = max(1, min(12, empty($parts[1]) ? (!empty($post_options['columns_count']) ? $post_options['columns_count'] : 1) : (int) $parts[1]));
		
		
//		echo '<hr><pre>';
//		var_dump($post_options);
//		echo '</pre><hr>';



		if (yreg_estate_param_is_on($post_options['slider'])) {
			?><div class="swiper-slide" data-style="<?php echo esc_attr($post_options['tag_css_wh']); ?>" style="<?php echo esc_attr($post_options['tag_css_wh']); ?>"><?php
		} else if ($columns > 1) {
			?><div class="column-1_<?php echo esc_attr($columns); ?> column_padding_bottom"><?php
		}
		?>
			<div<?php echo !empty($post_options['tag_id']) ? ' id="'.esc_attr($post_options['tag_id']).'"' : ''; ?>
				class="sc_team_item sc_team_item_<?php echo esc_attr($post_options['number']) . ($post_options['number'] % 2 == 1 ? ' odd' : ' even') . ($post_options['number'] == 1 ? ' first' : ''). (!empty($post_options['tag_class']) ? ' '.esc_attr($post_options['tag_class']) : ''); ?> columns_wrap"
				<?php echo (!empty($post_options['tag_css']) ? ' style="'.esc_attr($post_options['tag_css']).'"' : '') 
					. (!yreg_estate_param_is_off($post_options['tag_animation']) ? ' data-animation="'.esc_attr(yreg_estate_get_animation_classes($post_options['tag_animation'])).'"' : ''); ?>
			><div class="sc_team_item_avatar">
					<?php echo trim($post_options['photo']); ?>
			</div>
			<div class="sc_team_item_info">
				<div class="sc_team_item_title"><?php echo (!empty($post_options['link']) ? '<a href="'.esc_url($post_options['link']).'">' : '') . ($post_data['post_title']) . (!empty($post_options['link']) ? '</a>' : ''); ?></div>
				<div class="sc_team_item_position"><?php echo trim($post_options['position']);?></div>
				<?php echo trim($post_options['socials']); ?>
			</div>
				<div class="cL"></div>
				
				<?php
				if (!empty($post_options['phone'])) {
					echo '<div class="sc_team_item_phone"><span class="icon-mobile29"></span>'.trim($post_options['phone']).'</div>';
				}
				if (!empty($post_options['email'])) {
					echo '<div class="sc_team_item_email"><span class="icon-message106"></span>'.trim($post_options['email']).'</div>';
				}
				?>
			
			</div>
		<?php
		if (yreg_estate_param_is_on($post_options['slider']) || $columns > 1) {
			?></div><?php
		}
	}
}
?>