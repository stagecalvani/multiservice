<?php

/**
 * Theme Widget: Property search
 */


// Load widget
if (!function_exists('yreg_estate_widget_property_search_load')) {
	add_action( 'widgets_init', 'yreg_estate_widget_property_search_load' );
	function yreg_estate_widget_property_search_load() {
		register_widget('yreg_estate_widget_property_search');
	}
}

// Widget Class
class yreg_estate_widget_property_search extends WP_Widget {
	
	function __construct() {
		$widget_ops = array('classname' => 'widget_property_search scheme_dark scps', 'description' => esc_html__('Property search (extended)', 'yreg-estate'));
		parent::__construct( 'yreg_estate_widget_property_search', esc_html__('yregEstate - Property search', 'yreg-estate'), $widget_ops );

		// Add thumb sizes into list
		yreg_estate_add_thumb_sizes( array( 'layout' => 'widgets', 'w' => 75, 'h' => 75, 'title'=>esc_html__('Widgets', 'yreg-estate') ) );
	}
	
	
	// Show widget
	function widget($args, $instance) {
		extract($args);

		
		$url = ''; $id = '';
		$page = 'property';
		$url = yreg_estate_property_get_stream_page_link($url, $page);
		$title = apply_filters('widget_title', isset($instance['title']) ? $instance['title'] : '');
		$show_keyword = isset($instance['show_keyword']) ? (int) $instance['show_keyword'] : 0;
		$show_status = isset($instance['show_status']) ? (int) $instance['show_status'] : 0;
		
		$show_location = isset($instance['show_location']) ? (int) $instance['show_location'] : 0;
		$show_type = isset($instance['show_type']) ? (int) $instance['show_type'] : 0;
		$show_style = isset($instance['show_style']) ? (int) $instance['show_style'] : 0;
		
		$show_rooms = isset($instance['show_rooms']) ? (int) $instance['show_rooms'] : 0;
		$show_bedrooms = isset($instance['show_bedrooms']) ? (int) $instance['show_bedrooms'] : 0;
		$show_bathrooms = isset($instance['show_keyword']) ? (int) $instance['show_bathrooms'] : 0;
		$show_garages = isset($instance['show_garages']) ? (int) $instance['show_garages'] : 0;
		
		$show_area = isset($instance['show_area']) ? (int) $instance['show_area'] : 0;
		$show_price = isset($instance['show_price']) ? (int) $instance['show_price'] : 0;
		
		$show_amenities = isset($instance['show_keyword']) ? (int) $instance['show_amenities'] : 0;
		$show_options = isset($instance['show_keyword']) ? (int) $instance['show_options'] : 0;
		
		
		$output = '';
		$output .= '<form method="get" action="' . esc_url($url) . '">';
		
		
		/* ***** show_keyword ***** */
		if ( $show_keyword == 1 ) {
			$ps_keyword = '';
			if ( isset($_GET['ps_keyword']) ) {
				$ps_keyword = htmlspecialchars(trim($_GET['ps_keyword']));
				if ( strlen($ps_keyword) <= 2 ) {
					$ps_keyword = '';
				}
			}
			$output .= '<input type="text" class="mb" name="ps_keyword" placeholder="' . esc_attr__('Keyword', 'yreg-estate') . '" value="' . esc_html($ps_keyword) . '" >';
		}
		
		/* ***** show_status ***** */
		if ( $show_status == 1 ) {
			$ps_status = '';
			if ( isset($_GET['ps_status']) ) {
				$ps_status = htmlspecialchars(trim($_GET['ps_status']));
			}
			$output .= '<select class="mb" name="ps_status">';
			if ( $ps_status=='-1' ) { $selected="selected"; } else { $selected=""; }
			$output .= '<option '. esc_html($selected).' value="-1">'.esc_html__('Property Status', 'yreg-estate').'</option>';
			if ( $ps_status=='sale' ) { $selected="selected"; } else { $selected=""; }
			$output .= '<option '. esc_html($selected).' value="sale">'.esc_html__('For sale', 'yreg-estate').'</option>';
			if ( $ps_status=='rent' ) { $selected="selected"; } else { $selected=""; }
			$output .= '<option '. esc_html($selected).' value="rent">'.esc_html__('For rent', 'yreg-estate').'</option>';
			$output .= '</select>';
		}
		
		/* ***** show_location ***** */
		if ( $show_location == 1 ) {
			$ps_location = '';
			if ( isset($_GET['ps_location']) ) {
				$ps_location = htmlspecialchars(trim($_GET['ps_location']));
			}
			$list_location = yreg_estate_get_property_list('property_location_list');
			$output .= '<select class="mb" name="ps_location">';
			if ( $ps_location=='-1' ) { $selected="selected"; } else { $selected=""; }
			$output .= '<option '. esc_html($selected).' value="-1">'.esc_html__('Property Location', 'yreg-estate').'</option>';
			foreach ($list_location as $key => $value) {
				if ( $ps_location == $value ) { $selected="selected"; } else { $selected=""; }
				$output .= '<option '. esc_html($selected).' value="' . esc_html($key) . '">' . esc_html($value) . '</option>';
			}
			$output .= '</select>';
		}
		
		/* ***** show_type ***** */
		if ( $show_type == 1 ) {
			$ps_type = '';
			if ( isset($_GET['ps_type']) ) {
				$ps_type = htmlspecialchars(trim($_GET['ps_type']));
			}
			$list_type = yreg_estate_get_property_list('property_type_list');
			$output .= '<select class="mb" name="ps_type">';
			if ( $ps_type=='-1' ) { $selected="selected"; } else { $selected=""; }
			$output .= '<option '. esc_html($selected).' value="-1">'.esc_html__('Property Type', 'yreg-estate').'</option>';
			foreach ($list_type as $key => $value) {
				if ( $ps_type == $value ) { $selected="selected"; } else { $selected=""; }
				$output .= '<option '. esc_html($selected).' value="' . esc_html($key) . '">' . esc_html($value) . '</option>';
			}
			$output .= '</select>';
		}
		
		/* ***** show_style ***** */
		if ( $show_style == 1 ) {
			$ps_style = '';
			if ( isset($_GET['ps_style']) ) {
				$ps_style = htmlspecialchars(trim($_GET['ps_style']));
			}
			$list_style = yreg_estate_get_property_list('property_style_list');
			$output .= '<select class="mb" name="ps_style">';
			if ( $ps_style=='-1' ) { $selected="selected"; } else { $selected=""; }
			$output .= '<option '. esc_html($selected).' value="-1">'.esc_html__('Property Style', 'yreg-estate').'</option>';
			foreach ($list_style as $key => $value) {
				if ( $ps_style == $value ) { $selected="selected"; } else { $selected=""; }
				$output .= '<option '. esc_html($selected).' value="' . esc_html($value) . '">' . esc_html($value) . '</option>';
			}
			$output .= '</select>';
		}
		
		/* ***** show_rooms ***** */
		if ( $show_rooms == 1 ) {
			$ps_rooms = '';
			if ( isset($_GET['ps_rooms']) ) {
				$ps_rooms = htmlspecialchars(trim($_GET['ps_rooms']));
			}
			$output .= '<select class="mb" name="ps_rooms">';
			if ( $ps_rooms=='-1' ) { $selected="selected"; } else { $selected=""; }
			$output .= '<option '. esc_html($selected).' value="-1">'.esc_html__('Total Rooms', 'yreg-estate').'</option>';
			if ( $ps_rooms=='1' ) { $selected="selected"; } else { $selected=""; }
			$output .= '<option '. esc_html($selected).' value="1">'.esc_html__('Rooms at least 1', 'yreg-estate').'</option>';
			if ( $ps_rooms=='2' ) { $selected="selected"; } else { $selected=""; }
			$output .= '<option '. esc_html($selected).' value="2">'.esc_html__('Rooms at least 2', 'yreg-estate').'</option>';
			if ( $ps_rooms=='3' ) { $selected="selected"; } else { $selected=""; }
			$output .= '<option '. esc_html($selected).' value="3">'.esc_html__('Rooms at least 3', 'yreg-estate').'</option>';
			if ( $ps_rooms=='4' ) { $selected="selected"; } else { $selected=""; }
			$output .= '<option '. esc_html($selected).' value="4">'.esc_html__('Rooms at least 4', 'yreg-estate').'</option>';
			if ( $ps_rooms=='5' ) { $selected="selected"; } else { $selected=""; }
			$output .= '<option '. esc_html($selected).' value="5">'.esc_html__('Rooms 5 or more', 'yreg-estate').'</option>';
			$output .= '</select>';
		}
		
		/* ***** show_bedrooms ***** */
		if ( $show_bedrooms == 1 ) {
			$ps_bedrooms = '';
			if ( isset($_GET['ps_bedrooms']) ) {
				$ps_bedrooms = htmlspecialchars(trim($_GET['ps_bedrooms']));
			}
			$output .= '<select class="mb" name="ps_bedrooms">';
			if ( $ps_bedrooms=='-1' ) { $selected="selected"; } else { $selected=""; }
			$output .= '<option '. esc_html($selected).' value="-1">'.esc_html__('Bedrooms', 'yreg-estate').'</option>';
			if ( $ps_bedrooms=='1' ) { $selected="selected"; } else { $selected=""; }
			$output .= '<option '. esc_html($selected).' value="1">'.esc_html__('Bedrooms at least 1', 'yreg-estate').'</option>';
			if ( $ps_bedrooms=='2' ) { $selected="selected"; } else { $selected=""; }
			$output .= '<option '. esc_html($selected).' value="2">'.esc_html__('Bedrooms at least 2', 'yreg-estate').'</option>';
			if ( $ps_bedrooms=='3' ) { $selected="selected"; } else { $selected=""; }
			$output .= '<option '. esc_html($selected).' value="3">'.esc_html__('Bedrooms at least 3', 'yreg-estate').'</option>';
			if ( $ps_bedrooms=='4' ) { $selected="selected"; } else { $selected=""; }
			$output .= '<option '. esc_html($selected).' value="4">'.esc_html__('Bedrooms at least 4', 'yreg-estate').'</option>';
			if ( $ps_bedrooms=='5' ) { $selected="selected"; } else { $selected=""; }
			$output .= '<option '. esc_html($selected).' value="5">'.esc_html__('Bedrooms 5 or more', 'yreg-estate').'</option>';
			$output .= '</select>';
		}
		
		/* ***** show_bathrooms ***** */
		if ( $show_bathrooms == 1 ) {
			$ps_bathrooms = '';
			if ( isset($_GET['ps_bathrooms']) ) {
				$ps_bathrooms = htmlspecialchars(trim($_GET['ps_bathrooms']));
			}
			$output .= '<select class="mb" name="ps_bathrooms">';
			if ( $ps_bathrooms=='-1' ) { $selected="selected"; } else { $selected=""; }
			$output .= '<option '. esc_html($selected).' value="-1">'.esc_html__('Bathrooms', 'yreg-estate').'</option>';
			if ( $ps_bathrooms=='1' ) { $selected="selected"; } else { $selected=""; }
			$output .= '<option '. esc_html($selected).' value="1">'.esc_html__('Bathrooms at least 1', 'yreg-estate').'</option>';
			if ( $ps_bathrooms=='2' ) { $selected="selected"; } else { $selected=""; }
			$output .= '<option '. esc_html($selected).' value="2">'.esc_html__('Bathrooms at least 2', 'yreg-estate').'</option>';
			if ( $ps_bathrooms=='3' ) { $selected="selected"; } else { $selected=""; }
			$output .= '<option '. esc_html($selected).' value="3">'.esc_html__('Bathrooms at least 3', 'yreg-estate').'</option>';
			if ( $ps_bathrooms=='4' ) { $selected="selected"; } else { $selected=""; }
			$output .= '<option '. esc_html($selected).' value="4">'.esc_html__('Bathrooms at least 4', 'yreg-estate').'</option>';
			if ( $ps_bathrooms=='5' ) { $selected="selected"; } else { $selected=""; }
			$output .= '<option '. esc_html($selected).' value="5">'.esc_html__('Bathrooms 5 or more', 'yreg-estate').'</option>';
			$output .= '</select>';
		}
		
		/* ***** show_garages ***** */
		if ( $show_garages == 1 ) {
			$ps_garages = '';
			if ( isset($_GET['ps_garages']) ) {
				$ps_garages = htmlspecialchars(trim($_GET['ps_garages']));
			}
			$output .= '<select class="mb" name="ps_garages">';
			if ( $ps_garages=='-1' ) { $selected="selected"; } else { $selected=""; }
			$output .= '<option '. esc_html($selected).' value="-1">'.esc_html__('Car Parking', 'yreg-estate').'</option>';
			if ( $ps_garages=='1' ) { $selected="selected"; } else { $selected=""; }
			$output .= '<option '. esc_html($selected).' value="1">'.esc_html__('Car Parking at least 1', 'yreg-estate').'</option>';
			if ( $ps_garages=='2' ) { $selected="selected"; } else { $selected=""; }
			$output .= '<option '. esc_html($selected).' value="2">'.esc_html__('Car Parking at least 2', 'yreg-estate').'</option>';
			if ( $ps_garages=='3' ) { $selected="selected"; } else { $selected=""; }
			$output .= '<option '. esc_html($selected).' value="3">'.esc_html__('Car Parking at least 3', 'yreg-estate').'</option>';
			if ( $ps_garages=='4' ) { $selected="selected"; } else { $selected=""; }
			$output .= '<option '. esc_html($selected).' value="4">'.esc_html__('Car Parking at least 4', 'yreg-estate').'</option>';
			if ( $ps_garages=='5' ) { $selected="selected"; } else { $selected=""; }
			$output .= '<option '. esc_html($selected).' value="5">'.esc_html__('Car Parking 5 or more', 'yreg-estate').'</option>';
			$output .= '</select>';
		}
		
		/* ***** show_area ***** */
		if ( $show_area == 1 ) {
			$ps_area_big = (int) yreg_estate_get_custom_option('property_search_area_max');
			$ps_area_min = 0; $ps_area_max = $ps_area_big;
			if ( isset($_GET['ps_area_min']) ) {
				$ps_area_min = (int) htmlspecialchars(trim($_GET['ps_area_min']));
			}
			if ( isset($_GET['ps_area_max']) ) {
				$ps_area_max = (int) htmlspecialchars(trim($_GET['ps_area_max']));
			}
			$output .= '<div class="ps_area ps_range_slider">';
			$output .= '<div class="ps_area_info">';
			$output .= '<div class="ps_area_info_title">Area</div>';
			$output .= '<div class="ps_area_info_value"></div>';
			$output .= '<div class="cL"></div>';
			$output .= '</div>';
			$output .= '<div id="slider-range-area"></div>';
			$output .= '<input type="hidden" class="mb ps_area_min" name="ps_area_min" value="' . esc_html($ps_area_min) . '" >';
			$output .= '<input type="hidden" class="mb ps_area_max" name="ps_area_max" value="' . esc_html($ps_area_max) . '" >';
			$output .= '<input type="hidden" class="mb ps_area_big" name="ps_area_big" value="' . esc_html($ps_area_big) . '" >';
			$output .= '</div>';
		}
		
		/* ***** show_price ***** */
		if ( $show_price == 1 ) {
			$ps_price_big = (int) yreg_estate_get_custom_option('property_search_price_max');
			$ps_price_min = 0; $ps_price_max = $ps_price_big;
			if ( isset($_GET['ps_price_min']) ) {
				$ps_price_min = (int) htmlspecialchars(trim($_GET['ps_price_min']));
			}
			if ( isset($_GET['ps_price_max']) ) {
				$ps_price_max = (int) htmlspecialchars(trim($_GET['ps_price_max']));
			}

			$output .= '<div class="ps_price ps_range_slider">';
			$output .= '<div class="ps_price_info">';
			$output .= '<div class="ps_price_info_title">Price</div>';
			$output .= '<div class="ps_price_info_value"></div>';
			$output .= '<div class="cL"></div>';
			$output .= '</div>';
			$output .= '<div id="slider-range-price"></div>';
			$output .= '<input type="hidden" class="mb ps_price_min" name="ps_price_min" value="' . esc_html($ps_price_min) . '" >';
			$output .= '<input type="hidden" class="mb ps_price_max" name="ps_price_max" value="' . esc_html($ps_price_max) . '" >';
			$output .= '<input type="hidden" class="mb ps_price_big" name="ps_price_big" value="' . esc_html($ps_price_big) . '" >';
			$output .= '</div>';
		}
		
		
		/* ***** show_amenities ***** */
		if ( $show_amenities == 1 ) {
			$ps_amenities = array();
			if ( isset($_GET['ps_amenities']) ) {
				$ps_amenities = $_GET['ps_amenities'];
			}
			$list_amenities = yreg_estate_get_property_list('property_amenities_list');
			$output .= '<div class="ps_amenities">';
			$output .= '<div class="accent1h">'.esc_html__('Amenities', 'yreg-estate').'</div>';
			foreach ($list_amenities as $value1) {
				$checked = ''; $checkedclass = '';
				foreach ($ps_amenities as $key2 => $value2) {
					if ( ($key2 == $value1) and ( $checked == '' ) ) { 
						$checked="checked";
						$checkedclass = ' estateLabelCheckBoxSelected';
					}
				}
				$output .= '<label class="estateLabelCheckBox'.esc_attr($checkedclass).'">';
				$output .= '<input '.esc_attr($checked).' class="estateCheckBox" type="checkbox" name="ps_amenities['.esc_html($value1).']" value="1">';
				$output .= esc_html($value1);
				$output .= '</label>';
				
			}
			$output .= '</div>';
		}
		
		/* ***** show_options ***** */
		if ( $show_options == 1 ) {
			$ps_options = array();
			if ( isset($_GET['ps_options']) ) {
				$ps_options = $_GET['ps_options'];
			}
			$list_options = yreg_estate_get_property_list('property_options_list');
			$output .= '<div class="ps_options">';
			$output .= '<div class="accent1h">'.esc_html__('Options', 'yreg-estate').'</div>';
			foreach ($list_options as $value1) {
				$checked = ''; $checkedclass = '';
				foreach ($ps_options as $key2 => $value2) {
					if ( ($key2 == $value1) and ( $checked == '' ) ) { 
						$checked="checked";
						$checkedclass = ' estateLabelCheckBoxSelected';
					}
				}
				$output .= '<label class="estateLabelCheckBox'.esc_attr($checkedclass).'">';
				$output .= '<input '.esc_attr($checked).' class="estateCheckBox" type="checkbox" name="ps_options['.esc_html($value1).']" value="1">';
				$output .= esc_html($value1);
				$output .= '</label>';
			}
			$output .= '</div>';
		}
		
		
		$output .= '<input type="submit" class="sc_button sc_button_box sc_button_style_style2 sc_button_size_small aligncenter ps" value="Search">';
		$output .= '</form>';

		if (!empty($output)) {
	
			// Before widget (defined by themes)
			echo trim($before_widget);
			
			// Display the widget title if one was input (before and after defined by themes)
			if ($title) echo trim($before_title . $title . $after_title);
	
			echo trim($output);
			
			// After widget (defined by themes)
			echo trim($after_widget);
		}
	}
	
	
	// Update the widget settings.
	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['show_keyword'] = isset($new_instance['show_keyword']) ? 1 : 0;
		$instance['show_status'] = isset($new_instance['show_status']) ? 1 : 0;
		$instance['show_location'] = isset($new_instance['show_location']) ? 1 : 0;
		$instance['show_type'] = isset($new_instance['show_type']) ? 1 : 0;
		$instance['show_style'] = isset($new_instance['show_style']) ? 1 : 0;
		$instance['show_rooms'] = isset($new_instance['show_rooms']) ? 1 : 0;
		$instance['show_bedrooms'] = isset($new_instance['show_bedrooms']) ? 1 : 0;
		$instance['show_bathrooms'] = isset($new_instance['show_bathrooms']) ? 1 : 0;
		$instance['show_garages'] = isset($new_instance['show_garages']) ? 1 : 0;
		$instance['show_area'] = isset($new_instance['show_area']) ? 1 : 0;
		$instance['show_price'] = isset($new_instance['show_price']) ? 1 : 0;
		$instance['show_amenities'] = isset($new_instance['show_amenities']) ? 1 : 0;
		$instance['show_options'] = isset($new_instance['show_options']) ? 1 : 0;
		return $instance;
	}
	
	
	
	// Displays the widget settings controls on the widget panel.
	function form($instance) {

		// Set up some default widget settings
		$instance = wp_parse_args( (array) $instance, array(
			'title' => '',
			'show_keyword' => '1',
			'show_status' => '1',
			'show_location' => '1',
			'show_type' => '1',
			'show_style' => '1',
			'show_rooms' => '1',
			'show_bedrooms' => '1',
			'show_bathrooms' => '1',
			'show_garages' => '1',
			'show_area' => '1',
			'show_price' => '1',
			'show_amenities' => '1',
			'show_options' => '1'
			)
		);
		$title = $instance['title'];
		$show_keyword = (int) $instance['show_keyword'];
		$show_status = (int) $instance['show_status'];
		$show_location = (int) $instance['show_location'];
		$show_type = (int) $instance['show_type'];
		$show_style = (int) $instance['show_style'];
		$show_rooms = (int) $instance['show_rooms'];
		$show_bedrooms = (int) $instance['show_bedrooms'];
		$show_bathrooms = (int) $instance['show_bathrooms'];
		$show_garages = (int) $instance['show_garages'];
		$show_area = (int) $instance['show_area'];
		$show_price = (int) $instance['show_price'];
		$show_amenities = (int) $instance['show_amenities'];
		$show_options = (int) $instance['show_options'];
		?>



		<p>
			<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Widget title:', 'yreg-estate'); ?></label>
			<input id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" value="<?php echo esc_attr($title); ?>" class="widgets_param_fullwidth" />
		</p>

		<p>
			<label>
				<?php if ( $show_keyword==1 ) { $checked="checked"; } else { $checked=""; } ?>
				<input type="checkbox" name="<?php echo esc_attr($this->get_field_name('show_keyword')); ?>" <?php echo esc_attr($checked); ?> >
				<?php esc_html_e('Show keyword field', 'yreg-estate'); ?>
			</label>
		</p>
		
		<p>
			<label>
				<?php if ( $show_status==1 ) { $checked="checked"; } else { $checked=""; } ?>
				<input type="checkbox" name="<?php echo esc_attr($this->get_field_name('show_status')); ?>" <?php echo esc_attr($checked); ?> >
				<?php esc_html_e('Show status field', 'yreg-estate'); ?>
			</label>
		</p>
		
		<p>
			<label>
				<?php if ( $show_location==1 ) { $checked="checked"; } else { $checked=""; } ?>
				<input type="checkbox" name="<?php echo esc_attr($this->get_field_name('show_location')); ?>" <?php echo esc_attr($checked); ?> >
				<?php esc_html_e('Show location field', 'yreg-estate'); ?>
			</label>
		</p>
		
		<p>
			<label>
				<?php if ( $show_type==1 ) { $checked="checked"; } else { $checked=""; } ?>
				<input type="checkbox" name="<?php echo esc_attr($this->get_field_name('show_type')); ?>" <?php echo esc_attr($checked); ?> >
				<?php esc_html_e('Show type field', 'yreg-estate'); ?>
			</label>
		</p>
		
		<p>
			<label>
				<?php if ( $show_style==1 ) { $checked="checked"; } else { $checked=""; } ?>
				<input type="checkbox" name="<?php echo esc_attr($this->get_field_name('show_style')); ?>" <?php echo esc_attr($checked); ?> >
				<?php esc_html_e('Show style field', 'yreg-estate'); ?>
			</label>
		</p>
		
		<p>
			<label>
				<?php if ( $show_rooms==1 ) { $checked="checked"; } else { $checked=""; } ?>
				<input type="checkbox" name="<?php echo esc_attr($this->get_field_name('show_rooms')); ?>" <?php echo esc_attr($checked); ?> >
				<?php esc_html_e('Show rooms field', 'yreg-estate'); ?>
			</label>
		</p>
		
		<p>
			<label>
				<?php if ( $show_bedrooms==1 ) { $checked="checked"; } else { $checked=""; } ?>
				<input type="checkbox" name="<?php echo esc_attr($this->get_field_name('show_bedrooms')); ?>" <?php echo esc_attr($checked); ?> >
				<?php esc_html_e('Show bedrooms field', 'yreg-estate'); ?>
			</label>
		</p>
		
		<p>
			<label>
				<?php if ( $show_bathrooms==1 ) { $checked="checked"; } else { $checked=""; } ?>
				<input type="checkbox" name="<?php echo esc_attr($this->get_field_name('show_bathrooms')); ?>" <?php echo esc_attr($checked); ?> >
				<?php esc_html_e('Show bathrooms field', 'yreg-estate'); ?>
			</label>
		</p>
		
		<p>
			<label>
				<?php if ( $show_garages==1 ) { $checked="checked"; } else { $checked=""; } ?>
				<input type="checkbox" name="<?php echo esc_attr($this->get_field_name('show_garages')); ?>" <?php echo esc_attr($checked); ?> >
				<?php esc_html_e('Show garages field', 'yreg-estate'); ?>
			</label>
		</p>
		
		<p>
			<label>
				<?php if ( $show_area==1 ) { $checked="checked"; } else { $checked=""; } ?>
				<input type="checkbox" name="<?php echo esc_attr($this->get_field_name('show_area')); ?>" <?php echo esc_attr($checked); ?> >
				<?php esc_html_e('Show area field', 'yreg-estate'); ?>
			</label>
		</p>
		
		<p>
			<label>
				<?php if ( $show_price==1 ) { $checked="checked"; } else { $checked=""; } ?>
				<input type="checkbox" name="<?php echo esc_attr($this->get_field_name('show_price')); ?>" <?php echo esc_attr($checked); ?> >
				<?php esc_html_e('Show price field', 'yreg-estate'); ?>
			</label>
		</p>
		
		<p>
			<label>
				<?php if ( $show_amenities==1 ) { $checked="checked"; } else { $checked=""; } ?>
				<input type="checkbox" name="<?php echo esc_attr($this->get_field_name('show_amenities')); ?>" <?php echo esc_attr($checked); ?> >
				<?php esc_html_e('Show amenities field', 'yreg-estate'); ?>
			</label>
		</p>
		
		<p>
			<label>
				<?php if ( $show_options==1 ) { $checked="checked"; } else { $checked=""; } ?>
				<input type="checkbox" name="<?php echo esc_attr($this->get_field_name('show_options')); ?>" <?php echo esc_attr($checked); ?> >
				<?php esc_html_e('Show options field', 'yreg-estate'); ?>
			</label>
		</p>

	<?php
	}
	
	
	
}
